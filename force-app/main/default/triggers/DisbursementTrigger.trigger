trigger DisbursementTrigger on Disbursement__c (before insert, after insert, before update, after update, before delete, after delete) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            DisbursementTriggerHandler.updatePlanDisbursementDate(Trigger.new);
            DisbursementTriggerHandler.blockCreatingActualDbm(Trigger.new);
        }

        if(Trigger.isAfter) {
            Boolean needInsert = false;

            DisbursementTriggerHandler.createNewActualDisbursementCorresponding(Trigger.new);

            for(String dId : Trigger.newMap.keySet()) {
                if(Trigger.newMap.get(dId).Status__c == 'Done') {
                    needInsert = true;
                }
            }

            if(needInsert) {
                DisbursementTriggerHandler.setTotalDisbursedAmountInOpportunity(Trigger.new);
                DisbursementTriggerHandler.setStageNameOfOppToA(Trigger.new);
                DisbursementTriggerHandler.updateActualDbm(Trigger.new);
                System.debug('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
            }
        }
    }

    if(Trigger.isUpdate) {
        Boolean needBeforeUpdate = false;

        if(Trigger.isBefore) {
            // check Updated Date of Actual Disbursement
            // DisbursementTriggerHandler.updateDaysInWeek(Trigger.new);
            // if Status of Actual Dbm is Cancelled or Partially Done => please fill out the Note
            DisbursementTriggerHandler.fillOutNoteOfDbm(Trigger.new);
            // block updating Disbursement of previous month
            // DisbursementTriggerHandler.blockUpdatingDisbursementOfPreviousMonth(Trigger.new);
            // 
            DisbursementTriggerHandler.updateOnlyActivatedContractToDbm(Trigger.new);

            for(String dId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(dId).Status__c != Trigger.newMap.get(dId).Status__c) {
                    needBeforeUpdate = true;
                }
            }

            if(needBeforeUpdate) {
                DisbursementTriggerHandler.blockChangingToDoneActualDbm(Trigger.new);
                DisbursementTriggerHandler.blockCreatingActualDbm(Trigger.new);
            }
        }


        if(Trigger.isAfter) {
            Boolean needAfterUpdate1 = false;
            Boolean needAfterUpdate2 = false;
            Boolean needAfterUpdate3 = false;

            for(String dId : Trigger.newMap.keySet()) {
                if((Trigger.oldMap.get(dId).Amount__c != Trigger.newMap.get(dId).Amount__c)
                || (Trigger.oldMap.get(dId).RecordTypeId != Trigger.newMap.get(dId).RecordTypeId)
                || (Trigger.oldMap.get(dId).Status__c != Trigger.newMap.get(dId).Status__c)) {
                    needAfterUpdate1 = true;
                }

                if(Trigger.oldMap.get(dId).Disbursement_Date__c != Trigger.newMap.get(dId).Disbursement_Date__c) {
                    needAfterUpdate2 = true;
                }

                if(Trigger.oldMap.get(dId).Status__c != Trigger.newMap.get(dId).Status__c) {
                    needAfterUpdate3 = true;
                }
            }

            if(needAfterUpdate1) {
                DisbursementTriggerHandler.setTotalDisbursedAmountInOpportunity(Trigger.new);
                DisbursementTriggerHandler.updateActualDbm(Trigger.new);
                System.debug('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA NeedUpdate 1');
            }

            if(needAfterUpdate2) {
                DisbursementTriggerHandler.updateActualDbm(Trigger.new);
                System.debug('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA NeedUpdate 2');
            }

            if(needAfterUpdate3) {
                DisbursementTriggerHandler.setStageNameOfOppToA(Trigger.new);
            }
        }
    }

    if(Trigger.isDelete) {
        List<RecordType> rts = [SELECT Id, Name
                                FROM RecordType
                                WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                                LIMIT 1];
        
        RecordType rt = new RecordType(ID = '012N0000000Sr7VIAS');
        if (rts != null && rts.size() > 0) {
            rt = rts.get(0);
        }
        if(Trigger.isBefore) {
            DisbursementTriggerHandler.blockDeletingActualDbmWithRecordType(Trigger.old, rt);
        }
        
        if(Trigger.isAfter) {
            DisbursementTriggerHandler.setTotalDisbursedAmountInOpportunityWithRecordType(Trigger.old, rt);
            DisbursementTriggerHandler.updateActualDbmWithRecordType(Trigger.old, rt);
        }
    }
}