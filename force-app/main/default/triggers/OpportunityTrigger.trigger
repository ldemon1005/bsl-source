trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete) {

    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            // set pre-data for Opportunities
            OpportunityTriggerHandler.setPreDataOnOpp(Trigger.new);
            // set Status of each Stage
            OpportunityTriggerHandler.updateStatusOfEachStage(Trigger.new);
            // check Referral Source and Referral Person
            OpportunityTriggerHandler.checkRelatedReferralSource(Trigger.new);
            // update Branch Value when creating new Opportunity
            OpportunityTriggerHandler.updateBranchValueWhenCreatingOpp(Trigger.new);
            // cannot create new Opportunity with StageName C, B, A, L
            OpportunityTriggerHandler.blockInserting(Trigger.new);

        }   

        if(Trigger.isAfter) {
            // send Notification for salesman when New Opp is created
            OpportunityTriggerHandler.setNotificationWhenHavingNewOpp(Trigger.new);
            // set Credit At BIDV field in Account
            OpportunityTriggerHandler.setCreditRatingAtBidv(Trigger.new);
            // set Total Developed Number of Account in Referral
            OpportunityTriggerHandler.setTotalDevelopedNumberOfOpportunity(Trigger.new);
            // set Total Developed Potential Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedPotentialAmount(Trigger.new);
            // set Total Developed Approved Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedApprovedAmount(Trigger.new);
            // set Total Developed Contracted Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedContractedAmount(Trigger.new);
            // set Total Developed Disbursed Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedDisbursedAmount(Trigger.new);
        }
    }

    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            Boolean needBeforeUpdate1 = false;
            Boolean needBeforeUpdate2 = false;
            Boolean needBeforeUpdate3 = false;
            Boolean needBeforeUpdate4 = false;
            Boolean needBeforeUpdate5 = false;
            Boolean needBeforeUpdate6 = false;
            Boolean needBeforeUpdate7 = false;
            Boolean needBeforeUpdate8 = false;
            Boolean needBeforeUpdate9 = false;
            Boolean needBeforeUpdate10 = false;
            Boolean needBeforeUpdate11 = false;
            Boolean needBeforeUpdate12 = false;
            Boolean needBeforeUpdate13 = false;

            for(String oppId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(oppId).StageName != Trigger.newMap.get(oppId).StageName) {
                    needBeforeUpdate1 = true;
                }

                if(Trigger.oldMap.get(oppId).OwnerId != Trigger.newMap.get(oppId).OwnerId) {
                    needBeforeUpdate2 = true;
                }
                

                if(Trigger.oldMap.get(oppId).Approval_Data__c != Trigger.newMap.get(oppId).Approval_Data__c) {
                    needBeforeUpdate3 = true;
                }

                if(Trigger.oldMap.get(oppId).StageName != Trigger.newMap.get(oppId).StageName) {
                    needBeforeUpdate4 = true;
                    // block changing StageName back to C when Opp has StageName B, A or L
                    if((Trigger.oldMap.get(oppId).StageName == 'B')
                    || (Trigger.oldMap.get(oppId).StageName == 'A')
                    || (Trigger.oldMap.get(oppId).StageName == 'L')) {
                        needBeforeUpdate5 = true;
                    }

                    // if this Opp doesn't have any Activated Contract => cannot change StageName from C to B
                    if(Trigger.oldMap.get(oppId).StageName == 'C') {
                        needBeforeUpdate6 = true;
                    }

                    // block changing StageName back to B when Opp has StageName A or L
                    if((Trigger.oldMap.get(oppId).StageName == 'A')
                    || (Trigger.oldMap.get(oppId).StageName == 'L')) {
                        needBeforeUpdate7 = true;
                    }

                    // if this Opp doesn't have any Done Actual Disbursement => cannot change StageName from B to A
                    if((Trigger.oldMap.get(oppId).StageName == 'C')
                    || (Trigger.oldMap.get(oppId).StageName == 'B')) {
                        needBeforeUpdate8 = true;
                    }

                    // block changing StageName back to A when Opp has StageName L
                    if(Trigger.oldMap.get(oppId).StageName == 'L') {
                        needBeforeUpdate9 = true;
                    }

                    // if this Opp doesn't have Rejected Quote => cannot change StageName from A to L
                    if((Trigger.oldMap.get(oppId).StageName == 'C')
                    || (Trigger.oldMap.get(oppId).StageName == 'B')
                    || (Trigger.oldMap.get(oppId).StageName == 'A')) {
                        needBeforeUpdate10 = true;
                    }
                }

                if((Trigger.oldMap.get(oppId).Referral_Source__c != Trigger.newMap.get(oppId).Referral_Source__c)
                || (Trigger.oldMap.get(oppId).Referral_Person__c != Trigger.newMap.get(oppId).Referral_Person__c)) {
                    needBeforeUpdate11 = true;
                }

                if((Trigger.oldMap.get(oppId).Approved_Amount__c != Trigger.newMap.get(oppId).Approved_Amount__c)
                || (Trigger.oldMap.get(oppId).Contracted_Amount__c != Trigger.newMap.get(oppId).Contracted_Amount__c)) {
                    needBeforeUpdate12 = true;
                }

                if((Trigger.oldMap.get(oppId).Total_Disbursed_Amount__c != Trigger.newMap.get(oppId).Total_Disbursed_Amount__c)
                || (Trigger.oldMap.get(oppId).Contracted_Amount__c != Trigger.newMap.get(oppId).Contracted_Amount__c)) {
                    needBeforeUpdate13 = true;
                }
            }

            if(needBeforeUpdate1) {
                // set Status of each Stage
                OpportunityTriggerHandler.updateStatusOfEachStage(Trigger.new);
            }

            if(needBeforeUpdate2) {
                //
                OpportunityTriggerHandler.updateBranchValueWhenCreatingOpp(Trigger.new);
            }

            if(needBeforeUpdate3) {
                // update Approval Deadline for all leaderShip
                OpportunityTriggerHandler.updateApprovalDeadline(Trigger.new);
            }

            if(needBeforeUpdate4) {
                // set Approved Date of Opportunity
                OpportunityTriggerHandler.updateApprovedAndActivatedDate(Trigger.new);
                // Stop changing StageName
                OpportunityTriggerHandler.blockChangingStageNameOfOpp(Trigger.new);
            }

            if(needBeforeUpdate5) {
                OpportunityTriggerHandler.blockChangingStageNameBackToC(Trigger.new);
            }

            if(needBeforeUpdate6) {
                OpportunityTriggerHandler.blockChangingStageNameFromCtoB(Trigger.new);
            }

            if(needBeforeUpdate7) {
                OpportunityTriggerHandler.blockChangingStageNameBackToB(Trigger.new);
            }

            if(needBeforeUpdate8) {
                OpportunityTriggerHandler.blockChangingStageNameFromBtoA(Trigger.new);
            }

            if(needBeforeUpdate9) {
                OpportunityTriggerHandler.blockChangingStageNameBackToA(Trigger.new);
            }

            if(needBeforeUpdate10) {
                OpportunityTriggerHandler.blockChangingStageNameFromAtoL(Trigger.new);
            }

            if(needBeforeUpdate11) {
                // set Total Expected Potential/Contracted Amount to Zero
                OpportunityTriggerHandler.checkRelatedReferralSource(Trigger.new);
            }

            if(needBeforeUpdate12) {
                OpportunityTriggerHandler.checkingApprovedAndContractAmount(Trigger.new);
            }

            if(needBeforeUpdate13) {
                OpportunityTriggerHandler.checkingContractedAndDbmAmount(Trigger.new);
            }
        }

        if(Trigger.isAfter) {
            Boolean needAfterUpdate1 = false;
            Boolean needAfterUpdate2 = false;
            Boolean needAfterUpdate3 = false;
            Boolean needAfterUpdate4 = false;
            Boolean needAfterUpdate5 = false;
            Boolean needAfterUpdate6 = false;
            Boolean needAfterUpdate7 = false;
            Boolean needAfterUpdate8 = false;
            Boolean needAfterUpdate9 = false;
            Boolean needAfterUpdate10 = false;
            Boolean needAfterUpdate11 = false;

            System.debug('After Update Trigger In OOOOOOOOOOOOOOOPPPPPPPPPPP');
            for(String oppId : Trigger.newMap.keySet()) {

                System.debug('New Total Asset Value: ' + Trigger.newMap.get(oppId).Total_Asset_Value__c);
                if(Trigger.oldMap.get(oppId).Credit_Rating_at_BIDV__c != Trigger.newMap.get(oppId).Credit_Rating_at_BIDV__c) {
                    needAfterUpdate1 = true;
                }

                if(Trigger.oldMap.get(oppId).Approval_Data__c != Trigger.newMap.get(oppId).Approval_Data__c) {
                    needAfterUpdate2 = true;
                }

                if((Trigger.oldMap.get(oppId).StageName != Trigger.newMap.get(oppId).StageName)
                || (Trigger.oldMap.get(oppId).Approved_Amount__c != Trigger.newMap.get(oppId).Approved_Amount__c)
                || (Trigger.oldMap.get(oppId).OwnerId != Trigger.newMap.get(oppId).OwnerId)) {
                    needAfterUpdate3 = true;
                }

                if((Trigger.oldMap.get(oppId).StageName != Trigger.newMap.get(oppId).StageName)
                || (Trigger.oldMap.get(oppId).Contracted_Amount__c != Trigger.newMap.get(oppId).Contracted_Amount__c)
                || (Trigger.oldMap.get(oppId).OwnerId != Trigger.newMap.get(oppId).OwnerId)) {
                    needAfterUpdate4 = true;
                }

                if((Trigger.oldMap.get(oppId).StageName != Trigger.newMap.get(oppId).StageName)
                || (Trigger.oldMap.get(oppId).OwnerId != Trigger.newMap.get(oppId).OwnerId)) {
                    needAfterUpdate5 = true;
                }

                if(Trigger.oldMap.get(oppId).Referral_Source__c != Trigger.newMap.get(oppId).Referral_Source__c) {
                    needAfterUpdate6 = true;
                }

                if(Trigger.oldMap.get(oppId).Total_Expected_Potential_Contract_Amount__c != Trigger.newMap.get(oppId).Total_Expected_Potential_Contract_Amount__c) {
                    needAfterUpdate7 = true;                  
                }

                if(Trigger.oldMap.get(oppId).Approved_Amount__c != Trigger.newMap.get(oppId).Approved_Amount__c) {
                    needAfterUpdate8 = true;              
                }

                if(Trigger.oldMap.get(oppId).Contracted_Amount__c != Trigger.newMap.get(oppId).Contracted_Amount__c) {
                    needAfterUpdate9 = true;
                }

                if(Trigger.oldMap.get(oppId).Total_Disbursed_Amount__c != Trigger.newMap.get(oppId).Total_Disbursed_Amount__c) {
                    needAfterUpdate10 = true;
                }

                if((Trigger.oldMap.get(oppId).Total_Expected_Potential_Contract_Amount__c != Trigger.newMap.get(oppId).Total_Expected_Potential_Contract_Amount__c)
                || (Trigger.oldMap.get(oppId).OwnerId != Trigger.newMap.get(oppId).OwnerId)) {
                    needAfterUpdate11 = true;
                }
            }

            if(needAfterUpdate1) {
                // set Credit At BIDV field in Account
                OpportunityTriggerHandler.setCreditRatingAtBidv(Trigger.new);
            }

            if(needAfterUpdate2) {
                // create Task for Leadership
                OpportunityTriggerHandler.createTaskRemindLeadership(Trigger.new);
            }

            if(needAfterUpdate3) {
                // update Total Approved of KPI Value
                OpportunityTriggerHandler.updateTotalApprovedInKpi(Trigger.new);
            }

            if(needAfterUpdate4) {
                // update Total Contracted of KPI Value
                OpportunityTriggerHandler.updateTotalContractedInKpi(Trigger.new);
            }

            if(needAfterUpdate5) {
                // send Notification for salesman when StageName of Opp is changed to C
                OpportunityTriggerHandler.remindSalesmanToUpdateDisbursement(Trigger.new);
                // create Update Contract Task for Salesman
                OpportunityTriggerHandler.createTaskContractUpdating(Trigger.new);
                // update Remaining Task
                OpportunityTriggerHandler.updateTaskRemaining(Trigger.new);
            }

            if(needAfterUpdate6) {
                // update Asset that belong to Referral
                OpportunityTriggerHandler.updateAssetInReferral(Trigger.new);

                // set Total Developed Number of Account in Referral in old Referral
                OpportunityTriggerHandler.setTotalDevelopedNumberOfOpportunity(Trigger.old);

                // set Total Developed Number of Account in Referral in new Referral
                OpportunityTriggerHandler.setTotalDevelopedNumberOfOpportunity(Trigger.new);

                // set Total Developed Potential Amount in Referral
                OpportunityTriggerHandler.setTotalDevelopedPotentialAmount(Trigger.new);
                OpportunityTriggerHandler.setTotalDevelopedPotentialAmount(Trigger.old);

                // set Total Developed Approved Amount in Referral
                OpportunityTriggerHandler.setTotalDevelopedApprovedAmount(Trigger.new);
                OpportunityTriggerHandler.setTotalDevelopedApprovedAmount(Trigger.old);

                // set Total Developed Contracted Amount in Referral
                OpportunityTriggerHandler.setTotalDevelopedContractedAmount(Trigger.new);
                OpportunityTriggerHandler.setTotalDevelopedContractedAmount(Trigger.old);

                // set Total Developed Disbursed Amount in Referral
                OpportunityTriggerHandler.setTotalDevelopedDisbursedAmount(Trigger.new);
                OpportunityTriggerHandler.setTotalDevelopedDisbursedAmount(Trigger.old);
            }

            if(needAfterUpdate7) {
                // set Total Developed Potential Amount in Referral
                OpportunityTriggerHandler.setTotalDevelopedPotentialAmount(Trigger.new);
            }

            if(needAfterUpdate8) {
                // set Total Developed Approved Amount in Referral in new referral
                OpportunityTriggerHandler.setTotalDevelopedApprovedAmount(Trigger.new);
            }

            if(needAfterUpdate9) {
                // set Total Developed Contracted Amount in Referral
                OpportunityTriggerHandler.setTotalDevelopedContractedAmount(Trigger.new);
            }

            if(needAfterUpdate10) {
                // set Total Developed Disbursed Amount in Referral in new referral
                OpportunityTriggerHandler.setTotalDevelopedDisbursedAmount(Trigger.new);
            }

            if(needAfterUpdate11) {
                // Total Potential Amount in KPI
                OpportunityTriggerHandler.updateTotalPotentialInKpi(Trigger.new);
            }
        }
    }

    if(Trigger.isDelete) {
        if(Trigger.isAfter) {
            // set Total Developed Number of Account in Referral when deleting
            OpportunityTriggerHandler.setTotalDevelopedNumberOfOpportunitywDeleting(Trigger.old);
            // set Total Developed Potential Amount in Referral when deleting
            OpportunityTriggerHandler.setTotalDevelopedPotentialAmountwDeleting(Trigger.old);
            // set Total Developed Approved Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedApprovedAmountwDeleting(Trigger.old);
            // set Total Developed Contracted Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedContractedAmountwDeleting(Trigger.old);
            // set Total Developed Disbursed Amount in Referral
            OpportunityTriggerHandler.setTotalDevelopedDisbursedAmountwDeleting(Trigger.old);
            // Total Approved in KPI 
            OpportunityTriggerHandler.updateTotalApprovedInKpi(Trigger.old);
            // Total Contracted in KPI
            OpportunityTriggerHandler.updateTotalContractedInKpi(Trigger.old);
            // Total Potential Amount in KPI
            OpportunityTriggerHandler.updateTotalPotentialInKpi(Trigger.old);
        }
    }
}