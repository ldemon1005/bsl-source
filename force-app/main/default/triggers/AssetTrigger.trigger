/**
  * CMC System Integration Co Ltd.
  * AssetTrigger
  * @author tqtai
  */
trigger AssetTrigger on Asset__c (before insert, before update, before delete, after insert, after update, after delete) {
    
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            AssetTriggerHandler.cancelUpdatingAsset(Trigger.new);
            AssetTriggerHandler.addQuoteToAsset(Trigger.new);
            AssetTriggerHandler.updatePotentialDateInOpp(Trigger.new);
            AssetTriggerHandler.addAccountNameAndSalesmanToAsset(Trigger.new);
            AssetTriggerHandler.updateAssetInReferral(Trigger.new);
        }

        if(Trigger.isAfter) {
            AssetTriggerHandler.setCurrencyInOpportunity(Trigger.new);
            AssetTriggerHandler.setExpectedPotentialContractAmountInOpp(Trigger.new);
            AssetTriggerHandler.calculateDownPaymentAmountWhenCreatingAsset(Trigger.new);
            AssetTriggerHandler.updateSecurityDepositInAccount(Trigger.new);
        }
    }

    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            System.debug('New KeySet size In Asset Trigger: ' + Trigger.newMap.keySet().size());
            
            for(String aId : Trigger.newMap.keySet()) {
                if((Trigger.oldMap.get(aId).Asset_Value_Before_Tax__c != Trigger.newMap.get(aId).Asset_Value_Before_Tax__c)
                || (Trigger.oldMap.get(aId).Quantity__c != Trigger.newMap.get(aId).Quantity__c)
                || (Trigger.oldMap.get(aId).Expected_Contract_Amount__c != Trigger.newMap.get(aId).Expected_Contract_Amount__c)) {
                    // set Total Expected Potential/Contracted Amount to Zero
                    AssetTriggerHandler.cancelUpdatingAsset(Trigger.new);
                }

                if(Trigger.oldMap.get(aId).Opportunity__c != Trigger.newMap.get(aId).Opportunity__c) {
                    // Add Asset to Referral
                    AssetTriggerHandler.updateAssetInReferral(Trigger.new);
                }
            }
        }

        if(Trigger.isAfter) {
            AssetTriggerHandler.setExpectedPotentialContractAmountInOpp(Trigger.new);
            for(String aId : Trigger.newMap.keySet()) {
                if((Trigger.oldMap.get(aId).Asset_Value_Before_Tax__c != Trigger.newMap.get(aId).Asset_Value_Before_Tax__c)
                || (Trigger.oldMap.get(aId).Quantity__c != Trigger.newMap.get(aId).Quantity__c)
                || (Trigger.oldMap.get(aId).Tax_except_VAT_Amount__c != Trigger.newMap.get(aId).Tax_except_VAT_Amount__c)
                || (Trigger.oldMap.get(aId).VAT_rate__c != Trigger.newMap.get(aId).VAT_rate__c)
                || (Trigger.oldMap.get(aId).Installation_Cost__c != Trigger.newMap.get(aId).Installation_Cost__c)
                || (Trigger.oldMap.get(aId).Insurance_Premium__c != Trigger.newMap.get(aId).Insurance_Premium__c)
                || (Trigger.oldMap.get(aId).Maintenance_Cost__c != Trigger.newMap.get(aId).Maintenance_Cost__c)
                || (Trigger.oldMap.get(aId).Other_Costs__c != Trigger.newMap.get(aId).Other_Costs__c)) {
                    AssetTriggerHandler.calculateDownPaymentAmountWhenCreatingAsset(Trigger.new);
                }
                /*if(Trigger.oldMap.get(aId).Expected_Contract_Amount__c != Trigger.newMap.get(aId).Expected_Contract_Amount__c){
                    AssetTriggerHandler.updateExpectedContractAmount(Trigger.newMap, Trigger.oldMap);
                }*/
            }
            AssetTriggerHandler.updateSecurityDepositInAccount(Trigger.new);
        }
    }

    if(Trigger.isDelete) {
        if(Trigger.isBefore) {
            AssetTriggerHandler.cancelUpdatingAsset(Trigger.old);
        }

        if(Trigger.isAfter) {
            AssetTriggerHandler.setExpectedPotentialContractAmountInOpp(Trigger.old);
            AssetTriggerHandler.calculateDownPaymentAmountWhenCreatingAsset(Trigger.old);
            AssetTriggerHandler.updateSecurityDepositInAccount(Trigger.old);
        }
    }
}