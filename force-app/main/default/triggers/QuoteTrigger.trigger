trigger QuoteTrigger on Quote (before insert, before update, before delete, after insert, after update, after delete) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            QuoteTriggerHandler.setNoMoreQuoteInOpportunity(Trigger.new);
            QuoteTriggerHandler.addTotalAssetValueInQuote(Trigger.new);
            // QuoteTriggerHandler.calculateDownPaymentAmountwInsert(Trigger.new);
            QuoteTriggerHandler.calculateDownPaymentAmountwUpdate(Trigger.new);
            QuoteTriggerHandler.setSecurityDepositAmount(Trigger.new);
            QuoteTriggerHandler.setUpfrontFeeAmount(Trigger.new);
            QuoteTriggerHandler.setClosingPriceAmount(Trigger.new);
            QuoteTriggerHandler.updateExpectedContractAmountInQuote(Trigger.new);
        }

        if(Trigger.isAfter) {
            QuoteTriggerHandler.updateQuoteToAsset(Trigger.new);
            
            boolean needUpdate = false;

            for(String qId : Trigger.newMap.keySet()) {
                if((Trigger.newMap.get(qId).Status == 'Approved by BSL')
                || (Trigger.newMap.get(qId).Status == 'Rejected by Customer')
                || (Trigger.newMap.get(qId).Status == 'Rejected by BSL')) {
                    needUpdate = true;
                }
            }
            if (needUpdate) {
            	QuoteTriggerHandler.updateTotalPotentialAmountInOpp(Trigger.new);    
            }
        }
    }
    if(Trigger.isUpdate) {
        // update IRR_ khi co thay doi o IRR thi bỏ logic phia duoi
        if(Trigger.newMap.keySet().size()==1){
            for(String qId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(qId).IRR__C != Trigger.newMap.get(qId).IRR__C) {
                    return;
                }
        	}
        }       
        if(Trigger.isBefore) {
            Boolean needBeforeUpdate1 = false;
            Boolean needBeforeUpdate2 = false;
            Boolean needBeforeUpdate3 = false;
            Boolean needBeforeUpdate4 = false;
            Boolean needBeforeUpdate5 = false;
            Boolean needBeforeUpdate6 = false;

            //System.debug('New KeySet size In Quote Trigger: ' + Trigger.newMap.keySet().size());
            // block updating Quote
            QuoteTriggerHandler.blockUpdatingQuote(Trigger.new);
            for(String qId : Trigger.newMap.keySet()) {
                if((Trigger.oldMap.get(qId).Down_Payment_Rate__c != Trigger.newMap.get(qId).Down_Payment_Rate__c)
                || (Trigger.oldMap.get(qId).Total_Asset_Value_without_VAT__c != Trigger.newMap.get(qId).Total_Asset_Value_without_VAT__c)) {
                    // set Total Expected Potential/Contracted Amount to Zero
                    needBeforeUpdate1 = true;
                }

                if(Trigger.oldMap.get(qId).Status != Trigger.newMap.get(qId).Status) {
                    // block changing status of Quote
                    needBeforeUpdate2 = true;
                }

                if((Trigger.oldMap.get(qId).Down_Payment_Amount__c != Trigger.newMap.get(qId).Down_Payment_Amount__c)
                && (Trigger.oldMap.get(qId).Total_Asset_Value__c == Trigger.newMap.get(qId).Total_Asset_Value__c)) {
                    // set Total Expected Potential/Contracted Amount to Zero
                    //QuoteTriggerHandler.recalculateDownPaymentRate(Trigger.new);
                }

                if(Trigger.oldMap.get(qId).Security_Deposit_Rate__c != Trigger.newMap.get(qId).Security_Deposit_Rate__c) {
                    // calculating Security Deposit Amount of Quote when updating Security Deposit Rate
                    needBeforeUpdate3 = true;
                }

                if(Trigger.oldMap.get(qId).Security_Deposit_rate_Amount__c != Trigger.newMap.get(qId).Security_Deposit_rate_Amount__c) {
                    // calculating Security Deposit Amount of Quote when updating Security Deposit Rate
                    //QuoteTriggerHandler.recalculateSecurityDepositRate(Trigger.new);
                }

                if(Trigger.oldMap.get(qId).Upfront_Fee_Rate__c != Trigger.newMap.get(qId).Upfront_Fee_Rate__c
                   || Trigger.oldMap.get(qId).Total_Asset_Value_with_VAT__c != Trigger.newMap.get(qId).Total_Asset_Value_with_VAT__c) {
                    // calculating Upfront fee Amount of Quote when updating Upfront fee Rate
                    needBeforeUpdate4 = true;
                }

                if(Trigger.oldMap.get(qId).Upfront_Fee_Amount__c != Trigger.newMap.get(qId).Upfront_Fee_Amount__c) {
                    // recalculating Upfront fee Rate of Quote when updating Upfront fee Amount
                    //QuoteTriggerHandler.recalculateUpfrontFeeRate(Trigger.new);
                }

                if(Trigger.oldMap.get(qId).Closing_Price_Rate__c != Trigger.newMap.get(qId).Closing_Price_Rate__c) {
                    // calculating Closing Price Amount of Quote when updating Closing Price Rate
                    needBeforeUpdate5 = true;
                }

                if(Trigger.oldMap.get(qId).Closing_Price_Amount__c != Trigger.newMap.get(qId).Closing_Price_Amount__c) {
                    // recalculating Closing Price Rate of Quote when updating Closing Price Amount
                    //QuoteTriggerHandler.recalculateClosingPriceRate(Trigger.new);
                }

                if((Trigger.oldMap.get(qId).Expected_Contract_Amount__c != Trigger.newMap.get(qId).Expected_Contract_Amount__c)
                || (Trigger.oldMap.get(qId).Expected_Contract_Amount_after_VAT__c == Trigger.newMap.get(qId).Expected_Contract_Amount_after_VAT__c)
                || (Trigger.oldMap.get(qId).Down_Payment_Amount__c == Trigger.newMap.get(qId).Down_Payment_Amount__c)) {
                    //||(Trigger.oldMap.get(qId).Expected_Disbursement_Date__c != Trigger.newMap.get(qId).Expected_Disbursement_Date__c)
                    // update Expected Contract Amount in Quote (after tax with Operating Lease)
                    needBeforeUpdate6 = true;
                }
            }

            if(needBeforeUpdate1) {
                QuoteTriggerHandler.calculateDownPaymentAmountwUpdate(Trigger.new);
            }

            if(needBeforeUpdate2) {
                QuoteTriggerHandler.blockChangingStatusOfQuote(Trigger.new);
            }

            if(needBeforeUpdate3) {
                QuoteTriggerHandler.setSecurityDepositAmount(Trigger.new);
            }

            if(needBeforeUpdate4) {
                QuoteTriggerHandler.setUpfrontFeeAmount(Trigger.new);
            }

            if(needBeforeUpdate5) {
                QuoteTriggerHandler.setClosingPriceAmount(Trigger.new);
            }

            if(needBeforeUpdate6) {
                QuoteTriggerHandler.updateExpectedContractAmountInQuote(Trigger.new);
            }
        }

        if(Trigger.isAfter) {
            Boolean needAfterUpdate1 = false;
            Boolean needAfterUpdate2 = false;

            for(String qId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(qId).Status != Trigger.newMap.get(qId).Status) {
                    needAfterUpdate1 = true;
                }

                if((Trigger.oldMap.get(qId).Total_Asset_Value__c != Trigger.newMap.get(qId).Total_Asset_Value__c)
                || (Trigger.oldMap.get(qId).Down_Payment_Amount__c != Trigger.newMap.get(qId).Down_Payment_Amount__c)
                || (Trigger.oldMap.get(qId).Down_Payment_Rate__c != Trigger.newMap.get(qId).Down_Payment_Rate__c)
                || (Trigger.oldMap.get(qId).Monthly_Repayment_Amount__c != Trigger.newMap.get(qId).Monthly_Repayment_Amount__c)
                || (Trigger.oldMap.get(qId).Contract_Term__c != Trigger.newMap.get(qId).Contract_Term__c)
                || (Trigger.oldMap.get(qId).VAT_Rate__c != Trigger.newMap.get(qId).VAT_Rate__c)
                || (Trigger.oldMap.get(qId).Expected_Contract_Amount__c != Trigger.newMap.get(qId).Expected_Contract_Amount__c)
                || (Trigger.oldMap.get(qId).Expected_Contract_Amount_after_VAT__c != Trigger.newMap.get(qId).Expected_Contract_Amount_after_VAT__c)) {
                    needAfterUpdate2 = true;
                }
            }

            if(needAfterUpdate1) {
                // create new Task for Opportunity
                QuoteTriggerHandler.createCheckApprovalTask(Trigger.new);
                // change Approved Amount to 0 when the Status change to anything else (not Approved by BSL)
                QuoteTriggerHandler.updateApprovedAmountInOppToZero(Trigger.new);
                // calculate Total Expected Potential/Contract Amount in Opportunity
                QuoteTriggerHandler.updateTotalPotentialAmountInOpp(Trigger.new);
            }

            if(needAfterUpdate2) {
                // calculate Total Expected Potential/Contract Amount in Opportunity
                QuoteTriggerHandler.updateTotalPotentialAmountInOpp(Trigger.new);

                // update Approved Amount in Opp
                QuoteTriggerHandler.updateApprovedAmountInOpp(Trigger.new);
            }
        }
        
    }
    if(Trigger.isDelete) {
        if(Trigger.isAfter) {
            // calculate Total Expected Potential/Contract Amount in Opportunity when deleting
            QuoteTriggerHandler.updateTotalPotentialAmountInOppWhenDeleting(Trigger.old);
        }
    }
}