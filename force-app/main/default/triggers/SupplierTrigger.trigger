trigger SupplierTrigger on Supplier__c (before insert, before update, before delete, after insert, after update, after delete) {
      if(Trigger.isInsert) {
            if(Trigger.isBefore) {
                  SupplierTriggerHandler.updateBranchValueWhenCreatingSupplier(Trigger.new);
            }

            if(Trigger.isAfter) {
                  SupplierTriggerHandler.updateNewSupplierNumberInKpi(Trigger.new);
            }
      }

      if(Trigger.isUpdate) {
            if(Trigger.isBefore) {
                  for(String supId : Trigger.newMap.keySet()) {
                        if(Trigger.oldMap.get(supId).OwnerId != Trigger.newMap.get(supId).OwnerId) {
                              SupplierTriggerHandler.updateBranchValueWhenCreatingSupplier(Trigger.new);
                        }
                  }
            }

            if(Trigger.isAfter) {
                  for(String supId : Trigger.newMap.keySet()) {
                        if(Trigger.oldMap.get(supId).OwnerId != Trigger.newMap.get(supId).OwnerId) {
                              SupplierTriggerHandler.updateNewSupplierNumberInKpi(Trigger.new);
                              SupplierTriggerHandler.updateNewSupplierNumberInKpi(Trigger.old);
                        }
                  }
            }
      }

      if(Trigger.isDelete) {
            if(Trigger.isAfter) {
                  SupplierTriggerHandler.updateNewSupplierNumberInKpi(Trigger.old);
            }
      }
}