/** 
 *
 */
trigger ContractTrigger on Contract (before insert, before update, before delete, after insert, after update, after delete) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            ContractTriggerHandler.addNewCoOperationContract(Trigger.new);
        }

        if(Trigger.isAfter) {
            //
            ContractTriggerHandler.createNotificationForAllUserWhenCreatingNewCoOpContract(Trigger.new, ' New Co-operationContract', ' Check for new Contract');

            for(String ctrId : Trigger.newMap.keySet()) {
                if(Trigger.newMap.get(ctrId).Status == 'Activated') {
                    // update Opportunity Stage and auto-complete the task
                    ContractTriggerHandler.updateOppStageName(Trigger.new);
                    //
                    ContractTriggerHandler.setNotificationWhenContractIsActivated(Trigger.new);
                    //
                    ContractTriggerHandler.setContractedAmountInOpportunity(Trigger.new);
                }
            }
        }
        //update amount contract cho Account  
        if(Trigger.isAfter) {
            Set<Id> ListAccountIds = new Set<Id>();
            for(String ctrId : Trigger.newMap.keySet()) {
                if(Trigger.newMap.get(ctrId).Contract_Amount__c !=null) {
                    ListAccountIds.add(Trigger.newMap.get(ctrId).AccountId);
                }
            }
            ContractTriggerHandler.updateAmountContract(ListAccountIds);        
        }
    }

    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            for(String ctrId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(ctrId).Status != Trigger.newMap.get(ctrId).Status) {
                    ContractTriggerHandler.blockChangingStatusOfContract(Trigger.new);
                }
            }
        }

        if(Trigger.isAfter) {
            for(String ctrId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(ctrId).Status != Trigger.newMap.get(ctrId).Status) {
                    ContractTriggerHandler.setNotificationWhenContractIsActivated(Trigger.new);
                    //
                    ContractTriggerHandler.createNotificationForAllUserWhenCreatingNewCoOpContract(Trigger.new, ' Updating Contract', ' Check for new update of this Contract');
                }
                
                if((Trigger.oldMap.get(ctrId).Status != Trigger.newMap.get(ctrId).Status)
                || (Trigger.oldMap.get(ctrId).Contract_Amount__c != Trigger.newMap.get(ctrId).Contract_Amount__c)) {
                    // set Contract Amount in Opportunity
                    ContractTriggerHandler.setContractedAmountInOpportunity(Trigger.new);

                    // update Opportunity Stage and auto-complete the task
                    ContractTriggerHandler.updateOppStageName(Trigger.new);  
                }
            }
        }
        //update amount contract cho Account  
        if(Trigger.isAfter) {
            Set<Id> ListAccountIds = new Set<Id>();
            for(String ctrId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(ctrId).Contract_Amount__c != Trigger.newMap.get(ctrId).Contract_Amount__c) {
                        ListAccountIds.add(Trigger.newMap.get(ctrId).AccountId);  
                }
            }
            ContractTriggerHandler.updateAmountContract(ListAccountIds);
        }
    }

    if(Trigger.isDelete) {
        if(Trigger.isAfter) {
            ContractTriggerHandler.setContractedAmountInOpportunity(Trigger.old);
        }
         //update amount contract cho Account  
        if(Trigger.isAfter) {
            Set<Id> ListAccountIds = new Set<Id>();
            for(String ctrId : Trigger.oldMap.keySet()) {
                if(Trigger.oldMap.get(ctrId).Contract_Amount__c !=null) {
                    Contract contract = Trigger.newMap != null ? Trigger.newMap.get(ctrId) : null;
                    if (contract != null) {
                    	ListAccountIds.add(contract.AccountId);    
                    }
                }
            }
            ContractTriggerHandler.updateAmountContract(ListAccountIds);  
        }
    }
}