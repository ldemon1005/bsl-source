trigger ReferralTrigger on Referral__c (before insert, before update, after insert, after update) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            ReferralTriggerHandler.setAmountFieldValue(Trigger.new); 
        }

        if(Trigger.isAfter) {
            ReferralTriggerHandler.updateContactInReferral(Trigger.new);
        }
    }
    
    if(Trigger.isUpdate) {
        if(Trigger.isAfter) {
            for(String rId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(rId).Supplier__c != Trigger.newMap.get(rId).Supplier__c) {
                    ReferralTriggerHandler.updateContactInReferral(Trigger.new);
                }
            }
        }
    }
}