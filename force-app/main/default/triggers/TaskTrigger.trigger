trigger TaskTrigger on Task (before insert, before update, before delete, after insert, after update, after delete) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore) {
            TaskTriggerHandler.updateBranchValueWhenCreatingTask(Trigger.new);
        }

        if(Trigger.isAfter) {
            // add number of Activities in Opportunity
            TaskTriggerHandler.updateNumberOfActivitieswTaskTriggered(Trigger.new);
            // add Customer visiting Times
            TaskTriggerHandler.updateCustomerVisitNo(Trigger.new);
            // add Bidv visit number
            TaskTriggerHandler.updateBidvVisitNo(Trigger.new);
            // add Supplier visit number
            TaskTriggerHandler.updateSupplierVisitNo(Trigger.new);
        }
    }

    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            for(String taskId : Trigger.newMap.keySet()) {
                if((Trigger.oldMap.get(taskId).OwnerId != Trigger.newMap.get(taskId).OwnerId)
                || (Trigger.oldMap.get(taskId).Update_Text__c != Trigger.newMap.get(taskId).Update_Text__c)) {
                    TaskTriggerHandler.updateBranchValueWhenCreatingTask(Trigger.new);
                }
            }
        }

        if(Trigger.isAfter) {
            for(String taskId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(taskId).Status != Trigger.newMap.get(taskId).Status) {
                    TaskTriggerHandler.updateNumberOfActivitieswTaskTriggered(Trigger.new);
                    TaskTriggerHandler.createNotificationForSalesman(Trigger.new);
                }

                if((Trigger.oldMap.get(taskId).Type != Trigger.newMap.get(taskId).Type)
                || (Trigger.oldMap.get(taskId).ActivityDate != Trigger.newMap.get(taskId).ActivityDate)
                || (Trigger.oldMap.get(taskId).WhatId != Trigger.newMap.get(taskId).WhatId)
                || (Trigger.oldMap.get(taskId).OwnerId != Trigger.newMap.get(taskId).OwnerId)) {
                    TaskTriggerHandler.updateCustomerVisitNo(Trigger.new);
                    TaskTriggerHandler.updateBidvVisitNo(Trigger.new);
                    TaskTriggerHandler.updateSupplierVisitNo(Trigger.new);

                    TaskTriggerHandler.updateCustomerVisitNo(Trigger.old);
                    TaskTriggerHandler.updateBidvVisitNo(Trigger.old);
                    TaskTriggerHandler.updateSupplierVisitNo(Trigger.old);
                }
            }
        }
    }

    if(Trigger.isDelete) {
        if(Trigger.isAfter) {
            TaskTriggerHandler.updateCustomerVisitNo(Trigger.old);
            TaskTriggerHandler.updateBidvVisitNo(Trigger.old);
            TaskTriggerHandler.updateSupplierVisitNo(Trigger.old);
        }
    }
}