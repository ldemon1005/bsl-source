trigger LeadTrigger on Lead (before insert, before update, before delete, after insert, after update, after delete) {
      if(Trigger.isInsert) {
            if(Trigger.isBefore) {
                  LeadTriggerHandler.checkRelatedReferralSourceInLead(Trigger.new);
            }
      }

      if(Trigger.isUpdate) {
            if(Trigger.isBefore) {
                   for(String lId : Trigger.newMap.keySet()) {
                        if((Trigger.oldMap.get(lId).Referral_Source__c != Trigger.newMap.get(lId).Referral_Source__c)
                        || (Trigger.oldMap.get(lId).Referral_Person__c != Trigger.newMap.get(lId).Referral_Person__c)) {
                              LeadTriggerHandler.checkRelatedReferralSourceInLead(Trigger.new);
                        }
                  }                 
            }
      }
}