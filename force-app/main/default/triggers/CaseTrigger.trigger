trigger CaseTrigger on Case (before insert, after insert) {
      if(Trigger.isInsert) {
            if(Trigger.isAfter) {
                  CaseTriggerHandler.setNotificationWhenCreatingNewCase(Trigger.new);
            }
      }
}