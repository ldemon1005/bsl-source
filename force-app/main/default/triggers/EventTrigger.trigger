trigger EventTrigger on Event (before insert, before update, before delete, after insert, after update, after delete) {
    if(Trigger.isInsert) {
        if(Trigger.isAfter) {
            // add number of Activities in Opportunity
            EventTriggerHandler.updateNumberOfActivitieswEventTriggered(Trigger.new);
            // add Customer visiting Times
            EventTriggerHandler.updateActualCustomerVisitNoWithEvent(Trigger.new);
            // add Bidv visit number
            EventTriggerHandler.updateActualBidvVisitNoWithEvent(Trigger.new);
            // add Supplier visit number
            EventTriggerHandler.updateActualSupplierVisitNoWithEvent(Trigger.new);
        }
    }

    if(Trigger.isUpdate) {
        if(Trigger.isAfter) {
            for(String eventId : Trigger.newMap.keySet()) {
                if(Trigger.oldMap.get(eventId).EndDateTime != Trigger.newMap.get(eventId).EndDateTime) {
                    EventTriggerHandler.updateNumberOfActivitieswEventTriggered(Trigger.new);
                }

                if((Trigger.oldMap.get(eventId).Type != Trigger.newMap.get(eventId).Type)
                || (Trigger.oldMap.get(eventId).ActivityDate != Trigger.newMap.get(eventId).ActivityDate)
                || (Trigger.oldMap.get(eventId).WhatId != Trigger.newMap.get(eventId).WhatId)
                || (Trigger.oldMap.get(eventId).OwnerId != Trigger.newMap.get(eventId).OwnerId)) {
                    EventTriggerHandler.updateActualCustomerVisitNoWithEvent(Trigger.new);
                    EventTriggerHandler.updateActualBidvVisitNoWithEvent(Trigger.new);
                    EventTriggerHandler.updateActualSupplierVisitNoWithEvent(Trigger.new);

                    EventTriggerHandler.updateActualCustomerVisitNoWithEvent(Trigger.old);
                    EventTriggerHandler.updateActualBidvVisitNoWithEvent(Trigger.old);
                    EventTriggerHandler.updateActualSupplierVisitNoWithEvent(Trigger.old);
                }
            }
        }
    }

    if(Trigger.isDelete) {
        if(Trigger.isAfter) {
            EventTriggerHandler.updateActualCustomerVisitNoWithEvent(Trigger.old);
            EventTriggerHandler.updateActualBidvVisitNoWithEvent(Trigger.old);
            EventTriggerHandler.updateActualSupplierVisitNoWithEvent(Trigger.old);
        }
    }
}