trigger CampaignTrigger on Campaign (before insert, before update, before delete, after insert, after update, after delete) {
      if(Trigger.isInsert) {
            if(Trigger.isAfter) {
                  // create Notification for all user when new Campaign is created
                  CampaignTriggerHandler.createNotificationForAllUser(Trigger.new, ' We have new Campaign', ' Check for new Campaign');
            }
      }

      if(Trigger.isUpdate) {
            if(Trigger.isAfter) {
                  for(String cpId : Trigger.newMap.keySet()) {
                        if(Trigger.oldMap.get(cpId).Status != Trigger.newMap.get(cpId).Status) {
                              CampaignTriggerHandler.createNotificationForAllUser(Trigger.new, ' Updating Campaign', ' Check for new update');
                        }
                  }
            }
      }
}