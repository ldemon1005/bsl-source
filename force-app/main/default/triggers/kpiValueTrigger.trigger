trigger kpiValueTrigger on KPI_Value__c (before insert, before update, before delete, after insert, after update, after delete) {
      if(Trigger.isUpdate) {
            if(Trigger.isAfter) {
                  for(String kpiId : Trigger.newMap.keySet()) {
                        if(Trigger.oldMap.get(kpiId).Actual_Total_Disbursed_Cross_sell_SMTB__c != Trigger.newMap.get(kpiId).Actual_Total_Disbursed_Cross_sell_SMTB__c) {
                              kpiValueTriggerHandler.updateActualCrossSellSmtb(Trigger.new);
                        }

                        if(Trigger.oldMap.get(kpiId).Actual_Total_Disbursed_Cross_sell_SMTPFC__c != Trigger.newMap.get(kpiId).Actual_Total_Disbursed_Cross_sell_SMTPFC__c) {
                              kpiValueTriggerHandler.updateActualCrossSellSmtpfc(Trigger.new);
                        }

                        if(Trigger.oldMap.get(kpiId).Actual_Total_Disbursed_SCF__c != Trigger.newMap.get(kpiId).Actual_Total_Disbursed_SCF__c) {
                              kpiValueTriggerHandler.updateActualScf(Trigger.new);
                        }

                        if(Trigger.oldMap.get(kpiId).Actual_Total_Disbursed_BSL_Self_Develop__c != Trigger.newMap.get(kpiId).Actual_Total_Disbursed_BSL_Self_Develop__c) {
                              kpiValueTriggerHandler.updateActualBslSelfDevelop(Trigger.new);
                        }

                        if(Trigger.oldMap.get(kpiId).Actual_Total_Disbursed_Customer_Initiate__c != Trigger.newMap.get(kpiId).Actual_Total_Disbursed_Customer_Initiate__c) {
                              kpiValueTriggerHandler.updateActualCustomerInitiate(Trigger.new);
                        }

                        if(Trigger.oldMap.get(kpiId).Actual_Total_Disbursed_Others__c != Trigger.newMap.get(kpiId).Actual_Total_Disbursed_Others__c) {
                              kpiValueTriggerHandler.updateActualOther(Trigger.new);
                        }
                  }
            }
      }     
}