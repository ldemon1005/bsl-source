/**
 * @File Name          : AccountTrigger.trigger
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/22/2019, 2:07:52 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/22/2019, 2:07:52 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
      if(Trigger.isInsert) {
            if(Trigger.isBefore) {
                  AccountTriggerHandler.updateBranchValueWhenCreatingAcc(Trigger.new);
                  AccountTriggerHandler.checkRelatedReferralSourceInAccount(Trigger.new);
            }
      }

      if(Trigger.isUpdate) {
            if(Trigger.isBefore) {
                  for(String accId : Trigger.newMap.keySet()) {
                        if((Trigger.oldMap.get(accId).Referral_Source__c != Trigger.newMap.get(accId).Referral_Source__c)
                        || (Trigger.oldMap.get(accId).Referral_Person__c != Trigger.newMap.get(accId).Referral_Person__c)) {
                              AccountTriggerHandler.checkRelatedReferralSourceInAccount(Trigger.new);
                              AccountTriggerHandler.updateContactInCustomerInitiatedReferral(Trigger.new);
                        }

                        if(Trigger.oldMap.get(accId).OwnerId != Trigger.newMap.get(accId).OwnerId) {
                              AccountTriggerHandler.updateBranchValueWhenCreatingAcc(Trigger.new);
                        }
                  }
            }
      }
}