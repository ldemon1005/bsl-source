({
    clickCreate: function(component, event, helper) {
        var validExpense = component.find('disbursementform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        // If we pass error checking, do some real work
        if(validExpense){
            // Create the new expense
            var newDisbursement = component.get("v.newDisbursement");
            console.log("Create Disbursement: " + JSON.stringify(newDisbursement));
            helper.createDisbursement(component, newDisbursement);
            console.log("VALID DATA!");
            window.location.reload();
        }
    },
    
    // Load expenses from Salesforce
    doInit: function(component, event, helper) {
        // Create the action
        var action = component.get("c.getDisbursements");
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.disbursements", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
    }
})