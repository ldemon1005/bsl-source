({
    saveDisbursement: function(component, disbursement, callback) {
        // var action1 = component.get("c.getOpportunityFromContract");
        // action1.setParams({
        //     "ctrId": component.get("v.recordId")
        // });
        // action1.setCallback(this, function(response) {
        //     var stateCb = response.getState();

        //     if(stateCb === "SUCCESS") {
        //         component.set("v.newDisbursement.Opportunity__c", response.getReturnValue());
        //         console.log("Opportunity Id: " + response.getReturnValue());
        //     }
        //     else {
        //         console.log("Failed with state: " + state);
        //     }
        // });

        var action = component.get("c.saveDisbursement"); //  action save Disbursement
        console.log("Save Disbursement accessing");
        component.set("v.newDisbursement.Contract__c", component.get("v.recordId")); // set Contract Record Id for Disbursement
        component.set("v.newDisbursement.Status__c", component.find('select').get('v.value')); // set Contract Record Id for Disbursement
        
        console.log("Contract Id: " + component.get("v.recordId"));
        action.setParams({
            "disbursement": disbursement,
            "ctrId": component.get("v.recordId")
        });
        
        if (callback) {
            action.setCallback(this, callback);
        }

        // $A.enqueueAction(action1);
        $A.enqueueAction(action);
        
    },
    
    createDisbursement: function(component, disbursement) {
        this.saveDisbursement(component, disbursement, function(response){
            var state = response.getState();
            console.log("Create Disbursement accessing: " + state);
            if (state === "SUCCESS") {
                console.log("SUCCESS!!!!");
                var disbursements = component.get("v.disbursements");
                component.set("v.disbursements", disbursements);
                component.set("v.stateCreating", true);
            }
        });
    }
})