({
	loadData : function(component,page) {
		var action = component.get("c.getListData");
        var pageSize = 10;
        action.setParams({
            "pageSize": pageSize,
            "pageNumber": page || 1,
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            component.set("v.data", result.data);
            component.set("v.page", result.page);
            component.set("v.total", result.total);
            component.set("v.pages", Math.ceil(result.total/pageSize));
        });
        $A.enqueueAction(action);
	},
    
    findSupplier : function(component,idSup){
        var action = component.get("c.findSupplier");
        action.setParams({
            "idSup": idSup
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            console.log("result:" + result);
            component.set("v.name", result.Name);
        });
        $A.enqueueAction(action);
    },
    
    deduplicate : function(component,arr){
        console.log(111,arr);
        var isExist = (arr, x) => {
        	for(var i = 0; i < arr.length; i++) {
        		if (arr[i].id === x.id) return true;
        	}
        	return false;
      	}
      	var selection = [];
      	arr.forEach(element => {
        	if(!isExist(selection, element)) selection.push(element);
      	});
		component.set('v.selection',selection);
    },
    testCloseTab : function(component) {
        var closeSubtab = function closeSubtab(result) {
            console.log(result.id)
            var tabId = result.id;
            sforce.console.closeTab(tabId);
        };
        sforce.console.getEnclosingTabId(closeSubtab);
	}
})