({
	doInit: function (component, event, helper) {
        helper.loadData(component,1);
        component.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'User Name', fieldName: 'Username', type: 'text'},
            {label: 'Role', fieldName: 'UserRole.Name', type: 'text'},
        ]);
		var url = window.location.search.substring(1);
            console.log(url);
         var queryString = url.substring( url.lastIndexOf('id') + 3 );
            console.log(queryString);
       	var id = queryString.split("&")[0];
        console.log("ID:" + component.get("v.recordId"));
        component.set("v.id", id);
        helper.findSupplier(component, component.get("v.recordId"));
		console.log('done');
    },
    
    lookupSearch : function(component, event, helper) {
        // Get the SampleLookupController.search server side action
        const serverSearchAction = component.get('c.search');
        // Passes the action to the Lookup component by calling the search method
        component.find('lookup').search(serverSearchAction);
        helper.loadData(component,1);
    },
    onSubmit: function(component, event, helper) {
        const selection = component.get('v.selection');
        const supplierId = component.get("v.recordId");
            
        if (!selection.length) {
            component.set('v.errors', [
                { message: 'You must make a selection before submitting!' },
                { message: 'Please make a selection and try again.' }
            ]);
        } else {
           var listId = [];
            selection.forEach(element => {
                listId.push(element.id);
            });
            var action = component.get("c.SaveLookup");
            var pageSize = 10;
            action.setParams({
                "selection": listId,
                "supplierId": supplierId,
            });
            action.setCallback(this, function(response) {
				var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    alert("Add User to Supplier Team Successfully!");
                    window.history.back();
                }
            });
            $A.enqueueAction(action);
        }
    },
    clearErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.selection');
        
        const errors = component.get('v.errors');

        if (selection.length && errors.length) {
            component.set('v.errors', []);
        }
    },

    clearSelection: function(component, event, helper) {
        component.set('v.selection', []);
    },
    handleRowAction: function (component, event, helper) {
        let isExist = (arr, x) => arr.indexOf(x) > -1;
        var selectedRows = event.getParam('selectedRows');
        var selection = component.get('v.selection');
        console.log("action:" + selectedRows.length);
        if(selectedRows.length == 0) {
            component.set('v.selection', []);
        }else {
            selectedRows.forEach(element => {
                if(!isExist(selection, element)){
                    selection.push({
                        icon: "standard:user",
                        id: element.Id,
                        sObjectType: "User", 
                        subtitle: "",
                        title: element.Name
                   });
                }else {
                    //this.clearSelection();
                }
            });
    		helper.deduplicate(component,selection);
		}
    },
    closeModel : function(component, event, helper) {
        window.history.back();
    }
})