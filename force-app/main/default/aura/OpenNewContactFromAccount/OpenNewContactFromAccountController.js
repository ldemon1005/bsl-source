({
    init : function(cmp, event, helper) {
        var navService = cmp.find("navService");
        var pageReference = {
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Account',
                actionName: 'home'
            }
        };
        cmp.set("v.pageReference", pageReference);
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            cmp.set("v.url", url ? url : defaultUrl);
            console.log("url:" + url);
        }), $A.getCallback(function(error) {
            cmp.set("v.url", defaultUrl);
            console.log("url:" + defaultUrl);
        }));
    },
    
})