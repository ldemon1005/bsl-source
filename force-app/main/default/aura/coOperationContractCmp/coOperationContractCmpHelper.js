({
    saveContract: function(component, contract, callback) {
        var action = component.get("c.saveContract");
        console.log("Save Contract accessing");
        component.set("v.newContract.Supplier__c", component.get("v.recordId"));
        console.log("Supplier Id: " + component.get("v.recordId"));
        console.log("Account Id: " + '{!v.newContract.AccountId}');
        action.setParams({
            "contract": contract,
            "ctrTerm": component.find('contractforms').get('v.value')
        });
        if (callback) {
            action.setCallback(this, callback);
        }
        $A.enqueueAction(action);
    },
    
    createContract: function(component, contract) {
        this.saveContract(component, contract, function(response){
            var state = response.getState();
            console.log("Create Contract accessing: " + state);
            if (state === "SUCCESS") {
                console.log("SUCCESS!!!!");
                var contracts = component.get("v.contracts");
                component.set("v.contracts", contracts);
                component.set("v.stateCreating", true);
            }
        });
    }
})