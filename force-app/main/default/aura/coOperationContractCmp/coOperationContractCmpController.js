({
    clickCreate: function(component, event, helper) {
        var validExpense = component.find('contractform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        // If we pass error checking, do some real work
        if(validExpense){
            // Create the new expense
            var newContract = component.get("v.newContract");
            console.log("Create expense: " + JSON.stringify(newContract));
            helper.createContract(component, newContract);
            console.log("VALID DATA!");
            window.location.reload();
        }
    },
    
    // Load expenses from Salesforce
    doInit: function(component, event, helper) {
        // Create the action
        var action = component.get("c.getContracts");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.contracts", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });	
        $A.enqueueAction(action);
        
        var actionRT = component.get("c.getCoOpContract");
        actionRT.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.newContract.RecordTypeId", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });	
        $A.enqueueAction(actionRT);
        
        var actionAcc = component.get("c.getLinkedAccount");
        actionAcc.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.newContract.AccountId", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });	
        $A.enqueueAction(actionAcc);
    }
})