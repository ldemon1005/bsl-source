({
    doInit: function(component, event, helper) {
        var action = component.get("c.getKnowledges");
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                component.set("v.quoteTemplates", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    onPicklistChange : function(component, event, helper){
        let language =  event.getSource().get("v.value") ; 
        var quoteTemplates = component.get("v.quoteTemplates");
        for(var x in quoteTemplates){
            if(language == quoteTemplates[x].Form_language__c) {
                component.find('form_template__c').set('v.value', quoteTemplates[x].Article__c);
            } 
        }
    }
})