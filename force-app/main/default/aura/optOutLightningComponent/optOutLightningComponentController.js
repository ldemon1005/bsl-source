({

        //Function called on initialization of the component.
        doInit: function(component, event) {
            var action = component.get("c.getAccName"); 
            //Sets up a call to the getAccName method from the Apex Class.
            action.setParams({"accid": component.get("v.recordId") 
                              //Sets up the Account record ID from the Lightning Component as the accid Parameter when calling the getAccName method from the Apex Class.
                }

            );

            action.setCallback(this, function(response) {
                    //Set up the logic of what will happen when this client - side controller hears back from the Apex Controller.var state = response.getState(); //Get the state (SUCCESS orERROR) from the response from the Apex Controller.
                    if(component.isValid() && state === "SUCCESS") {
                    //If the Apex Controller sends back a successful result...
                    component.set("v.accname", response.getReturnValue()); //The return value should be a string containing the Account Name.Set this to the accname attribute in the Lightning Component so that it can be displayed to the user.
                } else {
                    console.log("Failed with state: " + state); //Will log the statein the console
                    //for a little help with debugging
                    //if there is a failure.
                }
            }

        );
        $A.enqueueAction(action); //Sends the call to the Apex Class to get the
        //Account Name.
    }

    ,
    //Function called when the button in the component is clicked/pressed.
    submitOptOut: function(component, event) {
        var action = component.get("c.optOutContacts"); //Sets up a call to

        //the optOutContacts method in the Apex Class.
        action.setParams({
                "accid": component.get("v.recordId") //Sets up the Account ID from
                //the Lightning Component as accid parameter in the call to the optOutContacts method.
            }

        );

        action.setCallback(this, function(response) {
                    //Set up the logic of what will happen when this client - side controller hears back from the Apex Controller.var state = response.getState(); //Get the state (SUCCESS or ERROR)from the response from the Apex Controller.Different text will be pushed back to the message attribute in the Lightning Component to be displayed to the user depending on what happens.
           			 if(state === "SUCCESS"){
                        component.set("v.message", "All Contacts have been successfully opted out.");

                        }

                        else if (state === "INCOMPLETE") {
                            component.set("v.message", "The operation may not have completed.")
                                        }

                            else if (state === "ERROR") {
                                component.set("v.message", "An error was encountered.");
                                    var errors = response.getError();

                                    if (errors) {
                                        if (errors[0] && errors[0].message) {
                                            console.log("Error message: " + errors[0].message);
                                        }
                                    } else {
                                        console.log("Unknown error");
                                        component.set("v.message", "An error was encountered.Please contact your administrator.");

                                        }
                                    }
                                }

                            );
                            $A.enqueueAction(action); //Send the call to the Apex controller to
                            //update the Contacts at the Account to email opt out status.
                            var btn = event.getSource(); 
                            //Find the button that was pressed to callthisfunction.
                            btn.set("v.disabled", true); //Disable the button from being pressed.
                        }
                    }
                )