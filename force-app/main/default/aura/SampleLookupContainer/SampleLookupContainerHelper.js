({
	loadData : function(component,page) {
		var action = component.get("c.getListData");
        var pageSize = 10;
        action.setParams({
            "pageSize": pageSize,
            "pageNumber": page || 1,
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            component.set("v.data", result.data);
            component.set("v.page", result.page);
            component.set("v.total", result.total);
            component.set("v.pages", Math.ceil(result.total/pageSize));
        });
        $A.enqueueAction(action);
	},
    
    getOpsIdSelected : function(component) {
		var action = component.get("c.opsIdSelected");
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
            console.log("ops:" + result);
        });
        $A.enqueueAction(action);
	},
    
 	findCampaign : function(component,idCamp){
        var action = component.get("c.findCampaign");
        action.setParams({
            "idCamp": idCamp
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
     		var campaigns = [];

            campaigns.push({
                id: result.Id,
                icon: "standard:campaign",
                sObjectType: "Campaign", 
                subtitle: "", 
                title: result.Name
            });
            component.set('v.campaigns',campaigns)
        });
        $A.enqueueAction(action);
    },
    
    findOp : function(component, idOp){
        console.log(222,idOp);
        var action = component.get("c.findOp");
        action.setParams({
            "idOp": idOp
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();
     		var ops = [];
            console.log("result:" + result);

            var selection = component.get('v.selection');
            
            selection.push({
                    icon: "standard:opportunity",
                    id: result.Id,
                    sObjectType: "Opportunity", 
                    subtitle: "",
                    title: result.Name
            });
            var selection = component.get('v.selection');
        	component.set('v.selection',selection);
        });
        $A.enqueueAction(action);
    },
    
    deduplicate : function(component,arr){
        console.log(111,arr);
        var isExist = (arr, x) => {
        	for(var i = 0; i < arr.length; i++) {
        		if (arr[i].id === x.id) return true;
        	}
        	return false;
      	}
      	var selection = [];
      	arr.forEach(element => {
        	if(!isExist(selection, element)) selection.push(element);
      	});
		component.set('v.selection',selection);
    }
})