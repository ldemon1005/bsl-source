({
	doInit: function (component, event, helper) {
        helper.getOpsIdSelected(component);
        helper.loadData(component,1);
        component.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Close Date', fieldName: 'CloseDate', type: 'date'},
            {label: 'Amount', fieldName: 'Amount', type: 'currency'}
        ]);
        var options = [
            {
                'value': 'Sent'
            },
            {
                'value': 'Responded'
            }
        ];
        component.set("v.options", options);
        /// add ops selected
        var vids = component.get("v.ids");
        if(vids != '' && vids != 'undefined') {
            var ids  = vids.split(',');
            console.log("ids:" + ids);
            var ilength = ids.length;
            console.log("length:" + ilength);
            if(ilength > 1) {
                ilength = ilength - 1;
            }
            
            for(var i = 0; i < ilength ; i++) {
                console.log(ids[i]);    
                helper.findOp(component, ids[i]);
            }
        }
        ///
        var url = window.location.search.substring(1);
        console.log("url:" + url);
       	var id = url.split("=")[1];
        id = id.split("&")[0];
        console.log("ID:" + id);
        if(id.length > 5) {
        	helper.findCampaign(component,id);
        }
        
		console.log('done');
    },
    lookupSearch : function(component, event, helper) {
        // Get the SampleLookupController.search server side action
        const serverSearchAction = component.get('c.search');
        // Passes the action to the Lookup component by calling the search method
        component.find('lookup').search(serverSearchAction);
        helper.loadData(component,1);
    },
	onChangeSearchText: function (component, event, helper) {
        const serverSearchAction = component.get('c.searchCampaign');
        component.find('campaign').search(serverSearchAction);
        helper.loadData(component,1);
    },
    onSubmit: function(component, event, helper) {
        const selection = component.get('v.selection');
        if (!selection.length) {
            component.set('v.errors', [
                { message: 'You must make a selection before submitting!' },
                { message: 'Please make a selection and try again.' }
            ]);
        } else {
            component.set('v.step',2);
        }
    },
    save : function(component, event, helper){
        const selection = component.get('v.selection');
        const campaigns = component.get('v.campaigns');
        const optionSelected = component.get('v.optionSelected');
        if (!campaigns.length) {
            component.set('v.errors', [
                { message: 'You must make a selection before submitting!' },
                { message: 'Please make a selection and try again.' }
            ]);
        } else {
            var listId = [];
            selection.forEach(element => {
                listId.push(element.id);
            });
            var action = component.get("c.SaveLookup");
            var pageSize = 10;
            action.setParams({
                "selection": listId,
                "campaignId": campaigns[0].id,
                "optionSelected": optionSelected
            });
            action.setCallback(this, function(response) {
				var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    alert("Add lookup success!");
                    window.history.back();
                } else {
                    alert("Could not insert. Dupplicated record!");
                    window.history.back();
                }
            });
            $A.enqueueAction(action);
                
        }
        
    },
    clearErrorsOnChange: function(component, event, helper) {
        const selection = component.get('v.selection');
        
        const errors = component.get('v.errors');

        if (selection.length && errors.length) {
            component.set('v.errors', []);
        }
    },

    clearSelection: function(component, event, helper) {
        component.set('v.selection', []);
    },
    handleRowAction: function (component, event, helper) {
        let isExist = (arr, x) => arr.indexOf(x) > -1;
        
        var selectedRows = event.getParam('selectedRows');
        var selection = component.get('v.selection');
        if(selectedRows.length == 0) {
            component.set('v.selection', []);
        }else {
            selectedRows.forEach(element => {
                if(!isExist(selection, element)){
                    selection.push({
                        icon: "standard:opportunity",
                        id: element.Id,
                        sObjectType: "Opportunity", 
                        subtitle: "",
                        title: element.Name
                   });
                }
            });
            helper.deduplicate(component,selection);
		}
    },
    closeModel : function(){
        window.history.back();
    }
})