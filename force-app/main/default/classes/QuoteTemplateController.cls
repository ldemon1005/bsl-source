public class QuoteTemplateController {
    @AuraEnabled
    public static List<Knowledge__kav> getKnowledges() {
 		List<Knowledge__kav> Knowledges = [select id, form_language__c, Article__c from Knowledge__kav where Article_Type__c = 'Quote Template'];            
        return Knowledges;
    }
}