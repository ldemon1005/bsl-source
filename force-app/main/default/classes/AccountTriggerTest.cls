@IsTest()
public class AccountTriggerTest {
    private static final RecordType rt = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
                         						LIMIT 1];
    
    @isTest() static void test1(){
        Referral__c referral1 = new Referral__c();
        referral1.RecordTypeId = rt.Id;
        insert referral1;
        
        Referral__c referral2 = new Referral__c();
        referral2.RecordTypeId = rt.Id;
        insert referral2;
        
        Account acct = new Account(Name='Test Account');
        acct.Referral_Source__c = referral1.Id;
        acct.Business_Registration_Number__c = '1';
        acct.BillingStreet = '11 duy tan';
        insert acct;
        
        acct.Referral_Source__c = referral2.Id;
        update acct;
        
        Test.startTest();
        Database.DeleteResult result = Database.delete(acct, false);
        Test.stopTest();
    }	
}