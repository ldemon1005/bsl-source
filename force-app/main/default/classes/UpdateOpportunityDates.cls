global without sharing class UpdateOpportunityDates implements Database.Batchable<SObject>, Database.Stateful {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT ID, Approval_Data__c, ' +
        'Approval_Deadline_BD__c, Approval_Deadline_BOM__c, Approval_Deadline_CC__c, Approval_Deadline_CCD__c, '+
        'Approval_Result_Actual_Date_BD__c, Approval_Result_Actual_Date_BOM__c, Approval_Result_Actual_Date_CC__c, Approval_Result_Actual_Date_CCD__c, '+
        'Submission_Date_BD__c, Submission_Date_BOM__c, Submission_Date_CC__c, Submission_Date_CCD__c '+
        'FROM Opportunity');
    }

    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        List<Opportunity> oppList = new List<Opportunity>();

        for (Opportunity opp : scope) {
            String jsonData = opp.Approval_Data__c;
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
            Map<String, Object> branchDirectorDatesMap = (Map<String, Object>)m.get('Branch Director');
            Map<String, Object> ccdDatesMap = (Map<String, Object>)m.get('CCD');
            Map<String, Object> creditCouncilDatesMap = (Map<String, Object>)m.get('Credit Council');
            Map<String, Object> bomDatesMap = (Map<String, Object>)m.get('BOM');

            // Branch Director Dates
            String adBdString = branchDirectorDatesMap.get('Approval Deadline').toString();
            Date adBd = Date.valueOf(adBdString);
            opp.Approval_Deadline_BD__c = adBd;

            String sdBdString = branchDirectorDatesMap.get('Submission Date').toString();
            Date sdBd = Date.valueOf(sdBdString);
            opp.Submission_Date_BD__c = sdBd;

            String aradBdString = branchDirectorDatesMap.get('Approval Result Actual Date').toString();
            Date aradBd = Date.valueOf(aradBdString);
            opp.Approval_Result_Actual_Date_BD__c = aradBd;

            // CCD
            String adCcdString = ccdDatesMap.get('Approval Deadline').toString();
            Date adCcd = Date.valueOf(adCcdString);
            opp.Approval_Deadline_CCD__c = adCcd;

            String sdCcdString = ccdDatesMap.get('Submission Date').toString();
            Date sdCcd = Date.valueOf(sdCcdString);
            opp.Submission_Date_CCD__c = sdCcd;

            String aradCcdString = ccdDatesMap.get('Approval Result Actual Date').toString();
            Date aradCcd = Date.valueOf(aradCcdString);
            opp.Approval_Result_Actual_Date_CCD__c = aradCcd;

            // Credit Council
            String adCcString = creditCouncilDatesMap.get('Approval Deadline').toString();
            Date adCc = Date.valueOf(adCcString);
            opp.Approval_Deadline_CC__c = adCc;

            String sdCcString = creditCouncilDatesMap.get('Submission Date').toString();
            Date sdCc = Date.valueOf(sdCcString);
            opp.Submission_Date_CC__c = sdCc;

            String aradCcString = creditCouncilDatesMap.get('Approval Result Actual Date').toString();
            Date aradCc = Date.valueOf(aradCcString);
            opp.Approval_Result_Actual_Date_CC__c = aradCc;

            // BOM
            String adBomString = bomDatesMap.get('Approval Deadline').toString();
            Date adBom = Date.valueOf(adBomString);
            opp.Approval_Deadline_BOM__c = adBom;

            String sdBomString = bomDatesMap.get('Submission Date').toString();
            Date sdBom = Date.valueOf(sdBomString);
            opp.Submission_Date_BOM__c = sdBom;

            String aradBomString = bomDatesMap.get('Approval Result Actual Date').toString();
            Date aradBom = Date.valueOf(aradBomString);
            opp.Approval_Result_Actual_Date_BOM__c = aradBom;

            oppList.add(opp);
        }

        update oppList;
    }

    global void finish(Database.BatchableContext bc) {

    }
}