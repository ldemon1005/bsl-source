public with sharing class SampleLookupController {
    
    public static ApexPages.StandardSetController  controller;
    public SampleLookupController(ApexPages.StandardSetController  constructor) {
 		controller = constructor;
 	}
    
     public SampleLookupController(ApexPages.StandardController controller) {
      // this is new constructor which should fix your problem
    }

    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
            Opportunity (Id, Name, StageName WHERE id NOT IN :selectedIds)
            LIMIT :MAX_RESULTS];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Opportunities & convert them into LookupSearchResult
        String opptyIcon = 'standard:opportunity';
        Opportunity [] opptys = ((List<Opportunity>) searchResults[0]);
        for (Opportunity oppty : opptys) {
            results.add(new LookupSearchResult(oppty.Id, 'Opportunity', opptyIcon, oppty.Name, 'Opportunity • '+ oppty.StageName));
        }
        return results;
    }
    
    @AuraEnabled
    public static List<LookupSearchResult> searchCampaign(String searchTerm) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
            Campaign (Id, Name)
            LIMIT :MAX_RESULTS];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Opportunities & convert them into LookupSearchResult
        String campIcon = 'standard:campaign';
        Campaign [] campaigns = ((List<Campaign>) searchResults[0]);
        for (Campaign camp : campaigns) {
            results.add(new LookupSearchResult(camp.Id, 'Campaign', campIcon, camp.Name, ''));
        }
        return results;
    }
    @AuraEnabled
	public static PagedResult getListData(Decimal pageSize,Decimal pageNumber) {
		Integer pSize = (Integer)pageSize;
		Integer offset = ((Integer)pageNumber - 1) * pSize;
		PagedResult res = new PagedResult();
		res.page = (Integer)pageNumber;
		List<Opportunity> results = [select id,Name,CloseDate,Amount from Opportunity LIMIT :pSize OFFSET :offset];
        res.total = [select count() from Opportunity]; 
        res.data = results;
        return res;
	}
    @AuraEnabled
	public static Campaign findCampaign(String idCamp) {
		Campaign camp = [select id,Name from Campaign where id=:idCamp limit 1];
        return camp;
	}
    
    @AuraEnabled
	public static Opportunity findOp(String idOp) {
		//idOp = '006N000000DK7P5IAL';
		Opportunity camp = [select id,Name from Opportunity where id=:idOp LIMIT 1];
        return camp;
	}
    @AuraEnabled
	public static void SaveLookup(List<String> selection, String campaignId,String optionSelected) {
        List<CampaignMember_c__c> campMembers = new List<CampaignMember_c__c>();
        for(String selectionId : selection){
            CampaignMember_c__c campMember = new CampaignMember_c__c();
            campMember.Campaign__c = campaignId;
            campMember.OpportunityId__c = selectionId;
            campMember.Status__c = optionSelected;
            campMembers.add(campMember);
        }
        System.debug(campMembers);
        upsert campMembers;
	}
    
    @AuraEnabled
    public static string isLightningExperience()
    {
            boolean isLightningExperience = false;
            string id = UserInfo.getUserId();
            for (User u: [SELECT Id, UserPreferencesLightningExperiencePreferred FROM User WHERE Id =: id LIMIT 1])
            {
                isLightningExperience = u.UserPreferencesLightningExperiencePreferred;
            }
            return string.valueOf(isLightningExperience);
    }
    
    @AuraEnabled
    public static String opsIdSelected(){
       return 'test';
    }
    
    public class PagedResult {
		@AuraEnabled
		public Integer page { get;set; }

		@AuraEnabled
		public Integer total { get;set; }

		@AuraEnabled
		public List<Opportunity> data { get;set; }
	}
}