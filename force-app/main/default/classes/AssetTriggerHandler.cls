/**
*
*/

public without sharing class AssetTriggerHandler {

    public static void checkInsertingAssets(List<Asset__c> assetList) {
        List<String> oppIdList = new List<String>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, Currency__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        RecordType flOppRt = [SELECT Id, Name
				              FROM RecordType
				              WHERE SobjectType = 'Opportunity' AND Name = 'Finance Lease'
                              LIMIT 1];
                              
        RecordType olOppRt = [SELECT Id, Name
				              FROM RecordType
				              WHERE SobjectType = 'Opportunity' AND Name = 'Operating Lease'
                              LIMIT 1];
                              
        String flOpp = flOppRt.Id;
        String olOpp = olOppRt.Id;

        for(Asset__c ast : assetList) {
            String oId = ast.Opportunity__c;

            for(Opportunity o : oppList) {
                if(oId.equals(o.Id)) {
                    
                }
            }
        }
    }
    
    /**
    *
    *
    */
    public static void setCurrencyInOpportunity(List<Asset__c> assetList) {
        // List of Opportunity Id 
        List<String> oppIdList = new List<String>();
        //List of Opportunity need to be updated
        List<Opportunity> updatedOppList = new List<Opportunity>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, Currency__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Asset__c asset : assetList) {
            String oppAId = asset.Opportunity__c;
            String assetCurrency = asset.Currency__c;

            for(Opportunity o : oppList) {
                if(oppAId.equals(o.Id)) {
                    o.Currency__c = assetCurrency;
                    updatedOppList.add(o);
                }
            }
        }

        update updatedOppList;
    }

    /**
    * addAccountNameAndSalesmanToAsset method
    * add Account Name and Salesman to Assets
    *
    * @param: List<Asset__c> assetList
    */
    public static void addAccountNameAndSalesmanToAsset(List<Asset__c> assetList) {
        // List of Opportunity 
        List<String> oppIdList = new List<String>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, AccountId, OwnerId
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Asset__c ast : assetList) {
            String oId = ast.Opportunity__c;

            for(Opportunity o : oppList) {
                if(oId.equals(o.Id)) {
                    if(o.AccountId != null) {
                        ast.Account_Name__c = o.AccountId;
                    }

                    if(o.OwnerId != null) {
                        ast.Salesman__c = o.OwnerId;
                    }
                }
            }
        }
    }

    /**
    * updateAssetInReferral method
    * When updating or creating an Opp => update Asset of Opp to Referral
    *
    * @param: List<Asset__c> assetList
    */
    public static void updateAssetInReferral(List<Asset__c> assetList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Asset__c asset : assetList) {
            oppIdList.add(asset.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, Referral_Source__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Opportunity o : oppList) {
            String oId = o.Id;

            for(Asset__c a : assetList) {
                if(oId.equals(a.Opportunity__c)) {
                    if(o.Referral_Source__c != null) {
                        a.Referral__c = o.Referral_Source__c;
                    }
                }
            }
        }        
    }

    /**
    *
    *
    */
    public static void setExpectedPotentialContractAmountInOpp(List<Asset__c> assetList) {
        String flRtOpp;
        String olRtOpp;
        String fl = 'Finance Lease';
        String ol = 'Operating Lease';

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // List of Opportunity need to be updated
        List<Opportunity> updatedOpportunityList = new List<Opportunity>();

        List<RecordType> rt = [SELECT Id, Name
                               FROM RecordType
                               WHERE SobjectType = 'Opportunity'];

        for(RecordType r : rt) {
            if(fl.equals(r.Name)) {
                flRtOpp = r.Id;
            }

            if(ol.equals(r.Name)) {
                olRtOpp = r.Id;
            }
        }

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, RecordTypeId, Total_Expected_Potential_Contract_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        List<Quote> quoteList = [SELECT Id, OpportunityId
                                 FROM Quote
                                 WHERE OpportunityId IN :oppIdList AND Status = 'Tentatively Accepted by Customer'];

        List<Asset__c> checkedAssetList = [SELECT Id, Opportunity__c, Expected_Contract_Amount__c
                                           FROM Asset__c
                                           WHERE Opportunity__c IN :oppIdList];

        if(quoteList.size() == 0) {
            for(Opportunity o : oppList) {
                String rcIdOpp = o.RecordTypeId;
                String oId = o.Id;
                Decimal expectedPotentialAmount = 0;

                for(Asset__c a : checkedAssetList) {
                    if(oId.equals(a.Opportunity__c)) {
                        if(flRtOpp.equals(rcIdOpp)) {
                            Decimal assetPotentialAmount = a.Expected_Contract_Amount__c;
                            expectedPotentialAmount += assetPotentialAmount;
                        }
                        else if(olRtOpp.equals(rcIdOpp)) {
                            Decimal assetPotentialAmount = a.Expected_Contract_Amount__c;
                            expectedPotentialAmount += assetPotentialAmount;
                        }
                    }
                }

                o.Total_Expected_Potential_Contract_Amount__c = expectedPotentialAmount;
                o.Expected_Potential_Contract_Amount__c = expectedPotentialAmount;
                updatedOpportunityList.add(o);
            }

            update updatedOpportunityList;
        }       
    }

    /**
    *
    *
    */
    public static void setExpectedPotentialContractAmountInOppwDeleting(List<Asset__c> assetList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // List of Opportunity need to be updated
        List<Opportunity> updatedOpportunityList = new List<Opportunity>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, Total_Expected_Potential_Contract_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        List<Quote> quoteList = [SELECT Id, OpportunityId
                                 FROM Quote
                                 WHERE OpportunityId IN :oppIdList AND Status = 'Tentatively Accepted by Customer'];

        List<Asset__c> checkedAssetList = [SELECT Id, Opportunity__c, Expected_Contract_Amount__c
                                           FROM Asset__c
                                           WHERE Opportunity__c IN :oppIdList];

        if(quoteList.size() == 0) {
            for(Opportunity o : oppList) {
                String oId = o.Id;
                Decimal expectedPotentialAmount = o.Total_Expected_Potential_Contract_Amount__c;

                for(Asset__c a : checkedAssetList) {
                    if(oId.equals(a.Opportunity__c)) {
                        Decimal assetPotentialAmount = a.Expected_Contract_Amount__c;
                        expectedPotentialAmount -= assetPotentialAmount;
                    }
                }

                o.Total_Expected_Potential_Contract_Amount__c = expectedPotentialAmount;
                o.Expected_Potential_Contract_Amount__c = expectedPotentialAmount;
                updatedOpportunityList.add(o);
            }

            update updatedOpportunityList;
        }       
    }

    /**
    *
    *
    *
    */
    public static void cancelUpdatingAsset(List<Asset__c> assetList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, StageName
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList AND StageName = 'B'];

        if(oppList.size() != 0) {
            for(Opportunity o : oppList) {
                String oppId = o.Id;

                for(Asset__c a : assetList) {
                    if(oppId.equals(a.Opportunity__c)) {
                        a.addError('Cannot update asset when Opportunity has Stage B!');
                    }
                }
            }
        }
    }
    
    /**
    *update Expected Contract Amount in quote
    *
    *
    */
    public static void updateExpectedContractAmount(map<id, Asset__c> newAsset, map<id, Asset__c> oldAsset) {
        List<String> idQuotes = new List<String>();
        for(Asset__c a : newAsset.values()){
            idQuotes.add(a.Quote__c);
        }
        Map<Id, Quote> quotes = new Map<Id, Quote>([select id,Expected_Contract_Amount__c from Quote where id in :idQuotes]);
        if(quotes.size() > 0){
            for(String id : newAsset.keySet()) {
                Decimal temp = newAsset.get(id).Expected_Contract_Amount__c - oldAsset.get(id).Expected_Contract_Amount__c;
                quotes.get(newAsset.get(id).Quote__c).Expected_Contract_Amount__c += temp; 
            }
        }
        List<Quote> l_quote = quotes.values();
        System.debug('cts 1: ' + l_quote);
        update l_quote;
    }

    /**
     * calculateDownPaymentAmountWhenCreatingAsset method
     * Update Down Payment Amount When creating Assets, and calculate Total Asset Value
     *
     * @param: List<Asset__c> assetList
     */
    public static void calculateDownPaymentAmountWhenCreatingAsset(List<Asset__c> assetList) {
        String financeLease;
        String financeLeaseRV;

        List<RecordType> rtList = [SELECT Id, Name
                                   FROM RecordType
                                   WHERE Name = 'Finance Lease' OR Name = 'Finance Lease with RV'];

        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Finance Lease')) {
                financeLease = r.Id;
            }
            else if(rName.equals('Finance Lease with RV')) {
                financeLeaseRV = r.Id;
            }
        }

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // List of Quote that need to be updated
        List<Quote> updatedQuoteList = new List<Quote>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Quote> quoteList = [SELECT Id, OpportunityId, RecordTypeId, Down_Payment_Amount__c, Down_Payment_Rate__c, Leaseable_Asset_Value__c, Total_Asset_Value__c
                                 FROM Quote
                                 WHERE OpportunityId IN :oppIdList];

        System.debug('Quote List Size: ' + quoteList.size());

        List<Opportunity> checkedOppList = [SELECT Id, Total_Asset_Value__c
                                            FROM Opportunity
                                            WHERE Id IN :oppIdList];

        List<Asset__c> checkedAssetList = [SELECT Id, Opportunity__c, Asset_Value__c
                                           FROM Asset__c
                                           WHERE Opportunity__c IN :oppIdList];

        for(Opportunity opp : checkedOppList) {
            Decimal newTotalAssetValue = 0;
            String oppId = opp.Id;

            for(Asset__c asset : checkedAssetList) {
                System.debug('Asset Value: ' + asset.Asset_Value__c);

                if(oppId.equals(asset.Opportunity__c)) {
                    newTotalAssetValue += asset.Asset_Value__c;
                }
            }

            for(Quote q : quoteList) {
                Decimal downPaymentRate = q.Down_Payment_Rate__c;

                System.debug('XXXXXXXXXXXXXXXXXXXXX ' + downPaymentRate);
                if(oppId.equals(q.OpportunityId)) {
                    if(financeLease.equals(q.RecordTypeId)) {
                        System.debug('New Asset Value CCCCCCCCCCCCCCCCCCC: ' + newTotalAssetValue);
                        q.Total_Asset_Value__c = newTotalAssetValue;
                        q.Down_Payment_Amount__c = newTotalAssetValue*downPaymentRate/100;
                    }
                    else if(financeLeaseRV.equals(q.RecordTypeId)) {
                        q.Total_Asset_Value__c = newTotalAssetValue;
                        q.Down_Payment_Amount__c = (q.Leaseable_Asset_Value__c)*(q.Down_Payment_Rate__c)/100;
                    }
                    else {
                        q.Total_Asset_Value__c = newTotalAssetValue;
                    }
                }

                updatedQuoteList.add(q);
            }
        }

        System.debug('MMMMMMMMMMMMMMMMMMMMMMMMM');

        update updatedQuoteList;
    }

    

    /**
    *
    *
    *
    * 
    */
    public static void addQuoteToAsset(List<Asset__c> assetList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Quote> quoteList = [SELECT Id, OpportunityId
                                 FROM Quote
                                 WHERE OpportunityId IN :oppIdList];

        for(Quote quote : quoteList) {
            String oppId = quote.OpportunityId;

            for(Asset__c a : assetList) {
                if(oppId.equals(a.Opportunity__c)) {
                    a.Quote__c = quote.Id;
                }
            }
        }
    }

    /**
    * updatePotentialDateInOpp method
    * Updating Potential Date In Opportunity of Assets
    *
    * @param: List<Asset__c> assetList
    */
    public static void updatePotentialDateInOpp(List<Asset__c> assetList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, Potential_Date__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        List<Asset__c> allAssetInOppList = [SELECT Id, Opportunity__c
                                            FROM Asset__c
                                            WHERE Opportunity__c IN :oppIdList];

        for(Opportunity o : oppList) {
            String oId = o.Id;
            List<Asset__c> assetInOppList = new List<Asset__c>();

            for(Asset__c a : allAssetInOppList) {
                if(oId.equals(a.Opportunity__c)) {
                    assetInOppList.add(a);
                }
            }

            if(assetInOppList.size() == 0) {
                o.Potential_Date__c = Date.today();
            }
        }

        update oppList;
    }
    /*
     * update amount  deposit tren quote khi gia tri tai asset thay doi
	*/
    public static void updateSecurityDepositInAccount(List<Asset__c> assets){
        Set<Id> idsQuote = new Set<Id>();
        for(Asset__c ass:assets){
            if(ass.Quote__c!=null){
                idsQuote.add(ass.Quote__c);
            }
        } 
        if(idsQuote.size()>0){
            List<Asset__c> listAsset = [Select ID,Total_Asset_Value_with_VAT__c,Asset_Value__c,Total_VAT__c,Quote__c From Asset__c WHere Quote__c In :idsQuote];
           	List<Quote> lstQuote = [Select ID,Security_Deposit_rate_Amount__c,Upfront_Fee_Amount__c,Closing_Price_Amount__c,Total_VAT__c,Total_Asset_Value_without_VAT__c,
                                    Security_Deposit_Rate__c,Upfront_Fee_Rate__c,Closing_Price_Rate__c From QUote WHere Id In :idsQuote
                                    AND (NOT(Opportunity.StageName = 'C' OR Opportunity.StageName = 'A' OR Opportunity.StageName = 'B' OR Opportunity.StageName = 'L'))]; 
            System.debug('listAsset');
            System.debug(listAsset);
            foR(Quote q: lstQuote){
                Decimal totalAmountWithoutVAT = 0;
                Decimal totalVAT = 0;
                for(Asset__c ass:listAsset){
                    if(q.ID ==  ass.Quote__c){
                        totalAmountWithoutVAT += (ass.Asset_Value__c==null?0:ass.Asset_Value__c);
                        totalVAT += (ass.Total_VAT__c==null?0:ass.Total_VAT__c);
                    }
                } 
                System.debug('totalAmountWithoutVAT');
                q.Total_VAT__c = totalVAT;
                q.Total_Asset_Value_without_VAT__c= totalAmountWithoutVAT;
                Decimal amountWithVAT = totalVAT + totalAmountWithoutVAT;
                q.Security_Deposit_rate_Amount__c = (amountWithVAT * (q.Security_Deposit_Rate__c==null?0:q.Security_Deposit_Rate__c))/100;
                q.Upfront_Fee_Amount__c = (amountWithVAT * (q.Upfront_Fee_Rate__c==null?0:q.Upfront_Fee_Rate__c))/100;
                q.Closing_Price_Amount__c = (amountWithVAT * (q.Closing_Price_Rate__c==null?0:q.Closing_Price_Rate__c))/100;
            }
            if(lstQuote.size()>0){
                update lstQuote;
            }
        }
        
    }

}