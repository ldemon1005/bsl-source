@IsTest(SeeAllData=true)
public class TestLookupControllerTest {
    @isTest() static void test(){
        Account acc = new Account(Name='test lookup');
        acc.Business_Registration_Number__c = '1';
        acc.BillingStreet = '11 duy tan';
        insert acc;
        
       	Date dt = Date.newInstance(2021, 1, 2);

		Opportunity opp = new Opportunity();

		opp.Name = 'test lookup';
		opp.BSL_Service__c = 'Finance Lease';
		opp.Contract_Ending_Selection__c = 'Ownership Transfer';
		opp.StageName = 'D1';
		opp.AccountId = acc.Id;
		opp.CloseDate = dt;

		insert opp;
        
        list<String> l_str = new list<String>();
        
        List<LookupSearchResult> search = TestLookupController.search('test', l_str);
    }
}