public class QuoteToPDF {
    public String styleTable{get;set;}
    public Quote quote{get;set;}
    public QuoteToPDF(ApexPages.StandardController controller) {
        try{
            styleTable = 'floatingStyle';
            if (!Test.isRunningTest()){
                List<String> fields = new List<String>{'Id','Name', 'Lease_Term__c','Leaseable_Asset_Value__c','Repayment_Date__c','ExpirationDate','Frequency_of_Interest_Repayment__c',
                    'Expected_Contract_Amount__c','Down_Payment_Rate__c','Down_Payment_Amount__c','Lease_Term_for_Insurance__c','Security_Deposit_rate_Amount__c',
                    'Upfront_Fee_Amount__c','Closing_Price_Amount__c','Fixed_Rate_In_The_First_Period__c','Expected_Disbursement_Date__c','Margin__c',
                    'Interest_Base__c','Number_Of_Months_For_Fixed_Rate__c','IRR__c','Susidy_from_Supplier__c','Commission_to_Supplier__c',
                    'Other_Expenses_Born_by_BSL__c','Company_Name__c','Out_Fix_Rate_Months__c','Grace_for__c','Cach_An_Han__c','Grace_Period__c','User_Ext__c','User_Mobile__c','User_Phone__c','User_Address__c','User_Company__c','User_Name__c','Lease_Amount__c','Lease_Rate__c','Security_Deposit_Rate__c','Closing_Price_Rate__c','Prepayment_Fee__c','Grace_Period__c','Grace_for__c','Payment_Method_for_Grace__c'
                    , 'Other_Expenses_Born_by_BSL__c'
                    , 'Commission_to_Supplier__c'
                    , 'Total_Asset_Value_with_VAT__c'
                    , 'Upfront_Fee_Rate__c'
                    , 'One_time_Repayment_of_VAT__c'
                    , 'Insurance_Disbursement_Datex__c', 'Frequency_of_Principal_Repayment__c', 'form_template__c'
                    };
                controller.addFields(fields);
                quote = (Quote)controller.getRecord();
                list<String> formatFields = new List<String>{'Total_Asset_Value_with_VAT__c', 'Down_Payment_Amount__c', 'Lease_Amount__c', 
                                                            'Security_Deposit_rate_Amount__c', 'Upfront_Fee_Amount__c', 'Closing_Price_Amount__c'};
                for(String str : fields){
                    String strReplace;
                    strReplace = '{!quote.' + str + '}';
                    if(formatFields.indexOf(str) >= 0){
                        List<String> args = new String[]{'0','number','#,###.0'};
                        Decimal d = (Decimal)quote.get(str);
                        quote.form_template__c = quote.form_template__c.replace(strReplace, String.valueOf(quote.get(str) == null ? '' : String.format(d.format(), args)));
                    }else{
                        quote.form_template__c = quote.form_template__c.replace(strReplace, String.valueOf(quote.get(str) == null ? '' : quote.get(str)));
                    }
                    
                }
                
                List<Asset__c> assets = [select id, name from Asset__c where Quote__c=:quote.Id];
                String asset_name = '';
                for(Asset__c a : assets){
                    asset_name = asset_name + a.Name + '<br>';
                }
                quote.form_template__c = quote.form_template__c.replace('{!quote.asset_name}',asset_name);
            }
            
        } catch(Exception e){
            System.debug(e.getMessage());
        }
    }
}