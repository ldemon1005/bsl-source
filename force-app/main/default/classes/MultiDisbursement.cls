public class MultiDisbursement {
    public String styleTable{get;set;}
    public String irrString{get;set;}
    public String xirrString{get;set;}
    public Double irrDouble{get;set;}
    public Decimal irrDec;
    public Quote quote{get;set;}
    public List<Asset__c> listAsset {get;set;}
    public List<RecordRepayment> listRepay {get;set;}
    public  String renderingService { get; private set; }
    public  String todayString { get; private set; }
    public Boolean isUpdateQuote = false;
    public MultiDisbursement(ApexPages.StandardController controller) {
        try{
            styleTable = 'floatingStyle';
            if (!Test.isRunningTest()){
                controller.addFields(new List<String>{'Id','Name', 'Lease_Term__c','Leaseable_Asset_Value__c','Repayment_Date__c','ExpirationDate','Frequency_of_Interest_Repayment__c',
                    'Assets__r','Expected_Contract_Amount__c','Down_Payment_Rate__c','Lease_Term_for_Insurance__c','Security_Deposit_rate_Amount__c',
                    'Upfront_Fee_Amount__c','Closing_Price_Amount__c','Fixed_Rate_In_The_First_Period__c','Expected_Disbursement_Date__c','Margin__c',
                    'Interest_Base__c','Number_Of_Months_For_Fixed_Rate__c','IRR__c','Susidy_from_Supplier__c','Commission_to_Supplier__c',
                    'Other_Expenses_Born_by_BSL__c','Grace_Period__c','Grace_for__c','Payment_Method_for_Grace__c'
                    , 'Other_Expenses_Born_by_BSL__c'
                    , 'Commission_to_Supplier__c'
                    , 'Total_Asset_Value_with_VAT__c'
                    , 'Upfront_Fee_Rate__c'
                    , 'One_time_Repayment_of_VAT__c'
                    , 'Account.Name'
                    , 'Frequency_of_Principal_Repayment__c', 'form_template__c'
                    });
            }
            quote = (Quote)controller.getRecord();
            caculateRepayment(quote);
            Date today = System.Today();
            todayString = today.day() + '/' + today.month() + '/' + today.year();
        } catch(Exception e){
            System.debug(e.getMessage());
        }
        
    }
    public void caculateRepayment(Quote quote){
        try{
            listAsset = [Select Name,Asset_Price_After_Tax__c,VAT_Amount_of_Asset__c,Insurance_Premium__c,
                         VAT_of_Insurance__c,Maintenance_Cost__c,VAT_of_Maintenance__c,Other_Costs__c,VAT_of_Other_Cost__c,Installation_Cost__c,VAT_of_Installation__c
                         from Asset__c where Quote__c=:quote.Id];
            for(Asset__c asset : listAsset){
                if(asset.Asset_Price_After_Tax__c!=null){
                    RecordRepayment.AssetPrice+=asset.Asset_Price_After_Tax__c;
                }
                if(asset.VAT_Amount_of_Asset__c!=null){
                    RecordRepayment.AssetPriceVAT+=asset.VAT_Amount_of_Asset__c;
                }
                if(asset.Insurance_Premium__c!=null){
                    RecordRepayment.InsurancePremium+=asset.Insurance_Premium__c;
                }
                if(asset.VAT_of_Insurance__c!=null){
                    RecordRepayment.InsurancePremiumVat+=asset.VAT_of_Insurance__c;
                }
                if(asset.Maintenance_Cost__c!=null){
                    RecordRepayment.MaintenanceCost+=asset.Maintenance_Cost__c;
                }
                if(asset.VAT_of_Maintenance__c!=null){
                    RecordRepayment.MaintenanceCostVAT+=asset.VAT_of_Maintenance__c;
                }
                RecordRepayment.OtherCosts+=asset.Other_Costs__c == null ? 0:asset.Other_Costs__c;
                RecordRepayment.OtherCostsVAT+=((asset.VAT_of_Other_Cost__c == null ? 0:asset.VAT_of_Other_Cost__c) +(asset.VAT_of_Installation__c == null ? 0:asset.VAT_of_Installation__c));
                
                RecordRepayment.InstallationCosts += asset.Installation_Cost__c == null ? 0:asset.Installation_Cost__c;
                RecordRepayment.InstallationCostsVAT = asset.VAT_of_Installation__c == null ? 0:asset.VAT_of_Installation__c;
            }
            RecordRepayment.AssetPrice = RecordRepayment.AssetPrice*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.AssetPriceVAT = RecordRepayment.AssetPriceVAT*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.InsurancePremium = RecordRepayment.InsurancePremium*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.InsurancePremiumVAT = RecordRepayment.InsurancePremiumVAT*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.MaintenanceCost = RecordRepayment.MaintenanceCost*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.MaintenanceCostVAT = RecordRepayment.MaintenanceCostVAT*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.OtherCosts = RecordRepayment.OtherCosts*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.OtherCostsVAT = RecordRepayment.OtherCostsVAT*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.InstallationCosts = RecordRepayment.InstallationCosts*((100-quote.Down_Payment_Rate__c))/100;
            RecordRepayment.InstallationCostsVAT = RecordRepayment.InstallationCostsVAT*((100-quote.Down_Payment_Rate__c))/100;
            
            List<Several_Disbursement__c> listDisbursement = [select id, Asset_Price__c, Installation__c, Insurance__c, Maintenance__c, Other_Costs__c, Disbursement__c, Disbursment_Date__c 
                                                              from Several_Disbursement__c where Quote__c = :quote.id order by Disbursment_Date__c ASC];
            if(listDisbursement.size() > 0){
                RecordRepayment.minSeveralDisbursement = listDisbursement[0].Disbursment_Date__c;
            }
            
            Decimal numberOfRepayment = quote.Lease_Term__c;
            
            if(quote.Frequency_of_Interest_Repayment__c !=null){
                RecordRepayment.interestRepayment = integer.valueof(quote.Frequency_of_Interest_Repayment__c);
            }
            if(quote.Frequency_of_Principal_Repayment__c !=null){
                RecordRepayment.principalRepayment = integer.valueof(quote.Frequency_of_Principal_Repayment__c);
            } 
            RecordRepayment.numRepayment = RecordRepayment.interestRepayment < RecordRepayment.principalRepayment ? RecordRepayment.interestRepayment : RecordRepayment.principalRepayment;
            listRepay = new List<RecordRepayment>();
            Integer nextTerm = 0;
            Decimal preDebt = 0;
            Decimal prePrincipal = 0;
            Decimal nextDisbursement = 0;
            
            RecordRepayment rTotal = new RecordRepayment(quote);
            RecordRepayment rTemp = new RecordRepayment(quote);
            for(Integer i = 0; i < listDisbursement.size(); i++){
                Several_Disbursement__c disb = listDisbursement[i];
                Integer numTerm = 1;
                if(i != (listDisbursement.size() - 1)){
                    numTerm = listDisbursement[i].Disbursment_Date__c.monthsBetween(listDisbursement[i+1].Disbursment_Date__c)/RecordRepayment.numRepayment;
                }else{
                    numTerm = listDisbursement[i].Disbursment_Date__c.monthsBetween(quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(numberOfRepayment)))/RecordRepayment.numRepayment;
                }
                RecordRepayment r0 = new RecordRepayment(quote);
                r0.RepaymentDate = listDisbursement[i].Disbursment_Date__c;
                r0.RepaymentDateStr = listDisbursement[i].Disbursment_Date__c.format();
                r0.TotalOutstandingDebt = (RecordRepayment.AssetPrice == null || disb.Asset_Price__c == null ? 0 : RecordRepayment.AssetPrice*disb.Asset_Price__c/100) 
                    					+ (RecordRepayment.AssetPriceVAT == null || disb.Asset_Price__c == null ? 0 : RecordRepayment.AssetPriceVAT*disb.Asset_Price__c/100)
                    					+ (RecordRepayment.InsurancePremium == null || disb.Insurance__c == null ? 0 : RecordRepayment.InsurancePremium*disb.Insurance__c/100)
                    					+ (RecordRepayment.InsurancePremiumVat == null || disb.Insurance__c == null ? 0 : RecordRepayment.InsurancePremiumVat*disb.Insurance__c/100)
                    					+ (RecordRepayment.MaintenanceCost == null || disb.Maintenance__c == null ? 0 : RecordRepayment.MaintenanceCost*disb.Maintenance__c/100)
                    					+ (RecordRepayment.MaintenanceCostVAT == null || disb.Maintenance__c == null ? 0 : RecordRepayment.MaintenanceCostVAT*disb.Maintenance__c/100)
                    					+ (RecordRepayment.OtherCosts == null || disb.Other_Costs__c == null ? 0 : RecordRepayment.OtherCosts*disb.Other_Costs__c/100)
                    					+ (RecordRepayment.OtherCostsVAT == null || disb.Other_Costs__c == null ? 0 : RecordRepayment.OtherCostsVAT*disb.Other_Costs__c/100)
                    					+ (RecordRepayment.InstallationCosts == null || disb.Installation__c == null ? 0 : RecordRepayment.InstallationCosts*disb.Installation__c/100)
                    					+ (RecordRepayment.InstallationCostsVAT == null || disb.Installation__c == null ? 0 : RecordRepayment.InstallationCostsVAT*disb.Installation__c/100);
             
                r0.TotalOutstandingDebt = -r0.TotalOutstandingDebt;
                if(i == 0){
                    r0.SecurityDeposit = quote.Security_Deposit_rate_Amount__c == null ? 0 : quote.Security_Deposit_rate_Amount__c;
                    r0.UpfrontOrClosingFee = (quote.Upfront_Fee_Amount__c == null ? 0 : quote.Upfront_Fee_Amount__c) 
                        					+ (quote.Susidy_from_Supplier__c == null ? 0 : quote.Susidy_from_Supplier__c)
                        					- (quote.Commission_to_Supplier__c == null ? 0 : quote.Commission_to_Supplier__c)
                        					- (quote.Other_Expenses_Born_by_BSL__c == null ? 0 : quote.Other_Expenses_Born_by_BSL__c);
                }
                r0.Cashflow = r0.TotalOutstandingDebt + (r0.SecurityDeposit == null ? 0 : r0.SecurityDeposit) + (r0.UpfrontOrClosingFee == null ? 0 : r0.UpfrontOrClosingFee);
                listRepay.add(r0);
                nextDisbursement = r0.TotalOutstandingDebt;
                System.debug('numTerm: ' + numTerm);
                for(Integer j = 0; j < numTerm; j++){
                    Integer currenTerm = nextTerm + j;
                    RecordRepayment r = new RecordRepayment(quote);
                    r.caculate(currenTerm, i + 1, preDebt, prePrincipal, nextDisbursement, listDisbursement[i]);
                    preDebt = r.TotalOutstandingDebt == null ? 0 : r.TotalOutstandingDebt;
                    prePrincipal = r.TotalPrincipalRepaymentWithVAT == null ? 0 : r.TotalPrincipalRepaymentWithVAT;
                    nextDisbursement = 0;
                    listRepay.add(r);
                }
                nextTerm += numTerm;
                rTotal.RepaymentOfAssetPrice += RecordRepayment.AssetPrice == null || disb.Asset_Price__c == null ? 0 : RecordRepayment.AssetPrice*disb.Asset_Price__c/100;
                rTotal.RepaymentOfAssetVAT += RecordRepayment.AssetPriceVAT == null || disb.Asset_Price__c == null ? 0 : RecordRepayment.AssetPriceVAT*disb.Asset_Price__c/100;
                rTotal.RepaymentOfInsurance += RecordRepayment.InsurancePremium == null || disb.Insurance__c == null ? 0 : RecordRepayment.InsurancePremium*disb.Insurance__c/100;
                rTotal.RepaymentOfInsuranceVAT += RecordRepayment.InsurancePremiumVat == null || disb.Insurance__c == null ? 0 : RecordRepayment.InsurancePremiumVat*disb.Insurance__c/100;
                rTotal.RepaymentOfMaintenance += RecordRepayment.MaintenanceCost == null || disb.Maintenance__c == null ? 0 : RecordRepayment.MaintenanceCost*disb.Maintenance__c/100;
                rTotal.RepaymentOfMaintenanceVAT += RecordRepayment.MaintenanceCostVAT == null || disb.Maintenance__c == null ? 0 : RecordRepayment.MaintenanceCostVAT*disb.Maintenance__c/100;
                rTotal.RepaymentOfOtherCosts += RecordRepayment.OtherCosts == null || disb.Other_Costs__c == null ? 0 : RecordRepayment.OtherCosts*disb.Other_Costs__c/100;
                rTotal.RepaymentOfOtherCostsVAT += RecordRepayment.OtherCostsVAT == null || disb.Other_Costs__c == null ? 0 : RecordRepayment.OtherCostsVAT*disb.Other_Costs__c/100;
                rTotal.RepaymentOfInstallationCosts += RecordRepayment.InstallationCosts == null || disb.Installation__c == null ? 0 : RecordRepayment.InstallationCosts*disb.Installation__c/100;
                rTotal.RepaymentOfInstallationCostsVAT += RecordRepayment.InstallationCostsVAT == null || disb.Installation__c == null ? 0 : RecordRepayment.InstallationCostsVAT*disb.Installation__c/100;
            }
          	
            List<Decimal> cashFlowIRR = new List<Decimal>();
            XIRR xirr = new XIRR();
            for(Integer i = 0; i < listRepay.size(); i++){
                if(RecordRepayment.minSeveralDisbursement != null && listRepay[i].RepaymentDate == RecordRepayment.minSeveralDisbursement.addMonths(integer.valueof(quote.Lease_Term_for_Insurance__c))){
                    listRepay[i].RepaymentOfInsurance = (rTotal.RepaymentOfInsurance - rTemp.RepaymentOfInsurance).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfInsuranceVAT = (rTotal.RepaymentOfInsuranceVAT - rTemp.RepaymentOfInsuranceVAT).round(System.RoundingMode.HALF_EVEN);
                }
                if(i == (listRepay.size() - 1)){
                    listRepay[i].RepaymentOfAssetPrice = (rTotal.RepaymentOfAssetPrice - rTemp.RepaymentOfAssetPrice).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfAssetVAT = (rTotal.RepaymentOfAssetVAT - rTemp.RepaymentOfAssetVAT).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfInsurance = (rTotal.RepaymentOfInsurance - rTemp.RepaymentOfInsurance).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfInsuranceVAT = (rTotal.RepaymentOfInsuranceVAT - rTemp.RepaymentOfInsuranceVAT).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfMaintenance = (rTotal.RepaymentOfMaintenance - rTemp.RepaymentOfMaintenance).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfMaintenanceVAT = (rTotal.RepaymentOfMaintenanceVAT - rTemp.RepaymentOfMaintenanceVAT).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfOtherCosts = (rTotal.RepaymentOfOtherCosts - rTemp.RepaymentOfOtherCosts).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfOtherCostsVAT = (rTotal.RepaymentOfOtherCostsVAT - rTemp.RepaymentOfOtherCostsVAT).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfInstallationCosts = (rTotal.RepaymentOfInstallationCosts - rTemp.RepaymentOfInstallationCosts).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].RepaymentOfInstallationCostsVAT = (rTotal.RepaymentOfInstallationCostsVAT - rTemp.RepaymentOfInstallationCostsVAT).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].SecurityDeposit = -(quote.Security_Deposit_rate_Amount__c == null ? 0 : quote.Security_Deposit_rate_Amount__c);
                    listRepay[i].UpfrontOrClosingFee = quote.Closing_Price_Amount__c;
                    listRepay[i].TotalPrincipalRepayment = (listRepay[i].RepaymentOfAssetPrice + listRepay[i].RepaymentOfInsurance + listRepay[i].RepaymentOfMaintenance 
                        									+ listRepay[i].RepaymentOfOtherCosts + listRepay[i].RepaymentOfInstallationCosts).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].TotalVATRepayment = (listRepay[i].RepaymentOfAssetVAT + listRepay[i].RepaymentOfInsuranceVAT + listRepay[i].RepaymentOfMaintenanceVAT 
                        									+ listRepay[i].RepaymentOfOtherCostsVAT + listRepay[i].RepaymentOfInstallationCostsVAT).round(System.RoundingMode.HALF_EVEN);
                    listRepay[i].TotalPrincipalRepaymentWithVAT = listRepay[i].TotalPrincipalRepayment + listRepay[i].TotalVATRepayment;
                    listRepay[i].TotalRepaymentByCustomer = listRepay[i].TotalPrincipalRepaymentWithVAT + listRepay[i].InterestPayment;
                    listRepay[i].Cashflow = ((listRepay[i].TotalRepaymentByCustomer == null ? 0 : listRepay[i].TotalRepaymentByCustomer) 
                                             + (listRepay[i].SecurityDeposit == null ? 0 : listRepay[i].SecurityDeposit) 
                                             + (listRepay[i].UpfrontOrClosingFee == null ? 0 : listRepay[i].UpfrontOrClosingFee) ).round(System.RoundingMode.HALF_EVEN);
                }else{
                    rTemp.RepaymentOfAssetPrice += listRepay[i].RepaymentOfAssetPrice;
                    rTemp.RepaymentOfAssetVAT += listRepay[i].RepaymentOfAssetVAT;
                    rTemp.RepaymentOfInsurance += listRepay[i].RepaymentOfInsurance;
                    rTemp.RepaymentOfInsuranceVAT += listRepay[i].RepaymentOfInsuranceVAT;
                    rTemp.RepaymentOfMaintenance += listRepay[i].RepaymentOfMaintenance;
                    rTemp.RepaymentOfMaintenanceVAT += listRepay[i].RepaymentOfMaintenanceVAT;
                    rTemp.RepaymentOfOtherCosts += listRepay[i].RepaymentOfOtherCosts;
                    rTemp.RepaymentOfOtherCostsVAT += listRepay[i].RepaymentOfOtherCostsVAT;
                    rTemp.RepaymentOfInstallationCosts += listRepay[i].RepaymentOfInstallationCosts;
                    rTemp.RepaymentOfInstallationCostsVAT += listRepay[i].RepaymentOfInstallationCostsVAT;
                    rTemp.TotalPrincipalRepayment += listRepay[i].RepaymentOfAssetPrice + listRepay[i].RepaymentOfInsurance + listRepay[i].RepaymentOfMaintenance + listRepay[i].RepaymentOfOtherCosts + listRepay[i].RepaymentOfInstallationCosts;
                    rTemp.TotalVATRepayment += listRepay[i].RepaymentOfAssetVAT + listRepay[i].RepaymentOfInsuranceVAT + listRepay[i].RepaymentOfMaintenanceVAT + listRepay[i].RepaymentOfOtherCostsVAT + listRepay[i].RepaymentOfInstallationCostsVAT;
                }      
                rTotal.TotalPrincipalRepayment += listRepay[i].TotalPrincipalRepayment;
                rTotal.TotalVATRepayment += listRepay[i].TotalVATRepayment;
                rTotal.TotalPrincipalRepaymentWithVAT += listRepay[i].TotalPrincipalRepaymentWithVAT;
                rTotal.InterestPayment += listRepay[i].InterestPayment;
                rTotal.TotalRepaymentByCustomer += listRepay[i].TotalRepaymentByCustomer;
                
                xirr.addCashflow(listRepay[i].RepaymentDate, listRepay[i].Cashflow);
                cashFlowIRR.add(listRepay[i].Cashflow);
            }
            listRepay.add(rTotal);
            for(RecordRepayment r : listRepay){
                r.RepaymentOfAssetPrice = r.RepaymentOfAssetPrice == 0 ? null : r.RepaymentOfAssetPrice;
                r.RepaymentOfAssetVAT = r.RepaymentOfAssetVAT == 0 ? null : r.RepaymentOfAssetVAT;
                r.RepaymentOfInsurance = r.RepaymentOfInsurance == 0 ? null : r.RepaymentOfInsurance;
                r.RepaymentOfInsuranceVAT = r.RepaymentOfInsuranceVAT == 0 ? null : r.RepaymentOfInsuranceVAT;
                r.RepaymentOfMaintenance = r.RepaymentOfMaintenance == 0 ? null : r.RepaymentOfMaintenance;
                r.RepaymentOfMaintenanceVAT = r.RepaymentOfMaintenanceVAT == 0 ? null : r.RepaymentOfMaintenanceVAT;
                r.RepaymentOfOtherCosts = r.RepaymentOfOtherCosts == 0 ? null : r.RepaymentOfOtherCosts;
                r.RepaymentOfOtherCostsVAT = r.RepaymentOfOtherCostsVAT == 0 ? null : r.RepaymentOfOtherCostsVAT;
                r.RepaymentOfInstallationCosts = r.RepaymentOfInstallationCosts == 0 ? null : r.RepaymentOfAssetPrice;
                r.RepaymentOfInstallationCostsVAT = r.RepaymentOfInstallationCostsVAT == 0 ? null : r.RepaymentOfInstallationCostsVAT;
                r.TotalPrincipalRepayment = r.TotalPrincipalRepayment == 0 ? null : r.TotalPrincipalRepayment;
                r.TotalVATRepayment = r.TotalVATRepayment == 0 ? null : r.TotalVATRepayment;
                r.TotalPrincipalRepaymentWithVAT = r.TotalPrincipalRepaymentWithVAT == 0 ? null : r.TotalPrincipalRepaymentWithVAT;
                r.InterestPayment = r.InterestPayment == 0 ? null : r.InterestPayment;
                r.TotalRepaymentByCustomer = r.TotalRepaymentByCustomer == 0 ? null : r.TotalRepaymentByCustomer;
            }
            //caculate irr, xirr
            Double guess= 0.0000001;
            irrString = IRR.caculateIRR(cashFlowIRR, guess);
            decimal irr = decimal.valueOf(irrString);
            irr = Decimal.valueOf((Math.pow(Double.valueOf(1 + irr), Double.valueOf(12/(Decimal.valueOf(quote.Frequency_of_Interest_Repayment__c)))) - 1)*100).setScale(2);
            irrString = String.valueOf(irr);
            Decimal xirrDec = xirr.calculate(guess)*100;
            xirrString = String.valueOf(xirrDec.setScale(2));
        }catch(Exception e){
            System.debug('MultiDisbursement ' +  e.getLineNumber() + ': ' + e.getMessage());
            System.debug('Exception e '+e.getLineNumber());
        }
    }
    public void updateIRR(){
        if(isUpdateQuote){
            System.debug('IRwerR');
            System.debug(quote.Irr__c);
            //update quote;
            
        }
    }
}