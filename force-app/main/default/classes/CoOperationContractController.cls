public with sharing class CoOperationContractController {
    @AuraEnabled
    public static List<Contract> getContracts() {        
        // OK, they're cool, let 'em through
        return [SELECT Id, Contract_Number__c, Name, StartDate, EndDate, 
                       ContractTerm, Contract_Content_Summary__c, Status 
                FROM Contract];
    }
    
    @AuraEnabled
    public static Contract saveContract(Contract contract, Decimal ctrTerm) {
        // Perform isUpdateable() checking first, then
        Integer n = ctrTerm.intValue();
        contract.ContractTerm = n;
        System.debug('AAAAAAAAAAAAAA');
        insert contract;
        return contract;
    }

    @AuraEnabled
    public static String getCoOpContract() {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Contract' AND Name = 'Co-operation'
                        LIMIT 1];
                
        String rtId = rt.Id;

        return rtId;
    }

    @AuraEnabled
    public static String getLinkedAccount() {
        Account lAcc = [SELECT Id
                        FROM Account
                        WHERE Name = '[BSL]-sharing--line'
                        LIMIT 1];
                
        String aId = lAcc.Id;

        return aId;
    }
}