@IsTest(SeeAllData=true)
public class QuoteTriggerTest {
    private static final RecordType recordType = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Quote' AND Name = 'Finance Lease'
                         						LIMIT 1];
    @isTest() static void test(){
        Opportunity opp = new Opportunity();
        opp.Name='Test' ;
        opp.StageName = 'D';
        opp.CloseDate = System.today() + 5;
        Insert opp ;
        
        Test.startTest();
        Quote quote = new Quote();
        quote.Status = 'Approved by BSL';
        quote.Down_Payment_Rate__c = 5;
        quote.Total_Asset_Value__c = 50;
        quote.Down_Payment_Amount__c = 5;
        quote.Down_Payment_Paid_To__c = 'BSL';
        quote.Lease_Term__c = 5;
        quote.Frequency_of_Principal_Repayment__c = '6';
        quote.Frequency_of_Interest_Repayment__c = '6';
        quote.Interest_Base__c = 5;
        quote.Margin__c = 5;
        quote.Fixed_Rate_In_The_First_Period__c = 5;
        quote.Number_Of_Months_For_Fixed_Rate__c = 5;
        quote.Security_Deposit_rate_Amount__c = 5;
        quote.Security_Deposit_Rate__c = 5;
        quote.Upfront_Fee_Rate__c = 5;
        quote.Closing_Price_Rate__c = 5;
        quote.Grace_Period__c = 0;
        quote.IRR__c = 5;
        quote.RecordTypeId = recordType.Id;
        quote.Name = 'qoute test';
        quote.OpportunityId = opp.Id;
        insert quote;
        
        quote.Down_Payment_Rate__c = 6;
        quote.Status = 'Rejected by BSL';
        quote.Down_Payment_Amount__c = 5;
        quote.Security_Deposit_Rate__c = 10;
        quote.Security_Deposit_rate_Amount__c = 15;
        quote.Expected_Contract_Amount__c = 30;
        update quote;
        
        delete quote;
		
		       
        Opportunity opp1 = new Opportunity();
        opp1.Name='Test' ;
        opp1.StageName = 'D1';
        opp1.CloseDate = System.today() + 5;
        Insert opp1 ;

        Quote quote1 = new Quote();
        quote1.Status = 'Approved by BSL';
        quote1.Down_Payment_Rate__c = 5;
        quote1.Total_Asset_Value__c = 50;
        quote1.Down_Payment_Amount__c = 5;
        quote1.Down_Payment_Paid_To__c = 'BSL';
        quote1.Lease_Term__c = 5;
        quote1.Frequency_of_Principal_Repayment__c = '6';
        quote1.Frequency_of_Interest_Repayment__c = '6';
        quote1.Interest_Base__c = 5;
        quote1.Margin__c = 5;
        quote1.Fixed_Rate_In_The_First_Period__c = 5;
        quote1.Number_Of_Months_For_Fixed_Rate__c = 5;
        quote1.Security_Deposit_rate_Amount__c = 5;
        quote1.Security_Deposit_Rate__c = 5;
        quote1.Upfront_Fee_Rate__c = 5;
        quote1.Closing_Price_Rate__c = 5;
        quote1.Grace_Period__c = 0;
        quote1.IRR__c = 5;
        quote1.RecordTypeId = recordType.Id;
        quote1.Name = 'qoute test';
        quote1.OpportunityId = opp1.Id;
        insert quote1;
        
        Test.stopTest(); 
    }
}