@isTest
private class TestContactTrigger {
    @isTest static void TestDeleteContact() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Contact acct = new Contact(FirstName='Test Contact', LastName = 'abc', Birthday__c  = Date.today());
        insert acct;
        // Perform test
        Test.startTest();
        Database.DeleteResult result = Database.delete(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
    }
    
    @isTest static void TestInsertContact() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Contact acct = new Contact(FirstName='Test Contact', LastName = 'abc1', Birthday__c  = Date.today());
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
    }
    
    @isTest static void TestUpdateContact() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Contact acct = new Contact(FirstName='Test Contact', LastName = 'abc', Birthday__c  = Date.today());
        insert acct;
        // Perform test
        Test.startTest();
        acct.FirstName = 'Change';
        Database.SaveResult result = Database.update(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
    }
    
}