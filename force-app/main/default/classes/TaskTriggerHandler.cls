public without sharing class TaskTriggerHandler {
    /**
    *
    *
    */
    public static void updateNumberOfActivitieswTaskTriggered(List<Task> taskList) {
        //get current DateTime
        DateTime myDateTime = DateTime.now();
        // List of WhatId
        List<String> whatIdList = new List<String>();
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // List of Opportunity need to be updated
        List<Opportunity> updatedOppList = new List<Opportunity>();

        for(Task t : taskList) {
            whatIdList.add(t.WhatId);
        }

        List<Opportunity> oppList = [SELECT Id, Number_of_Activities__c
                                     FROM Opportunity 
                                     WHERE Id IN :whatIdList];

        for(Opportunity opp : oppList) {
            oppIdList.add(opp.Id);
        }

        List<Task> tskList = [SELECT Id, Status, WhatId
                              FROM Task 
                              WHERE WhatId IN :oppIdList AND Status = 'Completed' AND (Type = 'Visit' OR Type = 'Meeting at BSL' OR Type = 'Call' OR Type = 'Email')];

        List<Event> eventList = [SELECT Id, EndDateTime, WhatId
                                 FROM Event 
                                 WHERE WhatId IN :oppIdList AND (Type = 'Visit' OR Type = 'Meeting at BSL' OR Type = 'Call' OR Type = 'Email')];                   

        for(Opportunity o : oppList) {
            Decimal numberOfActivities = 0;
            String oId = o.Id;

            for(Task tk : tskList) {
                if(oId.equals(tk.WhatId)) {
                    numberOfActivities++;
                }
            }

            for(Event e : eventList) {
                if((oId.equals(e.WhatId)) && (myDateTime > e.EndDateTime)) {
                    numberOfActivities++;
                }
            }

            o.Number_of_Activities__c = numberOfActivities;
            updatedOppList.add(o);
        }

        update updatedOppList;
    }

    /**
    * 
    *
    */
    public static void updateCustomerVisitNo(List<Task> taskList) {
        String quarterRcId;
        String monthRcId;
        String yearRcId;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'KPI_Value__c'];
        
        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Quarter')) {
                quarterRcId = r.Id;
            }

            if(rName.equals('Month')) {
                monthRcId = r.Id;
            }

            if(rName.equals('Year')) {
                yearRcId = r.Id;
            }
        }

        String visit = 'Visit';
        String meetingAtBsl = 'Meeting at BSL';
        Decimal thisYear = Decimal.valueOf(String.valueOf(Date.today().year()));
        String q1 = 'Q1';
        String q2 = 'Q2';
        String q3 = 'Q3';
        String q4 = 'Q4';
    
        // List of All User Id with Task
        List<String> allUserId = new List<String>();
        // List of KPI Id
        List<String> kpiIdList = new List<String>();
        // List of KPI Value that need to be updated
        List<KPI_Value__c> updatedKpiValueList = new List<KPI_Value__c>();

        for(Task t : taskList) {
            allUserId.add(t.OwnerId);
        }

        List<Task> validTaskList = [SELECT Id, Type, OwnerId, ActivityDate, WhatId
                                    FROM Task
                                    WHERE (OwnerId IN :allUserId) AND (Type = :visit OR Type = :meetingAtBsl)];

        System.debug('Size of Task List: ' + validTaskList.size());
        
        List<Event> validEventList = [SELECT Id, Type, OwnerId, EndDateTime, WhatId
                                      FROM Event
                                      WHERE (OwnerId IN :allUserId)];

        List<KPI__c> kpiList = [SELECT Id, OwnerId, KPI_Type__c, Year__c
                                FROM KPI__c
                                WHERE OwnerId IN :allUserId];

        for(KPI__c k : kpiList) {
            kpiIdList.add(k.Id);
        }

        List<KPI_Value__c> kpiValueList = [SELECT Id, Months__c, Quarter__c, Year__c, KPI__c, RecordTypeId, Actual_Customer_Visit_No__c
                                           FROM KPI_Value__c
                                           WHERE KPI__c IN :kpiIdList];

        System.debug('KPI List Size: ' + kpiIdList.size());
        System.debug('validTaskList List Size: ' + validTaskList.size());
        System.debug('validEventList List Size: ' + validEventList.size());
        System.debug('kpiValueList List Size: ' + kpiValueList.size());

        
        for(KPI_Value__c kv : kpiValueList) {
            String ownerId;
            Integer yearOfKpi;
            List<Task> taskInKvList = new List<Task>();
            List<Event> eventInKvList = new List<Event>();

            Decimal customerVisit;

            // define ownerId
            for(KPI__c kpi : kpiList) {
                String kpiId = kpi.Id;
                yearOfKpi = Integer.valueOf(kpi.Year__c);

                if(kpiId.equals(kv.KPI__c)) {
                    ownerId = kpi.OwnerId;
                }
            }

            System.debug('Owner Id: ' + ownerId);

            for(Task t : validTaskList) {
                Integer monthOfTask;
                Integer yearOfTask;
                String whatId;

                if(t.ActivityDate == null) {
                    monthOfTask = 0;
                    yearOfTask = 0;
                }
                else {
                    monthOfTask = t.ActivityDate.month();
                    yearOfTask = t.ActivityDate.year();
                }

                System.debug('MONTH: ' + monthOfTask);

                if(t.WhatId == null) {
                    whatId = 'xxx';
                }
                else {
                    whatId = t.WhatId;
                }

                System.debug('Owner Id of Task: ' + t.OwnerId);
                System.debug('What Id checking: ' + whatId);

                if(ownerId.equals(t.OwnerId) && (whatId.contains('001') || whatId.contains('006'))) {
                    System.debug('Entry If 1');

                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        System.debug('Entry If 2');

                        if(q1.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            System.debug('Entry If 3');

                            if((monthOfTask >= 1) && (monthOfTask <= 3)) {
                                System.debug('Quarter 1 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q2.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 4) && (monthOfTask <= 6)) {
                                System.debug('Quarter 2 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q3.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 7) && (monthOfTask <= 9)) {
                                System.debug('Quarter 3 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q4.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 10) && (monthOfTask <= 12)) {
                                System.debug('Quarter 4 Entry');
                                taskInKvList.add(t);
                            }
                        }
                    }

                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(monthOfTask);

                        if(monthOfOpp.equals(kv.Months__c) && (yearOfKpi == yearOfTask)) {
                            taskInKvList.add(t);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfOpp = String.valueOf(yearOfTask);

                        if(yearOfOpp.equals(kv.Year__c)) {
                            taskInKvList.add(t);
                        }
                    }
                }
            }

            customerVisit = taskInKvList.size();
            System.debug('Task In Kv List Size: ' + taskInKvList.size());
            System.debug('Customer Visit Number at First: ' + customerVisit);

            for(Event e : validEventList) {
                Integer monthOfEvent;
                Integer yearOfEvent;
                String whatId;

                if(e.EndDateTime == null) {
                    monthOfEvent = 0;
                    yearOfEvent = 0;
                }
                else {
                    monthOfEvent = e.EndDateTime.month();
                    yearOfEvent = e.EndDateTime.year();
                }

                if(e.WhatId == null) {
                    whatId = 'xxx';
                }
                else {
                    whatId = e.WhatId;
                }

                if(ownerId.equals(e.OwnerId) && (whatId.contains('001') || whatId.contains('006'))) {
                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        if(q1.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 1) && (monthOfEvent <= 3)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q2.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 4) && (monthOfEvent <= 6) && (yearOfKpi == yearOfEvent)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q3.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 7) && (monthOfEvent <= 9)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q4.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 10) && (monthOfEvent <= 12)) {
                                eventInKvList.add(e);
                            }
                        }
                    }

                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(monthOfEvent);

                        if(monthOfOpp.equals(kv.Months__c) && (yearOfKpi == yearOfEvent)) {
                            eventInKvList.add(e);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfOpp = String.valueOf(yearOfEvent);

                        if(yearOfOpp.equals(kv.Year__c)) {
                            eventInKvList.add(e);
                        }
                    }
                }
            }

            customerVisit += eventInKvList.size();
            System.debug('Last Customer Visit Number: ' + customerVisit);

            kv.Actual_Customer_Visit_No__c = customerVisit;
            updatedKpiValueList.add(kv);
        }
        

        update updatedKpiValueList;
    }

    /**
    * 
    *
    */
    public static void updateBidvVisitNo(List<Task> taskList) {
        String quarterRcId;
        String monthRcId;
        String yearRcId;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'KPI_Value__c'];
        
        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Quarter')) {
                quarterRcId = r.Id;
            }

            if(rName.equals('Month')) {
                monthRcId = r.Id;
            }

            if(rName.equals('Year')) {
                yearRcId = r.Id;
            }
        }

        String visit = 'Visit';
        String meetingAtBsl = 'Meeting at BSL';
        Decimal thisYear = Decimal.valueOf(String.valueOf(Date.today().year()));
        String q1 = 'Q1';
        String q2 = 'Q2';
        String q3 = 'Q3';
        String q4 = 'Q4';
    
        // List of All User Id with Task
        List<String> allUserId = new List<String>();
        // List of KPI Id
        List<String> kpiIdList = new List<String>();
        // List of KPI Value that need to be updated
        List<KPI_Value__c> updatedKpiValueList = new List<KPI_Value__c>();
        // List of Cross-self Referral Id
        List<String> refIdList = new List<String>();

        for(Task t : taskList) {
            allUserId.add(t.OwnerId);
        }

        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
                        LIMIT 1];
                
        String rtId = rt.Id;

        // Cross-self BIDV Referral
        List<Referral__c> csRefList = [SELECT Id, RecordTypeId
                                       FROM Referral__c
                                       WHERE RecordTypeId = :rtId];

        for(Referral__c r : csRefList) {
            refIdList.add(r.Id);
        }

        List<Task> validTaskList = [SELECT Id, Type, OwnerId, ActivityDate, WhatId
                                    FROM Task
                                    WHERE (OwnerId IN :allUserId) AND (Type = :visit OR Type = :meetingAtBsl)];
        
        List<Event> validEventList = [SELECT Id, Type, OwnerId, EndDateTime, WhatId
                                      FROM Event
                                      WHERE (OwnerId IN :allUserId)];

        List<KPI__c> kpiList = [SELECT Id, OwnerId, KPI_Type__c, Year__c
                                FROM KPI__c
                                WHERE OwnerId IN :allUserId];

        for(KPI__c k : kpiList) {
            kpiIdList.add(k.Id);
        }

        List<KPI_Value__c> kpiValueList = [SELECT Id, Months__c, Quarter__c, Year__c, KPI__c, RecordTypeId, Actual_Customer_Visit_No__c
                                           FROM KPI_Value__c
                                           WHERE KPI__c IN :kpiIdList];

        System.debug('KPI List Size: ' + kpiIdList.size());
        System.debug('validTaskList List Size: ' + validTaskList.size());
        System.debug('validEventList List Size: ' + validEventList.size());
        System.debug('kpiValueList List Size: ' + kpiValueList.size());

        
        for(KPI_Value__c kv : kpiValueList) {
            String ownerId;
            Integer yearOfKpi;
            List<Task> taskInKvList = new List<Task>();
            List<Event> eventInKvList = new List<Event>();

            Decimal customerVisit;

            // define ownerId
            for(KPI__c kpi : kpiList) {
                String kpiId = kpi.Id;
                yearOfKpi = Integer.valueOf(kpi.Year__c);

                if(kpiId.equals(kv.KPI__c)) {
                    ownerId = kpi.OwnerId;
                }
            }

            System.debug('Owner Id: ' + ownerId);

            for(Task t : validTaskList) {
                Integer monthOfTask;
                Integer yearOfTask;
                String whatId;

                if(t.ActivityDate == null) {
                    monthOfTask = 0;
                    yearOfTask = 0;
                }
                else {
                    monthOfTask = t.ActivityDate.month();
                    yearOfTask = t.ActivityDate.year();
                }

                System.debug('MONTH: ' + monthOfTask);

                if(t.WhatId == null) {
                    whatId = 'xxx';
                }
                else {
                    whatId = t.WhatId;
                }

                System.debug('Owner Id of Task: ' + t.OwnerId);
                System.debug('What Id checking: ' + whatId);

                if(ownerId.equals(t.OwnerId) && (refIdList.contains(whatId))) {
                    System.debug('Entry If 1');

                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        System.debug('Entry If 2');

                        if(q1.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            System.debug('Entry If 3');

                            if((monthOfTask >= 1) && (monthOfTask <= 3)) {
                                System.debug('Quarter 1 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q2.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 4) && (monthOfTask <= 6)) {
                                System.debug('Quarter 2 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q3.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 7) && (monthOfTask <= 9)) {
                                System.debug('Quarter 3 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q4.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 10) && (monthOfTask <= 12)) {
                                System.debug('Quarter 4 Entry');
                                taskInKvList.add(t);
                            }
                        }
                    }

                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(monthOfTask);

                        if(monthOfOpp.equals(kv.Months__c) && (yearOfKpi == yearOfTask)) {
                            taskInKvList.add(t);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfOpp = String.valueOf(yearOfTask);

                        if(yearOfOpp.equals(kv.Year__c)) {
                            taskInKvList.add(t);
                        }
                    }
                }
            }

            customerVisit = taskInKvList.size();
            System.debug('Task In Kv List Size: ' + taskInKvList.size());
            System.debug('Customer Visit Number at First: ' + customerVisit);

            for(Event e : validEventList) {
                Integer monthOfEvent;
                Integer yearOfEvent;
                String whatId;

                if(e.EndDateTime == null) {
                    monthOfEvent = 0;
                    yearOfEvent = 0;
                }
                else {
                    monthOfEvent = e.EndDateTime.month();
                    yearOfEvent = e.EndDateTime.year();
                }

                if(e.WhatId == null) {
                    whatId = 'xxx';
                }
                else {
                    whatId = e.WhatId;
                }

                if(ownerId.equals(e.OwnerId) && ((refIdList.contains(whatId)))) {
                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        if(q1.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 1) && (monthOfEvent <= 3)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q2.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 4) && (monthOfEvent <= 6)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q3.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 7) && (monthOfEvent <= 9)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q4.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 10) && (monthOfEvent <= 12)) {
                                eventInKvList.add(e);
                            }
                        }
                    }

                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(monthOfEvent);

                        if(monthOfOpp.equals(kv.Months__c) && (yearOfKpi == yearOfEvent)) {
                            eventInKvList.add(e);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfOpp = String.valueOf(yearOfEvent);

                        if(yearOfOpp.equals(kv.Year__c)) {
                            eventInKvList.add(e);
                        }
                    }
                }
            }

            customerVisit += eventInKvList.size();
            System.debug('Last Customer Visit Number: ' + customerVisit);

            kv.Actual_BIDV_Visit_No__c = customerVisit;
            updatedKpiValueList.add(kv);
        }
        

        update updatedKpiValueList;
    }

    /**
     * 
     *
     */
    public static void updateSupplierVisitNo(List<Task> taskList) {
        String quarterRcId;
        String monthRcId;
        String yearRcId;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'KPI_Value__c'];
        
        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Quarter')) {
                quarterRcId = r.Id;
            }

            if(rName.equals('Month')) {
                monthRcId = r.Id;
            }

            if(rName.equals('Year')) {
                yearRcId = r.Id;
            }
        }

        String visit = 'Visit';
        String meetingAtBsl = 'Meeting at BSL';
        Decimal thisYear = Decimal.valueOf(String.valueOf(Date.today().year()));
        String q1 = 'Q1';
        String q2 = 'Q2';
        String q3 = 'Q3';
        String q4 = 'Q4';
    
        // List of All User Id with Task
        List<String> allUserId = new List<String>();
        // List of KPI Id
        List<String> kpiIdList = new List<String>();
        // List of KPI Value that need to be updated
        List<KPI_Value__c> updatedKpiValueList = new List<KPI_Value__c>();

        for(Task t : taskList) {
            allUserId.add(t.OwnerId);
        }

        List<Task> validTaskList = [SELECT Id, Type, OwnerId, ActivityDate, WhatId
                                    FROM Task
                                    WHERE (OwnerId IN :allUserId) AND (Type = :visit OR Type = :meetingAtBsl)];
        
        List<Event> validEventList = [SELECT Id, Type, OwnerId, EndDateTime, WhatId
                                      FROM Event
                                      WHERE (OwnerId IN :allUserId)];

        List<KPI__c> kpiList = [SELECT Id, OwnerId, KPI_Type__c, Year__c
                                FROM KPI__c
                                WHERE OwnerId IN :allUserId];

        for(KPI__c k : kpiList) {
            kpiIdList.add(k.Id);
        }

        List<KPI_Value__c> kpiValueList = [SELECT Id, Months__c, Quarter__c, Year__c, KPI__c, RecordTypeId, Actual_Customer_Visit_No__c
                                           FROM KPI_Value__c
                                           WHERE KPI__c IN :kpiIdList];

        System.debug('KPI List Size: ' + kpiIdList.size());
        System.debug('validTaskList List Size: ' + validTaskList.size());
        System.debug('validEventList List Size: ' + validEventList.size());
        System.debug('kpiValueList List Size: ' + kpiValueList.size());

        
        for(KPI_Value__c kv : kpiValueList) {
            String ownerId;
            String kpiType;
            Integer yearOfKpi;
            List<Task> taskInKvList = new List<Task>();
            List<Event> eventInKvList = new List<Event>();

            Decimal customerVisit;

            // define ownerId
            for(KPI__c kpi : kpiList) {
                String kpiId = kpi.Id;
                yearOfKpi = Integer.valueOf(kpi.Year__c);

                if(kpiId.equals(kv.KPI__c)) {
                    ownerId = kpi.OwnerId;
                    kpiType = kpi.KPI_Type__c;
                }
            }

            System.debug('Owner Id: ' + ownerId);

            for(Task t : validTaskList) {
                Integer monthOfTask;
                Integer yearOfTask;
                String whatId;

                if(t.ActivityDate == null) {
                    monthOfTask = 0;
                    yearOfTask = 0;
                }
                else {
                    monthOfTask = t.ActivityDate.month();
                    yearOfTask = t.ActivityDate.year();
                }

                System.debug('MONTH: ' + monthOfTask);

                if(t.WhatId == null) {
                    whatId = 'xxx';
                }
                else {
                    whatId = t.WhatId;
                }

                System.debug('Owner Id of Task: ' + t.OwnerId);
                System.debug('What Id checking: ' + whatId);

                if(ownerId.equals(t.OwnerId) && whatId.contains('a05')) {
                    System.debug('Entry If 1');

                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        System.debug('Entry If 2');

                        if(q1.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            System.debug('Entry If 3');

                            if((monthOfTask >= 1) && (monthOfTask <= 3)) {
                                System.debug('Quarter 1 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q2.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 4) && (monthOfTask <= 6)) {
                                System.debug('Quarter 2 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q3.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 7) && (monthOfTask <= 9)) {
                                System.debug('Quarter 3 Entry');
                                taskInKvList.add(t);
                            }
                        }
                        else if(q4.equals(kv.Quarter__c) && (yearOfKpi == yearOfTask)) {
                            if((monthOfTask >= 10) && (monthOfTask <= 12)) {
                                System.debug('Quarter 4 Entry');
                                taskInKvList.add(t);
                            }
                        }
                    }
                    
                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(monthOfTask);

                        if(monthOfOpp.equals(kv.Months__c) && (yearOfKpi == yearOfTask)) {
                            taskInKvList.add(t);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfOpp = String.valueOf(yearOfTask);

                        if(yearOfOpp.equals(kv.Months__c)) {
                            taskInKvList.add(t);
                        }
                    }
                }
            }

            customerVisit = taskInKvList.size();
            System.debug('Task In Kv List Size: ' + taskInKvList.size());
            System.debug('Customer Visit Number at First: ' + customerVisit);

            for(Event e : validEventList) {
                Integer monthOfEvent;
                Integer yearOfEvent;
                String whatId;

                if(e.EndDateTime == null) {
                    monthOfEvent = 0;
                    yearOfEvent = 0;
                }
                else {
                    monthOfEvent = e.EndDateTime.month();
                    yearOfEvent = e.EndDateTime.year();
                }

                if(e.WhatId == null) {
                    whatId = 'xxx';
                }
                else {
                    whatId = e.WhatId;
                }

                if(ownerId.equals(e.OwnerId) && whatId.contains('a05')) {
                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        if(q1.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 1) && (monthOfEvent <= 3)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q2.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 4) && (monthOfEvent <= 6)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q3.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 7) && (monthOfEvent <= 9)) {
                                eventInKvList.add(e);
                            }
                        }
                        else if(q4.equals(kv.Quarter__c) && (yearOfKpi == yearOfEvent)) {
                            if((monthOfEvent >= 10) && (monthOfEvent <= 12)) {
                                eventInKvList.add(e);
                            }
                        }
                    }
                    
                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(monthOfEvent);

                        if(monthOfOpp.equals(kv.Months__c) && (yearOfKpi == yearOfEvent)) {
                            eventInKvList.add(e);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfOpp = String.valueOf(yearOfEvent);

                        if(yearOfOpp.equals(kv.Year__c)) {
                            eventInKvList.add(e);
                        }
                    }
                }
            }

            customerVisit += eventInKvList.size();
            System.debug('Last Customer Visit Number: ' + customerVisit);

            kv.Actual_Supplier_Visit_No__c = customerVisit;
            updatedKpiValueList.add(kv);
        }
        

        update updatedKpiValueList;
    }

    /**
    * createNotificationForSalesman method
    * create new notification for Salesman that notify him (or her) that Director has checked the Opportunity
    * 
    * @param: List<Task> taskList
    */
    public static void createNotificationForSalesman(List<Task> taskList) {
        String vApproval = 'Verify Approval';
        String cmp = 'Completed';
        // List of Verify Approval Task
        List<Task> checkedTaskList = new List<Task>();

        for(Task t : taskList) {
            if(vApproval.equals(t.Subject) && cmp.equals(t.Status)) {
                checkedTaskList.add(t); 
            }
        }

        for(Task t : checkedTaskList) {
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            mentionSegmentInput.id = t.CreatedById;
            messageBodyInput.messageSegments.add(mentionSegmentInput);

            textSegmentInput.text = 'Done!';
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = t.WhatId;

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }
    }

    /**
    * updateBranchValueWhenCreatingTask method
    * when Task is created => update BSL Branch in Task
    *
    * @param: List<Task> taskList
    */
    public static void updateBranchValueWhenCreatingTask(List<Task> taskList) {
        // Branch in BSL
        String hnBr = 'Hanoi Branch';
        String dnBr = 'Da Nang Branch';
        String hcmBr = 'Ho Chi Minh Branch';

        // List of Owner Id
        List<String> ownerIdList = new List<String>();

        for(Task tsk : taskList) {
            ownerIdList.add(tsk.OwnerId);
        }

        List<User> uList = [SELECT Id, Division
                            FROM User
                            WHERE Id IN :ownerIdList];

        for(Task t : taskList) {
            String ownerId = t.OwnerId;

            for(User u : uList) {
                if(ownerId.equals(u.Id)) {
                    if(hnBr.equals(u.Division)) {
                        t.BSL_Branch__c = 'Hanoi';
                    }

                    if(dnBr.equals(u.Division)) {
                        t.BSL_Branch__c = 'Danang';
                    }

                    if(hcmBr.equals(u.Division)) {
                        t.BSL_Branch__c = 'HCM';
                    }
                }
            }
        }

        
    }
}