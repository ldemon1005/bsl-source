@isTest(SeeAllData=true)
public class TestAllClassReportPhase2 {
	
	@isTest static void testInitReportDisbursementController() {
        ReportDisbursementController r = new ReportDisbursementController();
    }
    @isTest static void testInitOperatingLease() {
        List<Quote> listQuote = [Select Id,Name, Lease_Term__c,Leaseable_Asset_Value__c,Repayment_Date__c,ExpirationDate,Frequency_of_Interest_Repayment__c,Expected_Contract_Amount__c,Down_Payment_Rate__c,Lease_Term_for_Insurance__c,Security_Deposit_rate_Amount__c,
                                Upfront_Fee_Amount__c,Closing_Price_Amount__c,Fixed_Rate_In_The_First_Period__c,Expected_Disbursement_Date__c,Margin__c,
                				Interest_Base__c,Number_Of_Months_For_Fixed_Rate__c,Residual_Value__c,Total_Asset_Value_without_VAT__c,
                				Frequency_of_Repayment_month__c,Contract_Term__c,VAT_Rate__c
                                 ,IRR__c 
                                 ,Monthly_Repayment_Amount__c
                                 ,Waiting_period__c
                    			,Security_Deposit_Amount_after_VAT__c
                                 ,Susidy_from_Supplier__c
                                , Other_Expenses_Born_by_BSL__c
                    			, Commission_to_Supplier__c
                    			, Total_Asset_Value_with_VAT__c
                    			, Upfront_Fee_Rate__c
                    			, One_time_Repayment_of_VAT__c
                                 ,Grace_for__c
                                 ,Payment_Method_for_Grace__c
                                 ,Grace_Period__c
                                 ,Residual_Value_Risk_born_by_BSL__c
                                 FROM QUOTE Limit 150];
        for(Integer i =0;i<listQuote.size();i++){
            ApexPages.StandardController sc = new ApexPages.StandardController(listQuote.get(i));
        	OperatingLeaseController r = new OperatingLeaseController(sc);
        }
        ApexPages.StandardController sc = new ApexPages.StandardController(listQuote.get(0));
       	OperatingLeaseController r = new OperatingLeaseController(sc);
        r.styleTable = 's';
    }
    @isTest static void testQuotePdfController() {
        List<Quote> listQuote = [Select Id,Name, Lease_Term__c,Leaseable_Asset_Value__c,Repayment_Date__c,ExpirationDate,Frequency_of_Interest_Repayment__c,Expected_Contract_Amount__c,Down_Payment_Rate__c,Lease_Term_for_Insurance__c,Security_Deposit_rate_Amount__c,
                                Upfront_Fee_Amount__c,Closing_Price_Amount__c,Fixed_Rate_In_The_First_Period__c,Expected_Disbursement_Date__c,Margin__c,
                				Interest_Base__c,Number_Of_Months_For_Fixed_Rate__c,Residual_Value__c,Total_Asset_Value_without_VAT__c,
                				Frequency_of_Repayment_month__c,Contract_Term__c,VAT_Rate__c 
                                 ,IRR__c 
                                 ,Monthly_Repayment_Amount__c
                                 ,Waiting_period__c
                    			,Security_Deposit_Amount_after_VAT__c
                                , Other_Expenses_Born_by_BSL__c
                                 ,Susidy_from_Supplier__c
                    			, Commission_to_Supplier__c
                    			, Total_Asset_Value_with_VAT__c
                    			, Upfront_Fee_Rate__c
                    			, One_time_Repayment_of_VAT__c
                                 ,Grace_for__c
                                 ,Payment_Method_for_Grace__c
                                 ,Grace_Period__c
                                 ,Residual_Value_Risk_born_by_BSL__c
                                 FROM QUOTE Limit 150];
        // for(Integer i =0;i<listQuote.size();i++){
        //     ApexPages.StandardController sc = new ApexPages.StandardController(listQuote.get(i));
        // 	QuotePdfController r = new QuotePdfController(sc);
        // }
        ApexPages.StandardController sc = new ApexPages.StandardController(listQuote.get(0));
       	QuotePdfController r = new QuotePdfController(sc);
        r.changeStyle();
        r.styleTable = 's';
        r.changeStyle();
        r.renderingAsHtml();
    }
     @isTest static void tesi9Rate() {
        List<Quote> listQuote = [Select Id,Name, Lease_Term__c,Leaseable_Asset_Value__c,Repayment_Date__c,ExpirationDate,Frequency_of_Interest_Repayment__c,Expected_Contract_Amount__c,Down_Payment_Rate__c,Lease_Term_for_Insurance__c,Security_Deposit_rate_Amount__c,
                                Upfront_Fee_Amount__c,Closing_Price_Amount__c,Fixed_Rate_In_The_First_Period__c,Expected_Disbursement_Date__c,Margin__c,
                				Interest_Base__c,Number_Of_Months_For_Fixed_Rate__c,Residual_Value__c,Total_Asset_Value_without_VAT__c,
                				Frequency_of_Repayment_month__c,Contract_Term__c,VAT_Rate__c 
                                 ,IRR__c 
                                 ,Monthly_Repayment_Amount__c
                                 ,Waiting_period__c
                    			,Security_Deposit_Amount_after_VAT__c
                                 ,Susidy_from_Supplier__c
                                , Other_Expenses_Born_by_BSL__c
                    			, Commission_to_Supplier__c
                    			, Total_Asset_Value_with_VAT__c
                    			, Upfront_Fee_Rate__c
                    			, One_time_Repayment_of_VAT__c
                                 ,Grace_for__c
                                 ,Payment_Method_for_Grace__c
                                 ,Grace_Period__c
                                 ,Residual_Value_Risk_born_by_BSL__c
                                 FROM QUOTE Limit 150];
        for(Integer i =0;i<listQuote.size();i++){
            ApexPages.StandardController sc = new ApexPages.StandardController(listQuote.get(i));
        	i9AssetRateController r = new i9AssetRateController(sc);
            
        }
    }


}