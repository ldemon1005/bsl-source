public without sharing class CaseTriggerHandler {
    /**
    * setNotificationWhenCreatingNewCase method
    * Set Notification for user when creating new case
    *
    * @param: List<Case> caseList
    */
    public static void setNotificationWhenCreatingNewCase(List<Case> caseList) {
        // List of Salesman Dev Name
        List<String> salesmanDevNameList = new List<String>{'DanangBusiness1PartnerUser','HanoiBusiness1PartnerUser','HoChiMinhBusiness1PartnerUser'};
        // List of User Role Id
        List<String> srIdList = new List<String>();
        // List of Salesman User Role
        List<UserRole> srList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :salesmanDevNameList];

        for(UserRole ur : srList) {
            srIdList.add(ur.Id);
        }

        // List of Salesman User
        List<User> smList = [SELECT Id, Division
                             FROM User
                             WHERE UserRoleId IN :srIdList];

        // List of Salesman user Id
        List<String> smIdList = new List<String>();

        for(User u : smList) {
            smIdList.add(u.Id);
        }

        System.debug('Checked User List Size: ' + smList.size());

        // List of User Role Dev Name
        List<String> urDevNameList = new List<String>{'BSLPartnerManager',
                                                      'BSLPartnerExecutive',
                                                      'BSLPartnerUser',
                                                      'DanangBusiness1PartnerManager',
                                                      'HanoiBusiness1PartnerManager',
                                                      'HoChiMinhBusiness1PartnerManager',
                                                      'Deputy_Director_of_Hanoi_Branch',
                                                      'Deputy_Director_of_Ho_Chi_Minh_Branch',
                                                      'Director_of_Danang_Branch',
                                                      'Director_of_Hanoi_Branch',
                                                      'Director_of_Ho_Chi_Minh_Branch',
                                                      'Manager_of_Danang_Business_1',
                                                      'Manager_of_Danang_Business_2',
                                                      'Manager_of_Hanoi_Business_1',
                                                      'Manager_of_Hanoi_Business_2',
                                                      'Manager_of_Ho_Chi_Minh_Business_1',
                                                      'Manager_of_Ho_Chi_Minh_Business_2'};
        // List of User Role Id
        List<String> urIdList = new List<String>();
        // List of User Role
        List<UserRole> urList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :urDevNameList];

        for(UserRole ur : urList) {
            urIdList.add(ur.Id);
        }

        // List of Director User
        List<User> uList = [SELECT Id, Division 
                            FROM User
                            WHERE UserRoleId IN :urIdList];

        // List of Director User Id
        List<String> uIdList = new List<String>();

        for(User u : uList) {
            uIdList.add(u.Id);
        }

        System.debug('Checked User List Size: ' + uList.size());

        for(Case c : caseList) {
            String hoDivision = 'HO';
            String ownerId = c.OwnerId;
            List<User> refUserList = new List<User>();

            // if Owner of Case is salesman
            if(smIdList.contains(ownerId)) {
                String smBranch;

                // get branch name of Salesman
                for(User smu : smList) {
                    if(ownerId.equals(smu.Id)) {
                        smBranch = smu.Division;
                    }
                }

                for(User du : uList) {
                    if((smBranch.equals(du.Division)) || (hoDivision.equals(du.Division))) {
                        refUserList.add(du);
                    }
                }

                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                for(User u : refUserList) {
                    System.debug('IN LOOP 2222222222');
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput.id = u.Id;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                }

                textSegmentInput.text = ' Check out the new Case';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = c.Id;
                feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;

                System.debug('IN LOOP 111111111');

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
            }

            // if Owner of Case is HO
            if(uIdList.contains(ownerId)) {
                String uBranch;
                String oppBranch;

                if(c.Opportunity_Name__c != null) {
                    oppBranch = c.Branch_of_Opportunity__c;
                }

                // update uBranch
                if(oppBranch.equals('Danang')) {
                    uBranch = 'Da Nang Branch';
                }
                else if(oppBranch.equals('Hanoi')) {
                    uBranch = 'Hanoi Branch';
                }
                else if(oppBranch.equals('HCM')) {
                    uBranch = 'Ho Chi Minh Branch';
                }
                else {
                    uBranch = ' ';
                }   

                // get branch name of Salesman
                for(User u : uList) {
                    if(uBranch.equals(u.Division)) {
                        refUserList.add(u);
                    }
                }

                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                for(User u : refUserList) {
                    System.debug('IN LOOP 2222222222');
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput.id = u.Id;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                }

                textSegmentInput.text = ' Check out the new Case';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = c.Id;
                feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;

                System.debug('IN LOOP 111111111');

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
            }
        }
    }
}