public class OperatingLeaseController {
	public String styleTable{get;set;}
    public String irrString{get;set;}
    public Double irrDouble{get;set;}
    public Decimal irrDec;
    public Boolean isUpdateQuote = false;
    public Quote quote{get;set;}
    public List<Asset__c> listAsset {get;set;}
    public List<Repayment> listRepay {get;set;}
    public  String todayString { get; private set; }

    public String renderingService { get; private set; }
    public OperatingLeaseController(ApexPages.StandardController controller) {
        try{
            styleTable = 'floatingStyle';
            if (!Test.isRunningTest()){
                controller.addFields(new List<String>{'Id','Name', 'Lease_Term__c','Leaseable_Asset_Value__c','Repayment_Date__c','ExpirationDate','Frequency_of_Interest_Repayment__c',
                                'Assets__r','Expected_Contract_Amount__c','Down_Payment_Rate__c','Lease_Term_for_Insurance__c','Security_Deposit_rate_Amount__c',
                                'Upfront_Fee_Amount__c','Closing_Price_Amount__c','Fixed_Rate_In_The_First_Period__c','Expected_Disbursement_Date__c','Margin__c',
                				'Interest_Base__c','Number_Of_Months_For_Fixed_Rate__c','Residual_Value__c','Total_Asset_Value_without_VAT__c',
                				'Frequency_of_Repayment_month__c','Contract_Term__c','VAT_Rate__c','IRR__c'
                    ,'Monthly_Repayment_Amount__c'
                    ,'Waiting_period__c'
                    ,'Residual_Value__c'
                    ,'Security_Deposit_Amount_after_VAT__c'
                    ,'Residual_Value_Risk_born_by_BSL__c'
                    ,'Account.Name'
                    });
            }
            quote = (Quote)controller.getRecord();
            Decimal numberOfRepayment = 36;
            Integer interestRepayment = 1;
            if(quote.Frequency_of_Repayment_month__c !=null & quote.Contract_Term__c !=null){
                interestRepayment = integer.valueof(quote.Frequency_of_Repayment_month__c);
                numberOfRepayment = quote.Contract_Term__c;
                numberOfRepayment = numberOfRepayment / interestRepayment;
            }
            Decimal preValueDebt =0;
            if(quote.Expected_Contract_Amount__c !=null){
                preValueDebt = quote.Expected_Contract_Amount__c;
            }
            Repayment r0 = new Repayment(quote);
            r0.RepaymentTerm = 0;
            r0.ResidualValue = !quote.Residual_Value_Risk_born_by_BSL__c ? quote.Residual_Value__c : 0;
            r0.OutstandingPrincipal = -quote.Total_Asset_Value_without_VAT__c;
            r0.SecurityDeposit = quote.Security_Deposit_Amount_after_VAT__c == null ? 0 : quote.Security_Deposit_Amount_after_VAT__c;
            r0.UpfrontOrClosingFee = quote.Upfront_Fee_Amount__c;
            r0.Cashflow = (r0.OutstandingPrincipal == null ? 0:r0.OutstandingPrincipal) + ( r0.SecurityDeposit == null ? 0: r0.SecurityDeposit) 
                            + ( r0.UpfrontOrClosingFee == null ? 0: r0.UpfrontOrClosingFee) ;
            listRepay = new List<Repayment> ();
            listRepay.add(r0);
            Repayment rN = new Repayment(quote, true);
            List<Double> cashflows = new List<Double>();
            cashflows.add(r0.Cashflow);
            double LeaseRentMonthlyInDouble = IRR.caculatePMT(((quote.Margin__c == null ? 0:quote.Margin__c)+
                                                       (quote.Interest_Base__c == null ? 0:quote.Interest_Base__c))/100* interestRepayment
                                                      ,numberOfRepayment,quote.Leaseable_Asset_Value__c);
            System.debug('1');
            System.debug(LeaseRentMonthlyInDouble);
            //long LeaseRentMonthly = (LeaseRentMonthlyInDouble/1000).Round() * 1000;            
            long LeaseRentMonthly = (LeaseRentMonthlyInDouble).Round();         
            System.debug(LeaseRentMonthly);
            decimal interestCost = quote.Waiting_period__c != null && quote.Waiting_period__c != 0 
                ? quote.Total_Asset_Value_without_VAT__c * ((quote.Margin__c + quote.Interest_Base__c)/100)/12*quote.Waiting_period__c
                : 0;
            decimal interestCostPerFrequency = interestCost * interestRepayment;
            r0.InterestCostForWaitingPeriod = interestCost;
            for(Decimal i =0; i<numberOfRepayment;i++){
                Repayment r = new Repayment(quote);
                r.RepaymentTerm = integer.valueof(i)+1;
                double interestCostForWaitingPeriod = interestCostPerFrequency / ((decimal)numberOfRepayment 
                                                                                         * integer.valueOf(quote.Frequency_of_Repayment_month__c));
                r.caculateOperatingLease(integer.valueof(i), LeaseRentMonthly, interestCostForWaitingPeriod);                
                preValueDebt = r.TotalOutstandingDebt;
                if(i == (numberOfRepayment -1 )){
                    r.UpfrontOrClosingFee = quote.Closing_Price_Amount__c;
                }  
                listRepay.add(r);
                cashflows.add(r.Cashflow ==null?0:r.Cashflow);
            }
            List<decimal> newCashflows = new List<decimal>();
            for(integer i = 0; i < listRepay.size(); i++) {
                system.debug('listRepay.get(i).Cashflow ' + string.valueOf(i));
                system.debug(listRepay.get(i).Cashflow);
                system.debug(cashflows.get(i));
                if (listRepay.get(i).Cashflow == null)
                	newCashflows.add((decimal)0);
                else 
                    newCashflows.add(listRepay.get(i).Cashflow);
            }
            System.debug('calculating IRR');
            Double guess= 0.1;
            irrString = IRR.caculateIRR(cashflows,guess);
            System.debug('end calculating IRR');
            Double irrNumber = Double.valueOf(irrString);
            irrDouble = (Math.pow((1+irrNumber),(12 /interestRepayment)) -1) *100;
            Decimal irrDec = irrDouble;
            irrDec= irrDec.setScale(2);
            irrString = irrDec+'%';
            if(quote.Irr__c != irrDec){
                 quote.Irr__c = irrDec;
                quote.Monthly_repayment_amount__c = listRepay.get(1).TotalMonthly;
                isUpdateQuote = true;
        	}
            Date today = System.Today();
            todayString = today.day() + '/' + today.month() + '/' + today.year();
            
        }catch(Exception e){
                System.debug(e.getMessage());
        }
    }
    
    public void updateIRR(){
         if(isUpdateQuote){
             System.debug('quote.Irr__c');
             System.debug(quote.Irr__c);
             update quote;
    
         }
    }
    
}