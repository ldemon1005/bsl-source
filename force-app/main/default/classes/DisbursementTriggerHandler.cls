/**
* @author tqtai 
*/
public without sharing class DisbursementTriggerHandler {

    public static List<Disbursement__c> getPlanDisbursementList(List<Disbursement__c> dbtList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Plan'
                        LIMIT 1];
                
        String rtId = rt.Id;

        List<Disbursement__c> actualDbtList = new List<Disbursement__c>();

        for(Disbursement__c db : dbtList) {
            if(rtId.equals(db.RecordTypeId)) {
                actualDbtList.add(db);
            }
        }

        return actualDbtList;
    }

    /**
    * updatePlanDisbursementDate method
    * update Plan Disbursement Date for Plan Disbursement
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void updatePlanDisbursementDate(List<Disbursement__c> dbmList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Plan'
                        LIMIT 1];
                
        String rtId = rt.Id;

        for(Disbursement__c d : dbmList) {
            if(rtId.equals(d.RecordTypeId)) {
                d.Plan_Disbursement_Date__c = d.Disbursement_Date__c;
            }
        }
    }

    /**
    * getDayOfWeekOfDisbursementDate method
    * Get the Day of Week of Disbursement Date
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static String getDayOfWeekOfDisbursementDate(Disbursement__c dbm) {
        String dayOfWeek;

        if(dbm.Plan_Disbursement_Date__c != null) {
            Date dbmDate = dbm.Plan_Disbursement_Date__c;
            Datetime dbmDateTime = Datetime.newInstance(dbmDate.year(), dbmDate.month(), dbmDate.day());
            dayOfWeek = dbmDateTime.format('E');
        }
        else {
            return 'x';
        }

        return dayOfWeek;
    }

    /**
    * createNewActualDisbursementCorresponding method
    * when create new Plan Disbursement => new Actual Disbursement is created
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void createNewActualDisbursementCorresponding(List<Disbursement__c> dbmList) {
        String planDbmRt;
        String actualDbmRt;

        List<RecordType> rtList = [SELECT Id, Name
                                   FROM RecordType
                                   WHERE SobjectType = 'Disbursement__c'];
                
        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Plan')) {
                planDbmRt = r.Id;
            }

            if(rName.equals('Actual')) {
                actualDbmRt = r.Id;
            }
        }

        // List of Plan Disbursement
        List<Disbursement__c> planDbmList = getPlanDisbursementList(dbmList);
        // List of New Actual Disbursement
        List<Disbursement__c> newActualDbmList = new List<Disbursement__c>();

        for(Disbursement__c d : dbmList) {
            if(planDbmRt.equals(d.RecordTypeId)) {
                // check day in week of Plan Disbursement
                String dayOfWeek;
                Date endOfWeek;

                if(d.Disbursement_Date__c != null) {
                    Date dbmDate = d.Plan_Disbursement_Date__c;
                    Datetime dbmDateTime = Datetime.newInstance(dbmDate.year(), dbmDate.month(), dbmDate.day());
                    dayOfWeek = dbmDateTime.format('E');

                    System.debug('Disbursement Date checking: ' + dbmDate);
                }
                else {
                    dayOfWeek = 'x';
                }
                
                if(dayOfWeek.equals('Mon')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(7);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }
                }

                if(dayOfWeek.equals('Tue')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(6);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }    
                }
                
                if(dayOfWeek.equals('Wed')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(5);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }
                }

                if(dayOfWeek.equals('Thu')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(4);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }
                }

                if(dayOfWeek.equals('Fri')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(3);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }
                }

                if(dayOfWeek.equals('Sat')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(2);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }
                }

                if(dayOfWeek.equals('Sun')) {
                    if(d.Plan_Disbursement_Date__c != null) {
                        endOfWeek = d.Plan_Disbursement_Date__c.addDays(1);
                    }
                    else {
                        endOfWeek = d.Disbursement_Date__c.addDays(7);
                    }
                }


                Disbursement__c newDbm = new Disbursement__c();

                newDbm.RecordTypeId = actualDbmRt;

                if(d.Amount__c != null) {
                    newDbm.Amount__c = d.Amount__c;
                }
                
                if(dayOfWeek.equals('Sat') || dayOfWeek.equals('Sun')) {
                    d.addError('Disbursement Date cannot be on Saturday or Sunday!');
                }
                else {
                    newDbm.Disbursement_Date__c = d.Disbursement_Date__c;
                    newDbm.Plan_Disbursement_Date__c = d.Disbursement_Date__c;
                }

                if(d.Opportunity__c != null) {
                    newDbm.Opportunity__c = d.Opportunity__c;
                }

                newDbm.Status__c = 'Pending';
                newDbm.Actual_Deadline__c = endOfWeek;

                newActualDbmList.add(newDbm);
            }
        }

        System.debug('Size of NewActualDbm List: ' + newActualDbmList.size());
        insert newActualDbmList;
    }

    /**
    * updateDaysInWeek method
    * When Salesman update Actual Disbursement Plan, the Deadline of the updating is next Monday
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void updateDaysInWeek(List<Disbursement__c> dbmList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
                
        String rtId = rt.Id;

        for(Disbursement__c d : dbmList) {
            if(rtId.equals(d.RecordTypeId)) {
                if(d.Actual_Deadline__c != null) {
                    Date dl = d.Actual_Deadline__c;

                    if(Date.today() > dl) {
                        d.addError('The Date to update is expired!');
                    }
                }
            }
        }      
    }

    /**
    * fillOutNoteOfDbm method
    * When Actual Disbursement has Status Cancelled or Partially Done => Salesman need to fill out the note 
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void fillOutNoteOfDbm(List<Disbursement__c> dbmList) {
        for(Disbursement__c d : dbmList) {
            String sttDbm = d.Status__c;

            if((sttDbm.equals('Partially Done') || sttDbm.equals('Cancelled')) && (d.Note__c == null)) {
                d.addError('Please fill out the Note!');
            }
        }
    }

    /**
    * blockUpdatingDisbursementOfPreviousMonth method
    * Cannot update Disbursement Record of Previous Month 
    *
    * @param: List<Disbursement__c> dbmList
    */

    // public static void blockUpdatingDisbursementOfPreviousMonth(List<Disbursement__c> dbmList) {
    //     // This month
    //     Integer tMonth = Date.today().month();

    //     for(Disbursement__c d : dbmList) {
    //         Date dbmDate;

    //         if(d.Disbursement_Date__c != null) {
    //             dbmDate = d.Disbursement_Date__c;
    //         }

    //         Integer dbmMonth = dbmDate.month();

    //         if(dbmMonth < tMonth) {
    //             d.addError('Cannot update Disbursement Record of Previous Month!');
    //         }
    //     }
    // }
    // 
    //
    public static void blockDeletingActualDbmWithRecordType(List<Disbursement__c> dbmList, RecordType rt) {       
                
        String rtId = rt.Id;

        // Opp Id List 
        List<String> oppIdList = new List<String>();

        for(Disbursement__c dbm : dbmList) {
            if(dbm.Opportunity__c != null) {
                oppIdList.add(dbm.Opportunity__c);
            }
            else {
                oppIdList.add('x');
            }
        }

        List<Opportunity> oppList = [SELECT Id
                                     FROM Opportunity
                                     WHERE (StageName = 'A') AND (Id IN :oppIdList)];

        for(Disbursement__c d : dbmList) {
            String oppId;
            if(d.Opportunity__c != null) {
                oppId = d.Opportunity__c;
            }
            else {
                oppId = 'c';
            }
            
            String dStt = 'Done';

            if(dStt.equals(d.Status__c) && rtId.equals(d.RecordTypeId)) {
                for(Opportunity o : oppList) {
                    if(oppId.equals(o.Id)) {
                        d.addError('Cannot delete Actual Disbursement When StageName of Opp is A');
                    }
                }
            }
        }
    }
    /**
    * blockDeletingActualDbm method
    * When Stagename of Opp is A => cannot delete Actual Dbm
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void blockDeletingActualDbm(List<Disbursement__c> dbmList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
        
        DisbursementTriggerHandler.blockDeletingActualDbmWithRecordType(dbmList, rt);
    }
    
    public static void blockCreatingActualDbmWithRecordType(List<Disbursement__c> dbmList, RecordType rt) {
                
        String rtId = rt.Id;

        // Opp Id List 
        List<String> oppIdList = new List<String>();

        for(Disbursement__c dbm : dbmList) {
            oppIdList.add(dbm.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id
                                     FROM Opportunity
                                     WHERE (StageName = 'L') AND (Id IN :oppIdList)];

        for(Disbursement__c d : dbmList) {
            String oppId = d.Opportunity__c;
            String dStt = 'Done';

            if(dStt.equals(d.Status__c) && rtId.equals(d.RecordTypeId)) {
                for(Opportunity o : oppList) {
                    if(oppId.equals(o.Id)) {
                        d.addError('Cannot create Actual "Done" Disbursement When StageName of Opp is L');
                    }
                }
            }
        }
    }
    

    /**
    * blockCreatingActualDbm method
    * when StageName of Opp is L => cannot create (or update) Actual "Done" Disbursement
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void blockCreatingActualDbm(List<Disbursement__c> dbmList) {
        
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
        DisbursementTriggerHandler.blockCreatingActualDbmWithRecordType(dbmList, rt);
    }
    
    public static void setTotalDisbursedAmountInOpportunityWithRecordType(List<Disbursement__c> dbmList, RecordType rt) {
                
        String rtId = rt.Id;

        // Done Status of Disbursement
        String dn = 'Done';
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Disbursement__c d : dbmList) {
            oppIdList.add(d.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Total_Disbursed_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        System.debug('Size of oppList: ' + oppList.size());

        List<Disbursement__c> checkedDbmList = [SELECT Amount__c, Opportunity__c, Status__c
                                                FROM Disbursement__c
                                                WHERE Opportunity__c IN :oppIdList AND RecordTypeId = :rtId];

        System.debug('Size of checkedDbmList: ' + checkedDbmList.size());

        for(Opportunity opp : oppList) {
            String oppId = opp.Id;
            Decimal ttDbAmount = 0;

            for(Disbursement__c dbm : checkedDbmList) {
                if(oppId.equals(dbm.Opportunity__c) && dn.equals(dbm.Status__c)) {
                    System.debug('IN LOOP OF DISBURSEMENT');
                    if(dbm.Amount__c != null) {
                        ttDbAmount += dbm.Amount__c;
                    }
                }
            }

            System.debug('Ref Amount: ' + ttDbAmount);
            opp.Total_Disbursed_Amount__c = ttDbAmount;
        }

        update oppList;
    }

    /**
    * setTotalDisbursedAmountInOpportunity method
    * Calculating Total Disbursed Amount in Opportunity
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void setTotalDisbursedAmountInOpportunity(List<Disbursement__c> dbmList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
        
        DisbursementTriggerHandler.setTotalDisbursedAmountInOpportunityWithRecordType(dbmList, rt);
    }

    /**
    * setStageNameOfOppToA method
    * when A Actual Disbursement has Status Done => change Stage of Opp to A
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void setStageNameOfOppToA(List<Disbursement__c> dbmList) {
        String ldOppStageName = 'L\'';

        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
                
        String rtId = rt.Id;

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Disbursement__c d : dbmList) {
            oppIdList.add(d.Opportunity__c);
        }

        List<Opportunity> oppList = [SELECT Id, StageName
                                     FROM Opportunity
                                     WHERE (Id IN :oppIdList) AND (NOT(StageName = 'A'))];

        for(Opportunity opp : oppList) {
            String oppId = opp.Id;

            for(Disbursement__c dbm : dbmList) {
                if((rtId.equals(dbm.RecordTypeId)) && (oppId.equals(dbm.Opportunity__c))) {
                    String sttDbm = dbm.Status__c;

                    if(sttDbm.equals('Done') && !(ldOppStageName.equals(opp.StageName))) {
                        opp.StageName = 'A';
                    }
                }
            }
        }

        update oppList;
    }

    /**
    * updateOnlyActivatedContractToDbm method
    * When Actual Disbursement is updated to Done, Salesman 
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void updateOnlyActivatedContractToDbm(List<Disbursement__c> dbmList) {
        // Done Status
        String dn = 'Done';
        // Activated Status
        String atv = 'Activated';
        // List of Opp Id
        List<String> oppIdList = new List<String>();

        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
                
        String rtId = rt.Id;

        for(Disbursement__c d : dbmList) {
            oppIdList.add(d.Opportunity__c);
        }

        List<Contract> ctrList = [SELECT Id, Opportunity_Name__c, Status
                                  FROM Contract
                                  WHERE Opportunity_Name__c IN :oppIdList];

        for(Disbursement__c dbm : dbmList) {
            if(rtId.equals(dbm.RecordTypeId)) {
                String oId;
                String cId;

                if(dbm.Opportunity__c != null) {
                    oId = dbm.Opportunity__c;
                }
                else {
                    oId = 'x';
                }

                if(dbm.Contract__c != null) {
                    cId = dbm.Contract__c;
                }
                else {
                    cId = 'x';
                }

                for(Contract c : ctrList) {
                    if(oId.equals(c.Opportunity_Name__c)) {
                        if(cId.equals(c.Id) && (!atv.equals(c.Status)) && (dn.equals(dbm.Status__c))) {
                            dbm.addError('Cannot add unactivated Contract to ');
                        }
                    }
                }
            }
        }
    }

    /**
    * blockChangingToDoneActualDbm method
    * When this Opp has no Activated Contract => cannot change Actual Dbm to Done
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void blockChangingToDoneActualDbm(List<Disbursement__c> dbmList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
                
        String rtId = rt.Id;

        // Done Status of Disbursement
        String dn = 'Done';
        // Activated Status of Contract
        String atv = 'Activated';

        // List of Opp Id
        List<String> oppIdList = new List<String>();

        for(Disbursement__c d : dbmList) {
            oppIdList.add(d.Opportunity__c);
        }

        List<Contract> ctrList = [SELECT Status, Opportunity_Name__c
                                  FROM Contract
                                  WHERE Opportunity_Name__c IN :oppIdList];

        for(Disbursement__c db : dbmList) {
            if(rtId.equals(db.RecordTypeId)) {
                String oppId;

                if(db.Opportunity__c != null) {
                    oppId = db.Opportunity__c;
                }
                else {
                    oppId = 'x';
                }
                
                List<Contract> refCtrList = new List<Contract>();

                for(Contract c : ctrList) {
                    if(oppId.equals(c.Opportunity_Name__c) && (atv.equals(c.Status))) {
                        refCtrList.add(c);
                    }
                }

                if((refCtrList.size() == 0) && (dn.equals(db.Status__c))) {
                    db.addError('Cannot change to Done because this Opportunity has no Activated Contract');
                }
            }
        }
    }

    /**
    * updateActualDbm method
    * update Actual Disbursement Cross-self Bidv
    *
    * @param: List<Disbursement__c> dbmList
    */
    public static void updateActualDbm(List<Disbursement__c> dbmList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Disbursement__c' AND Name = 'Actual'
                        LIMIT 1];
                
        DisbursementTriggerHandler.updateActualDbmWithRecordType(dbmList, rt);
    }
    
    public static void updateActualDbmWithRecordType(List<Disbursement__c> dbmList, RecordType rt) {
                
        String actualRtId = rt.Id;

        Decimal thisYear = Decimal.valueOf(String.valueOf(Date.today().year()));
        String csBidvRcId;
        String csSmtbRcId;
        String csSmtpfcRcId;
        String bslSelfRcId; 
        String ciRcId;
        String otherRcId;
        String scfRcId;

        List<RecordType> refRtList = [SELECT Id, Name
                                   FROM RecordType
                                   WHERE SobjectType = 'Referral__c'];
                
        for(RecordType r1 : refRtList) {
            String r1Name = r1.Name;

            if(r1Name.equals('Crossell-BIDV')) {
                csBidvRcId = r1.Id;
            }

            if(r1Name.equals('Crossell-SMTB')) {
                csSmtbRcId = r1.Id;
            }

            if(r1Name.equals('Crossell-SMTPFC')) {
                csSmtpfcRcId = r1.Id;
            }

            if(r1Name.equals('BSLselfdev')) {
                bslSelfRcId = r1.Id;
            }

            if(r1Name.equals('Customer-initiate')) {
                ciRcId = r1.Id;
            }

            if(r1Name.equals('Other')) {
                otherRcId = r1.Id;
            }

            if(r1Name.equals('Supply Chain Finance')) {
                scfRcId = r1.Id;
            }
        }

        String quarterRcId;
        String monthRcId;
        String yearRcId;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'KPI_Value__c'];
        
        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Quarter')) {
                quarterRcId = r.Id;
            }

            if(rName.equals('Month')) {
                monthRcId = r.Id;
            }

            if(rName.equals('Year')) {
                yearRcId = r.Id;
            }
        }
        String q1 = 'Q1';
        String q2 = 'Q2';
        String q3 = 'Q3';
        String q4 = 'Q4';
        Date startDateOfQuarter1 = Date.newInstance(thisYear.intValue(), 1, 1);
        Date endDateOfQuarter1 = Date.newInstance(thisYear.intValue(), 3, 31);
        Date startDateOfQuarter2 = Date.newInstance(thisYear.intValue(), 4, 1);
        Date endDateOfQuarter2 = Date.newInstance(thisYear.intValue(), 6, 30);
        Date startDateOfQuarter3 = Date.newInstance(thisYear.intValue(), 7, 1);
        Date endDateOfQuarter3 = Date.newInstance(thisYear.intValue(), 9, 30);
        Date startDateOfQuarter4 = Date.newInstance(thisYear.intValue(), 10, 1);
        Date endDateOfQuarter4 = Date.newInstance(thisYear.intValue(), 12, 31);

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        // List of OwnerId
        List<String> ownerIdList = new List<String>();

        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(Disbursement__c d : dbmList) {
            ownerIdList.add(d.OwnerId);
        }

        List<Disbursement__c> checkedDbmList = [SELECT RecordTypeId, OwnerId, Amount__c, Disbursement_Date__c, Referral_Record_Type_In_Opp__c
                                                FROM Disbursement__c
                                                WHERE OwnerId IN :ownerIdList AND RecordTypeId = :actualRtId AND Status__c = 'Done'];

        String yearInString = String.valueOf(thisYear);

        List<KPI__c> kpiList = [SELECT Id, OwnerId
                                FROM KPI__c
                                WHERE Year__c = :yearInString AND OwnerId IN :ownerIdList];

        for(KPI__c k : kpiList) {
            kpiIdList.add(k.Id);
        }

        List<KPI_Value__c> kpiValueList = [SELECT Id, Months__c, Quarter__c, Year__c, Actual_Total_Disbursed_Cross_sell_BIDV__c, RecordTypeId, KPI__c
                                           FROM KPI_Value__c
                                           WHERE KPI__c IN :kpiIdList];

        for(KPI_Value__c kpi : kpiValueList) {
            String ownerId;
            String quarterOfKpiValue;
            String monthOfKpiValue;
            String yearOfKpiValue;

            if(kpi.Quarter__c != null) {
                System.debug('IIIINNNNNNN QUARTER');
                quarterOfKpiValue = kpi.Quarter__c;
            }
            else {
                quarterOfKpiValue = 'x';
            }
            
            if(kpi.Months__c != null) {
                System.debug('IIINNNNNNN MONTH');
                monthOfKpiValue = kpi.Months__c;
            }
            else {
                monthOfKpiValue = 'x';
            }

            if(kpi.Year__c != null) {
                System.debug('IIINNNNNNN YEAAARRR');
                yearOfKpiValue = kpi.Year__c;
            }
            else {
                yearOfKpiValue = 'x';
            }

            Decimal totalCrossSell = 0;
            Decimal totalCsSmtb = 0;
            Decimal totalCsSmtpfc = 0;
            Decimal totalSelfDv = 0;
            Decimal totalCi = 0;
            Decimal totalOther = 0;
            Decimal totalScf = 0;

            for(KPI__c k : kpiList) {
                String kId = k.Id;

                if(kId.equals(kpi.KPI__c)) {
                    ownerId = k.OwnerId;
                    System.debug('Owner Id: ' + ownerId);
                }
            }
            System.debug('Owner ID: ' + ownerId);

            for(Disbursement__c dbm : checkedDbmList) {
                String rc;

                if(dbm.Referral_Record_Type_In_Opp__c != null) {
                    rc = dbm.Referral_Record_Type_In_Opp__c;
                    System.debug('Referral RecordType with Opp: ' + rc);
                }
                else {
                    rc = 'xxx';
                }
                
                if(ownerId.equals(dbm.OwnerId)) {
                    if(quarterRcId.equals(kpi.RecordTypeId)) {                                          
                        if(q1.equals(quarterOfKpiValue)) {
                            if((dbm.Disbursement_Date__c >= startDateOfQuarter1) && (dbm.Disbursement_Date__c <= endDateOfQuarter1)) {                    
                                if(csBidvRcId.contains(rc)) {
                                    totalCrossSell += dbm.Amount__c;
                                }
                                System.debug('QUARTER 1 IF: ' + totalCrossSell);

                                if(csSmtbRcId.contains(rc)) {
                                    totalCsSmtb += dbm.Amount__c;
                                }

                                if(csSmtpfcRcId.contains(rc)) {
                                    totalCsSmtpfc += dbm.Amount__c;
                                }

                                if(ciRcId.contains(rc)) {
                                    totalCi += dbm.Amount__c;
                                }

                                if(bslSelfRcId.contains(rc)) {
                                    totalSelfDv += dbm.Amount__c;
                                }

                                if(otherRcId.contains(rc)) {
                                    totalOther += dbm.Amount__c;
                                }

                                if(scfRcId.contains(rc)) {
                                    totalScf += dbm.Amount__c;
                                }
                            }
                        }
                        else if(q2.equals(quarterOfKpiValue)) {
                            if((dbm.Disbursement_Date__c >= startDateOfQuarter2) && (dbm.Disbursement_Date__c <= endDateOfQuarter2)) {
                                if(csBidvRcId.contains(rc)) {
                                    totalCrossSell += dbm.Amount__c;
                                }
                                System.debug('QUARTER 2 IF: ' + totalCrossSell);

                                if(csSmtbRcId.contains(rc)) {
                                    totalCsSmtb += dbm.Amount__c;
                                }

                                if(csSmtpfcRcId.contains(rc)) {
                                    totalCsSmtpfc += dbm.Amount__c;
                                }

                                if(ciRcId.contains(rc)) {
                                    totalCi += dbm.Amount__c;
                                }

                                if(bslSelfRcId.contains(rc)) {
                                    totalSelfDv += dbm.Amount__c;
                                }

                                if(otherRcId.contains(rc)) {
                                    totalOther += dbm.Amount__c;
                                }

                                if(scfRcId.contains(rc)) {
                                    totalScf += dbm.Amount__c;
                                }
                            }
                        }
                        else if(q3.equals(quarterOfKpiValue)) {
                            if((dbm.Disbursement_Date__c >= startDateOfQuarter3) && (dbm.Disbursement_Date__c <= endDateOfQuarter3)) {
                                if(csBidvRcId.contains(rc)) {
                                    totalCrossSell += dbm.Amount__c;
                                }
                                System.debug('QUARTER 3 IF: ' + totalCrossSell);

                                if(csSmtbRcId.contains(rc)) {
                                    totalCsSmtb += dbm.Amount__c;
                                }

                                if(csSmtpfcRcId.contains(rc)) {
                                    totalCsSmtpfc += dbm.Amount__c;
                                }

                                if(ciRcId.contains(rc)) {
                                    totalCi += dbm.Amount__c;
                                }

                                if(bslSelfRcId.contains(rc)) {
                                    totalSelfDv += dbm.Amount__c;
                                }

                                if(otherRcId.contains(rc)) {
                                    totalOther += dbm.Amount__c;
                                }

                                if(scfRcId.contains(rc)) {
                                    totalScf += dbm.Amount__c;
                                }
                            }
                        }
                        else {
                            if((dbm.Disbursement_Date__c >= startDateOfQuarter4) && (dbm.Disbursement_Date__c <= endDateOfQuarter4)) {
                                if(csBidvRcId.contains(rc)) {
                                    totalCrossSell += dbm.Amount__c;
                                }
                                System.debug('QUARTER 4 IF: ' + totalCrossSell);

                                if(csSmtbRcId.contains(rc)) {
                                    totalCsSmtb += dbm.Amount__c;
                                }

                                if(csSmtpfcRcId.contains(rc)) {
                                    totalCsSmtpfc += dbm.Amount__c;
                                }

                                if(ciRcId.contains(rc)) {
                                    totalCi += dbm.Amount__c;
                                }

                                if(bslSelfRcId.contains(rc)) {
                                    totalSelfDv += dbm.Amount__c;
                                }

                                if(otherRcId.contains(rc)) {
                                    totalOther += dbm.Amount__c;
                                }

                                if(scfRcId.contains(rc)) {
                                    totalScf += dbm.Amount__c;
                                }
                            }
                        }                                       
                    }
                    
                    if(monthRcId.equals(kpi.RecordTypeId)) {
                        String monthOfOpp = String.valueOf(dbm.Disbursement_Date__c.month());

                        if(monthOfOpp.equals(monthOfKpiValue)) {
                            if(csBidvRcId.contains(rc)) {
                                totalCrossSell += dbm.Amount__c;
                            }

                            if(csSmtbRcId.contains(rc)) {
                                totalCsSmtb += dbm.Amount__c;
                            }

                            if(csSmtpfcRcId.contains(rc)) {
                                totalCsSmtpfc += dbm.Amount__c;
                            }

                            if(ciRcId.contains(rc)) {
                                totalCi += dbm.Amount__c;
                            }

                            if(bslSelfRcId.contains(rc)) {
                                totalSelfDv += dbm.Amount__c;
                            }

                            if(otherRcId.contains(rc)) {
                                totalOther += dbm.Amount__c;
                            }

                            if(scfRcId.contains(rc)) {
                                totalScf += dbm.Amount__c;
                            }
                        }
                    }

                    if(yearRcId.equals(kpi.RecordTypeId)) {
                        System.debug('Id Of KPI Value: ' + kpi.Id);
                        String yearOfOpp = String.valueOf(dbm.Disbursement_Date__c.year());

                        System.debug('111111111111: ' + csBidvRcId);
                        System.debug('222222222222: ' + yearOfOpp);
                        System.debug('333333333333: ' + yearOfKpiValue);
                        System.debug('444444444444: ' + rc);

                        if(yearOfOpp.equals(yearOfKpiValue)) {
                            if(csBidvRcId.contains(rc)) {
                                totalCrossSell += dbm.Amount__c;
                            }
                            System.debug('QUARTER 1 IF: ' + totalCrossSell);

                            if(csSmtbRcId.contains(rc)) {
                                totalCsSmtb += dbm.Amount__c;
                            }

                            if(csSmtpfcRcId.contains(rc)) {
                                totalCsSmtpfc += dbm.Amount__c;
                            }

                            if(ciRcId.contains(rc)) {
                                totalCi += dbm.Amount__c;
                            }

                            if(bslSelfRcId.contains(rc)) {
                                totalSelfDv += dbm.Amount__c;
                            }

                            if(otherRcId.contains(rc)) {
                                totalOther += dbm.Amount__c;
                            }

                            if(scfRcId.contains(rc)) {
                                totalScf += dbm.Amount__c;
                            }
                        }
                    }
                }
            }

            kpi.Actual_Total_Disbursed_Cross_sell_BIDV__c = totalCrossSell;
            kpi.Actual_Total_Disbursed_Cross_sell_SMTB__c = totalCsSmtb;
            kpi.Actual_Total_Disbursed_Cross_sell_SMTPFC__c = totalCsSmtpfc;
            kpi.Actual_Total_Disbursed_BSL_Self_Develop__c = totalSelfDv;
            kpi.Actual_Total_Disbursed_Customer_Initiate__c = totalCi;
            kpi.Actual_Total_Disbursed_Others__c = totalOther;
            kpi.Actual_Total_Disbursed_SCF__c = totalScf;
        }

        update kpiValueList;
    }
}