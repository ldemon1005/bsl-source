/**
* CMC Technology & Solution
* AccountEditingReminds class
* Remind Salesman update Annual Revenue, Potential Investment Plan On 10 January of each year
*
* @author: tqtai
*/
global class AccountEditingReminds implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Integer thisMonth = Date.today().month();

        if(thisMonth == 1) {
            List<Task> newTaskList = new List<Task>();
            // User Role Id List of Salesman Role (Da Nang, Ha Noi, Ho Chi Minh)
            list<String> l_saleman = new list<String>();
            l_saleman.add('Danang Business 1 Partner User');
            l_saleman.add('Hanoi Business 1 Partner User');
            l_saleman.add('Ho Chi Minh Business 1 Partner User');
            list<UserRole> l_userrole = [select id from UserRole where name in :l_saleman];
            List<String> salesmanRoleIdList = new List<String>();
            
            for(UserRole ur : l_userrole){
                salesmanRoleIdList.add(ur.Id);
            }

            List<User> salesmanUserList = [SELECT Id, FirstName, UserRoleId
                                        FROM User
                                        WHERE UserRoleId IN :salesmanRoleIdList];

            for(User u : salesmanUserList) {
                System.debug('11111');

                Task t = new Task();
                
                t.Subject = 'Dear ' + u.FirstName + '! Today is 10th January, please update Annual Revenue and Potential Investment Plan for your Account';
                t.Type = 'Email';
                t.OwnerId = u.Id;
                t.ActivityDate = Date.today();

                newTaskList.add(t);

            }

            insert newTaskList;

            for(Task tsk : newTaskList) {
                String uId = tsk.OwnerId;

                for(User ur : salesmanUserList) {
                    if(uId.equals(ur.Id)) {
                        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                        mentionSegmentInput.id = ur.Id;
                        messageBodyInput.messageSegments.add(mentionSegmentInput);

                        textSegmentInput.text = 'Today is 10th January, please update Annual Revenue and Potential Investment Plan for your Account! When you complete, please change Status of your Sales Activities (in subject of this post) to Done';
                        messageBodyInput.messageSegments.add(textSegmentInput);

                        feedItemInput.body = messageBodyInput;
                        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                        feedItemInput.subjectId = tsk.Id;
                        feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
                        

                        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                    }
                }
            }
            

            System.debug('Size: ' + newTaskList.size());
        }     
    }
}