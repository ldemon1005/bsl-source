@isTest()
public class ContactTriggerHandlerTest {
    private static final RecordType rt = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                         						LIMIT 1];
    @isTest static void test(){
        Account acc = new Account();
        acc.Business_Registration_Number__c = '1111';
        acc.Name = 'account';
        acc.BillingStreet = '11 duy tan';
        insert acc;
        
        Referral__c referral = new Referral__c();
        referral.RecordTypeId = rt.Id;
        referral.Name = 'referral';
        insert referral;
        
        Supplier__c supplier = new Supplier__c();
        supplier.Name = 'supplier';
        insert supplier;
        
        Contact contact = new Contact();
        contact.LastName = 'contact';
        contact.Birthday__c = Date.today() - 365*20;
        contact.AccountId = acc.Id;
        contact.Referral_Name__c = referral.Id;
        contact.Supplier_Name__c = supplier.Id;
        insert contact;
        
        list<contact> l_contact = new list<contact>();
        l_contact.add(contact);
        ContactTriggerHandler.checkContact(l_contact);
        ContactTriggerHandler.setAccountLineForReferralAndSupplierContact(l_contact);
    }
}