@IsTest public with sharing class SupplierCampaignController_Test {
    public static Campaign createCampaign(){
        Campaign camp = new Campaign(Name = 'Campaign test');
        camp.EndDate = Date.today().addDays(15);
        insert camp;
        return camp;
    }
    public static Referral__c createRef(){
        RecordType rt = [SELECT Id, Name FROM RecordType where SobjectType = 'Referral__c' limit 1];
        Referral__c ref = new Referral__c(Name = 'Campaign test');
        ref.RecordTypeId = rt.Id;
        insert ref;
        return ref;
    }
    public static Supplier__c createSupplier(){
        Supplier__c supplier = new Supplier__c(Name = 'Supplier test');
        insert supplier;
        return supplier;
    }
   	@IsTest(SeeAllData=true) public static void findCampaign() {
        // Instantiate a new controller with all parameters in the page
        //optOutController controller = new optOutController();
		SupplierCampaignController controller = new SupplierCampaignController();
        Campaign camp = createCampaign();
        Campaign c = SupplierCampaignController.findCampaign(camp.Id);
        //System.assertEquals('1', '1');                           
    }    
    
    @IsTest(SeeAllData=true) public static void findRef() {
        // Instantiate a new controller with all parameters in the page
        //optOutController controller = new optOutController();
		SupplierCampaignController controller = new SupplierCampaignController();      
        Supplier__c supplier = createSupplier();
        System.debug(supplier);
        Supplier__c c = SupplierCampaignController.findSup(supplier.Id);
        //System.assertEquals('1', '1');                           
    }                              
    
    @IsTest(SeeAllData=true) public static void searchCampaign() {
        // Instantiate a new controller with all parameters in the page
        //optOutController controller = new optOutController();
		SupplierCampaignController controller = new SupplierCampaignController();  
        Campaign camp = createCampaign();
        SupplierCampaignController.searchCampaign(camp.Name);
        //System.assertEquals('1', '1');                           
    }    
}