public class Repayment {
    public Quote quote;
    public static Decimal AssetPrice = 0;
    public static Decimal VATofAsset = 0;
    public static Decimal AssetValue = 0;
    public static Decimal InsurancePremium  = 0;
    public static Decimal VATofInsurance = 0;
    public static Decimal MaintenanceCost = 0;
    public static Decimal VATofMaintenance = 0;
    public static Decimal OtherCosts = 0;
    public static Decimal VATofOtherCost = 0;

	public Integer RepaymentTerm{get;set;}
    public Date RepaymentDate{get;set;}
    public String RepaymentDateStr{get;set;}
    public Integer DaysInMonth{get;set;}
    public Decimal TotalPrincipalRepayment{get;set;}
    public Decimal TotalVATRepayment{get;set;}
    public Decimal TotalPrincipalRepaymentWithVAT{get;set;}
    public Decimal TotalOutstandingDebt {get;set;}
    public Decimal InterestPayment{get;set;}
    public Decimal TotalRepaymentByCustomer{get;set;}
    public Decimal RepaymentOfAssetPrice{get;set;}
    public Decimal RepaymentOfAssetVAT{get;set;}
    public Decimal RepaymentOfInsurance{get;set;}
    public Decimal RepaymentOfInsuranceVAT{get;set;}
    public Decimal RepaymentOfMaintenace{get;set;}
    public Decimal RepaymentOfMaintenanceVAT{get;set;}
    public Decimal RepaymentOfOtherCosts{get;set;}
    public Decimal RepaymentOfOtherCostsVAT{get;set;}
    public Decimal SecurityDeposit{get;set;}
    public Decimal UpfrontOrClosingFee{get;set;}
    public Decimal Cashflow{get;set;}
    public decimal TotalLeaseRentPayment {get;set;}
    //
    public Integer CumulativeDay{get;set;}
    public Double Discfactor {get;set;}   
    public Double DFSum {get;set;}   
    public Integer SumOfDays {get;set;} 
    public Decimal InstallmentAmount {get;set;} 
    //
    public Decimal PrinciplePaymentVAT {get;set;} 
    public Decimal PrinciplePayment {get;set;} 
    public Decimal VATPrinciplePayment {get;set;} 
   	public Decimal OutstandingPrincipal {get;set;} 
    public Decimal AccumulatedPrincipalpayment  {get;set;}
    public Decimal AccumulatedInterestPayment {get;set;}
    public Decimal AnnualFee {get;set;}
    
    //
    public Decimal ResidualValue {get;set;}
    public Decimal ResidualValueCollection {get;set;}

    public Decimal MonthlyLease {get;set;}
    public Decimal InterestCost {get;set;}
    public Decimal InterestCostForWaitingPeriod {get;set;}
    public Decimal TotalMonthly {get;set;}
    public Decimal LeaseRent {get;set;}
    public Decimal VATLeaseRent {get;set;}
    public Decimal LeaseRentVAT {get;set;}
    //
    public Repayment(Quote quote){
        this.quote = quote;
    }
    public Repayment(Quote quote, Boolean isCreateField){
        this.quote = quote;
        TotalPrincipalRepayment = 0;
        TotalVATRepayment = 0;
        TotalPrincipalRepaymentWithVAT =0;
        InterestPayment = 0;
        TotalRepaymentByCustomer = 0;
        RepaymentOfAssetPrice = 0;
        RepaymentOfAssetVAT =0;
        RepaymentOfInsurance =0;
        RepaymentOfInsuranceVAT =0;
        RepaymentOfMaintenace =0;
        RepaymentOfMaintenanceVAT =0;
        RepaymentOfOtherCosts =0;
        RepaymentOfOtherCostsVAT =0;
    }
    public void caculateIR9(Integer i, Integer numberDaysPre){
        this.RepaymentTerm = i+1;
        Integer interestRepayment = 1;
        if(quote.Frequency_of_Interest_Repayment__c !=null){
            interestRepayment = integer.valueof(quote.Frequency_of_Interest_Repayment__c);
        }
        Date temp ;
        if(quote.Expected_Disbursement_Date__c!=null){
            temp = quote.Expected_Disbursement_Date__c;
        } else {
            temp =  Date.newInstance(2020,2,26);
        }
            try {
                this.RepaymentDate = temp.addMonths(integer.valueof(i+1)*interestRepayment);
                this.RepaymentDateStr= this.RepaymentDate.format();
                this.DaysInMonth = temp.addMonths(integer.valueof(i)*interestRepayment).daysBetween(this.RepaymentDate);
                this.CumulativeDay = numberDaysPre + this.DaysInMonth;
            } catch (Exception e) {
                  System.debug(e.getMessage());  
            }
        
    }
    public void caculate(Integer i, Decimal preValueDebt){
        this.RepaymentTerm = i+1;
        Integer interestRepayment = 1;
        if(quote.Frequency_of_Interest_Repayment__c !=null){
            interestRepayment = integer.valueof(quote.Frequency_of_Interest_Repayment__c);
        }
        Date temp ;
        if(quote.Expected_Disbursement_Date__c!=null){
            temp = quote.Expected_Disbursement_Date__c;
        } else {
            temp =  Date.newInstance(2020,2,26);
        }
            try {
                Decimal leaseTerm = quote.Lease_Term__c;
                if (quote.Payment_Method_for_Grace__c == 'Equated by Remaining Rrepayment Term') {
                    leaseTerm -= quote.Grace_for__c == null ? 0 : quote.Grace_Period__c;
                }
                boolean isInGradePeriod = quote.Payment_Method_for_Grace__c == 'Equated by Remaining Rrepayment Term'
                    && this.RepaymentTerm <= quote.Grace_Period__c;
                boolean isOneTimeVat = quote.One_time_Repayment_of_VAT__c != null && quote.One_time_Repayment_of_VAT__c;
                Integer gracePeriod  = quote.Grace_Period__c != null 
                    ? integer.valueof(quote.Grace_Period__c/interestRepayment) + 1
                    : 1;
                this.RepaymentDate = temp.addMonths(integer.valueof(i+1)*interestRepayment);
                this.RepaymentDateStr= this.RepaymentDate.format();
                this.DaysInMonth = temp.addMonths(integer.valueof(i)*interestRepayment).daysBetween(this.RepaymentDate);
                this.RepaymentOfAssetPrice = isInGradePeriod ? 0 : (Repayment.AssetPrice * (100-quote.Down_Payment_Rate__c))/(100*leaseTerm) * interestRepayment;
                this.RepaymentOfAssetVAT = isInGradePeriod 
                    ? 0 
                    : (!isOneTimeVat
                		? (Repayment.VATofAsset * (100-quote.Down_Payment_Rate__c))/(100*leaseTerm) * interestRepayment
                        : this.RepaymentTerm ==  gracePeriod 
                       		? (Repayment.VATofAsset * (100-quote.Down_Payment_Rate__c))/100
                       		: 0
                      );
                this.RepaymentOfMaintenace = isInGradePeriod 
                          ? 0 
                          : (Repayment.MaintenanceCost * (100-quote.Down_Payment_Rate__c))/(100*leaseTerm) * interestRepayment;
      			this.RepaymentOfMaintenanceVAT = isInGradePeriod 
                    ? 0 
                    : (!isOneTimeVat
                		? (Repayment.VATofMaintenance * (100-quote.Down_Payment_Rate__c))/(100*leaseTerm)* interestRepayment
                        : this.RepaymentTerm ==  gracePeriod 
                       		? (Repayment.VATofMaintenance * (100-quote.Down_Payment_Rate__c))/100
                       		: 0
                      );
                this.RepaymentOfOtherCosts = isInGradePeriod 
                          ? 0 
                          : (Repayment.OtherCosts * (100-quote.Down_Payment_Rate__c))/(100*leaseTerm)* interestRepayment;
                this.RepaymentOfOtherCostsVAT = isInGradePeriod 
                    ? 0 
                    : (!isOneTimeVat
                		? (Repayment.VATofOtherCost * (100-quote.Down_Payment_Rate__c))/(100*leaseTerm)* interestRepayment
                        : this.RepaymentTerm ==  gracePeriod 
                       		? (Repayment.VATofOtherCost * (100-quote.Down_Payment_Rate__c))/100
                       		: 0
                      );
                
                if( quote.Lease_Term_for_Insurance__c!=null & this.RepaymentTerm * interestRepayment <=quote.Lease_Term_for_Insurance__c & quote.Down_Payment_Rate__c != null){
                    Decimal leaseTermForInsurance = quote.Lease_Term_for_Insurance__c;
                    if (quote.Payment_Method_for_Grace__c == 'Equated by Remaining Rrepayment Term') {
                        leaseTermForInsurance -= quote.Grace_Period__c;
                    }
                    decimal repayInsurance = Repayment.InsurancePremium * (100-quote.Down_Payment_Rate__c);
                    decimal repayInsuranceVat = Repayment.VATofInsurance * (100-quote.Down_Payment_Rate__c);
                    this.RepaymentOfInsurance = isInGradePeriod ? 0 : repayInsurance/(100*leaseTermForInsurance) * interestRepayment;
                    this.RepaymentOfInsuranceVAT = isInGradePeriod 
                    ? 0 
                    : (!isOneTimeVat
                		? repayInsuranceVat/(100*leaseTermForInsurance) * interestRepayment
                        : this.RepaymentTerm ==  gracePeriod 
                       		? repayInsuranceVat/100
                       		: 0
                      );
                }
                this.TotalPrincipalRepayment = this.RepaymentOfAssetPrice 
                    + (this.RepaymentOfInsurance == null ? 0 : this.RepaymentOfInsurance)
                    + this.RepaymentOfMaintenace 
                    + this.RepaymentOfOtherCosts;
                this.TotalVATRepayment = this.RepaymentOfAssetVAT + (this.RepaymentOfInsuranceVAT == null ? 0:this.RepaymentOfInsuranceVAT) + this.RepaymentOfMaintenanceVAT + this.RepaymentOfOtherCostsVAT;
                this.TotalPrincipalRepaymentWithVAT = (this.TotalPrincipalRepayment == null ? 0 : this.TotalPrincipalRepayment) 
                    + (this.TotalVATRepayment == null ? 0 : this.TotalVATRepayment);
                this.TotalOutstandingDebt = preValueDebt - this.TotalPrincipalRepaymentWithVAT;
                if(this.RepaymentTerm * interestRepayment <= quote.Number_Of_Months_For_Fixed_Rate__c){
                    this.InterestPayment = (preValueDebt * this.quote.Fixed_Rate_In_The_First_Period__c/100)* this.DaysInMonth /365;
                } else {
                    this.InterestPayment = (preValueDebt * (this.quote.Margin__c +this.quote.Interest_Base__c )/100)* this.DaysInMonth /365;
                }
                system.debug(this.InterestPayment);
                this.TotalRepaymentByCustomer = (this.TotalPrincipalRepaymentWithVAT == null ? 0 : this.TotalPrincipalRepaymentWithVAT) 
                    + (this.InterestPayment == null ? 0 : this.InterestPayment);
                //
                this.Cashflow = (this.TotalRepaymentByCustomer == null ? 0 : this.TotalRepaymentByCustomer) 
                    + (this.SecurityDeposit == null ? 0 : this.SecurityDeposit)
                    + (this.UpfrontOrClosingFee == null ? 0 : this.UpfrontOrClosingFee);
            } catch (Exception e) {
                  System.debug(e.getMessage());  
            }
        
        
    }
    public void caculateOperatingLease(Integer i,double LeaseRentMonthly, double interestCost ){
        this.RepaymentTerm = i+1;
        Integer interestRepayment = 1;
        if(quote.Frequency_of_Interest_Repayment__c !=null){
            interestRepayment = integer.valueof(quote.Frequency_of_Interest_Repayment__c);
        }
        Date temp ;
        if(quote.Expected_Disbursement_Date__c!=null){
            temp = quote.Expected_Disbursement_Date__c;
        } else {
            temp =  Date.newInstance(2020,2,26);
        }
            try {
                double numberOfRepayment = 0;
                if(quote.Frequency_of_Repayment_month__c !=null & quote.Contract_Term__c !=null){
                    interestRepayment = integer.valueof(quote.Frequency_of_Repayment_month__c);
                    numberOfRepayment = quote.Contract_Term__c;
                    numberOfRepayment = numberOfRepayment / interestRepayment;
                }
                this.RepaymentDate = temp.addMonths(integer.valueof(i+1)*interestRepayment);
                this.RepaymentDateStr= this.RepaymentDate.format();
                this.DaysInMonth = temp.addMonths(integer.valueof(i)*interestRepayment).daysBetween(this.RepaymentDate);
                this.ResidualValue = !quote.Residual_Value_Risk_born_by_BSL__c 
                    ? quote.Residual_Value__c *(quote.Interest_Base__c+quote.Margin__c)/(100*12)*interestRepayment
                    : 0; 
               	system.debug(this.ResidualValue);
                this.LeaseRent = LeaseRentMonthly;
                if (this.RepaymentTerm == numberOfRepayment) {
                    this.SecurityDeposit = quote.Security_Deposit_Amount_after_VAT__c == null 
                        ? 0
                        : -quote.Security_Deposit_Amount_after_VAT__c;
                }
                this.InterestCostForWaitingPeriod = interestCost;
                this.TotalMonthly = (this.LeaseRent == null ? 0:this.LeaseRent) 
                    + (this.InterestCostForWaitingPeriod == null ? 0 : this.InterestCostForWaitingPeriod)
                    + (this.ResidualValue == null ? 0:this.ResidualValue);
                this.VATLeaseRent = this.TotalMonthly *quote.VAT_Rate__c/100;
                this.LeaseRentVAT = this.TotalMonthly 
                    + (this.VATLeaseRent == null ? 0 : this.VATLeaseRent)
                    + (this.SecurityDeposit == null ? 0 : this.SecurityDeposit);
                if (this.RepaymentTerm == numberOfRepayment) {
                    this.LeaseRentVAT += quote.Residual_Value__c;
                    this.ResidualValueCollection = quote.Residual_Value__c;
                }
                this.CashFlow = this.TotalMonthly 
                    + (this.SecurityDeposit == null ? 0 : this.SecurityDeposit)
                    + (this.ResidualValueCollection == null ? 0 : this.ResidualValueCollection);
                this.TotalLeaseRentPayment = this.TotalMonthly + this.VATLeaseRent;
                
            } catch (Exception e) {
                  System.debug(e.getMessage());  
            }
        
    }
}