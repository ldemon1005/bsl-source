@IsTest(SeeAllData=true)
public class ActualDisbursementControllerTest {
    private static RecordType rtQuote;
    private static RecordType rtContract;
    private static RecordType rtPlan;
    private static RecordType rtActual;
    private static RecordType rtMonth;
    private static RecordType rtQuarter;
    private static RecordType rtCrossellBIDV;
    private static RecordType rtOther;
    private static RecordType rtBSLselfdev;
    private static RecordType rtCustomerInitiate;
    private static RecordType rtSupplyChainFinance;
    private static RecordType rtCrossellSMTB;
    private static RecordType rtCrossellSMTPFC;
    
    static void init(){
        list<RecordType> l_RecordType = [SELECT Id, Name, SobjectType FROM RecordType];
        
        for(RecordType rt : l_RecordType){
            if(rt.SobjectType == 'Referral__c'){
                switch on rt.Name {
                    when 'Crossell-BIDV' {
                        rtCrossellBIDV = rt;
                    }	
                    when 'Other' {		
                        rtOther = rt;
                    }
                    when 'BSLselfdev' {		
                        rtBSLselfdev = rt;
                    }
                    when 'Customer-initiate' {		
                        rtCustomerInitiate = rt;
                    }
                    when 'Supply Chain Finance' {		
                        rtSupplyChainFinance = rt;
                    }
                    when 'Crossell-SMTB' {		
                        rtCrossellSMTB = rt;
                    }
                    when 'Crossell-SMTPFC' {		
                        rtCrossellSMTPFC = rt;
                    }
                    when else {		  
                        System.debug('error record type');
                    }
                }
            }
            
            if(rt.SobjectType == 'Disbursement__c') {
                switch on rt.Name {
                    when 'Plan' {
                        rtPlan = rt;
                    }	
                    when 'Actual' {		
                        rtActual = rt;
                    }
                    when else {		  
                        System.debug('error record type');
                    }
                }
            }
            
            if(rt.SobjectType == 'KPI_Value__c') {
                switch on rt.Name {
                    when 'Month' {
                        rtMonth = rt;
                    }	
                    when 'Quarter' {		
                        rtQuarter = rt;
                    }
                    when else {		  
                        System.debug('error record type');
                    }
                }
            }
            
            if(rt.SobjectType == 'Quote' && rt.Name == 'Finance Lease'){
                rtQuote = rt;
            }
            
            if(rt.SobjectType == 'Contract' && rt.Name == 'Finance Lease'){
                rtContract = rt;
            }
        }
    }

    private static Account createAccount() {
		Account a = new Account();

		a.Name = 'Account Testing';
		a.BillingStreet = '11 duy tan';
		insert a;

		return a;
	}

    private static Quote createQuote(Opportunity o, String sq, Decimal dpRate, String dpPaid, Decimal sdRate, Decimal ufRate, Decimal cpRate) {
        Quote q = new Quote();
        if (rtQuote != null) {
        	q.RecordTypeId = rtQuote.Id;  // Finance Lease    
        }
		q.Name = 'Quote test';
		q.Status = sq;
		q.OpportunityId = o.Id;
		q.Down_Payment_Rate__c = dpRate;
		q.Down_Payment_Paid_To__c = dpPaid;
		q.Lease_Term__c = 12;
		q.Frequency_of_Interest_Repayment__c = '3';
		q.Frequency_of_Repayment_month__c = '3';
		q.Interest_Base__c = 10;
		q.Margin__c = 10;
		q.Fixed_Rate_In_The_First_Period__c = 10;
		q.Number_Of_Months_For_Fixed_Rate__c = 10;
		q.Security_Deposit_Rate__c = sdRate;
		q.Upfront_Fee_Rate__c = ufRate;
		q.Closing_Price_Rate__c = cpRate;
		q.Grace_Period__c = 0;
		q.IRR__c = 10;        

		insert q;

		return q;
	}

	private static Contract createContract(Account a, Opportunity o, Quote q, String sc, Decimal ctrAmount) {
		Contract ctr = new Contract();
        if (rtContract != null) {
        	ctr.RecordTypeId = rtContract.Id;    
        }
		ctr.Name = 'Contract Test';
		ctr.AccountId = a.Id;
		ctr.Opportunity_Name__c = o.Id;
		ctr.SBQQ_Quote__c = q.Id;
		ctr.Contract_Amount__c = ctrAmount;
		ctr.StartDate = Date.newInstance(2019, 1, 1);
		ctr.Status = sc;
		ctr.ContractTerm = 12;

		insert ctr;

		return ctr;
	}

    @isTest static void testActualDisbursement(){
        Account a = createAccount();
        // create Contract
        Opportunity opp = new Opportunity();
        opp.AccountId = a.Id;
        opp.Name='Test' ;
        opp.StageName = 'D1';
        opp.CloseDate = System.today() + 5;
        insert opp ;

        Quote q = createQuote(opp, 'Approved by BSL', 10, 'Supplier', 10, 10, 10);
        Contract ctr = createContract(a, opp, q, 'Activated', 0);
        
        Disbursement__c dis = new Disbursement__c();
        dis.RecordTypeId = rtPlan.Id;
        dis.Amount__c = 1000;
        dis.Disbursement_Date__c = Date.newInstance(2019, 6, 26);
        dis.Opportunity__c = opp.id;
        dis.Status__c = 'Plan';
        insert dis;
        
        String str = ActualDisbursementController.getOpportunityFromContract(ctr.id);
        list<Disbursement__c> list_disbursement =  ActualDisbursementController.getDisbursements();
        // Disbursement__c disbursement = ActualDisbursementController.saveDisbursement(dis, ctr.id);
        
        System.assertEquals('1', '1');
    }
}