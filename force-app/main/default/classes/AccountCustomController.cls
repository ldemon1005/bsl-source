public with sharing class AccountCustomController {
	public String city { get;set; }
	public String country { get;set; }
	public String street { get;set; }
    public String state { get;set; }
    public String postalCode { get;set; }
	
   	integer totalRecs = 0;
   	integer count= 0;
   	integer LimitSize= 10;
  
  	public AccountCustomController(){}
   	public List<Account> accounts=new List<Account>();
   	public List<Account> getAccounts() {
        List<Account> accounts = getData(true);
        return accounts;
    }
    
    public List<Account> maps=new List<Account>();
   	public List<Account> getMaps() {
        List<Account> maps = getData(false);
        return maps;
    }
    
    public List<Account> getData(Boolean limit_record){
        String str_country = '\'%\'' + ' or BillingCountry = null )';
        String str_city = '\'%\'' + ' or BillingCity = null )';
        String str_postalCode = '\'%\'' + ' or BillingPostalCode = null )';
        String str_street = '\'%\'' + ' or BillingStreet = null )';
        String str_state = '\'%\'' + ' or BillingState = null )';
        if(!String.isEmpty(country)){
        	str_country = '\'%' + country + '%\')';
        }
        if(!String.isEmpty(city)){
        	str_city = '\'%' + city + '%\')';
        }
        if(!String.isEmpty(postalCode)){
        	str_postalCode = '\'%' + postalCode + '%\')';
        }
        if(!String.isEmpty(street)){
        	str_street = '\'%' + street + '%\')';
        }
        if(!String.isEmpty(state)){
        	str_state = '\'%' + state + '%\')';
        }
        String query_str = 'select Id, Name , BillingStreet, BillingCity, BillingCountry,' + 
         						  ' BillingPostalCode, BillingState, BillingLatitude, BillingLongitude, Phone from Account where '+ 
                                  ' (BillingCity like ' + str_city + ' and (BillingCountry like ' + str_country +
                                  ' and (BillingStreet like ' + str_street + ' and (BillingPostalCode like ' + str_postalCode + ' and (BillingState like ' + str_state;
        totalRecs = Database.query(query_str).size();
 
        List<Account> listAccount = new List<Account>();
        if(limit_record == true){
        	listAccount = Database.query(query_str + ' limit ' + LimitSize  + ' OFFSET ' + count);  
        }else{
            listAccount = Database.query(query_str);
            listAccount = getLocation(listAccount);
        }

		System.debug(listAccount.size());
        return listAccount;
    }
    public List<account> getLocation(List<account> accounts){
        String geocodingKey = 'AIzaSyDvExojagB2ARgQNagPQMBK1Jvz-R0HGY4';
     	List<Account> result = new List<Account>();
        List<Account> accUpdate = new List<Account>();
        for(Account acc : accounts){
            if(acc.BillingStreet != null) acc.BillingStreet = acc.BillingStreet.trim().replaceAll('\\s',' ').replaceAll('"',' ');
            if(acc.BillingCity != null) acc.BillingCity = acc.BillingCity.trim().replaceAll('\\s',' ').replaceAll('"',' ');
            if(acc.BillingCountry != null) acc.BillingCountry = acc.BillingCountry.trim().replaceAll('\\s',' ').replaceAll('"',' ');
            if(acc.BillingState != null) acc.BillingState = acc.BillingState.trim().replaceAll('\\s',' ').replaceAll('"',' ');
            
            if(acc.BillingLatitude == null && acc.BillingLongitude == null){
                if((acc.BillingStreet != null) || (acc.BillingCity != null)) {
                    // create a string for the address to pass to Google Geocoding API
                    String geoAddress = '';
                    if(acc.BillingStreet != null){
                        geoAddress += acc.BillingStreet + ', ';
                    }
                    if(acc.BillingCity != null){
                        geoAddress += acc.BillingCity + ', ';
                    }
                    if(acc.BillingState != null){
                        geoAddress += acc.BillingState + ', ';
                    }
                    if(acc.BillingCountry != null){
                        geoAddress += acc.BillingCountry + ', ';
                    }
                    if(acc.BillingPostalCode != null){
                        geoAddress += acc.BillingPostalCode;
                    }
              
                    // encode the string so we can pass it as part of URL
                    geoAddress = EncodingUtil.urlEncode(geoAddress, 'UTF-8');
                    // build and make the callout to the Geocoding API
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    request.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + geoAddress + '&key=' + geocodingKey + '&sensor=false');
                    request.setMethod('GET');
                    request.setTimeout(60000);
                    try {
                        // make the http callout
                        HttpResponse response = http.send(request);
                        // parse JSON to extract co-ordinates
                        JSONParser responseParser = JSON.createParser(response.getBody());
                        // initialize co-ordinates
                        double latitude = null;
                        double longitude = null;
                        while(responseParser.nextToken() != null) {
                            if((responseParser.getCurrentToken() == JSONToken.FIELD_NAME) && (responseParser.getText() == 'location')) {
                                responseParser.nextToken();
                                while(responseParser.nextToken() != JSONToken.END_OBJECT) {         
                                    String locationText = responseParser.getText();
                                    responseParser.nextToken();
                                    if (locationText == 'lat'){
                                        latitude = responseParser.getDoubleValue();
                                    } else if (locationText == 'lng'){
                                        longitude = responseParser.getDoubleValue();
                                    }
                                }
                            }
                        }
                        if(latitude != null) {
                    		acc.BillingLongitude = longitude;
                            acc.BillingLatitude  = latitude;
                            result.add(acc);
                            accUpdate.add(acc);
                        }
                    } catch (Exception e) {
                        System.debug(LoggingLevel.ERROR, 'Error Geocoding Address - ' + e.getMessage());
                    }
            	}
            }else {
                result.add(acc);
            }
            
        }    
        //update accUpdate;
        System.debug('accUpdate size: ' + accUpdate.size());
        return result;
    }
    
    public void updatePage() {
       	accounts.clear();
        count = 0;
       	accounts = getData(true);
        maps.clear();
        maps = getData(false);
        
    }
    public PageReference Firstbtn() {
     	count=0;
        return null;
    }
    public PageReference prvbtn() {
      	count=count-limitsize;
        return null;
    }
   
 	public PageReference Nxtbtn() {
 		count=count+limitsize;
        return null;
    }
 	public PageReference lstbtn() {
 		count= totalrecs - math.mod(totalRecs,LimitSize);
        return null;
    }


    public Boolean getNxt() {
        if((count+ LimitSize) > totalRecs)
       return true;
     else
       return false;
        
    }
     public Boolean getPrv() {
      if(count== 0)
          return true;
        else
          return false;
        }
    }