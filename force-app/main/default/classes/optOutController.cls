public class optOutController {
	//Method to retrieve account Name.
	@AuraEnabled
    public static String getAccName(ID accid) {
        return [SELECT ID, Name FROM Account WHERE ID = :accid LIMIT 1][0].Name;
    }
    
    //Method to opt out the Contacts at the Account with the specificed ID parameter.
    @AuraEnabled
    public static void optOutContacts(ID accid) {
        List<Contact> contactsToOptOut = new List<Contact>([SELECT ID, AccountID, HasOptedOutOfEmail from 
                                                           Contact WHERE AccountID=: accid]);
        for(Contact c : contactsToOptOut){
            c.HasOptedOutOfEmail = TRUE;
        }
        update contactsToOptOut;
    }
}