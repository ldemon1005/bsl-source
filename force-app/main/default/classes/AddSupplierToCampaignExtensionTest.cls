/**
 * @File Name          : AddSupplierToCampaignExtensionTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/5/2019, 9:00:43 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/5/2019, 9:00:43 AM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest(SeeAllData=true)
public class AddSupplierToCampaignExtensionTest {
    @isTest() static void test1(){
        Supplier__c supplier1 = new Supplier__c();
        supplier1.Name = 'supplier 1';
        
        Supplier__c supplier2 = new Supplier__c();
        supplier2.Name = 'supplier 2';
        
        list<Supplier__c> l_supplier = new list<Supplier__c>();
        l_supplier.add(supplier1);
        l_supplier.add(supplier2);
        insert l_supplier;
        
        PageReference pageRef = Page.AddSupplierToCampaignPage;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(l_supplier);
        stdSetController.setSelected(l_supplier);
      
        AddSupplierToCampaignExtension ext = new AddSupplierToCampaignExtension(stdSetController);
        
        String SupplierId = ext.SupplierId;
        List<Supplier__c> ll = ext.SupplierList;
        String ReferralId = ext.currentRecordId;
        String ReferralId1 = ext.getSupplierIdSelected();
    }
}