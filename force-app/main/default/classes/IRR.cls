public class IRR {
    public static Double irrResult(Double[]values,Double[] dates, Double rate) {
        Double r = rate + 1;
        Double result = values[0];
        for (Integer i = 1; i < values.size(); i++) {
            result += (double)values[i] / Math.pow(r, (dates[i] - dates[0]) / (double)365);
        }
        return result;
    }
    public static Double irrResultDeriv(Double[]values,Double[] dates, Double rate) {
        Double r = rate + 1;
        Double result = 0;
        for (Integer i = 1; i < values.size(); i++) {
            Double frac = (double)(dates[i] - dates[0]) / (double)365;
            result -= frac * values[i] / Math.pow(r, 	frac + 1);
        }
        return result;
    }
    public static String caculateIRR(Double[] values,Double guess) {
        System.debug(values.size());
        // Credits: algorithm inspired by Apache OpenOffice
        
        // Calculates the resulting amount
        
        // Calculates the first derivation
        // Initialize dates and check that values contains at least one positive value and one negative value
        Double[] dates = new List<Double>();
        Boolean positive = false;
        Boolean negative = false;
        for (Integer i = 0; i < values.size(); i++) {
            Double da;
            if(i==0){
                da = 0;
            }else {
                da = dates[i - 1] + 365;
            }
            dates.add(da);
            if (values[i] > 0) positive = true;
            if (values[i] < 0) negative = true;
        }
        // Return error if values does not contain at least one positive value and one negative value
        if (!positive | !negative) return '#NUM!';
        // Initialize guess and resultRate
        Double resultRate = guess;
        // Set maximum epsilon for end of iteration
        Double epsMax = 0.0000000001;
        // Set maximum number of iterations
        Integer iterMax = 60;
        // Implement Newton's method
        Double newRate, epsRate, resultValue;
        Double iteration = 0;
        Boolean contLoop = true;
        do {
            resultValue = irrResult(values, dates, resultRate);
            decimal deriv = irrResultDeriv(values, dates, resultRate);
            newRate = resultRate - resultValue / deriv;
            epsRate = Math.abs(newRate - resultRate);
            resultRate = newRate;
            contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax);
        } while(contLoop && (++iteration < iterMax));
        if(contLoop) return '#NUM!';
        // Return internal rate of return
        return resultRate+'';
    }
    public static double caculatePMT(double rate, double nper, double pv) {
        double v = (1 + (rate / 12));
        double t = (-(nper / 12) * 12);
        double result = (pv * (rate / 12)) / (1 - Math.pow(v, t));
        return result;
    }
    public static double caculatePMT(Decimal rate, Decimal nper, Decimal pv) {
        
        double v = (1 + (rate / 12));
        double t = (-(nper / 12) * 12);
        double result = (pv * (rate / 12)) / (1 - Math.pow(v, t));
        return result;
    }
}