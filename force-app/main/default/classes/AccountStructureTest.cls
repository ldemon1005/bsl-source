/**
 * @File Name          : AccountStructureTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/4/2019, 5:56:59 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/4/2019, 5:36:21 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest(SeeAllData=true)
public class AccountStructureTest {
    @isTest static void getAccounts(){
        AccountStructure accountStructure = new AccountStructure();
        List<id> listId = new List<id>();
        for(account acc : [select id from account limit 2]){
            listId.add(acc.id);
        }
		list<account> accounts = accountStructure.getAccounts();
        System.assertEquals('1', '1');
    }
    @isTest static void formatObjectStructure(){
        AccountStructure accountStructure = new AccountStructure();

        Account acc1 = new Account();
        acc1.Name = 'account 1';
        acc1.Business_Registration_Number__c = '1';
        acc1.BillingStreet = '11 duy tan';
        insert acc1;

        Account acc2 = new Account();
        acc2.Name = 'account 2';
        acc2.ParentId = acc1.Id;
        acc2.Business_Registration_Number__c = '2';
        acc2.BillingStreet = '11 duy tan';
        insert acc2;

        List<Account> l_account = new List<Account>();
        l_account.add(acc1);
        l_account.add(acc2);

        accountStructure.setcurrentId(acc1.Id);
        accountStructure.formatObjectStructure(acc1.id);
        accountStructure.getObjectStructure();
        List<Account> list_acc = accountStructure.getAccounts();
        System.assertEquals('1', '1');
    }
    
    /**
    * Wrapper class
    */
    public with sharing class ObjectStructureMap{

        public String nodeId;
        public Boolean[] levelFlag = new Boolean[]{};
        public Boolean[] closeFlag = new Boolean[]{};
        public String nodeType;
        public Boolean currentNode;
        
        /**
        * @Change this to your sObject
        */
        public Account account;
        
        public String getnodeId() { return nodeId; }
        public Boolean[] getlevelFlag() { return levelFlag; }
        public Boolean[] getcloseFlag() { return closeFlag; }
        public String getnodeType() { return nodeType; }
        public Boolean getcurrentNode() { return currentNode; }


        /**
        * @Change this to your sObject
        */
        public Account getaccount() { return account; }
        
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setlevelFlag( Boolean l ) { this.levelFlag.add(l); }
        public void setlcloseFlag( Boolean l ) { this.closeFlag.add(l); }
        public void setnodeType( String nt ) { this.nodeType = nt; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }

        /**
        * @Change this to your sObject
        */
        public void setaccount( Account a ) { this.account = a; }

        /**
        * @Change the parameters to your sObject
        */
        public ObjectStructureMap( String nodeId, Boolean[] levelFlag,Boolean[] closeFlag , String nodeType, Boolean lastNode, Boolean currentNode, Account a ){
            
            this.nodeId         = nodeId;
            this.levelFlag      = levelFlag; 
            this.closeFlag      = closeFlag;
            this.nodeType       = nodeType;
            this.currentNode    = currentNode;

            //Change this to your sObject  
            this.account = a;
        }
    }
}