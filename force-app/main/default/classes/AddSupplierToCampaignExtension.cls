public with sharing class AddSupplierToCampaignExtension {
	public ApexPages.StandardSetController  controller;

    public AddSupplierToCampaignExtension(ApexPages.StandardSetController  constructor) {
 		controller = constructor;
        SupplierId = '';
		SupplierList = controller.getSelected();  
       	for(Supplier__c o : SupplierList) {
            SupplierId += o.id + ',';
        }
 	}
    
     public AddSupplierToCampaignExtension(ApexPages.StandardController constructor) {
      	currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
     }
    
    public String currentRecordId {get;set;}
    transient public String SupplierId  {get; set;}
    
    transient public List<Supplier__c> SupplierList  {get; set;}
    

    public String getSupplierIdSelected(){
        return SupplierId;
    }
}