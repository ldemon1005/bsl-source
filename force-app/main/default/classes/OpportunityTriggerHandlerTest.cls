@isTest(SeeAllData=true)
private class OpportunityTriggerHandlerTest{

	private static String getApprovalData(String bdAppr, String bdV, String ccdAppr, String ccdV, String ccAppr, String ccV, String bomAppr, String bomV) {
		String appr = '{\n' +
        '   "Branch Director": {\n' +
        '       "Approval Authority": ' + bdAppr + ',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + bdV + '\n' +
        '   },\n' +
        '   "CCD": {\n' +
        '       "Approval Authority": ' + ccdAppr +',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + ccdV + '\n' +
        '   },\n' +
        '   "Credit Council": {\n' +
        '       "Approval Authority": ' + ccV +',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + ccV +'\n' +
        '   },\n' +
        '   "BOM": {\n' +
        '       "Approval Authority": ' + bomAppr + ',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + bomV + '\n' +
        '   }\n' +
        '}';

		return appr;
	}

	private static Referral__c createReferral() {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Referral__c r = new Referral__c();

		r.RecordTypeId = rtId;
		r.Name = 'referral test';

		insert r;

		return r;
	}

	private static Account createAccount() {
		Account a = new Account();
		a.Business_Registration_Number__c = '1111';
		a.Name = 'Account Testing';
		a.BillingStreet = '11 duy tan';
		insert a;

		return a;
	}

	private static Opportunity createOpp(Account a, Referral__c r, String stage, String apprDt, Decimal apprAmount, Decimal ctrAmount, Decimal dbmAmount) {
		Date dt = Date.newInstance(2021, 1, 2);

		Opportunity o = new Opportunity();

		o.Name = 'Testing Opp';
		o.Referral_Source__c = r.Id;
		o.BSL_Service__c = 'Finance Lease';
		o.Contract_Ending_Selection__c = 'Ownership Transfer';
		o.StageName = stage;
		o.Approval_Data__c = apprDt;
		o.AccountId = a.Id;
		o.CloseDate = dt;
		o.Approved_Amount__c = apprAmount;
		o.Contracted_Amount__c = ctrAmount;
		o.Total_Disbursed_Amount__c = dbmAmount;

		insert o;

		return o;
	}

	private static Supplier__c createSupplier(String sName) {
		Supplier__c s = new Supplier__c(Name = sName);
		insert s;

		return s;
	}

	private static Asset__c createAsset(Opportunity opp, Supplier__c sp, Decimal pr, Decimal q, Decimal epc) {
		Asset__c asset = new Asset__c();
		asset.Name = 'Asset Test';
		asset.Currency__c = 'VND';
		asset.Opportunity__c = opp.Id;
		asset.Supplier__c = sp.Id;
		asset.Asset_Value_Before_Tax__c = pr;
		asset.Quantity__c = q;
		asset.Expected_Contract_Amount__c = epc;

		insert asset;

		return asset;
	}

	private static Quote createQuote(Opportunity o, Decimal dpRate, String dpPaid, Decimal sdRate, Decimal ufRate, Decimal cpRate) {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Quote' AND Name = 'Finance Lease'
				     LIMIT 1];
        
		String rtId = rt.Id;	

		Quote q = new Quote();
		q.RecordTypeId = rtId; // Finance Lease
		q.Name = 'Quote test';
		q.Status = 'Presented to Customer';
		q.OpportunityId = o.Id;
		q.Down_Payment_Rate__c = dpRate;
		q.Down_Payment_Paid_To__c = dpPaid;
		q.Lease_Term__c = 12;
		q.Frequency_of_Interest_Repayment__c = '3';
		q.Frequency_of_Repayment_month__c = '3';
		q.Interest_Base__c = 10;
		q.Margin__c = 10;
		q.Fixed_Rate_In_The_First_Period__c = 10;
		q.Number_Of_Months_For_Fixed_Rate__c = 10;
		q.Security_Deposit_Rate__c = sdRate;
		q.Upfront_Fee_Rate__c = ufRate;
		q.Closing_Price_Rate__c = cpRate;
		q.Grace_Period__c = 12;
		q.IRR__c = 10;

		insert q;

		return q;
	}

	private static Contract createContract(Account a, Opportunity o, Quote q, Decimal ctrAmount) {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Contract' AND Name = 'Finance Lease'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Contract ctr = new Contract();
		ctr.RecordTypeId = rtId;
		ctr.Name = 'Contract Test';
		ctr.AccountId = a.Id;
		ctr.Opportunity_Name__c = o.Id;
		ctr.SBQQ_Quote__c = q.Id;
		ctr.Contract_Amount__c = ctrAmount;
		ctr.StartDate = Date.newInstance(2019, 1, 1);
		ctr.Status = 'Draft';
		ctr.ContractTerm = 12;

		insert ctr;

		return ctr;
	}

	private static Disbursement__c createPlanDisbursement(Opportunity o, Decimal amount) {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Disbursement__c' AND Name = 'Plan'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Disbursement__c d = new Disbursement__c();
		d.RecordTypeId = rtId;
		d.Opportunity__c = o.Id;
		d.Disbursement_Date__c = Date.newInstance(2019, 7, 7);
		d.Amount__c = amount;
		d.Status__c = 'Plan';
		
		insert d;

		return d;
	}
	
	@isTest
	static void setNotificationWhenHavingNewOppTest() {
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 0, 0, 0);

		List<FeedItem> fiList = [SELECT Id FROM FeedItem LIMIT 1];
		// System.assertEquals(1, fiList.size());
	}

	@isTest
	static void remindSalesmanToUpdateDisbursementTest() {
		String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 0, 0, 0);
		o.Approval_Data__c = apprDataTest;
		o.StageName = 'C';
		
		update o;

		List<FeedItem> fiList = [SELECT Id FROM FeedItem LIMIT 2];
		// System.assertEquals(2, fiList.size());
	}

	@isTest
	static void updateBranchValueWhenCreatingOppTest1() {
		UserRole ur = [SELECT Id
				   FROM UserRole
				   WHERE Name = 'Danang Business 1 Partner User'
				   LIMIT 1];

		String urId = ur.Id;

		User u1 = [SELECT Id
			     FROM User
			     WHERE UserRoleId = :urId
			     LIMIT 1];

		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 0, 0, 0);
		o.OwnerId = u1.Id;
		update o;

		List<Opportunity> oTestList = [SELECT BSL_Branch__c
					   		 FROM Opportunity
					   		 WHERE BSL_Branch__c = 'Danang' AND Name = 'Testing Opp'];

		// System.assertEquals(1, oTestList.size());

	}

	@isTest
	static void updateBranchValueWhenCreatingOppTest2() {
		// UserRole ur = [SELECT Id
		// 		   FROM UserRole
		// 		   WHERE Name = 'Hanoi Business 1 Partner User'
		// 		   LIMIT 1];

		// String urId = ur.Id;

		// User u1 = [SELECT Id
		// 	     FROM User
		// 	     WHERE UserRoleId = :urId
		// 	     LIMIT 1];

		// Referral__c r = createReferral();
		// Account a = createAccount();
		// Opportunity o = createOpp(a, r, 'D3', null, 0, 0, 0);
		// o.OwnerId = u1.Id;
		// update o;

		// List<Opportunity> oTestList = [SELECT BSL_Branch__c
		// 			   		 FROM Opportunity
		// 			   		 WHERE BSL_Branch__c = 'Hanoi' AND Name = 'Testing Opp'];

		// // System.assertEquals(1, oTestList.size());

	}

	@isTest
	static void updateBranchValueWhenCreatingOppTest3() {
		// UserRole ur = [SELECT Id
		// 		   FROM UserRole
		// 		   WHERE Name = 'Ho Chi Minh Business 1 Partner User'
		// 		   LIMIT 1];

		// String urId = ur.Id;

		// User u1 = [SELECT Id
		// 	     FROM User
		// 	     WHERE UserRoleId = :urId
		// 	     LIMIT 1];

		// Referral__c r = createReferral();
		// Account a = createAccount();
		// Opportunity o = createOpp(a, r, 'D3', null, 0, 0, 0);
		// o.OwnerId = u1.Id;
		// update o;

        /*
		List<Opportunity> oTestList = [SELECT BSL_Branch__c
					   		 FROM Opportunity
					   		 WHERE BSL_Branch__c = 'Hochiminh' AND Name = 'Testing Opp'];
*/

		// System.assertEquals(1, oTestList.size());

	}

	@isTest
	static void updateStatusOfEachStageTest1() {
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 0, 0, 0);
		

		List<Opportunity> oTestList = [SELECT Status_of_Each_Stage__c
					   		 FROM Opportunity
					   		 WHERE Name = 'Testing Opp'];

		// System.assertEquals('Approaching and obtaining information', oTestList[0].Status_of_Each_Stage__c);

	}

	@isTest
	static void updateStatusOfEachStageTest2() {
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D2', null, 0, 0, 0);
		

		List<Opportunity> oTestList = [SELECT Status_of_Each_Stage__c
					   		 FROM Opportunity
					   		 WHERE Name = 'Testing Opp'];

		// System.assertEquals('Negotiating and preparing AF', oTestList[0].Status_of_Each_Stage__c);

	}

	@isTest
	static void updateStatusOfEachStageTest3() {
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 0, 0, 0);
		

		List<Opportunity> oTestList = [SELECT Status_of_Each_Stage__c
					   		 FROM Opportunity
					   		 WHERE Name = 'Testing Opp'];

		// System.assertEquals('Credit review', oTestList[0].Status_of_Each_Stage__c);

	}

	@isTest
	static void updateStatusOfEachStageTest4() {
		String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 0, 0, 0);
		o.Approval_Data__c = apprDataTest;
		o.StageName = 'C';
		update o;

		List<Opportunity> oTestList = [SELECT Status_of_Each_Stage__c
					   		 FROM Opportunity
					   		 WHERE Name = 'Testing Opp'];

		// System.assertEquals('Approved', oTestList[0].Status_of_Each_Stage__c);

	}

	@isTest
	static void checkingApprovedAndContractAmountTest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 1000, 2000, 0);
		}
		catch(Exception e) {
			Test.startTest();
			Boolean expectedExceptionThrown =  e.getMessage().contains('Approved Amount cannot be lower than Contracted Amount');
			// System.assertEquals(expectedExceptionThrown, true);
			Test.stopTest();
		}
	}

	@isTest
	static void checkingContractedAndDbmAmountTest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 4000);
		}
		catch(Exception e) {
			Test.startTest();
			Boolean expectedExceptionThrown =  e.getMessage().contains('Contracted Amount cannot be lower than Disbursed Amount');
			// System.assertEquals(expectedExceptionThrown, true);
			Test.stopTest();
		}
	}

	@isTest
	static void updateApprovalDeadlineTest1() {
		String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
		o.Approval_Data__c = apprDataTest;
		update o;

		Opportunity oTest = [SELECT Approval_Deadline_BD__c
					   FROM Opportunity
					   WHERE Name = 'Testing Opp'
					   LIMIT 1];

		Date dTest = Date.newInstance(2019, 5, 30);
		// System.assertEquals(dTest, oTest.Approval_Deadline_BD__c);
	}

	@isTest
	static void updateApprovalDeadlineTest2() {
		String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
		o.Approval_Data__c = apprDataTest;
		update o;

		Opportunity oTest = [SELECT Approval_Deadline_CCD__c
					   FROM Opportunity
					   WHERE Name = 'Testing Opp'
					   LIMIT 1];

		Date dTest = Date.newInstance(2019, 5, 30);
		// System.assertEquals(dTest, oTest.Approval_Deadline_CCD__c);
	}

	@isTest
	static void updateApprovalDeadlineTest3() {
		String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
		o.Approval_Data__c = apprDataTest;
		update o;

		Opportunity oTest = [SELECT Approval_Deadline_CC__c
					   FROM Opportunity
					   WHERE Name = 'Testing Opp'
					   LIMIT 1];

		Date dTest = Date.newInstance(2019, 5, 30);
		// System.assertEquals(dTest, oTest.Approval_Deadline_CC__c);
	}

	@isTest
	static void updateApprovalDeadlineTest4() {
		String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
		o.Approval_Data__c = apprDataTest;
		update o;

		Opportunity oTest = [SELECT Approval_Deadline_BOM__c
					   FROM Opportunity
					   WHERE Name = 'Testing Opp'
					   LIMIT 1];

		Date dTest = Date.newInstance(2019, 5, 30);
		// System.assertEquals(dTest, oTest.Approval_Deadline_BOM__c);
	}

	@isTest
	static void updateAssetInReferralTest() {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Referral__c r = createReferral();
		Account a = createAccount();
		Supplier__c sp = createSupplier('Supplier Test');
		Opportunity o = createOpp(a, r, 'D1', null, 0, 0, 0);
		Asset__c asset = createAsset(o, sp, 100, 1, 50);

		Referral__c r1 = new Referral__c();

		r1.RecordTypeId = rtId;
		r1.Name = 'referral test 1';

		insert r1;

		o.Referral_Source__c = r1.Id;

		update o;

		Asset__c aTest = [SELECT Referral__c
					FROM Asset__c
					WHERE Name = 'Asset Test'
					LIMIT 1];

		// System.assertEquals(r1.Id, aTest.Referral__c);
	}

	@isTest
	static void blockChangingStageNameOfOppTest1() {
		try {
			String apprDataTest = getApprovalData('true', 'true', 'true', 'true', 'true', 'true', 'true', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			o.StageName = 'C';
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest2() {
		try {
			String apprDataTest = getApprovalData('true', 'true', 'true', 'true', 'true', 'false', 'false', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			o.StageName = 'C';
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest3() {
		try {
			String apprDataTest = getApprovalData('true', 'true', 'true', 'false', 'false', 'false', 'false', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			o.StageName = 'C';
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest4() {
		try {
			String apprDataTest = getApprovalData('true', 'false', 'false', 'false', 'false', 'false', 'false', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			o.StageName = 'C';
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest5() {
		try {
			String apprDataTest = getApprovalData('true', 'false', 'false', 'false', 'false', 'false', 'true', 'true');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest6() {
		try {
			String apprDataTest = getApprovalData('true', 'false', 'false', 'false', 'true', 'true', 'false', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest7() {
		try {
			String apprDataTest = getApprovalData('true', 'false', 'true', 'true', 'false', 'false', 'false', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameOfOppTest8() {
		try {
			String apprDataTest = getApprovalData('true', 'true', 'false', 'false', 'false', 'false', 'false', 'false');
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
			o.Approval_Data__c = apprDataTest;
			update o;
		}
		catch (Exception e) {
			// Test.startTest();
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName now!');
			// // System.assertEquals(expectedExceptionThrown, true);
			// Test.stopTest();
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameBackToCTest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'B', null, 4000, 2000, 1000);
			o.StageName = 'C';
			update o;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName back to C');
			// // System.assertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}

	}

	@isTest
	static void blockChangingStageNameBackToBTest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'A', null, 4000, 2000, 1000);
			o.StageName = 'B';
			update o;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName back to B');
			// // System.assertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}

	}

	@isTest
	static void blockChangingStageNameBackToATest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'L', null, 4000, 2000, 1000);
			o.StageName = 'A';
			update o;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName back to A');
			// // System.assertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}

	}

	@isTest
	static void blockChangingStageNameFromCtoBTest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'C', null, 4000, 2000, 1000);
			o.StageName = 'B';
			update o;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName to B now');
			// // System.assertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameFromBtoATest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'B', null, 4000, 2000, 1000);
			o.StageName = 'A';
			update o;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName to A now');
			// // System.assertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}
	}

	@isTest
	static void blockChangingStageNameFromAtoLTest() {
		try {
			Referral__c r = createReferral();
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'A', null, 4000, 2000, 1000);
			o.StageName = 'L';
			update o;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot change StageName to L now');
			// // System.assertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}
	}

	@isTest
	static void setTotalDevelopedNumberOfOpportunitywDeletingTest() {
		Referral__c r = createReferral();
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D1', null, 4000, 2000, 1000);
		delete o;

		Referral__c rTest = [SELECT Total_Developed_Number_of_Opportunities__c
					   FROM Referral__c
					   WHERE Name = 'referral test'
					   LIMIT 1];

		// System.assertEquals(0, rTest.Total_Developed_Number_of_Opportunities__c);
	}
}