@isTest
public class MultiDisbursementTest {
    // create new Account record
    @testSetup
    private static void createAccount() {
        Account a = new Account();
        a.Business_Registration_Number__c = '2222';
        a.BillingStreet = '11 duy tan';
        a.Name = 'Account Testing MultiDisbursement';
        insert a;
        
        // create new Referral record
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                         LIMIT 1];
        Referral__c r = new Referral__c();
        r.RecordTypeId = rt.Id; 
        r.Name = 'referral test';
        insert r;
       
    }
    
    
    
    // create new Opportunity record
    private static Opportunity createOpp(String rcName) {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Opportunity' AND Name =: rcName
                         LIMIT 1];
        
        String rtId = rt.Id;
        
        Date dt = Date.newInstance(2021, 1, 2);
        
        Account a = [select id from account where name = 'Account Testing MultiDisbursement'];
        Referral__c r = [select id from Referral__c where name = 'referral test'];
        
        Opportunity o = new Opportunity();
        
        o.Name = 'Testing Opp';
        o.Referral_Source__c = r.Id;
        o.Contract_Ending_Selection__c = 'Ownership Transfer';
        o.StageName = 'D1';
        o.AccountId = a.Id;
        o.CloseDate = dt;
        
        insert o;
        
        return o;
    }
    
    private static Supplier__c createSupplier() {
        Supplier__c s = new Supplier__c(Name = 'Supplier Test');
        insert s;
        
        return s;
    }
    
    private static Asset__c createAsset(String rcName, Quote quote) {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Asset__c' AND Name =: rcName
                         LIMIT 1];
        
        Opportunity opp = createOpp('Finance Lease');
        
        Supplier__c s = createSupplier();
        
        Asset__c asset = new Asset__c();
        asset.Name = 'Asset Test';
        asset.Currency__c = 'VND';
        asset.Account_Name__c = quote.AccountId;
        asset.Opportunity__c = opp.Id;
        asset.Supplier__c = s.Id;
        asset.Asset_Value_Before_Tax__c = 1000000000;
        asset.Quantity__c = 1;
        asset.Expected_Contract_Amount__c = 100000000;
        asset.VAT_rate__c = 10; // %
        asset.Insurance_Premium__c = 2000000;
        asset.VAT_of_Insurance__c = 200000;
        asset.Maintenance_Cost__c = 2000000;
        asset.VAT_of_Maintenance__c = 200000;
        asset.Installation_Cost__c = 2000000;
        asset.VAT_of_Installation__c = 200000;
        asset.Other_Costs__c = 2000000;
        asset.VAT_of_Other_Cost__c = 200000;
        asset.Quote__c = quote.Id;
        
        insert asset;
        
        return asset;
    }
    
    private static Quote createQuote(String rcName, String graceFor, String gracePaymentMethod, Decimal gracePeriod) {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Quote' AND Name =: rcName
                         LIMIT 1];
        
        String rtId = rt.Id;
        
        Date dt = Date.newInstance(2020, 6, 1);
        
        Opportunity o = createOpp('Finance Lease');
        
        Quote q = new Quote();
        q.RecordTypeId = rt.Id; // Finance Lease
        q.Name = 'Quote test';
        q.Status = 'Presented to Customer';
        q.Grace_for__c = graceFor;
        q.Payment_Method_for_Grace__c = gracePaymentMethod;
        q.Expected_Disbursement_Date__c = dt;
        
        q.OpportunityId = o.Id;
        q.Lease_Term__c = 36;
        q.Lease_Term_for_Insurance__c = 12;
        q.Frequency_of_Principal_Repayment__c = '1';
        q.Frequency_of_Interest_Repayment__c = '1';
        q.Grace_Period__c = gracePeriod;
        q.Interest_Base__c = 6.8; // %
        q.Margin__c = 3; // %
        q.Fixed_Rate_In_The_First_Period__c = 8; // %
        q.Number_Of_Months_For_Fixed_Rate__c = 6;
        q.Down_Payment_Rate__c = 15; // %
        q.Down_Payment_Paid_To__c = 'Supplier';
        q.Security_Deposit_Rate__c = 10; // %
        q.Upfront_Fee_Rate__c = 0.1; // %
        q.Closing_Price_Rate__c = 0.1; // %
        q.Commission_to_Supplier__c = 50000000;
        q.Other_Expenses_Born_by_BSL__c = 5000000;
        q.Susidy_from_Supplier__c = 45000000;
        
        insert q;
        Asset__c asset = MultiDisbursementTest.createAsset('Finance Lease', q);
        Several_Disbursement__c severalDisb1 = MultiDisbursementTest.createSeveralDisb(q, '1st', dt);
        Several_Disbursement__c severalDisb2 = MultiDisbursementTest.createSeveralDisb(q, '2nd', dt.addMonths(3));
        
        return q;
    }
    
    public static Several_Disbursement__c createSeveralDisb(Quote quote, String termDisb, Date dateDisb){
        Several_Disbursement__c severalDisb = new Several_Disbursement__c();
        severalDisb.Disbursment_Date__c = dateDisb;
        severalDisb.Asset_Price__c = 50; //'%'
        severalDisb.Other_Costs__c = 50; //'%'
        severalDisb.Installation__c = 50; //'%'
        severalDisb.Insurance__c = 50; //'%'
        severalDisb.Maintenance__c = 50; //'%'
        severalDisb.Quote__c = quote.id;
        severalDisb.Disbursement__c = termDisb;
        insert severalDisb;
        return severalDisb;
    }
    
    @isTest static void testOnlyPrincipalAndAccumulated() {
        
        Quote quote = MultiDisbursementTest.createQuote('Finance Lease', 'Only Principal', 'Accumulated to the Repayment Term after Grace', 3);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        MultiDisbursement r = new MultiDisbursement(sc);
        Test.stopTest();
    }
    
    @isTest static void testOnlyInterestAndAccumulated() {
        
        Quote quote = MultiDisbursementTest.createQuote('Finance Lease', 'Only Interest', 'Accumulated to the Repayment Term after Grace', 3);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        MultiDisbursement r = new MultiDisbursement(sc);
        Test.stopTest();
    }
    
    @isTest static void testBothAndAccumulated() {
        
        Quote quote = MultiDisbursementTest.createQuote('Finance Lease', 'Both Principal and Interest', 'Accumulated to the Repayment Term after Grace', 3);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        MultiDisbursement r = new MultiDisbursement(sc);
        Test.stopTest();
    }
    
    
    @isTest static void testOnlyPrincipalAndEquated() {
        
        Quote quote = MultiDisbursementTest.createQuote('Finance Lease', 'Only Principal', 'Equated by Remaining Rrepayment Term', 3);
        
        Test.startTest();
        System.debug('start tesst');
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        System.debug('init QuotePdfController');
        MultiDisbursement r = new MultiDisbursement(sc);
        Test.stopTest();
        System.debug('stop tesst');
    }
    
    @isTest static void testBothAndEquated() {
        
        Quote quote = MultiDisbursementTest.createQuote('Finance Lease', 'Both Principal and Interest', 'Equated by Remaining Rrepayment Term', 3);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        MultiDisbursement r = new MultiDisbursement(sc);
        Test.stopTest();
    }
}