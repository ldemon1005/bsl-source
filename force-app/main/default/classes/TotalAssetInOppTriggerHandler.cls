/**
 *
 *
 */
public with sharing class TotalAssetInOppTriggerHandler {
    
    /**
     *
     */
    public static void setTotalAssetInOpportunity(List<Asset__c> assetList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // List of Opportunity Id that need to be removed
        List<String> removedOppIdList = new List<String>();
        // List of Opportunity Id that is filted
        List<String> filtedOppIdList = new List<String>();
        // List of Opportunity that need to be updated
        List<Opportunity> updateOppList = new List<Opportunity>();

        for(Asset__c a : assetList) {
            oppIdList.add(a.Opportunity__c);
        }
        
        List<Quote> qList = [SELECT Id, OpportunityId, Status
                             FROM Quote
                             WHERE OpportunityId IN :oppIdList AND Status = 'Tentatively Accepted by Customer'];

        for(Quote q : qList) {
            removedOppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Id, Total_Expected_Potential_Contract_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList AND Id NOT IN :removedOppIdList];

        for(Opportunity opp : oppList) {
            filtedOppIdList.add(opp.Id);
        }

        List<Asset__c> astFiltedList = [SELECT Id, Name, Expected_Contract_Amount__c, Opportunity__c
                                        FROM Asset__c
                                        WHERE Opportunity__c IN :filtedOppIdList];
        System.debug('oppList size: ' + oppList.size());
        System.debug('astFiltedList size: ' + astFiltedList.size());

        for(Opportunity o : oppList) {
            String oppId = o.Id;
            Decimal oppPtAmount = 0;
            System.debug('oppId: ' + oppId);
            System.debug('Total Expected Potential/Contract Amount: ' + oppPtAmount);

            for(Asset__c at : astFiltedList) {
                if(oppId.equals(at.Opportunity__c)) {
                    Decimal astExpectedAmount = at.Expected_Contract_Amount__c;
                    System.debug('IN LOOPS GET NAME : ' + at.Name);
                    System.debug('IN LOOPS: ' + at.Expected_Contract_Amount__c);
                    oppPtAmount += astExpectedAmount;
                }
            }
            System.debug('Result: ' + oppPtAmount);

            o.Total_Expected_Potential_Contract_Amount__c = oppPtAmount;
            updateOppList.add(o);
        }
        System.debug('List of updateOppList: ' + updateOppList.size());

        update updateOppList;
    }
}