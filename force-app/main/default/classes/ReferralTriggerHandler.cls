/** 
*  CMC System Integration Co., Ltd
*  ReferralTriggerHandler
*
*  @description handling the ReferralTrigger
*  @author tqtai
*  @since 18/1/2019 
*/
public with sharing class ReferralTriggerHandler {
    /**
    * setAmountFieldValue method
    * Set all of amount field in Referral to 0 when creating
    *
    * @param List<Referral__c> listRef
    */
    public static void setAmountFieldValue(List<Referral__c> listRef) {
        // List of Referral need to be updated
        List<Referral__c> updatedRefList = new List<Referral__c>();

        for(Referral__c r : listRef) {
            r.Total_Developed_Approved_Amount__c = 0;
            r.Total_Developed_Contracted_Amount__c = 0;
            r.Total_Developed_Disbursed_Amount__c = 0;
            r.Total_Developed_Number_of_Opportunities__c = 0;
            r.Total_Developed_Potential_Amount__c = 0;

            updatedRefList.add(r);
        }
    }

    /**
    * updateContactInReferral method
    * If SCF Referral is created or updated => all Contacts of Supplier will be child of this Referral
    *
    * @param  listRef listRef 
    */
    public static void updateContactInReferral(List<Referral__c> listRef) {
        // List of Supplier Id
        List<String> supplierIdList = new List<String>();

        for(Referral__c ref : listRef) {
            supplierIdList.add(ref.Supplier__c);
        }

        RecordType rt = [SELECT Id, Name 
                         FROM RecordType 
                         WHERE SobjectType = 'Referral__c' AND Name = 'Supply Chain Finance'
                         LIMIT 1];

        RecordType rtSup = [SELECT Id, Name 
                            FROM RecordType 
                            WHERE SobjectType = 'Contact' AND Name = 'Supplier'
                            LIMIT 1];

        String rtSupId = rtSup.Id;

        System.debug('RecordType Id: ' + rt.Id);

        List<Contact> ctList = [SELECT Id, Supplier_Name__c, Referral_Name__c
                                FROM Contact
                                WHERE Supplier_Name__c IN :supplierIdList AND RecordTypeId = :rtSupId];

        for(Referral__c r : listRef) {
            String rtId = rt.Id;

            if(rtId.equals(r.RecordTypeId)) {
                String sId = r.Supplier__c;
                System.debug('Supplier Id: ' + sId);

                for(Contact c : ctList) {
                    if(sId.equals(c.Supplier_Name__c)) {
                        c.Referral_Name__c = r.Id;
                    }
                }
            }
        }

        update ctList;
    }
}