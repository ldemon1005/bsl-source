/**
 * @File Name          : AccountEditingRemindsTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/4/2019, 4:50:31 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/4/2019, 3:55:39 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest
public class AccountEditingRemindsTest {
	public static String CRON_EXP = '0 0 0 10 1 ? *';
	@isTest static void testschedule() {
		Test.startTest(); 
		AccountEditingReminds aer = new AccountEditingReminds();
		System.schedule('Testing', CRON_EXP, aer);
		Test.stopTest();

		/*Task tTest = [SELECT Id
				  FROM Task
				  WHERE Subject LIKE '%Today is 10th January, please update Annual Revenue and Potential Investment Plan for your Account'
				  LIMIT 1];*/

		//System.assertNotEquals(null, tTest);
    	}
}