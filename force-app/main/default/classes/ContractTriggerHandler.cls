/**
 *
 *
 */
public without sharing class ContractTriggerHandler {

    /**
    * updateOppStageName method
    * if Opportunity has Activated Contract => change Status to B
    *
    * @param: List<Contract> ctrList
    */
    public static void updateOppStageName(List<Contract> ctrList) {
        String activatedCtr = 'Activated';
        String ldOppStageName = 'L\'';

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Contract c : ctrList) {
            oppIdList.add(c.Opportunity_Name__c);
        }

        List<Opportunity> checkedOppList = [SELECT Id, StageName
                                            FROM Opportunity
                                            WHERE Id IN :oppIdList];

        List<Task> creatingContractTaskList = [SELECT Status, WhatId
                                               FROM Task
                                               WHERE WhatId IN :oppIdList AND Subject = 'Add Contract for this Opportunity'];

        List<Contract> checkedContractList = [SELECT Opportunity_Name__c, Status
                                              FROM Contract
                                              WHERE Opportunity_Name__c IN :oppIdList];

        
        for(Opportunity opp : checkedOppList) {
            String oppId = opp.Id;
            List<Contract> refCtrList = new List<Contract>();

            for(Contract ct : checkedContractList) {
                if(oppId.equals(ct.Opportunity_Name__c) && (activatedCtr.equals(ct.Status))) {
                    refCtrList.add(ct);
                }
            }

            System.debug('List of Ref Contract: ' + refCtrList.size());

            for(Contract ctr : ctrList) {
                if(oppId.equals(ctr.Opportunity_Name__c) && (activatedCtr.equals(ctr.Status)) && (refCtrList.size() <= 1) && !(ldOppStageName.equals(opp.StageName))) {
                    opp.StageName = 'B';
                }
            }

            for(Task t : creatingContractTaskList) {
                if(oppId.equals(t.WhatId)) {
                    t.Status = 'Completed';
                }
            }
        }

        update checkedOppList;
        update creatingContractTaskList;
        
    }

    /**
    * blockChangingStatusOfContract method
    * when Contract has status 'Activated' => cannot change Status of Opp
    *
    * @param: List<Contract> ctrList
    */
    public static void blockChangingStatusOfContract(List<Contract> ctrList) {
        // Activated Status of Contract
        String a = 'Activated';
        // List of Opportunity 
        List<String> oppIdList = new List<String>();

        for(Contract ctr : ctrList) {
            oppIdList.add(ctr.Opportunity_Name__c);
        }

        List<Opportunity> oppList = [SELECT Id, StageName
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList AND (StageName = 'B' OR StageName = 'A' OR StageName = 'L')];
        /*
        for(Contract c : ctrList) {
            String oppId = c.Opportunity_Name__c;

            for(Opportunity opp : oppList) {
                if(oppId.equals(opp.Id)) {
                    if(a.equals(c.Status)) {
                        c.addError('Cannot change Status of Contract now');
                    }
                }
            }    
        } */
    }

    /**
    *
    *
    */
    public static void setContractedAmountInOpportunity(List<Contract> ctrList) {
        String activatedStatus = 'Activated';

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        RecordType rt = [SELECT Id, Name
				         FROM RecordType
				         WHERE SobjectType = 'Contract' AND Name = 'Operating Lease'
                         LIMIT 1];
                         
        String operatingLeaseRtId = rt.Id;
        
        for(Contract c : ctrList) {
            oppIdList.add(c.Opportunity_Name__c);
        }

        List<Opportunity> oppList = [SELECT Id, Contracted_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        List<Contract> checkedCtrList = [SELECT Opportunity_Name__c, Contract_Amount__c, RecordTypeId, Contract_Amount_before_VAT__c
                                         FROM Contract
                                         WHERE Opportunity_Name__c IN :oppIdList AND Status = :activatedStatus];

        System.debug('Size of checked Contract List: ' + checkedCtrList.size());

        for(Opportunity o : oppList) {
            String oId = o.Id;
            Decimal ctrAmount = 0;

            for(Contract ctr : checkedCtrList) {
                if(oId.equals(ctr.Opportunity_Name__c)) {

                    if(operatingLeaseRtId.equals(ctr.RecordTypeId)) {
                        ctrAmount += ctr.Contract_Amount_before_VAT__c;
                    }
                    else {
                        if(ctr.Contract_Amount__c != null) {
                            ctrAmount += ctr.Contract_Amount__c;
                        }
                    }
                }
            }

            o.Contracted_Amount__c = ctrAmount;

            System.debug('New Contract Amount: ' + ctrAmount);
        }

        update oppList;
    }

    /**
    * setNotificationWhenContractIsActivated method
    * When Contract is activated => set Notification for salesman
    *
    * @param List<Contract> ctrList
    */
    public static void setNotificationWhenContractIsActivated(List<Contract> ctrList) {
        RecordType rt = [SELECT Id 
                         FROM RecordType 
                         WHERE SobjectType = 'Contract' AND Name = 'Co-operation'
                         LIMIT 1];

        String coOpRtId = rt.Id;

        for(Contract c : ctrList) {
            String stt = c.Status;

            if(stt.equals('Activated') && !coOpRtId.equals(c.RecordTypeId)) {
                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                mentionSegmentInput.id = c.OwnerId;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = 'This Contract is activated!';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = c.Id;
                feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
            }
        }
    }


    /**
     * createNotificationForAllUserWhenCreatingNewCoOpContract description
     * @param  cpList  cpList description
     * @param  body    body description
     * @param  comment comment description
     */
    public static void createNotificationForAllUserWhenCreatingNewCoOpContract(List<Contract> ctrList, String body, String comment) {
        RecordType rt = [SELECT Id 
                         FROM RecordType 
                         WHERE SobjectType = 'Contract' AND Name = 'Co-operation'
                         LIMIT 1];

        String coOpRtId = rt.Id;

        // List of Salesman Dev Name
        List<String> salesmanDevNameList = new List<String>{'DanangBusiness1PartnerUser','HanoiBusiness1PartnerUser','HoChiMinhBusiness1PartnerUser'};
        // List of User Role Id
        List<String> srIdList = new List<String>();
        // List of User Role
        List<UserRole> srList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :salesmanDevNameList];

        for(UserRole ur : srList) {
            srIdList.add(ur.Id);
        }

        // List of Director User
        List<User> smList = [SELECT Id 
                             FROM User
                             WHERE UserRoleId IN :srIdList
                             LIMIT 10];

        System.debug('Checked User List Size: ' + smList.size());

        // List of User Role Dev Name
        List<String> urDevNameList = new List<String>{'BSLPartnerManager',
                                                      'DanangBusiness1PartnerManager',
                                                      'HanoiBusiness1PartnerManager',
                                                      'HoChiMinhBusiness1PartnerManager',
                                                      'Deputy_Director_of_Hanoi_Branch',
                                                      'Deputy_Director_of_Ho_Chi_Minh_Branch',
                                                      'Director_of_Danang_Branch',
                                                      'Director_of_Hanoi_Branch',
                                                      'Director_of_Ho_Chi_Minh_Branch',
                                                      'Manager_of_Danang_Business_1',
                                                      'Manager_of_Danang_Business_2',
                                                      'Manager_of_Hanoi_Business_1',
                                                      'Manager_of_Hanoi_Business_2',
                                                      'Manager_of_Ho_Chi_Minh_Business_1',
                                                      'Manager_of_Ho_Chi_Minh_Business_2'};
        // List of User Role Id
        List<String> urIdList = new List<String>();
        // List of User Role
        List<UserRole> urList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :urDevNameList];

        for(UserRole ur : urList) {
            urIdList.add(ur.Id);
        }

        // List of Director User
        List<User> uList = [SELECT Id 
                            FROM User
                            WHERE UserRoleId IN :urIdList
                            LIMIT 10];

        System.debug('Checked User List Size: ' + uList.size());
        
        for(Contract c : ctrList) {
            if(coOpRtId.equals(c.RecordTypeId)) {
                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                for(User u : uList) {
                    System.debug('IN LOOP 2222222222');
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput.id = u.Id;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                }

                textSegmentInput.text = body;
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = c.Id;
                feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;

                System.debug('IN LOOP 111111111');

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);

                System.debug('Feed Element Id: ' + feedElement.Id);


                // Add comments
                String communityId = null;
                String feedElementId = feedElement.Id;

                ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
                ConnectApi.MessageBodyInput messageBodyInput1 = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput1 = new ConnectApi.TextSegmentInput();

                messageBodyInput1.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                for(User u : smList) {
                    System.debug('IN LOOP 33333333');
                    ConnectApi.MentionSegmentInput mentionSegmentInput1 = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput1.id = u.Id;
                    messageBodyInput1.messageSegments.add(mentionSegmentInput1);
                }

                textSegmentInput1.text = comment;
                messageBodyInput1.messageSegments.add(textSegmentInput1);

                commentInput.body = messageBodyInput1;

                ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(communityId, feedElementId, commentInput, null);
            }            
        }
    }

    /**
    *
    */
    public static void addNewCoOperationContract(List<Contract> ctrList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Contract' AND Name = 'Co-operation'
                        LIMIT 1];
                
        String rtId = rt.Id;

        String coOperation = rtId;

        Account a = [SELECT Id
                     FROM Account
                     WHERE Name = '[BSL]-sharing--line'
                     LIMIT 1];

        for(Contract c : ctrList) {
            if(coOperation.equals(c.RecordTypeId)) {
                c.AccountId = a.Id;
            }
        }
    }
    /*
     * update Amount contract tren account
	*/
    public static void updateAmountContract(Set<Id> accounts){
        if(accounts.size()>0){
            List<Contract> contracts = [Select id,Contract_Amount_before_VAT__c, Contract_Amount__c, VATinQuote__c,AccountId FRom Contract Where AccountId IN :accounts];
            List<Account> listAcc = new List<Account>();
            for(Id i : accounts){
                if(i==null){
                    continue;
                }
                Decimal totalAmountContract = 0;
                Decimal totalVAT = 0;
                for(Contract con : contracts){
                    if(con.AccountId == i){
                        if(con.Contract_Amount_before_VAT__c!=null){
                        totalAmountContract += con.Contract_Amount_before_VAT__c;
                        }
                        if(con.VATinQuote__c != null ){
                            totalVAT += con.VATinQuote__c;
                        }
                    }
                }
                Account a = new Account(id=i, Contract_Amount_before_VAT__c= totalAmountContract - totalVAT);
                listAcc.add(a);
            }
            if(listAcc.size()>0){
                update listAcc;
            }
        }
    }
}