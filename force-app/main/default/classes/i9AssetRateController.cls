public class i9AssetRateController {
    public String irrString{get;set;}
    public Double irrDouble{get;set;}
    public Quote quote{get;set;}
    public List<Asset__c> listAsset {get;set;}
    public List<Repayment> listRepay {get;set;}
    public String renderingService { get; private set; }
    public  String todayString { get; private set; }
    public Decimal InstallmentAmount {get;set;}
    public Boolean isUpdateQuote = false;
    public i9AssetRateController(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()){
                controller.addFields(new List<String>{'Id','Name', 'Lease_Term__c','Leaseable_Asset_Value__c','Repayment_Date__c','ExpirationDate','Frequency_of_Interest_Repayment__c',
                                    'Assets__r','Expected_Contract_Amount__c','Down_Payment_Rate__c','Lease_Term_for_Insurance__c','Security_Deposit_rate_Amount__c',
                                    'Upfront_Fee_Amount__c','Closing_Price_Amount__c','Fixed_Rate_In_The_First_Period__c','Expected_Disbursement_Date__c','Margin__c',
                                    'Interest_Base__c','Number_Of_Months_For_Fixed_Rate__c','IRR__C', 'Susidy_from_Supplier__c'
                    , 'Other_Expenses_Born_by_BSL__c'
                    , 'Commission_to_Supplier__c'
                    , 'Total_Asset_Value_with_VAT__c'
                    , 'Upfront_Fee_Rate__c'
                    , 'One_time_Repayment_of_VAT__c'
                    , 'Account.Name'
                    });
            }
            
        try{
            quote = (Quote)controller.getRecord();
            Decimal numberOfRepayment = 36;
            Integer interestRepayment = 1;
            if(quote.Frequency_of_Interest_Repayment__c !=null & quote.Lease_Term__c !=null){
                interestRepayment = integer.valueof(quote.Frequency_of_Interest_Repayment__c);
                numberOfRepayment = quote.Lease_Term__c;
                numberOfRepayment = numberOfRepayment / interestRepayment;
            }
            Decimal preValueDebt =0;
            Repayment r0 = new Repayment(quote);
            r0.RepaymentTerm = 0;
            r0.RepaymentDate = quote.Expected_Disbursement_Date__c;
            r0.RepaymentDateStr = r0.RepaymentDate.format();
            r0.SecurityDeposit =  quote.Security_Deposit_rate_Amount__c;
            System.debug(quote.Susidy_from_Supplier__c);
        	decimal upFeeAmount = quote.Total_Asset_Value_with_VAT__c * (quote.Upfront_Fee_Rate__c / 100) / (decimal)1.1;
            r0.UpfrontOrClosingFee = upFeeAmount
                + (quote.Susidy_from_Supplier__c == null ? 0 : quote.Susidy_from_Supplier__c)
                - (quote.Other_Expenses_Born_by_BSL__c == null ? 0 : quote.Other_Expenses_Born_by_BSL__c)
                - (quote.Commission_to_Supplier__c == null ? 0 : quote.Commission_to_Supplier__c)
                ;
            r0.OutstandingPrincipal = -quote.Expected_Contract_Amount__c;
            r0.Cashflow = r0.OutstandingPrincipal + (r0.SecurityDeposit == null ? 0:r0.SecurityDeposit) 
                + (r0.UpfrontOrClosingFee == null ? 0 : r0.UpfrontOrClosingFee) ;
            listRepay = new List<Repayment>();
            listRepay.add(r0);
            Double FixedInterestRate = ((quote.Margin__c == null ? 0:quote.Margin__c) + (quote.Interest_Base__c == null ? 0:quote.Interest_Base__c))/100;
            Double FixedInterestRateValue = 1/(1+(FixedInterestRate/12));
            Integer numberDayPre = 0;
            Double SumDiscfactor = (double)0;
            List<Double> cashflows = new List<Double>();
            cashflows.add(r0.Cashflow);
            for(Decimal i =0; i<numberOfRepayment;i++){
                Repayment r = new Repayment(quote);
                r.RepaymentTerm = integer.valueof(i)+1;
                r.caculateIR9(integer.valueof(i),numberDayPre);
           		//r.Discfactor = Math.pow(FixedInterestRateValue, r.CumulativeDay/r.DaysInMonth);
           		Double realRate = quote.Fixed_Rate_In_The_First_Period__c/100;
                Double dayRate = (double)r.CumulativeDay/(double)r.DaysInMonth;
           		r.Discfactor = 1/Math.pow((1+realRate/12), dayRate);
                numberDayPre = r.CumulativeDay;
                SumDiscfactor += (double)r.Discfactor;
                listRepay.add(r);
          	}
            decimal totalVATPrinciplePayment = 0;
            for(Decimal i =1; i<= numberOfRepayment;i++){
                Repayment r = listRepay.get(integer.valueof(i));
                r.InterestPayment = ((i==1 ? -1:1 ) *listRepay.get(integer.valueof(i)-1).OutstandingPrincipal * (quote.Fixed_Rate_In_The_First_Period__c) *r.DaysInMonth)/ (365*100);
                //r.InstallmentAmount = quote.Expected_Contract_Amount__c/SumDiscfactor;
                if (i == numberOfRepayment) {
                    r.InstallmentAmount = (decimal)listRepay.get(integer.valueof(i)-1).OutstandingPrincipal + (decimal)r.InterestPayment;
                } else {
                    r.InstallmentAmount = quote.Expected_Contract_Amount__c/SumDiscfactor;
                }
                if (i == numberOfRepayment) {
                    r.PrinciplePaymentVAT = listRepay.get(integer.valueof(i)-1).OutstandingPrincipal;
                } else {
                    r.PrinciplePaymentVAT = r.InstallmentAmount -  r.InterestPayment;
                }
                
                r.OutstandingPrincipal = (i==1 ? -1:1 )*listRepay.get(integer.valueof(i)-1).OutstandingPrincipal - r.PrinciplePaymentVAT;
                r.PrinciplePayment = r.PrinciplePaymentVAT /1.1;
                r.VATPrinciplePayment = r.PrinciplePaymentVAT - r.PrinciplePayment;    
                
                r.Cashflow = r.InstallmentAmount;
                if(i==numberOfRepayment){
                    r.OutstandingPrincipal = null;
                    r.PrinciplePaymentVAT =  listRepay.get(integer.valueof(i)-1).OutstandingPrincipal;
                    r.SecurityDeposit = - quote.Security_Deposit_rate_Amount__c;
                    r.UpfrontOrClosingFee = quote.Upfront_Fee_Amount__c + quote.Closing_Price_Amount__c;
                	r.Cashflow = r.InstallmentAmount + (r.UpfrontOrClosingFee == null ? 0: r.UpfrontOrClosingFee) + (r.SecurityDeposit == null ?0: r.SecurityDeposit);    
                }
                cashflows.add(r.Cashflow);
          	}            
            listRepay.get(1).DFSum = SumDiscfactor;
            listRepay.get(1).SumOfDays = listRepay.get(integer.valueof(numberOfRepayment)).CumulativeDay;
            
            Double guess= 0.1;
            irrString = IRR.caculateIRR(cashflows,guess);
            Double irrNumber = Double.valueOf(irrString);
            irrDouble = (Math.pow((1+irrNumber),(12 /interestRepayment)) -1) *100;
            Decimal irrDec = irrDouble;
            irrDec= irrDec.setscale(2);
            irrString = irrDec+'%';
            System.debug('IRR');
            System.debug(quote.Irr__c);
            if(quote.Irr__c != irrDec){
                 quote.Irr__c = irrDec;
                isUpdateQuote = true;
        	}
            /*
            Double guess= 0.1;
            irrString = IRR.caculateIRR(cashflows,guess);
            Double irrNumber = Double.valueOf(irrString);
            irrDouble = (Math.pow((1+irrNumber),(12 /interestRepayment)) -1) *100;
            Decimal irrDec = irrDouble;
            irrDec= irrDec.setscale(2,RoundingMode.UP);
            irrString = irrDec+'%';
            if(quote.Irr__c != irrDec && quote.Irr__c!=null){
                quote.Irr__c = irrDec;
                isUpdateQuote = true;
        	}
			*/
            Date today = System.Today();
            todayString = today.day() + '/' + today.month() + '/' + today.year();
        }catch(Exception e){
                System.debug(e.getMessage());
        }
    }
     public void updateIRR(){
         if(isUpdateQuote){
             update quote;
         }
    }
}