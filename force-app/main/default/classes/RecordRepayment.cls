public class RecordRepayment {
    public Quote quote;
    public static Decimal AssetPrice = 0;
    public static Decimal AssetPriceVAT = 0;
    public static Decimal AssetValue = 0;
    public static Decimal InsurancePremium  = 0;
    public static Decimal InsurancePremiumVat = 0;
    public static Decimal MaintenanceCost = 0;
    public static Decimal MaintenanceCostVAT = 0;
    public static Decimal OtherCosts = 0;
    public static Decimal OtherCostsVAT = 0;
    public static Decimal InstallationCosts = 0;
    public static Decimal InstallationCostsVAT = 0;    
    public static boolean firstAfrerGracePeriod = true;    
    
    public static List<Decimal> preAsset = new List<Decimal>();
    public static List<Decimal> preAssetVAT = new List<Decimal>();
    public static List<Decimal> preInsurance = new List<Decimal>();
    public static List<Decimal> preInsuranceVAT = new List<Decimal>();
    public static List<Decimal> preMaintenance = new List<Decimal>();
    public static List<Decimal> preMaintenanceVAT = new List<Decimal>();
    public static List<Decimal> preOtherCosts = new List<Decimal>();
    public static List<Decimal> preOtherCostsVAT = new List<Decimal>();
    public static List<Decimal> preInstallationCosts = new List<Decimal>();
    public static List<Decimal> preInstallationCostsVAT = new List<Decimal>();
    
    public static Decimal tempAsset = 0;
    public static Decimal tempAssetVAT = 0;
    public static Decimal tempInsurance = 0;
    public static Decimal tempInsuranceVAT = 0;
    public static Decimal tempMaintenance = 0;
    public static Decimal tempMaintenanceVAT = 0;
    public static Decimal tempOtherCosts = 0;
    public static Decimal tempOtherCostsVAT = 0;
    public static Decimal tempInstallationCosts = 0;
    public static Decimal tempInstallationCostsVAT = 0;
    public static Decimal tempInterestPayment = 0;
    
    public static date minSeveralDisbursement;
    
    Decimal tempRepaymentOfAssetPrice = 0;
    Decimal tempRepaymentOfAssetVAT = 0;
    Decimal tempRepaymentOfInsurance = 0;
    Decimal tempRepaymentOfInsuranceVAT = 0;
    Decimal tempRepaymentOfMaintenance = 0;
    Decimal tempRepaymentOfMaintenanceVAT = 0;
    Decimal tempRepaymentOfOtherCosts = 0;
    Decimal tempRepaymentOfOtherCostsVAT = 0;
    Decimal tempRepaymentOfInstallationCosts = 0;
    Decimal tempRepaymentOfInstallationCostsVAT = 0;
    
    public static Decimal insuranceTerm {get;set;}
    
    Public static Integer interestRepayment = 1;
    Public static Integer principalRepayment = 1;
    Public static Integer numRepayment = 1;
    
    public Integer RepaymentTerm{get;set;}
    public Date RepaymentDate{get;set;}
    public String RepaymentDateStr{get;set;}
    public Integer DaysInMonth{get;set;}
    public Decimal TotalPrincipalRepayment{get;set;}
    public Decimal TotalVATRepayment{get;set;}
    public Decimal TotalPrincipalRepaymentWithVAT{get;set;}
    public Decimal TotalOutstandingDebt {get;set;}
    public Decimal InterestPayment{get;set;}
    public Decimal TotalRepaymentByCustomer{get;set;}
    public Decimal RepaymentOfAssetPrice{get;set;}
    public Decimal RepaymentOfAssetVAT{get;set;}
    public Decimal RepaymentOfInsurance{get;set;}
    public Decimal RepaymentOfInsuranceVAT{get;set;}
    public Decimal RepaymentOfMaintenance{get;set;}
    public Decimal RepaymentOfMaintenanceVAT{get;set;}
    public Decimal RepaymentOfOtherCosts{get;set;}
    public Decimal RepaymentOfOtherCostsVAT{get;set;}
    public Decimal RepaymentOfInstallationCosts{get;set;}
    public Decimal RepaymentOfInstallationCostsVAT{get;set;}
    public Decimal SecurityDeposit{get;set;}
    public Decimal UpfrontOrClosingFee{get;set;}
    public Decimal Cashflow{get;set;}
    public decimal TotalLeaseRentPayment {get;set;}
    //
    public Integer CumulativeDay{get;set;}
    public Double Discfactor {get;set;}   
    public Double DFSum {get;set;}   
    public Integer SumOfDays {get;set;} 
    public Decimal InstallmentAmount {get;set;} 
    //
    public Decimal PrinciplePaymentVAT {get;set;} 
    public Decimal PrinciplePayment {get;set;} 
    public Decimal VATPrinciplePayment {get;set;} 
    public Decimal OutstandingPrincipal {get;set;} 
    public Decimal AccumulatedPrincipalpayment  {get;set;}
    public Decimal AccumulatedInterestPayment {get;set;}
    public Decimal AnnualFee {get;set;}
    
    //
    public Decimal ResidualValue {get;set;}
    public Decimal ResidualValueCollection {get;set;}
    
    public Decimal MonthlyLease {get;set;}
    public Decimal InterestCost {get;set;}
    public Decimal InterestCostForWaitingPeriod {get;set;}
    public Decimal TotalMonthly {get;set;}
    public Decimal LeaseRent {get;set;}
    public Decimal VATLeaseRent {get;set;}
    public Decimal LeaseRentVAT {get;set;}
    
    public RecordRepayment(Quote quote){
        this.quote = quote;
        TotalPrincipalRepayment = 0;
        TotalVATRepayment = 0;
        TotalPrincipalRepaymentWithVAT =0;
        InterestPayment = 0;
        TotalRepaymentByCustomer = 0;
        RepaymentOfAssetPrice = 0;
        RepaymentOfAssetVAT =0;
        RepaymentOfInsurance =0;
        RepaymentOfInsuranceVAT =0;
        RepaymentOfMaintenance =0;
        RepaymentOfMaintenanceVAT =0;
        RepaymentOfOtherCosts =0;
        RepaymentOfOtherCostsVAT =0;
        RepaymentOfInstallationCosts =0;
        RepaymentOfInstallationCostsVAT =0;
    }
    
    public void caculate(Integer i,Integer numDisb, Decimal preDebt, Decimal prePrincipal, Decimal nextDisbursement, Several_Disbursement__c disb){
        Decimal numberOfRepayment = quote.Lease_Term__c/RecordRepayment.numRepayment;
        Decimal dayInYear = 365;
        this.RepaymentTerm = i + 1;
        this.RepaymentDate = quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(this.RepaymentTerm*RecordRepayment.numRepayment));
        this.RepaymentDateStr = this.RepaymentDate.format();
        this.DaysInMonth = Integer.valueOf(quote.Expected_Disbursement_Date__c.addMonths(i*RecordRepayment.numRepayment).daysBetween(this.RepaymentDate));
        boolean isInGradePeriod = false;
        boolean isPrincipalTerm = false;
        boolean isInterestTerm = false;
        if(quote.Grace_Period__c != null && quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(quote.Grace_Period__c)) >= this.RepaymentDate){
            isInGradePeriod = true;
        }
        if(nextDisbursement != 0){
            caculateRepayment(i, disb, numDisb); 
        }
        
        if(Math.mod(this.RepaymentTerm*RecordRepayment.numRepayment, Integer.valueOf(quote.Frequency_of_Principal_Repayment__c)) == 0){
            isPrincipalTerm = true;
        }
        if(Math.mod(this.RepaymentTerm*RecordRepayment.numRepayment, Integer.valueOf(quote.Frequency_of_Interest_Repayment__c)) == 0){
            isInterestTerm = true;
        }
        //caculate principal 
        if(isPrincipalTerm == true){
            if(isInGradePeriod == true && (quote.Grace_for__c == 'Only Principal' || quote.Grace_for__c == 'Both Principal and Interest')){
                // tính lại giá trị principal tại các lần giải ngân.
                Decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0;
                for(Integer k = 0;k < numDisb;k++){
                    temp1 += RecordRepayment.preAsset[k] == null ? 0 : RecordRepayment.preAsset[k];
                    temp2 += RecordRepayment.preAssetVAT[k] == null ? 0 : RecordRepayment.preAssetVAT[k];
                    temp3 += RecordRepayment.preMaintenance[k] == null ? 0 : RecordRepayment.preMaintenance[k];
                    temp4 += RecordRepayment.preMaintenanceVAT[k] == null ? 0 : RecordRepayment.preMaintenanceVAT[k];
                    temp5 += RecordRepayment.preOtherCosts[k] == null ? 0 : RecordRepayment.preOtherCosts[k];
                    temp6 += RecordRepayment.preOtherCostsVAT[k] == null ? 0 : RecordRepayment.preOtherCostsVAT[k];
                    temp7 += RecordRepayment.preInstallationCosts[k] == null ? 0 : RecordRepayment.preInstallationCosts[k];
                    temp8 += RecordRepayment.preInstallationCostsVAT[k] == null ? 0 : RecordRepayment.preInstallationCostsVAT[k];
                    
                    if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) >= this.RepaymentDate){
                        temp9 += RecordRepayment.preInsurance[k] == null ? 0 : RecordRepayment.preInsurance[k];
                        temp10 += RecordRepayment.preInsuranceVAT[k] == null ? 0 : RecordRepayment.preInsuranceVAT[k];
                    }
                }
                
                RecordRepayment.tempAsset += temp1;
                RecordRepayment.tempAssetVAT += temp2;
                RecordRepayment.tempMaintenance += temp3;
                RecordRepayment.tempMaintenanceVAT += temp4;
                RecordRepayment.tempOtherCosts += temp5;
                RecordRepayment.tempOtherCostsVAT += temp6;
                RecordRepayment.tempInstallationCosts += temp7;
                RecordRepayment.tempInstallationCostsVAT += temp8;   
                if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) >= this.RepaymentDate){
                    RecordRepayment.tempInsurance += temp9;
                    RecordRepayment.tempInsuranceVAT += temp10;
                }   
            }else {
                for(Integer k = 0;k < numDisb;k++){
                    this.RepaymentOfAssetPrice += RecordRepayment.preAsset[k] == null ? 0 : RecordRepayment.preAsset[k];
                    this.RepaymentOfAssetVAT += RecordRepayment.preAssetVAT[k] == null ? 0 : RecordRepayment.preAssetVAT[k];
                    this.RepaymentOfMaintenance += RecordRepayment.preMaintenance[k] == null ? 0 : RecordRepayment.preMaintenance[k];
                    this.RepaymentOfMaintenanceVAT += RecordRepayment.preMaintenanceVAT[k] == null ? 0 : RecordRepayment.preMaintenanceVAT[k];
                    this.RepaymentOfOtherCosts += RecordRepayment.preOtherCosts[k] == null ? 0 : RecordRepayment.preOtherCosts[k];
                    this.RepaymentOfOtherCostsVAT += RecordRepayment.preOtherCostsVAT[k] == null ? 0 : RecordRepayment.preOtherCostsVAT[k];
                    this.RepaymentOfInstallationCosts += RecordRepayment.preInstallationCosts[k] == null ? 0 : RecordRepayment.preInstallationCosts[k];
                    this.RepaymentOfInstallationCostsVAT += RecordRepayment.preInstallationCostsVAT[k] == null ? 0 : RecordRepayment.preInstallationCostsVAT[k];
                    
                    if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) >= this.RepaymentDate){
                        this.RepaymentOfInsurance += RecordRepayment.preInsurance[k] == null ? 0 : RecordRepayment.preInsurance[k];
                        this.RepaymentOfInsuranceVAT += RecordRepayment.preInsuranceVAT[k] == null ? 0 : RecordRepayment.preInsuranceVAT[k];
                    }
                }  
                if(quote.Grace_Period__c != null){
                    Decimal monthsBetween = quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(quote.Grace_Period__c)).monthsBetween(this.RepaymentDate);
                    if(quote.Payment_Method_for_Grace__c == 'Accumulated to the Repayment Term after Grace'){
                        if(monthsBetween <= quote.Grace_Period__c && RecordRepayment.firstAfrerGracePeriod == true){
                            this.RepaymentOfAssetPrice += RecordRepayment.tempAsset;
                            this.RepaymentOfAssetVAT += RecordRepayment.tempAssetVAT;
                            this.RepaymentOfMaintenance += RecordRepayment.tempMaintenance;
                            this.RepaymentOfMaintenanceVAT += RecordRepayment.tempMaintenanceVAT;
                            this.RepaymentOfOtherCosts += RecordRepayment.tempOtherCosts;
                            this.RepaymentOfOtherCostsVAT += RecordRepayment.tempOtherCostsVAT;
                            this.RepaymentOfInstallationCosts += RecordRepayment.tempInstallationCosts;
                            this.RepaymentOfInstallationCostsVAT += RecordRepayment.tempInstallationCostsVAT;
                            
                            if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) >= this.RepaymentDate){
                                this.RepaymentOfInsurance += RecordRepayment.tempInsurance;
                                this.RepaymentOfInsuranceVAT += RecordRepayment.tempInsuranceVAT;
                            }
                            RecordRepayment.firstAfrerGracePeriod = false;
                        }
                    }
                    if(quote.Payment_Method_for_Grace__c == 'Equated by Remaining Rrepayment Term'){
                        if(monthsBetween <= quote.Grace_Period__c){
                            RecordRepayment.tempAsset = RecordRepayment.tempAsset/(numberOfRepayment - i);
                            RecordRepayment.tempAssetVAT = RecordRepayment.tempAssetVAT/(numberOfRepayment - i);
                            RecordRepayment.tempMaintenance = RecordRepayment.tempMaintenance/(numberOfRepayment - i);
                            RecordRepayment.tempMaintenanceVAT = RecordRepayment.tempMaintenanceVAT/(numberOfRepayment - i);
                            RecordRepayment.tempOtherCosts = RecordRepayment.tempOtherCosts/(numberOfRepayment - i);
                            RecordRepayment.tempOtherCostsVAT = RecordRepayment.tempOtherCostsVAT/(numberOfRepayment - i);
                            RecordRepayment.tempInstallationCosts = RecordRepayment.tempInstallationCosts/(numberOfRepayment - i);
                            RecordRepayment.tempInstallationCostsVAT = RecordRepayment.tempInstallationCostsVAT/(numberOfRepayment - i);   
                            if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) >= this.RepaymentDate){
                                RecordRepayment.tempInsurance = RecordRepayment.tempInsurance/(numberOfRepayment - i);
                                RecordRepayment.tempInsuranceVAT = RecordRepayment.tempInsuranceVAT/(numberOfRepayment - i);
                            }
                        }
                        this.RepaymentOfAssetPrice += RecordRepayment.tempAsset;
                        this.RepaymentOfAssetVAT += RecordRepayment.tempAssetVAT;
                        this.RepaymentOfMaintenance += RecordRepayment.tempMaintenance;
                        this.RepaymentOfMaintenanceVAT += RecordRepayment.tempMaintenanceVAT;
                        this.RepaymentOfOtherCosts += RecordRepayment.tempOtherCosts;
                        this.RepaymentOfOtherCostsVAT += RecordRepayment.tempOtherCostsVAT;
                        this.RepaymentOfInstallationCosts += RecordRepayment.tempInstallationCosts;
                        this.RepaymentOfInstallationCostsVAT += RecordRepayment.tempInstallationCostsVAT;
                        
                        if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) >= this.RepaymentDate){
                            this.RepaymentOfInsurance += RecordRepayment.tempInsurance;
                            this.RepaymentOfInsuranceVAT += RecordRepayment.tempInsuranceVAT;
                        }
                    }
                }
            }
        }
        // làm tròn
        this.RepaymentOfAssetPrice = this.RepaymentOfAssetPrice.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfAssetVAT = this.RepaymentOfAssetVAT.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfMaintenance = this.RepaymentOfMaintenance.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfMaintenanceVAT = this.RepaymentOfMaintenanceVAT.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfOtherCosts = this.RepaymentOfOtherCosts.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfOtherCostsVAT = this.RepaymentOfOtherCostsVAT.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfInstallationCosts = this.RepaymentOfInstallationCosts.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfInstallationCostsVAT = this.RepaymentOfInstallationCostsVAT.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfInsurance = this.RepaymentOfInsurance.round(System.RoundingMode.HALF_EVEN);
        this.RepaymentOfInsuranceVAT = this.RepaymentOfInsuranceVAT.round(System.RoundingMode.HALF_EVEN);
        //end caculate principal 
        
        this.TotalPrincipalRepayment = this.RepaymentOfAssetPrice + this.RepaymentOfMaintenance + this.RepaymentOfOtherCosts + this.RepaymentOfInstallationCosts + this.RepaymentOfInsurance;
        this.TotalVATRepayment = this.RepaymentOfAssetVAT + this.RepaymentOfMaintenanceVAT + this.RepaymentOfOtherCostsVAT + this.RepaymentOfInstallationCostsVAT + RepaymentOfInsuranceVAT;
        this.TotalPrincipalRepaymentWithVAT = this.TotalPrincipalRepayment + this.TotalVATRepayment;
        this.TotalOutstandingDebt = preDebt - prePrincipal - nextDisbursement;
        
        //caculate interest 
        if(quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(quote.Number_Of_Months_For_Fixed_Rate__c)) >= this.RepaymentDate){
            if(isInGradePeriod == true && (quote.Grace_for__c == 'Only Interest' || quote.Grace_for__c == 'Both Principal and Interest')){
                RecordRepayment.tempInterestPayment += ((this.TotalOutstandingDebt*quote.Fixed_Rate_In_The_First_Period__c/(dayInYear*100)))*this.DaysInMonth;
                this.InterestPayment = 0;
            }else{
                this.InterestPayment = ((this.TotalOutstandingDebt*quote.Fixed_Rate_In_The_First_Period__c/(dayInYear*100)))*this.DaysInMonth;
            }
        }else{
            if(isInGradePeriod == true && (quote.Grace_for__c == 'Only Interest' || quote.Grace_for__c == 'Both Principal and Interest')){
                RecordRepayment.tempInterestPayment += ((this.TotalOutstandingDebt*(quote.Interest_Base__c + quote.Margin__c))/(dayInYear*100))*this.DaysInMonth;
                this.InterestPayment = 0;
            }else{
                this.InterestPayment = ((this.TotalOutstandingDebt*(quote.Interest_Base__c + quote.Margin__c)/(dayInYear*100)))*this.DaysInMonth;
            }
        }
        if(isInGradePeriod == false && quote.Grace_Period__c != null){
            Decimal monthsBetween = quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(quote.Grace_Period__c)).monthsBetween(this.RepaymentDate);
            if(quote.Payment_Method_for_Grace__c == 'Accumulated to the Repayment Term after Grace'){
                if(monthsBetween <= 3){
                    this.InterestPayment += RecordRepayment.tempInterestPayment;
                }
            }
            if(quote.Payment_Method_for_Grace__c == 'Equated by Remaining Rrepayment Term'){
                if(monthsBetween <= 3){
                    RecordRepayment.tempInterestPayment = RecordRepayment.tempInterestPayment/(numberOfRepayment - i);
                }
                this.InterestPayment += RecordRepayment.tempInterestPayment;
            }
        }
        //end caculate interest
        this.TotalRepaymentByCustomer = this.InterestPayment + this.TotalPrincipalRepaymentWithVAT;
        this.Cashflow = this.TotalRepaymentByCustomer + (this.SecurityDeposit == null ? 0 : this.SecurityDeposit) + (this.UpfrontOrClosingFee == null ? 0 : this.UpfrontOrClosingFee);
    }
    
    //caculate principal affter disbursement
    public void caculateRepayment(Integer i, Several_Disbursement__c disb, Integer numDisb){
        Decimal numberOfRepayment = (quote.Lease_Term__c - i*RecordRepayment.numRepayment)/RecordRepayment.principalRepayment;
        
        RecordRepayment.insuranceTerm = 1;
        if(quote.Lease_Term_for_Insurance__c != null && RecordRepayment.principalRepayment != null && RecordRepayment.principalRepayment != 0){
         	RecordRepayment.insuranceTerm = quote.Lease_Term_for_Insurance__c/RecordRepayment.principalRepayment;   
        }
        
        Decimal preAsset = 0;
        Decimal preAssetVAT = 0;
        Decimal preInsurance = 0;
        Decimal preInsuranceVAT = 0;
        Decimal preMaintenance = 0;
        Decimal preMaintenanceVAT = 0;
        Decimal preOtherCosts = 0;
        Decimal preOtherCostsVAT = 0;
        Decimal preInstallationCosts = 0;
        Decimal preInstallationCostsVAT = 0;
        
        RecordRepayment.preAsset.add(RecordRepayment.AssetPrice == null || disb.Asset_Price__c == null ? preAsset : ((RecordRepayment.AssetPrice*disb.Asset_Price__c/100)/numberOfRepayment));
        RecordRepayment.preAssetVAT.add(RecordRepayment.AssetPriceVAT == null || disb.Asset_Price__c == null ? preAssetVAT : ((RecordRepayment.AssetPriceVAT*disb.Asset_Price__c/100)/numberOfRepayment));
        RecordRepayment.preOtherCosts.add(RecordRepayment.OtherCosts == null || disb.Other_Costs__c == null ? preOtherCosts : ((RecordRepayment.OtherCosts*disb.Other_Costs__c/100)/numberOfRepayment));
        RecordRepayment.preOtherCostsVAT.add(RecordRepayment.OtherCostsVAT == null || disb.Other_Costs__c == null ? preOtherCostsVAT : ((RecordRepayment.OtherCostsVAT*disb.Other_Costs__c/100)/numberOfRepayment));
        RecordRepayment.preInstallationCosts.add(RecordRepayment.InstallationCosts == null || disb.Installation__c == null ? preInstallationCosts : ((RecordRepayment.InstallationCosts*disb.Installation__c/100)/numberOfRepayment));
        RecordRepayment.preInstallationCostsVAT.add(RecordRepayment.InstallationCostsVAT == null || disb.Installation__c == null ? preInstallationCostsVAT : ((RecordRepayment.InstallationCostsVAT*disb.Installation__c/100)/numberOfRepayment));
        RecordRepayment.preMaintenance.add( RecordRepayment.MaintenanceCost == null || disb.Maintenance__c == null ? preMaintenance : ((RecordRepayment.MaintenanceCost*disb.Maintenance__c/100)/numberOfRepayment));
        RecordRepayment.preMaintenanceVAT.add(RecordRepayment.MaintenanceCostVAT == null || disb.Maintenance__c == null ? preMaintenanceVAT : ((RecordRepayment.MaintenanceCostVAT*disb.Maintenance__c/100)/numberOfRepayment));
        
        if(RecordRepayment.minSeveralDisbursement != null && RecordRepayment.minSeveralDisbursement.addMonths(Integer.valueOf(quote.Lease_Term_for_Insurance__c)) > quote.Expected_Disbursement_Date__c.addMonths(Integer.valueOf(quote.Lease_Term__c))){
            RecordRepayment.insuranceTerm = numberOfRepayment;
        }
        RecordRepayment.preInsurance.add(RecordRepayment.InsurancePremium == null || disb.Insurance__c == null ? preInsurance : ((RecordRepayment.InsurancePremium*disb.Insurance__c/100)/(RecordRepayment.insuranceTerm)));
        RecordRepayment.preInsuranceVAT.add(RecordRepayment.InsurancePremiumVAT == null || disb.Insurance__c == null ? preInsuranceVAT : ((RecordRepayment.InsurancePremiumVAT*disb.Insurance__c/100))/(RecordRepayment.insuranceTerm));
    }
    
    public integer lcm(Integer x,Integer y){
        Integer a, b,t, gcd, lcm;
        a = x;
        b = y;
        while (b != 0) {
            t = b;
            b = math.mod(a,b);
            a = t;
        }
        gcd = a;
        lcm = (x*y)/gcd;
        return lcm;
    }
}