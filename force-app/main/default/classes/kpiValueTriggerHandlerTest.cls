/**
 * @File Name          : kpiValueTriggerHandlerTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/5/2019, 3:09:49 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/5/2019, 3:09:49 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest(SeeAllData=true)
private class kpiValueTriggerHandlerTest{

	@isTest static void test1() {
        String kpiYearRecordType = '';
        String kpiQuarterRecordType = '';
        String kpiValueQuarterRecordType = '';
        String kpiValueYearRecordType = '';
        
        list<RecordType> rtKpis = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'KPI__c'];
        for(RecordType rt : rtKpis){
            if(rt.Name == 'Quarter') kpiQuarterRecordType = rt.Id; 
            if(rt.Name == 'Years') kpiYearRecordType = rt.Id; 
        }
        
        list<RecordType> rtKpiValues = [SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'KPI_Value__c'];
        System.debug(rtKpiValues);
        for(RecordType rtValue : rtKpiValues){
            if(rtValue.Name == 'Quarter') kpiValueQuarterRecordType = rtValue.Id; 
            if(rtValue.Name == 'Year') kpiValueYearRecordType = rtValue.Id; 
        }
        
		Test.startTest();
        Opportunity opp = new Opportunity();
        opp.Name='Test' ;
        opp.StageName = 'D1';
        opp.CloseDate = System.today() + 5;
        Insert opp ;
        System.debug(kpiQuarterRecordType);
        KPI__c kpi = new KPI__C();
        kpi.Name = 'Test';
        kpi.Year__c = '2019';
        kpi.KPI_Type__c = 'Quarter';
        kpi.RecordTypeId = kpiQuarterRecordType;//cần thay dữ liểu khi lên product
        Insert kpi;
        
        KPI__c kpi_year = new KPI__C();
        kpi_year.Name = 'Test year';
        kpi_year.Year__c = '2019';
        kpi_year.KPI_Type__c = 'Year';
        kpi_year.RecordTypeId = kpiYearRecordType;//cần thay dữ liểu khi lên product
        Insert kpi_year;
        
        KPI_Value__c kpi_value_q1 = new KPI_Value__C();
        kpi_value_q1.RecordTypeId = kpiValueQuarterRecordType;//cần thay dữ liểu khi lên product
        kpi_value_q1.KPI__c = kpi.id;
        kpi_value_q1.Quarter__c = 'Q1';
        
        KPI_Value__c kpi_value_q2 = new KPI_Value__C();
        kpi_value_q2.RecordTypeId = kpiValueQuarterRecordType;//cần thay dữ liểu khi lên product
        kpi_value_q2.KPI__c = kpi.id;
        kpi_value_q2.Quarter__c = 'Q2';
        
        KPI_Value__c kpi_value_q3 = new KPI_Value__C();
        kpi_value_q3.RecordTypeId = kpiValueQuarterRecordType;//cần thay dữ liểu khi lên product
        kpi_value_q3.KPI__c = kpi.id;
        kpi_value_q3.Quarter__c = 'Q3';
        
        KPI_Value__c kpi_value_q4 = new KPI_Value__C();
        kpi_value_q4.RecordTypeId = kpiValueQuarterRecordType;//cần thay dữ liểu khi lên product
        kpi_value_q4.KPI__c = kpi.id;
        kpi_value_q4.Quarter__c = 'Q4';
        
        KPI_Value__c kpi_value_y1 = new KPI_Value__C();
        kpi_value_y1.RecordTypeId = kpiValueYearRecordType;//cần thay dữ liểu khi lên product Year
        kpi_value_y1.KPI__c = kpi_year.id;
        
        list<KPI_Value__c> list_kpi_value = new list<KPI_Value__c>();
        list_kpi_value.add(kpi_value_q1);
        list_kpi_value.add(kpi_value_q2);
        list_kpi_value.add(kpi_value_q3);
        list_kpi_value.add(kpi_value_q4);
        list_kpi_value.add(kpi_value_y1);
        
        Insert list_kpi_value;
            
		kpiValueTriggerHandler.updateActualCrossSellSmtb(list_kpi_value);
		kpiValueTriggerHandler.updateActualCrossSellSmtpfc(list_kpi_value);
		kpiValueTriggerHandler.updateActualScf(list_kpi_value);
		kpiValueTriggerHandler.updateActualBslSelfDevelop(list_kpi_value);
		kpiValueTriggerHandler.updateActualCustomerInitiate(list_kpi_value);
		kpiValueTriggerHandler.updateActualOther(list_kpi_value);
		Test.stopTest();
		
		System.assertEquals('test1','test1');
		
	}
}