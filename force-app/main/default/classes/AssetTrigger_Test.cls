@IsTest(SeeAllData=true)
public class AssetTrigger_Test {
	@isTest static void TestInsertOpportunity() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        // 
        Opportunity opp1= new Opportunity();
        opp1.Name='Test' ;
        opp1.StageName = 'D1';
        opp1.CloseDate = System.today() + 5;
        insert opp1;
        
        Supplier__c supplier = new Supplier__c();
        supplier.Name = 'supplier test';
        insert supplier;
        
        Asset__c asset = new Asset__c();
        asset.Name = 'asset test';
        asset.Opportunity__c = opp1.Id;
        asset.Supplier__c = supplier.Id;
        asset.Currency__c = 'USD';
        asset.Expected_Contract_Amount__c = 5;
		insert asset;        
        // Perform test
        Test.startTest();
        Database.DeleteResult result = Database.delete(asset, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestDeleteOpportunity() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Opportunity opp1= new Opportunity();
        opp1.Name='Test' ;
        opp1.StageName = 'D1';
        opp1.CloseDate = System.today() + 5;
        insert opp1;
        
        Supplier__c supplier = new Supplier__c();
        supplier.Name = 'supplier test';
        insert supplier;
        
        Asset__c asset = new Asset__c();
        asset.Name = 'asset test';
        asset.Opportunity__c = opp1.Id;
        asset.Supplier__c = supplier.Id;
        asset.Currency__c = 'USD';
        asset.Expected_Contract_Amount__c = 5;
		insert asset;
        // Perform test
        Test.startTest();
        Database.SaveResult srList = Database.insert(asset, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(srList.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestUpdateOpportunity() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
       	Opportunity opp1= new Opportunity();
        opp1.Name='Test' ;
        opp1.StageName = 'D1';
        opp1.CloseDate = System.today() + 5;
        insert opp1;
        
        Supplier__c supplier = new Supplier__c();
        supplier.Name = 'supplier test';
        insert supplier;
        
        Asset__c asset = new Asset__c();
        asset.Name = 'asset test';
        asset.Opportunity__c = opp1.Id;
        asset.Supplier__c = supplier.Id;
        asset.Currency__c = 'USD';
        asset.Expected_Contract_Amount__c = 5;
		insert asset;
        
        // Perform test
        Test.startTest();
        asset.Name = 'Test';
        asset.Asset_Value_Before_Tax__c = 5;
        asset.Quantity__c = 5;
        asset.Expected_Contract_Amount__c = 5;
        Database.SaveResult result = Database.update(asset, false);
        delete asset;
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
}