public class QuoteExtension {
    public ApexPages.StandardSetController  controller;
    
    public QuoteExtension(ApexPages.StandardController constructor) {
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
    }
    
    public String currentRecordId {get;set;}
}