public class ReportDisbursementController {
    public List<OpportunityReport> oppReports{get;set;}
    public Boolean hasFiveWeek{get;set;}
    public String nextMonthName{get;set;}
    public String preMonthName{get;set;}
    public String week1Name{get;set;}
    public String week2Name{get;set;}
    public String week3Name{get;set;}
    public String week4Name{get;set;}
    public String week5Name{get;set;}
    public Integer numberWeekFirstDateOfMonth;
    Public Date lastDateOfMonth;
    public Date firstDateOfMonth;
    public ReportDisbursementController(){
        oppReports = new List<OpportunityReport>();
        init();
        System.debug(oppReports);
        getNameDate();
    } 
    public void getNameDate(){
        DateTime preMonth = Date.newInstance(Date.today().year(),Date.today().month()-1,1);
        preMonthName =  preMonth.format('MMMMM');
        DateTime nextMonth = Date.newInstance(Date.today().year(),Date.today().month()+1,1);
        nextMonthName = nextMonth.format('MMMMM');
        Integer endDateOfFirstWeek =0;
        System.debug('first:' + getWeekOfYear(firstDateOfMonth));
        for(Integer i = 1 ; i< 8; i++){
            Date firstDateOf2Week = Date.newInstance(Date.today().year(),Date.today().month(),i);
            System.debug(getWeekOfYear(firstDateOf2Week));
            if(checkWeek(firstDateOf2Week,numberWeekFirstDateOfMonth) == 2){
                endDateOfFirstWeek = i -1;
                break;
            }
        }
		week1Name = getNameWeek (1,endDateOfFirstWeek) ;  
        week2Name = getNameWeek (endDateOfFirstWeek + 1,endDateOfFirstWeek + 7) ;
        week3Name = getNameWeek (endDateOfFirstWeek + 8,endDateOfFirstWeek + 15) ;
        if(hasFiveWeek){
            week4Name = getNameWeek (endDateOfFirstWeek + 15,endDateOfFirstWeek + 22) ;
            week5Name = getNameWeek (endDateOfFirstWeek + 23,Date.daysInMonth(Date.today().year(),Date.today().month())) ;
        } else {
            week4Name = getNameWeek (endDateOfFirstWeek + 15,Date.daysInMonth(Date.today().year(),Date.today().month())) ;
            week5Name = '';
        }
        
    }
    public String getNameWeek(Integer startWeek, Integer endWeek){
        return startWeek +'/'+ Date.today().month() +  ' - '   +  endWeek +'/'+ Date.today().month() ;    
    }
    public void init(){
        Integer preMonth = Date.today().month() -1;
        Integer nextMonth = Date.today().month() +1;
        Integer currentYear = Date.today().Year() -1;
        List<Opportunity> Opps = [Select name, id, stagename, Referral_Type__c from Opportunity];
        List<Disbursement__c> disburs =[select name, Amount__c,Disbursement_Date__c,Opportunity__r.Name,Opportunity__r.StageName
                                        ,Opportunity__r.Referral_Type__c,Opportunity__r.Owner.firstname, Opportunity__r.Id 
                                        from Disbursement__c where CALENDAR_MONTH(Disbursement_Date__c) >= :preMonth
                                        AND CALENDAR_MONTH(Disbursement_Date__c) <= :nextMonth 
                                       AND CALENDAR_YEAR(Disbursement_Date__c) = :currentYear];
        firstDateOfMonth = Date.newInstance(Date.today().year(),Date.today().month(),1);
        numberWeekFirstDateOfMonth = getWeekOfYear(firstDateOfMonth);
        Integer daysInMonth = Date.daysInMonth(Date.today().year(),Date.today().month());
        lastDateOfMonth = Date.newInstance(Date.today().year(),Date.today().month(),daysInMonth);
        if(checkWeek(lastDateOfMonth,numberWeekFirstDateOfMonth) ==5){
            hasFiveWeek = true;
        } else {
            hasFiveWeek =false;
        }
        System.debug(hasFiveWeek);
        Set <Opportunity> lstOpp = new Set<Opportunity>();
        for(Disbursement__c d :disburs){
            if(d.Opportunity__r!=null){
                lstOpp.add(d.Opportunity__r);
            }
        }
        Integer stt = 1;
        for(Opportunity opp: lstOpp){
            OpportunityReport opr = new OpportunityReport();
            opr.stt = stt;
            stt ++;
            opr.Opp = opp;
            for(Disbursement__c d :disburs){
                
                switch on checkWeek(d.Disbursement_Date__c,numberWeekFirstDateOfMonth) {
                    when 0 {	
                        opr.AmountPreMonth += d.Amount__c == null ? 0:d.Amount__c;
                    }	
                    when 1 {	
                        opr.AWeek1 += d.Amount__c == null ? 0:d.Amount__c;
                    }	
                    when 2 {
                        opr.AWeek2 += d.Amount__c == null ? 0:d.Amount__c;
                    }
                    when 3 {
                        opr.AWeek3 += d.Amount__c == null ? 0:d.Amount__c;    
                    }
                    when 4 {
                        opr.AWeek4 += d.Amount__c == null ? 0:d.Amount__c;
                    }
                    when 5 {
                        opr.AWeek5 += d.Amount__c == null ? 0:d.Amount__c;
                    }
                    when else {	
                        opr.AmountNextMonth += d.Amount__c == null ? 0:d.Amount__c;
                    }
                 }
             }
            oppReports.add(opr);
        }
		
        
    }
    public Integer checkWeek(Date startdate, Integer numberWeekFirstDateOfMonth){
        Integer result = getWeekOfYear(startdate) - numberWeekFirstDateOfMonth + 1;
        if(result <= 0){
            result = 0;
        }
        return result;
    }
    public static Integer getWeekOfYear(Date startdate){
        Date todaydateinstance = date.newinstance(startdate.year(), startdate.month(), startdate.day());
        Integer currentyear = startdate.year();
        Date yearstartdate = date.newinstance(currentyear, 01, 01);
        Date year2ndweek = yearstartdate.adddays(7).tostartofweek();
        if(startdate<year2ndweek)
        return 1;
        integer numberDaysDue = year2ndweek.daysBetween(todaydateinstance);
        Integer numberOfWeek;
        if(math.mod(numberDaysDue,7)==0)
        numberOfWeek = math.MOD(Integer.valueof(math.FLOOR( ( numberDaysDue )/7)),52)+1;
        else
        numberOfWeek = math.MOD(Integer.valueof(math.FLOOR( ( numberDaysDue )/7)),52)+2;
        return numberOfWeek;
    }
    public class OpportunityReport{
        public Opportunity Opp{get;set;}
        public Integer stt{get;set;}
        public Decimal AmountPreMonth{get;set;}
        public Decimal AWeek1{get;set;}
        public Decimal AWeek2{get;set;}
        public Decimal AWeek3{get;set;}
        public Decimal AWeek4{get;set;}
        public Decimal AWeek5{get;set;}
        public Decimal AmountNextMonth{get;set;}
        public OpportunityReport(){
            AmountPreMonth = 0;
            AWeek1= 0;
            AWeek2= 0;
            AWeek3= 0;
            AWeek4= 0;
            AWeek5= 0;
            stt = 0;
            AmountNextMonth= 0;
        }
    }
}