public with sharing class AddReferralsToCampaignExtension {
	public ApexPages.StandardSetController  controller;

    public AddReferralsToCampaignExtension(ApexPages.StandardSetController  constructor) {
 		controller = constructor;
        ReferralId = '';
		ReferralList = controller.getSelected();  
       	for(Referral__c o : ReferralList) {
            ReferralId += o.id + ',';
        }
 	}
    
     public AddReferralsToCampaignExtension(ApexPages.StandardController constructor) {
      	currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
     }
    
    public String currentRecordId {get;set;}
    transient public String ReferralId  {get; set;}
    
    transient public List<Referral__c> ReferralList  {get; set;}
    

    public pageReference deleteReferrals(){
		delete controller.getSelected();
        return controller.cancel();
 	}
	
    public String getReferralIdSelected(){
        return ReferralId;
    }
}