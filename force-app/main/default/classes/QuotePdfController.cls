public class QuotePdfController {
    public String styleTable{get;set;}
    public String irrString{get;set;}
    public Double irrDouble{get;set;}
    public Decimal irrDec;
    public Quote quote{get;set;}
    public List<Asset__c> listAsset {get;set;}
    public List<Repayment> listRepay {get;set;}
    public  String renderingService { get; private set; }
    public  String todayString { get; private set; }
    public Boolean isUpdateQuote = false;
    public QuotePdfController(ApexPages.StandardController controller) {
        try{
            styleTable = 'floatingStyle';
            if (!Test.isRunningTest()){
                controller.addFields(new List<String>{'Id','Name', 'Lease_Term__c','Leaseable_Asset_Value__c','Repayment_Date__c','ExpirationDate','Frequency_of_Interest_Repayment__c',
                    'Assets__r','Expected_Contract_Amount__c','Down_Payment_Rate__c','Lease_Term_for_Insurance__c','Security_Deposit_rate_Amount__c',
                    'Upfront_Fee_Amount__c','Closing_Price_Amount__c','Fixed_Rate_In_The_First_Period__c','Expected_Disbursement_Date__c','Margin__c',
                    'Interest_Base__c','Number_Of_Months_For_Fixed_Rate__c','IRR__c','Susidy_from_Supplier__c','Commission_to_Supplier__c',
                    'Other_Expenses_Born_by_BSL__c','Grace_Period__c','Grace_for__c','Payment_Method_for_Grace__c'
                    , 'Other_Expenses_Born_by_BSL__c'
                    , 'Commission_to_Supplier__c'
                    , 'Total_Asset_Value_with_VAT__c'
                    , 'Upfront_Fee_Rate__c'
                    , 'One_time_Repayment_of_VAT__c'
                    , 'Account.Name'
                    });
            }
            quote = (Quote)controller.getRecord();
            caculateRepayment(quote);
            Date today = System.Today();
            todayString = today.day() + '/' + today.month() + '/' + today.year();
        } catch(Exception e){
            System.debug(e.getMessage());
        }
        
    }
    public void caculateRepayment(Quote quote){
        try{
            listAsset = [Select Name,Asset_Price_After_Tax__c,VAT_Amount_of_Asset__c,Insurance_Premium__c,
                         VAT_of_Insurance__c,Maintenance_Cost__c,VAT_of_Maintenance__c,Other_Costs__c,VAT_of_Other_Cost__c,Installation_Cost__c,VAT_of_Installation__c
                         from Asset__c where Quote__c=:quote.Id];
            for(Asset__c asset : listAsset){
                if(asset.Asset_Price_After_Tax__c!=null){
                    Repayment.AssetPrice+=asset.Asset_Price_After_Tax__c;
                }
                if(asset.VAT_Amount_of_Asset__c!=null){
                    Repayment.VATofAsset+=asset.VAT_Amount_of_Asset__c;
                }
                if(asset.Insurance_Premium__c!=null){
                    Repayment.InsurancePremium+=asset.Insurance_Premium__c;
                }
                if(asset.VAT_of_Insurance__c!=null){
                    Repayment.VATofInsurance+=asset.VAT_of_Insurance__c;
                }
                if(asset.Maintenance_Cost__c!=null){
                    Repayment.MaintenanceCost+=asset.Maintenance_Cost__c;
                }
                if(asset.VAT_of_Maintenance__c!=null){
                    Repayment.VATofMaintenance+=asset.VAT_of_Maintenance__c;
                }
                Repayment.OtherCosts+=((asset.Other_Costs__c == null ? 0:asset.Other_Costs__c) +(asset.Installation_Cost__c == null ? 0:asset.Installation_Cost__c));
                Repayment.VATofOtherCost+=((asset.VAT_of_Other_Cost__c == null ? 0:asset.VAT_of_Other_Cost__c) +(asset.VAT_of_Installation__c == null ? 0:asset.VAT_of_Installation__c));
                
            }
            Repayment.AssetValue = (Repayment.AssetPrice == null ? 0:Repayment.AssetPrice) + (Repayment.VATofAsset == null ? 0:Repayment.VATofAsset);
            Decimal numberOfRepayment = 36;
            Integer interestRepayment = 1;
            if(quote.Frequency_of_Interest_Repayment__c !=null & quote.Lease_Term__c !=null){
                interestRepayment = integer.valueof(quote.Frequency_of_Interest_Repayment__c);
                numberOfRepayment = quote.Lease_Term__c;
                numberOfRepayment = numberOfRepayment / interestRepayment;
            }
            Decimal preValueDebt =0;
            if(quote.Expected_Contract_Amount__c !=null){
                preValueDebt = quote.Expected_Contract_Amount__c;
            }
            Repayment r0 = new Repayment(quote);
            r0.RepaymentTerm = 0;
            r0.TotalOutstandingDebt = - preValueDebt;
            r0.SecurityDeposit = quote.Security_Deposit_rate_Amount__c;
            
            decimal upFeeAmount = (quote.Total_Asset_Value_with_VAT__c == null ? 0 : quote.Total_Asset_Value_with_VAT__c) * (quote.Upfront_Fee_Rate__c / 100) / (decimal)1.1;
            r0.UpfrontOrClosingFee = upFeeAmount
                + (quote.Susidy_from_Supplier__c == null ? 0 : quote.Susidy_from_Supplier__c)
                - (quote.Other_Expenses_Born_by_BSL__c == null ? 0 : quote.Other_Expenses_Born_by_BSL__c)
                - (quote.Commission_to_Supplier__c == null ? 0 : quote.Commission_to_Supplier__c)
                ;
            r0.Cashflow = (r0.TotalOutstandingDebt == null ? 0:r0.TotalOutstandingDebt) + ( r0.SecurityDeposit == null ? 0: r0.SecurityDeposit) 
                + ( r0.UpfrontOrClosingFee == null ? 0: r0.UpfrontOrClosingFee) ;
            listRepay = new List<Repayment> ();
            listRepay.add(r0);
            Repayment rN = new Repayment(quote, true);
            List<Double> cashflows = new List<Double>();
            List<Date> dates = new List<Date>();
            //cashflows.add(r0.Cashflow);
            dates.add(quote.Expected_Disbursement_Date__c);
            for(Decimal i =0; i<numberOfRepayment; i++){
                Repayment r = new Repayment(quote);
                r.RepaymentTerm = integer.valueof(i)+1;
                r.caculate(integer.valueof(i),preValueDebt);
                preValueDebt = r.TotalOutstandingDebt;
                if(i == (numberOfRepayment -1 )){
                    r.UpfrontOrClosingFee = quote.Closing_Price_Amount__c;
                    r.TotalOutstandingDebt = null;
                    r.TotalPrincipalRepaymentWithVAT = listRepay.get(integer.valueof(numberOfRepayment) -1).TotalOutstandingDebt;
                    r.SecurityDeposit = (listRepay.get(0) == null || listRepay.get(0).SecurityDeposit == null) 
                        ? 0 
                        : - listRepay.get(0).SecurityDeposit;
                    r.Cashflow += ((r.SecurityDeposit == null ? 0 : r.SecurityDeposit) + (r.UpfrontOrClosingFee == null ? 0 : r.UpfrontOrClosingFee));
                }
                
                listRepay.add(r);
                cashflows.add(r.Cashflow ==null?0:r.Cashflow);
                dates.add(r.RepaymentDate);    
                
                rN.TotalPrincipalRepayment += (r.TotalPrincipalRepayment == null ? 0:r.TotalPrincipalRepayment);
                rN.TotalVATRepayment += (r.TotalVATRepayment  == null ? 0: r.TotalVATRepayment );
                rN.TotalPrincipalRepaymentWithVAT += (r.TotalPrincipalRepaymentWithVAT  == null ? 0: r.TotalPrincipalRepaymentWithVAT );
                rN.InterestPayment +=(r.InterestPayment == null ? 0:r.InterestPayment);
                rN.TotalRepaymentByCustomer += (r.TotalRepaymentByCustomer == null ? 0 :r.TotalRepaymentByCustomer);
                rN.RepaymentOfAssetPrice +=  (r.RepaymentOfAssetPrice  == null ? 0: r.RepaymentOfAssetPrice );
                rN.RepaymentOfAssetVAT +=  (r.RepaymentOfAssetVAT  == null ? 0: r.RepaymentOfAssetVAT );
                rN.RepaymentOfInsurance +=  (r.RepaymentOfInsurance  == null ? 0: r.RepaymentOfInsurance );
                rN.RepaymentOfInsuranceVAT += (r.RepaymentOfInsuranceVAT  == null ? 0: r.RepaymentOfInsuranceVAT);
                rN.RepaymentOfMaintenace +=  (r.RepaymentOfMaintenace  == null ? 0: r.RepaymentOfMaintenace );
                rN.RepaymentOfMaintenanceVAT +=  (r.RepaymentOfMaintenanceVAT  == null ? 0: r.RepaymentOfMaintenanceVAT );
                rN.RepaymentOfOtherCosts +=  (r.RepaymentOfOtherCosts  == null ? 0: r.RepaymentOfOtherCosts );
                rN.RepaymentOfOtherCostsVAT +=  (r.RepaymentOfOtherCostsVAT  == null ? 0: r.RepaymentOfOtherCostsVAT );
            }
            System.debug(interestRepayment);
            caseGraceMonth(interestRepayment);
            
            List<decimal> newCashflows = new List<decimal>();
            for(integer i = 0; i < listRepay.size(); i++) {
                if (listRepay.get(i).Cashflow == null)
                    newCashflows.add((decimal)0);
                else 
                    newCashflows.add(listRepay.get(i).Cashflow);
            }
            Double guess= 0.1;
            irrString = IRR.caculateIRR(newCashflows, guess);
            Double irrNumber = irrString != '#NUM!' ? Double.valueOf(irrString) : 0;
            irrDouble = (Math.pow((1+irrNumber),(12 /interestRepayment)) -1) *100;
            Decimal irrDec = irrDouble;
            irrDec= irrDec.setScale(2);
            irrString = irrDec+'%';
            if(quote.Irr__c != irrDec){
                quote.Irr__c = irrDec;
                isUpdateQuote = true;
            }
            Repayment rSum = new Repayment(quote, true);
            for (integer i = 1; i < listRepay.size(); i++) {
                Repayment repayment = listRepay.get(i);
                rSum.TotalPrincipalRepayment += repayment.TotalPrincipalRepayment == null ? 0 : repayment.TotalPrincipalRepayment;
                rSum.TotalVATRepayment += repayment.TotalVATRepayment == null ? 0 : repayment.TotalVATRepayment;
                rSum.TotalPrincipalRepaymentWithVAT += repayment.TotalPrincipalRepaymentWithVAT == null 
                    ? 0 
                    : repayment.TotalPrincipalRepaymentWithVAT;
                rSum.InterestPayment += repayment.InterestPayment == null ? 0 : repayment.InterestPayment;
                rSum.TotalRepaymentByCustomer += repayment.TotalRepaymentByCustomer == null ? 0 : repayment.TotalRepaymentByCustomer;
                rSum.RepaymentOfAssetPrice += repayment.RepaymentOfAssetPrice == null ? 0 : repayment.RepaymentOfAssetPrice;
                rSum.RepaymentOfAssetVAT += repayment.RepaymentOfAssetVAT == null ? 0 : repayment.RepaymentOfAssetVAT;
                rSum.RepaymentOfInsurance += repayment.RepaymentOfInsurance == null ? 0 : repayment.RepaymentOfInsurance;
                rSum.RepaymentOfInsuranceVAT += repayment.RepaymentOfInsuranceVAT == null ? 0 : repayment.RepaymentOfInsuranceVAT;
                rSum.RepaymentOfMaintenace += repayment.RepaymentOfMaintenace == null ? 0 : repayment.RepaymentOfMaintenace;
                rSum.RepaymentOfMaintenanceVAT += repayment.RepaymentOfMaintenanceVAT == null ? 0 : repayment.RepaymentOfMaintenanceVAT;
                rSum.RepaymentOfOtherCosts += repayment.RepaymentOfOtherCosts == null ? 0 : repayment.RepaymentOfOtherCosts;
                rSum.RepaymentOfOtherCostsVAT += repayment.RepaymentOfOtherCostsVAT == null ? 0 : repayment.RepaymentOfOtherCostsVAT;
            }
            listRepay.add(rSum);
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    public void caseGraceMonth(Integer interestRepayment){
        if(quote.Grace_for__c ==null & quote.Payment_Method_for_Grace__c==null & quote.Grace_Period__c ==null){
            return;
        }
        if(quote.Grace_for__c =='Only Principal' & quote.Payment_Method_for_Grace__c =='Accumulated to the Repayment Term after Grace'){
            System.debug('11111111111111');
            graceMonthOnlyPeincipal(interestRepayment);
        }
        if(quote.Grace_for__c =='Only Interest' & quote.Payment_Method_for_Grace__c=='Accumulated to the Repayment Term after Grace'){
            System.debug('2222222222222');
            graceMonthOnlyInterest(interestRepayment);
        }
        if(quote.Grace_for__c =='Both Principal and Interest' & quote.Payment_Method_for_Grace__c=='Accumulated to the Repayment Term after Grace'){
            System.debug('333333333333333333');
            graceMonthBothPrincipalAndInterest(interestRepayment);
        }
        if(quote.Grace_for__c =='Only Principal' & quote.Payment_Method_for_Grace__c =='Equated by Remaining Rrepayment Term'){
            System.debug('44444');
            graceMonthOnlyPeincipalWithEquated(interestRepayment);
        }
        if(quote.Grace_for__c =='Both Principal and Interest' & quote.Payment_Method_for_Grace__c =='Equated by Remaining Rrepayment Term'){
            System.debug('55555');
            graceMonthOnlyInterestAndPeincipalWithEquated(interestRepayment);
        }
    }
    
    public void graceMonthOnlyInterestAndPeincipalWithEquated(Integer interestRepayment){
        try{
            if(quote.Grace_Period__c !=null || (quote.Grace_Period__c/interestRepayment) >1){
                Integer gracePeriod  = integer.valueof(quote.Grace_Period__c/interestRepayment);
                Repayment reGreace = listRepay.get(gracePeriod + 1);
                //reGreace.InterestPayment = 0;
                for(Integer i =0 ;i< gracePeriod ;i++){
                    Repayment replace = listRepay.get(i+1);
                    resetRepaymentByGracePeriod(replace, null);
                    reGreace.InterestPayment += (replace.InterestPayment == null? 0 : replace.InterestPayment);
                    replace.InterestPayment = null;
                    replace.TotalRepaymentByCustomer = (replace.TotalPrincipalRepaymentWithVAT == null ? 0: replace.TotalPrincipalRepaymentWithVAT)
                        + (replace.InterestPayment == null ? 0: replace.InterestPayment);
                    replace.Cashflow = (replace.TotalRepaymentByCustomer == null ? 0 : replace.TotalRepaymentByCustomer) 
                        + (replace.SecurityDeposit == null ? 0 : replace.SecurityDeposit)
                        + (replace.UpfrontOrClosingFee == null ? 0 : replace.UpfrontOrClosingFee);
                    replace.Cashflow = replace.Cashflow == 0 ? null : replace.Cashflow;
                }
                reGreace.TotalRepaymentByCustomer = (reGreace.TotalPrincipalRepaymentWithVAT == null ? 0: reGreace.TotalPrincipalRepaymentWithVAT)
                    + (reGreace.InterestPayment == null ? 0: reGreace.InterestPayment);
                reGreace.Cashflow = (reGreace.TotalRepaymentByCustomer == null ? 0 : reGreace.TotalRepaymentByCustomer) 
                    + (reGreace.SecurityDeposit == null ? 0 : reGreace.SecurityDeposit)
                    + (reGreace.UpfrontOrClosingFee == null ? 0 : reGreace.UpfrontOrClosingFee);
            }
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    /*
* Truong hop grace For = Only peicipal
* tinh lai lai suat
* */
    public void graceMonthOnlyPeincipalWithEquated(Integer interestRepayment){
        try{
            if(quote.Grace_Period__c !=null || (quote.Grace_Period__c/interestRepayment) >1){
                Integer gracePeriod  = integer.valueof(quote.Grace_Period__c/interestRepayment);
                Repayment reGreace = listRepay.get(gracePeriod + 1);
                //resetRepaymentByGracePeriod(reGreace,0);
                for(Integer i =0 ;i< gracePeriod ;i++){
                    Repayment replace = listRepay.get(i+1);
                    resetRepaymentByGracePeriod(replace, null);
                    replace.TotalRepaymentByCustomer = (replace.TotalPrincipalRepaymentWithVAT == null ? 0: replace.TotalPrincipalRepaymentWithVAT)
                        + (replace.InterestPayment == null ? 0: replace.InterestPayment);
                    replace.TotalOutstandingDebt = -listRepay.get(0).TotalOutstandingDebt;
                    replace.Cashflow = (replace.TotalRepaymentByCustomer == null ? 0 : replace.TotalRepaymentByCustomer) 
                        + (replace.SecurityDeposit == null ? 0 : replace.SecurityDeposit)
                        + (replace.UpfrontOrClosingFee == null ? 0 : replace.UpfrontOrClosingFee);
                    
                }
                //reGreace.TotalOutstandingDebt = listRepay.get(gracePeriod).TotalOutstandingDebt - reGreace.TotalPrincipalRepaymentWithVAT;
                if(reGreace.RepaymentTerm * interestRepayment <= quote.Number_Of_Months_For_Fixed_Rate__c){
                    reGreace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * reGreace.quote.Fixed_Rate_In_The_First_Period__c/100)* reGreace.DaysInMonth /365;
                } else {
                    reGreace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * (reGreace.quote.Margin__c +reGreace.quote.Interest_Base__c )/100)* reGreace.DaysInMonth /365;
                }
                reGreace.TotalRepaymentByCustomer = (reGreace.TotalPrincipalRepaymentWithVAT == null ? 0: reGreace.TotalPrincipalRepaymentWithVAT)
                    + (reGreace.InterestPayment == null ? 0: reGreace.InterestPayment);
                reGreace.Cashflow = (reGreace.TotalRepaymentByCustomer == null ? 0 : reGreace.TotalRepaymentByCustomer) 
                    + (reGreace.SecurityDeposit == null ? 0 : reGreace.SecurityDeposit)
                    + (reGreace.UpfrontOrClosingFee == null ? 0 : reGreace.UpfrontOrClosingFee);
            }
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    /*
    * Truong hop grace For = Only peicipal
    * tinh lai lai suat
    * */
    public void graceMonthOnlyPeincipal(Integer interestRepayment){
        try{
            if(quote.Grace_Period__c !=null || (quote.Grace_Period__c/interestRepayment) >1){
                Integer gracePeriod  = integer.valueof(quote.Grace_Period__c/interestRepayment);
                Repayment reGreace = listRepay.get(gracePeriod + 1);
                //resetRepaymentByGracePeriod(reGreace,0);
                for(Integer i =0 ;i< gracePeriod ;i++){
                    Repayment replace = listRepay.get(i+1);
                    System.debug('replace.TotalVATRepayment');
                    System.debug(i);
                    System.debug(replace.TotalVATRepayment);
                    if(replace.RepaymentTerm * interestRepayment <= quote.Number_Of_Months_For_Fixed_Rate__c){
                        replace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * replace.quote.Fixed_Rate_In_The_First_Period__c/100)* replace.DaysInMonth /365;
                    } else {
                        replace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * (replace.quote.Margin__c +replace.quote.Interest_Base__c )/100)* replace.DaysInMonth /365;
                    }
                    reGreace.TotalPrincipalRepayment += replace.TotalPrincipalRepayment==null? 0:replace.TotalPrincipalRepayment;
                    reGreace.TotalVATRepayment += replace.TotalVATRepayment==null? 0:replace.TotalVATRepayment;
                    reGreace.TotalPrincipalRepaymentWithVAT += replace.TotalPrincipalRepaymentWithVAT==null? 0:replace.TotalPrincipalRepaymentWithVAT;
                    reGreace.RepaymentOfAssetPrice += replace.RepaymentOfAssetPrice==null? 0:replace.RepaymentOfAssetPrice;
                    reGreace.RepaymentOfAssetVAT += replace.RepaymentOfAssetVAT==null? 0:replace.RepaymentOfAssetVAT;
                    reGreace.RepaymentOfInsurance += replace.RepaymentOfInsurance==null? 0:replace.RepaymentOfInsurance;
                    reGreace.RepaymentOfInsuranceVAT += replace.RepaymentOfInsuranceVAT ==null? 0:replace.RepaymentOfInsuranceVAT;
                    reGreace.RepaymentOfMaintenace += replace.RepaymentOfMaintenace==null? 0:replace.RepaymentOfMaintenace;
                    reGreace.RepaymentOfMaintenanceVAT += replace.RepaymentOfMaintenanceVAT==null? 0:replace.RepaymentOfMaintenanceVAT;
                    reGreace.RepaymentOfOtherCosts += replace.RepaymentOfOtherCosts==null? 0:replace.RepaymentOfOtherCosts;
                    reGreace.RepaymentOfOtherCostsVAT += replace.RepaymentOfOtherCostsVAT==null? 0:replace.RepaymentOfOtherCostsVAT;
                    resetRepaymentByGracePeriod(replace, null);
                    if(replace.RepaymentTerm * interestRepayment <= quote.Number_Of_Months_For_Fixed_Rate__c){
                        replace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * replace.quote.Fixed_Rate_In_The_First_Period__c/100)* replace.DaysInMonth /365;
                    } else {
                        replace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * (replace.quote.Margin__c +replace.quote.Interest_Base__c )/100)* replace.DaysInMonth /365;
                    }
                    replace.TotalRepaymentByCustomer = (replace.TotalPrincipalRepaymentWithVAT == null ? 0: replace.TotalPrincipalRepaymentWithVAT)
                        + (replace.InterestPayment == null ? 0: replace.InterestPayment);
                    replace.TotalOutstandingDebt = -listRepay.get(0).TotalOutstandingDebt;
                    replace.Cashflow = (replace.TotalRepaymentByCustomer == null ? 0 : replace.TotalRepaymentByCustomer) 
                        + (replace.SecurityDeposit == null ? 0 : replace.SecurityDeposit)
                        + (replace.UpfrontOrClosingFee == null ? 0 : replace.UpfrontOrClosingFee);
                    
                }
                
                if(reGreace.RepaymentTerm * interestRepayment <= quote.Number_Of_Months_For_Fixed_Rate__c){
                    reGreace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * reGreace.quote.Fixed_Rate_In_The_First_Period__c/100)* reGreace.DaysInMonth /365;
                } else {
                    reGreace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * (reGreace.quote.Margin__c +reGreace.quote.Interest_Base__c )/100)* reGreace.DaysInMonth /365;
                }
                reGreace.TotalRepaymentByCustomer = (reGreace.TotalPrincipalRepaymentWithVAT == null ? 0: reGreace.TotalPrincipalRepaymentWithVAT)
                    + (reGreace.InterestPayment == null ? 0: reGreace.InterestPayment);
                reGreace.Cashflow = (reGreace.TotalRepaymentByCustomer == null ? 0 : reGreace.TotalRepaymentByCustomer) 
                    + (reGreace.SecurityDeposit == null ? 0 : reGreace.SecurityDeposit)
                    + (reGreace.UpfrontOrClosingFee == null ? 0 : reGreace.UpfrontOrClosingFee);
            }
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    /*
    * Truong hop grace For = Only Interest
    * tinh lai lai suat
    * */
    public void graceMonthOnlyInterest(Integer interestRepayment){
        try{
            if(quote.Grace_Period__c !=null || (quote.Grace_Period__c/interestRepayment) >1){
                Integer gracePeriod  = integer.valueof(quote.Grace_Period__c/interestRepayment);
                Repayment reGreace = listRepay.get(gracePeriod + 1);
                //reGreace.InterestPayment = 0;
                for(Integer i =0 ;i< gracePeriod ;i++){
                    Repayment replace = listRepay.get(i+1);
                    reGreace.InterestPayment += (replace.InterestPayment == null? 0:replace.InterestPayment);
                    replace.InterestPayment = null;
                    replace.TotalRepaymentByCustomer = (replace.TotalPrincipalRepaymentWithVAT == null ? 0: replace.TotalPrincipalRepaymentWithVAT)
                        + (replace.InterestPayment == null ? 0: replace.InterestPayment);
                    replace.Cashflow = (replace.TotalRepaymentByCustomer == null ? 0 : replace.TotalRepaymentByCustomer) 
                        + (replace.SecurityDeposit == null ? 0 : replace.SecurityDeposit)
                        + (replace.UpfrontOrClosingFee == null ? 0 : replace.UpfrontOrClosingFee);
                    replace.Cashflow = replace.Cashflow == 0 ? null : replace.Cashflow;
                }
                reGreace.TotalRepaymentByCustomer = (reGreace.TotalPrincipalRepaymentWithVAT == null ? 0: reGreace.TotalPrincipalRepaymentWithVAT)
                    + (reGreace.InterestPayment == null ? 0: reGreace.InterestPayment);
                reGreace.Cashflow = (reGreace.TotalRepaymentByCustomer == null ? 0 : reGreace.TotalRepaymentByCustomer) 
                    + (reGreace.SecurityDeposit == null ? 0 : reGreace.SecurityDeposit)
                    + (reGreace.UpfrontOrClosingFee == null ? 0 : reGreace.UpfrontOrClosingFee);
            }
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    /*
    * Both Principal and interest
    * tinh lai lai suat
    * */
    public void graceMonthBothPrincipalAndInterest(Integer interestRepayment){
        try{
            if(quote.Grace_Period__c !=null || (quote.Grace_Period__c/interestRepayment) >1){
                Integer gracePeriod  = integer.valueof(quote.Grace_Period__c/interestRepayment);
                Repayment reGreace = listRepay.get(gracePeriod + 1);
                //resetRepaymentByGracePeriod(reGreace,0);
                reGreace.TotalRepaymentByCustomer = 0;     
                reGreace.InterestPayment = 0;                
                reGreace.InterestPayment = -(listRepay.get(0).TotalOutstandingDebt * reGreace.quote.Fixed_Rate_In_The_First_Period__c/100)* listRepay.get(1).DaysInMonth /365;
                for(Integer i =0 ;i< gracePeriod ;i++){
                    Repayment replace = listRepay.get(i+1);                    
                    reGreace.TotalPrincipalRepayment += replace.TotalPrincipalRepayment==null? 0:replace.TotalPrincipalRepayment;
                    reGreace.TotalVATRepayment += replace.TotalVATRepayment==null? 0:replace.TotalVATRepayment;
                    reGreace.TotalPrincipalRepaymentWithVAT += replace.TotalPrincipalRepaymentWithVAT==null? 0:replace.TotalPrincipalRepaymentWithVAT;
                    reGreace.RepaymentOfAssetPrice += replace.RepaymentOfAssetPrice==null? 0:replace.RepaymentOfAssetPrice;
                    reGreace.RepaymentOfAssetVAT += replace.RepaymentOfAssetVAT==null? 0:replace.RepaymentOfAssetVAT;
                    reGreace.RepaymentOfInsurance += replace.RepaymentOfInsurance==null? 0:replace.RepaymentOfInsurance;
                    reGreace.RepaymentOfInsuranceVAT += replace.RepaymentOfInsuranceVAT ==null? 0:replace.RepaymentOfInsuranceVAT;
                    reGreace.RepaymentOfMaintenace += replace.RepaymentOfMaintenace==null? 0:replace.RepaymentOfMaintenace;
                    reGreace.RepaymentOfMaintenanceVAT += replace.RepaymentOfMaintenanceVAT==null? 0:replace.RepaymentOfMaintenanceVAT;
                    reGreace.RepaymentOfOtherCosts += replace.RepaymentOfOtherCosts==null? 0:replace.RepaymentOfOtherCosts;
                    reGreace.RepaymentOfOtherCostsVAT += replace.RepaymentOfOtherCostsVAT==null? 0:replace.RepaymentOfOtherCostsVAT;
                    resetRepaymentByGracePeriod(replace, null);
                    replace.InterestPayment = null;
                    replace.TotalRepaymentByCustomer = null;
                    replace.TotalOutstandingDebt = -listRepay.get(0).TotalOutstandingDebt;
                    replace.Cashflow = (replace.TotalRepaymentByCustomer == null ? 0 : replace.TotalRepaymentByCustomer) 
                        + (replace.SecurityDeposit == null ? 0 : replace.SecurityDeposit)
                        + (replace.UpfrontOrClosingFee == null ? 0 : replace.UpfrontOrClosingFee);
                    replace.Cashflow = replace.Cashflow == 0 ? null : replace.Cashflow;
                    
                    reGreace.InterestPayment += (replace.TotalOutstandingDebt * reGreace.quote.Fixed_Rate_In_The_First_Period__c/100)* listRepay.get(i+2).DaysInMonth /365;                    
                }
                reGreace.TotalRepaymentByCustomer = (reGreace.TotalPrincipalRepaymentWithVAT == null ? 0: reGreace.TotalPrincipalRepaymentWithVAT)
                    + (reGreace.InterestPayment == null ? 0: reGreace.InterestPayment);
                reGreace.Cashflow = (reGreace.TotalRepaymentByCustomer == null ? 0 : reGreace.TotalRepaymentByCustomer);
            }
        }catch(Exception e){
            System.debug(e.getMessage());
        }
    }
    public void resetRepaymentByGracePeriod(Repayment reGreace, Decimal value){
        reGreace.TotalPrincipalRepayment = value;
        reGreace.TotalVATRepayment = value;
        reGreace.TotalPrincipalRepaymentWithVAT = value;
        reGreace.RepaymentOfAssetPrice = value;
        reGreace.RepaymentOfAssetVAT = value;
        reGreace.RepaymentOfInsurance = value;
        reGreace.RepaymentOfInsuranceVAT = value;
        reGreace.RepaymentOfMaintenace = value;
        reGreace.RepaymentOfMaintenanceVAT = value;
        reGreace.RepaymentOfOtherCosts = value;
        reGreace.RepaymentOfOtherCostsVAT = value;
    }
    public void updateIRR(){
        if(isUpdateQuote){
            System.debug('IRwerR');
            System.debug(quote.Irr__c);
            update quote;
            
        }
    }
    
    // Allow the page to set the PDF file name
    public String renderedFileName { 
        get; 
        set { renderedFileName = this.sanitizeFileName(value); }
    }
    // Rendered content MIME type, used to affect HTTP response
    public String renderedContentType {
        get {
            String renderedContentType = 'text/html'; // the default
            
            if( ! this.renderingAsHtml() ) {
                // Provides a MIME type for a PDF document 
                renderedContentType = 'application/pdf';
                
                // Add a file name for the PDF file
                if( this.renderedFileName != null) {
                    // This is supposed to set the file name, but it doesn't work
                    renderedContentType += '#' + this.renderedFileName;
                    
                    // This is a work-around to set the file name
                    ApexPages.currentPage().getHeaders().put(
                        'content-disposition', 'attachment; filename=' + 
                        this.renderedFileName);
                }
            }
            
            return renderedContentType;
        }
    }
    
    // Are we rendering to HTML or PDF?
    public Boolean renderingAsHtml() {
        return ( (renderingService == null) || 
                ( ! renderingService.startsWith('PDF')) );
    }
    
    // Action method to save (or "print") to PDF
    public PageReference saveToPdf() {
        renderingService = 'PDF';
        return null;
    }
    
    // Private helper -- basic, conservative santization
    private String sanitizeFileName(String unsafeName) {
        String allowedCharacters = '0-9a-zA-Z-_.';
        String sanitizedName = 
            unsafeName.replaceAll('[^' + allowedCharacters + ']', '');
        // You might also want to check filename length, 
        // that the filename ends in '.pdf', etc.
        return(sanitizedName);
    }
    public PageReference  changeStyle(){
        if(styleTable == 'floatingStyle'){
            styleTable = 'fixme';
            return null;
        } else {
            styleTable = 'floatingStyle';
            System.debug(ApexPages.currentPage().getUrl() );
            PageReference pageRef =  new PageReference('/apex/RenderPdf?id='+quote.id);
            pageRef.setRedirect(TRUE);
            return pageRef;
        }
    }
    
}