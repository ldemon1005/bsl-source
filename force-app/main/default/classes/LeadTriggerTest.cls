@IsTest(SeeAllData=true)
public class LeadTriggerTest {
	@isTest static void TestInsertOpportunity() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Lead opp = new Lead(FirstName = 'ABC', LastName = 'XYZ',Company ='CMC');
        insert opp;
        
        // Perform test
        Test.startTest();
        Database.DeleteResult result = Database.delete(opp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestDeleteOpportunity() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Lead opp = new Lead(FirstName = 'ABC', LastName = 'XYZ',Company ='CMC');
        // Perform test
        Test.startTest();
        Database.SaveResult srList = Database.insert(opp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(srList.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestUpdateOpportunity() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Lead opp = new Lead(FirstName = 'ABC', LastName = 'XYZ',Company ='CMC');
       insert opp;
        
        // Perform test
        Test.startTest();
        opp.FirstName = 'Test';
        Database.SaveResult result = Database.update(opp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
}