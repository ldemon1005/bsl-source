@isTest
private class AccountTriggerHandlerTest {
    private static Referral__c createReferral() {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
                         LIMIT 1];
        // Cross-sell Bidv Record Type Id
        String csBidvRt = rt.Id;

        Referral__c r = new Referral__c();

        r.RecordTypeId = csBidvRt; // Cross-Sell BIDV record type
        r.Name = 'BIDV-123';

        insert r;

        return r;
    }

    private static Contact createContact(String rId) {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Contact' AND Name = 'Referral'
                         LIMIT 1];
        // BSLselfdev Record Type Id
        String refContactRt = rt.Id;

        Contact c = new Contact();

        c.RecordTypeId = refContactRt; // Referral Record Type
        c.LastName = 'Tran';
        c.Birthday__c = Date.newInstance(1994, 1, 30);
        c.Referral_Name__c = rId;

        insert c;

        return c;
    }

    private static void createAccount(String rId, String cId) {
        Account a = new Account();

        a.Name = 'ABC';
        a.Business_Registration_Number__c = '1111';
        a.Referral_Source__c = rId;
        a.Referral_Person__c = cId;
        a.BillingStreet = '11 duy tan';
        insert a;
    }

    /**
    * checkRelatedReferralSourceInAccountTest1 method
    * Case 1: Contact doesn't belong to Referral Source
    */
    static testMethod void checkRelatedReferralSourceInAccountTest1() {
        RecordType rt1 = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
                         LIMIT 1];
        // BSLselfdev Record Type Id
        String csBidvRt = rt1.Id;

        Referral__c rt = new Referral__c();

        rt.RecordTypeId = csBidvRt; // Cross-Sell BIDV record type
        rt.Name = 'BIDV-1234';

        insert rt;

        Referral__c rtx = createReferral();
        Contact ct = createContact(rt.Id);

        try {
            Test.startTest();
            createAccount(rtx.Id, ct.Id);
            Test.stopTest();
        }
        catch(Exception e) {
            System.assert(e.getMessage().contains('This Contact doesn\'t belong to This Referral!'));
        }
    }

    /**
    * checkRelatedReferralSourceInAccountTest2 method
    * Case 2: Contact belongs to Referral Source
    */
    static testMethod void checkRelatedReferralSourceInAccountTest2() {
        Referral__c rtx = createReferral();
        Contact ct = createContact(rtx.Id);

        Test.startTest();
        createAccount(rtx.Id, ct.Id);
        Test.stopTest();
        
        List<Account> aList = [SELECT Id FROM Account WHERE Name = 'ABC'];
        System.assertEquals(1, aList.size()); 
    }

    /**
    * checkRelatedReferralSourceInAccountTest1 method
    * Case 3: Referral Source RecordType is BSL Self-Dev
    */
    static testMethod void checkRelatedReferralSourceInAccountTest3() {
        RecordType rt2 = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                         LIMIT 1];
        // BSLselfdev Record Type Id
        String bslSelfDevRcId = rt2.Id;

        Referral__c rt = new Referral__c();

        rt.RecordTypeId = bslSelfDevRcId; // BSLselfdev record type
        rt.Name = 'BIDV-1234';

        insert rt;

        Contact ct = createContact(rt.Id);

        try {
            Test.startTest();
            createAccount(rt.Id, ct.Id);
            Test.stopTest();
        }
        catch(Exception e) {
            System.assert(e.getMessage().contains('BSL-SelfDev Referral doesn\'t have contact!'));
        }
    }
}