@isTest
private class SupplierTriggerHandlerTest{
	private static KPI__c createKpiForUser(String n, String y, String tp) {
        KPI__c kpi = new KPI__c();

        kpi.Name = n;
        kpi.Year__c = y;
        kpi.KPI_Type__c = tp;

        insert kpi;

        return kpi;
	}

	private static KPI_Value__c createKpiValueForUser(String rc, String t, KPI__c kpi) {
		String quarterRcId;
		String monthRcId;
		String yearRcId;

		List<RecordType> rtList = [SELECT Id, Name
								FROM RecordType
								WHERE SobjectType = 'KPI_Value__c'];
		
		for(RecordType r : rtList) {
			String rName = r.Name;

			if(rName.equals('Quarter')) {
				quarterRcId = r.Id;
			}

			if(rName.equals('Month')) {
				monthRcId = r.Id;
			}

			if(rName.equals('Year')) {
				yearRcId = r.Id;
			}
		}

		KPI_Value__c kv = new KPI_Value__c();

		kv.RecordTypeId = rc;

		// Month RecordType
		if(rc.equals(monthRcId)) {
			kv.Months__c = t;
		}

		// Quarter RecordType
		if(rc.equals(quarterRcId)) {
			kv.Quarter__c = t;
		}

		kv.KPI__c = kpi.Id;

		insert kv;

		return kv;
	}

	private static Supplier__c createSupplier(String n) {
		Supplier__c s = new Supplier__c();
		s.Name = n;
		s.BSL_Branch__c = 'Danang';
		insert s;

		return s;
	}

	@isTest
	static void updateBranchValueWhenCreatingSupplierTest() {
		UserRole ur = [SELECT Id
				   FROM UserRole
				   WHERE Name = 'Danang Business 1 Partner User'
				   LIMIT 1];

		String urId = ur.Id;

		User u1 = [SELECT Id
			     FROM User
			     WHERE UserRoleId = :urId
			     LIMIT 1];

		System.runAs(u1) {
			Supplier__c sp = createSupplier('test');

			Supplier__c sTest = [SELECT BSL_Branch__c
						   FROM Supplier__c
						   WHERE Name = 'test'
						   LIMIT 1];

			//System.assertEquals('Danang', sTest.BSL_Branch__c);
		}
	}

	@isTest
	static void updateNewSupplierNumberInKpiTest1() {
		String quarterRcId;
		String monthRcId;
		String yearRcId;

		List<RecordType> rtList = [SELECT Id, Name
								FROM RecordType
								WHERE SobjectType = 'KPI_Value__c'];
		
		for(RecordType r : rtList) {
			String rName = r.Name;

			if(rName.equals('Quarter')) {
				quarterRcId = r.Id;
			}

			if(rName.equals('Month')) {
				monthRcId = r.Id;
			}

			if(rName.equals('Year')) {
				yearRcId = r.Id;
			}
		}

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
		String kpiId = kpiTest.Id;
        	KPI_Value__c kvTest = createKpiValueForUser(quarterRcId, 'Q2', kpiTest);
		Supplier__c sp = createSupplier('Test');

		KPI_Value__c kTest = [SELECT Actual_New_Supplier_Development__c
					    FROM KPI_Value__c
					    WHERE KPI__c = :kpiId
					    LIMIT 1];

		// System.assertEquals(1, kTest.Actual_New_Supplier_Development__c);
	}

	// @isTest
	// static void updateNewSupplierNumberInKpiTest2() {
	// 	String quarterRcId;
	// 	String monthRcId;
	// 	String yearRcId;

	// 	List<RecordType> rtList = [SELECT Id, Name
	// 							FROM RecordType
	// 							WHERE SobjectType = 'KPI_Value__c'];
		
	// 	for(RecordType r : rtList) {
	// 		String rName = r.Name;

	// 		if(rName.equals('Quarter')) {
	// 			quarterRcId = r.Id;
	// 		}

	// 		if(rName.equals('Month')) {
	// 			monthRcId = r.Id;
	// 		}

	// 		if(rName.equals('Year')) {
	// 			yearRcId = r.Id;
	// 		}
	// 	}

	// 	UserRole ur = [SELECT Id
	// 			   FROM UserRole
	// 			   WHERE Name = 'Danang Business 1 Partner User'
	// 			   LIMIT 1];

	// 	String urId = ur.Id;

	// 	list<User> l_u1 = [SELECT Id
	// 		     FROM User
	// 		     WHERE UserRoleId = :urId
	// 		     LIMIT 2];
      //   System.runAs(l_u1[0]){
      //       KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
      //       String kpiId = kpiTest.Id;
      //       KPI_Value__c kvTest = createKpiValueForUser(quarterRcId, 'Q2', kpiTest);
      //       Supplier__c sp = createSupplier('Test supplier cmc');
            
      //       KPI_Value__c kTest = [SELECT Actual_New_Supplier_Development__c
	// 				    FROM KPI_Value__c
	// 				    WHERE KPI__c = :kpiId
	// 				    LIMIT 1];
      //   }
      //   Supplier__c sp = [select id from Supplier__c where name = 'Test supplier cmc'];
      //   sp.OwnerId = l_u1[1].Id;
      //   update sp;
        
		

		

	// 	//System.assertEquals(0, kTest.Actual_New_Supplier_Development__c);
	// }

	@isTest
	static void updateNewSupplierNumberInKpiTest3() {
		String quarterRcId;
		String monthRcId;
		String yearRcId;

		List<RecordType> rtList = [SELECT Id, Name
								FROM RecordType
								WHERE SobjectType = 'KPI_Value__c'];
		
		for(RecordType r : rtList) {
			String rName = r.Name;

			if(rName.equals('Quarter')) {
				quarterRcId = r.Id;
			}

			if(rName.equals('Month')) {
				monthRcId = r.Id;
			}

			if(rName.equals('Year')) {
				yearRcId = r.Id;
			}
		}

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
		String kpiId = kpiTest.Id;
        	KPI_Value__c kvTest = createKpiValueForUser(quarterRcId, 'Q2', kpiTest);
		Supplier__c sp = createSupplier('Test');
		delete sp;

		KPI_Value__c kTest = [SELECT Actual_New_Supplier_Development__c
					    FROM KPI_Value__c
					    WHERE KPI__c = :kpiId
					    LIMIT 1];

		System.assertEquals(0, kTest.Actual_New_Supplier_Development__c);
	}
}