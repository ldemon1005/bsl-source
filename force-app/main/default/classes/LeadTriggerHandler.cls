public without sharing class LeadTriggerHandler {
    /**
    *
    *
    *
    */
    public static void checkRelatedReferralSourceInLead(List<Lead> lList) {
        RecordType rt = [SELECT Id, Name
                        FROM RecordType
                        WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                        LIMIT 1];
                
        String rtId = rt.Id;

        // BSLselfdev Record Type Id
        String bslSelfDevRcId = rtId;
        // List of Referral Source Id
        List<String> refSourceIdList = new List<String>();
        // Pair of Referral Source and Referral Person
        Map<String, List<String>> pairRefSourceAndRefPersonId = new Map<String, List<String>>();

        for(Lead l : lList) {
            refSourceIdList.add(l.Referral_Source__c);
        }

        List<Contact> ctList = [SELECT Id, Referral_Name__c
                                FROM Contact
                                WHERE Referral_Name__c IN :refSourceIdList];

        List<Referral__c> refList = [SELECT Id, RecordTypeId
                                     FROM Referral__c
                                     WHERE Id IN :refSourceIdList];

        System.debug('refList size: ' + refList.size());

        if(refList.size() != 0) {
            // create pair of Referral and Contact
            for(String ref : refSourceIdList) {
                List<String> ctIdList = new List<String>();

                for(Contact c : ctList) {
                    if(ref.equals(c.Referral_Name__c)) {
                        ctIdList.add(c.Id);
                    }
                }

                pairRefSourceAndRefPersonId.put(ref, ctIdList);
            }

            for(Lead ld : lList) {
                String refRcId;
                String refSourceId = ld.Referral_Source__c;
                String refPersonId = ld.Referral_Person__c;

                for(Referral__c r : refList) {
                    if(refSourceId.equals(r.Id)) {
                        refRcId = r.RecordTypeId;
                    }
                }

                System.debug('Referral Record Type Id: ' + refRcId);
                System.debug('Referral Person Id: ' + refPersonId);

                List<String> refCtIdList = pairRefSourceAndRefPersonId.get(refSourceId);
                
                if(!(refSourceId == null) && !(refPersonId == null)) {
                    System.debug('In If');
                    if(!refCtIdList.contains(refPersonId)) {
                        ld.addError('This Contact doesn\'t belong to This Referral!');
                    }

                    if(refRcId.equals(bslSelfDevRcId)) {
                        ld.addError('BSL-SelfDev Referral doesn\'t have contact!');
                    }
                }
            }
        }
    }
}