global class NotificationDayCheckingClass implements Schedulable{
    global void execute(SchedulableContext ctx) {
        /** 
         * Remind salesman for sending gift, email to Contact in their birthday
         */
        Date td = Date.today();
        Date checkingDay = Date.today().addDays(7);
        Date checkingDay2 = Date.today().addDays(15);
        Date checkingDay3 = Date.today().addMonths(2).addDays(-1);
        // Get String of Date
        String checkedDate = String.valueOf(checkingDay.day()) + '-' + String.valueOf(checkingDay.month());

        // List of User Role Dev Name
        List<String> urDevNameList = new List<String>{'DanangBusiness1PartnerManager',
                                                      'HanoiBusiness1PartnerManager',
                                                      'HoChiMinhBusiness1PartnerManager',
                                                      'Deputy_Director_of_Hanoi_Branch',
                                                      'Deputy_Director_of_Ho_Chi_Minh_Branch',
                                                      'Director_of_Danang_Branch',
                                                      'Director_of_Hanoi_Branch',
                                                      'Director_of_Ho_Chi_Minh_Branch',
                                                      'Manager_of_Danang_Business_1',
                                                      'Manager_of_Danang_Business_2',
                                                      'Manager_of_Hanoi_Business_1',
                                                      'Manager_of_Hanoi_Business_2',
                                                      'Manager_of_Ho_Chi_Minh_Business_1',
                                                      'Manager_of_Ho_Chi_Minh_Business_2'};
        // List of User Role Id
        List<String> urIdList = new List<String>();
        // List of User Role
        List<UserRole> urList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :urDevNameList];

        for(UserRole ur : urList) {
            urIdList.add(ur.Id);
        }

        // List of Director User
        List<User> uList = [SELECT Id, Division 
                            FROM User
                            WHERE UserRoleId IN :urIdList];

        // List of Branch User
        List<User> brUserList = [SELECT Id, Division
                                 FROM User
                                 WHERE Division = 'Da Nang Branch' OR Division = 'Hanoi Branch' OR Division = 'Ho Chi Minh Branch'];

        // List of new Task
        List<Task> newTaskList = new List<Task>();

        List<Contact> checkingContactList = [SELECT Id, OwnerId
                                             FROM Contact
                                             WHERE Birthday_In_Text__c = :checkedDate];

        List<Account> checkingAccountList = [SELECT Id, OwnerId
                                             FROM Account
                                             WHERE Establishment_Date_In_Text__c = :checkedDate];

        List<Campaign> checkingCampaignList = [SELECT Id, OwnerId
                                               FROM Campaign
                                               WHERE EndDate = :checkingDay2];

        List<Contract> checkingContractList = [SELECT Id, OwnerId
                                               FROM Contract
                                               WHERE EndDate = :checkingDay3];

        List<Opportunity> checkingOpportunityList = [SELECT Id, OwnerId, BSL_Branch__c, Approval_Deadline_BD__c, Approval_Deadline_CCD__c, Approval_Deadline_CC__c, Approval_Deadline_BOM__c
                                                     FROM Opportunity
                                                     WHERE Approval_Deadline_BD__c = :td OR Approval_Deadline_CCD__c = :td OR Approval_Deadline_CC__c = :td OR Approval_Deadline_BOM__c = :td];
        
        String body = 'Upcoming Birthdays (7 Days later), please check it in Name Field ';
        String body1 = 'Upcoming Establishment Date (7 Days later), please check it in Related To Field ';

        for(Contact c : checkingContactList) {
            Task t = new Task();

            t.Subject = body;
            t.Type = 'Email';
            t.WhoId = c.Id;
            t.OwnerId = c.OwnerId;
            t.ActivityDate = Date.today();

            newTaskList.add(t);

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            mentionSegmentInput.id = c.OwnerId;
            messageBodyInput.messageSegments.add(mentionSegmentInput);

            textSegmentInput.text = 'Upcoming Birthdays (7 Days later)';
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = c.Id;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
            

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }

        for(Account a : checkingAccountList) {
            Task t = new Task();

            t.Subject = body1;
            t.Type = 'Email';
            t.WhatId = a.Id;
            t.OwnerId = a.OwnerId;
            t.ActivityDate = Date.today();

            newTaskList.add(t);

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            mentionSegmentInput.id = a.OwnerId;
            messageBodyInput.messageSegments.add(mentionSegmentInput);

            textSegmentInput.text = 'Upcoming Establishment Date (7 Days later)';
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = a.Id;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
            

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }

        for(Campaign cp : checkingCampaignList) {
            Task t = new Task();

            t.Subject = 'REMIND: This Campaign will be end after 15 days';
            t.Type = 'Email';
            t.WhatId = cp.Id;
            t.OwnerId = cp.OwnerId;
            t.ActivityDate = Date.today();

            newTaskList.add(t);

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            mentionSegmentInput.id = cp.OwnerId;
            messageBodyInput.messageSegments.add(mentionSegmentInput);

            textSegmentInput.text = 'This Campaign will be end after 15 days';
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = cp.Id;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
            

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }

        for(Contract ctr : checkingContractList) {
            Task t = new Task();

            t.Subject = 'REMIND: This Contract will be end after 2 months';
            t.Type = 'Email';
            t.WhatId = ctr.Id;
            t.OwnerId = ctr.OwnerId;
            t.ActivityDate = Date.today();

            newTaskList.add(t);

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            mentionSegmentInput.id = ctr.OwnerId;
            messageBodyInput.messageSegments.add(mentionSegmentInput);

            textSegmentInput.text = 'This Contract will be end after 2 months';
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = ctr.Id;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
            

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }
    
        insert newTaskList;

        for(Opportunity opp : checkingOpportunityList) {
            String uBranch;
            String oppBranch;
            List<User> refUserList = new List<User>();

            for(User u : brUserList) {
                String ownerId = opp.OwnerId;

                if(ownerId.equals(u.Id)) {
                    refUserList.add(u);
                }
            }

            if(opp.BSL_Branch__c != null) {
                oppBranch = opp.BSL_Branch__c;
            }
            else {
                oppBranch = 'x';
            }

            // update uBranch
            if(oppBranch.equals('Danang')) {
                uBranch = 'Da Nang Branch';
            }
            else if(oppBranch.equals('Hanoi')) {
                uBranch = 'Hanoi Branch';
            }
            else if(oppBranch.equals('HCM')) {
                uBranch = 'Ho Chi Minh Branch';
            }
            else {
                uBranch = ' ';
            }   

            // get branch name of Salesman
            for(User u : uList) {
                if(uBranch.equals(u.Division)) {
                    refUserList.add(u);
                }
            }

            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            for(User u : refUserList) {
                System.debug('IN LOOP 2222222222');
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput.id = u.Id;
                messageBodyInput.messageSegments.add(mentionSegmentInput);
            }

            textSegmentInput.text = ' Today is one of Approval Deadlines in this Opportunity. Please check out the information in Approval Tab in Opportunity';
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = opp.Id;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;

            System.debug('IN LOOP 111111111');

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);

            System.debug('CCCCCCCCCCCCCCCC: ' + refUserList.size());
        }
    }
}