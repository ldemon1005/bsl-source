public with sharing class TestMap {
	public String city { get;set; }
	public String country { get;set; }
	public String street { get;set; }
    public String state { get;set; }
    public String postalCode { get;set; }
	
   	integer totalRecs = 0;
   	integer count= 0;
   	integer LimitSize= 10;
  
  	public TestMap(){}
   	public List<Account> accounts=new List<Account>();
   	public List<Account> getAccounts() {
        List<Account> accounts = getData();
        return accounts;
    }
    public List<Account> getData(){
        String str_country = '\'%\'' + ' or BillingCountry = null )';
        String str_city = '\'%\'' + ' or BillingCity = null )';
        String str_postalCode = '\'%\'' + ' or BillingPostalCode = null )';
        String str_street = '\'%\'' + ' or BillingStreet = null )';
        String str_state = '\'%\'' + ' or BillingState = null )';
        if(!String.isEmpty(country)){
        	str_country = '\'%' + country + '%\')';
        }
        if(!String.isEmpty(city)){
        	str_city = '\'%' + city + '%\')';
        }
        if(!String.isEmpty(postalCode)){
        	str_postalCode = '\'%' + postalCode + '%\')';
        }
        if(!String.isEmpty(street)){
        	str_street = '\'%' + street + '%\')';
        }
        if(!String.isEmpty(state)){
        	str_state = '\'%' + state + '%\')';
        }
        String query_str = 'select Id, Name , BillingStreet, BillingCity, BillingCountry,' + 
         						  ' BillingPostalCode, BillingState, Phone from Account where '+ 
                                  ' (BillingCity like ' + str_city + ' and (BillingCountry like ' + str_country +
                                  ' and (BillingStreet like ' + str_street + ' and (BillingPostalCode like ' + str_postalCode + ' and (BillingState like ' + str_state;
        totalRecs = Database.query(query_str).size();
        System.debug(query_str);
        List<Account> listAccount = Database.query(query_str + ' limit ' + LimitSize  + ' OFFSET ' + count);
        return listAccount;
    }
    public void updatePage() {
       	accounts.clear();
        count = 0;
       	accounts = getData();
    }
    public PageReference Firstbtn() {
     	count=0;
        return null;
    }
    public PageReference prvbtn() {
      count=count-limitsize;
        return null;
    }
   
 	public PageReference Nxtbtn() {
 		count=count+limitsize;
        return null;
    }
 	public PageReference lstbtn() {
 		count= totalrecs - math.mod(totalRecs,LimitSize);
        return null;
    }


    public Boolean getNxt() {
        if((count+ LimitSize) > totalRecs)
       return true;
     else
       return false;
        
    }
     public Boolean getPrv() {
      if(count== 0)
          return true;
        else
          return false;
        }
    }