global class ApprovalExpiryDateChecking implements Schedulable {
    global void execute(SchedulableContext ctx) {
        // Finance Lease recordtype Id
        String flRtId;

        // Operating Lease recordtype Id
        String olRtId;

        List<String> oppIdList = new List<String>();

        List<Opportunity> checkedOppList = [SELECT Id, Approval_Expiry_Date__c, Remaining_Refused_by_Customer__c, Remaining_Disbursement_Amount__c
                                            FROM Opportunity
                                            WHERE Approval_Expiry_Date__c <= TODAY 
                                            AND Remaining_Refused_by_Customer__c = false 
                                            AND Verified_by_Branch_Leadership__c = false
                                            AND Remaining_Disbursement_Amount__c != 0];

        for(Opportunity opp : checkedOppList) {
            oppIdList.add(opp.Id);
        }

        // Quote List of Opportunity
        List<Quote> qList = [SELECT Status, Expected_Contract_Amount__c, Expected_Contract_Amount_after_VAT__c, Expected_Contract_Amount_ol__c, RecordTypeId, OpportunityId
                             FROM Quote
                             WHERE OpportunityId = :oppIdList];

        List<RecordType> rtList = [SELECT Id, Name
                                   FROM RecordType
                                   WHERE SobjectType = 'Quote'];

        for(RecordType rct : rtList) {
            String rctName = rct.Name;
                        
            if(rctName.equals('Finance Lease')) {
                flRtId = rct.Id;
            }

            if(rctName.equals('Operating Lease')) {
                olRtId = rct.Id;
            }
        }

        for(Opportunity o : checkedOppList) {
            String oppId = o.Id;

            for(Quote q : qList) {
                if(oppId.equals(q.OpportunityId)) {
                    if(olRtId.equals(q.RecordTypeId)) {
                        if(q.Expected_Contract_Amount_ol__c != null) {
                            o.Approved_Amount__c = q.Expected_Contract_Amount_ol__c;
                        }
                        else {
                            o.Approved_Amount__c = 0;
                        }
                    }
                    else if (flRtId.equals(q.RecordTypeId)) {
                        if(q.Expected_Contract_Amount__c != null) {
                            o.Approved_Amount__c = q.Expected_Contract_Amount__c;
                        }
                        else {
                            o.Approved_Amount__c = 0;
                        }
                    }
                }
            }

            o.StageName = 'L\'';
            o.Status_of_Each_Stage__c = 'Approval Date Expired';
            System.debug('StageName is fucking changedddddd');
        }

        update checkedOppList;
    }
}