/**
 * @File Name          : AccountTriggerHandler.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/22/2019, 2:04:29 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/22/2019, 2:04:29 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public without sharing class AccountTriggerHandler {
    /**
    * checkRelatedReferralSourceInAccount method
    * checking Referral Source and Contact in Account (if Contact doesn't belong to Referral Source => Add Error)
    *
    * @param: List<Account> accList
    */
    public static void checkRelatedReferralSourceInAccount(List<Account> accList) {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                         LIMIT 1];
        // BSLselfdev Record Type Id
        String bslSelfDevRcId = rt.Id;
        // List of Referral Source Id
        List<String> refSourceIdList = new List<String>();
        // Pair of Referral Source and Referral Person
        Map<String, List<String>> pairRefSourceAndRefPersonId = new Map<String, List<String>>();

        for(Account a : accList) {
            refSourceIdList.add(a.Referral_Source__c);
        }

        List<Contact> ctList = [SELECT Id, Referral_Name__c
                                FROM Contact
                                WHERE Referral_Name__c IN :refSourceIdList];

        List<Referral__c> refList = [SELECT Id, RecordTypeId
                                     FROM Referral__c
                                     WHERE Id IN :refSourceIdList];

        if(refList.size() != 0) {
            for(String ref : refSourceIdList) {
                List<String> ctIdList = new List<String>();

                for(Contact c : ctList) {
                    if(ref.equals(c.Referral_Name__c)) {
                        ctIdList.add(c.Id);
                    }
                }

                pairRefSourceAndRefPersonId.put(ref, ctIdList);
            }

            for(Account acc : accList) {
                String refRcId;
                String refSourceId = acc.Referral_Source__c;
                String refPersonId = acc.Referral_Person__c;

                for(Referral__c r : refList) {
                    if(refSourceId.equals(r.Id)) {
                        refRcId = r.RecordTypeId;
                    }
                }

                List<String> refCtIdList = pairRefSourceAndRefPersonId.get(refSourceId);
                
                if(!(refSourceId == null) && !(refPersonId == null)) {
                    if(!refCtIdList.contains(refPersonId)) {
                        acc.addError('This Contact doesn\'t belong to This Referral!');
                    }

                    if(refRcId.equals(bslSelfDevRcId)) {
                        acc.addError('BSL-SelfDev Referral doesn\'t have contact!');
                    }
                }
            }
        }
    }

    /**
    * updateBranchValueWhenCreatingAcc method
    * when Account is created => update BSL Branch in Account and Account
    *
    * @param: List<Account> accList
    */
    public static void updateBranchValueWhenCreatingAcc(List<Account> accList) {
        // Branch in BSL
        String hnBr = 'Hanoi Branch';
        String dnBr = 'Da Nang Branch';
        String hcmBr = 'Ho Chi Minh Branch';

        // List of Owner Id
        List<String> ownerIdList = new List<String>();

        for(Account a : accList) {
            ownerIdList.add(a.OwnerId);
        }

        List<User> uList = [SELECT Id, Division
                            FROM User
                            WHERE Id IN :ownerIdList];

        for(Account acc : accList) {
            String ownerId = acc.OwnerId;

            for(User u : uList) {
                if(ownerId.equals(u.Id)) {
                    if(hnBr.equals(u.Division)) {
                        acc.BSL_Branch__c = 'Hanoi';
                    }

                    if(dnBr.equals(u.Division)) {
                        acc.BSL_Branch__c = 'Danang';
                    }

                    if(hcmBr.equals(u.Division)) {
                        acc.BSL_Branch__c = 'HCM';
                    }
                }
            }
        }

        
    }

    /**
    * updateContactInCustomerInitiatedReferral method
    * when Referral Source in Account is Customer-Initiated => update BSL Branch in Account and Account
    *
    * @param: List<Account> accList
    */
    public static void updateContactInCustomerInitiatedReferral(List<Account> accList) {
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Referral__c' AND Name = 'Customer-initiate'
                         LIMIT 1];

        String customerInitiatedRtId = rt.Id;

        List<String> checkedRefIdList = new List<String>();
        List<String> refIdList = new List<String>();
        List<String> accIdList = new List<String>();

        for(Account a : accList) {
            accIdList.add(a.Id);
            refIdList.add(a.Referral_Source__c);
        }

        List<Contact> ctList = [SELECT Id, AccountId, Referral_Name__c
                                FROM Contact
                                WHERE AccountId IN : accIdList];

        List<Referral__c> refList = [SELECT Id
                                     FROM Referral__c
                                     WHERE RecordTypeId = :customerInitiatedRtId AND Id IN :refIdList];

        for(Referral__c r : refList) {
            checkedRefIdList.add(r.Id);
        }

        for(Account acc : accList) {
            String refId = acc.Referral_Source__c;
            String accId = acc.Id;

            if(refIdList.contains(refId)) {
                for(Contact c : ctList) {
                    if(accId.equals(c.AccountId)) {
                        c.Referral_Name__c = refId;
                    }
                }
            }
        }

        update ctList;
    }
}