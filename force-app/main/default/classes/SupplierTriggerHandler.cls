public without sharing class SupplierTriggerHandler {
    /**
    * updateBranchValueWhenCreatingSupplier method
    * when Supplier is created => update BSL Branch in Supplier
    *
    * @param: List<Supplier__c> sList
    */
    public static void updateBranchValueWhenCreatingSupplier(List<Supplier__c> sList) {
        // Branch in BSL
        String hnBr = 'Hanoi Branch';
        String dnBr = 'Da Nang Branch';
        String hcmBr = 'Ho Chi Minh Branch';

        // List of Owner Id
        List<String> ownerIdList = new List<String>();

        for(Supplier__c s : sList) {
            ownerIdList.add(s.OwnerId);
        }

        List<User> uList = [SELECT Id, Division
                            FROM User
                            WHERE Id IN :ownerIdList];

        for(Supplier__c sp : sList) {
            String ownerId = sp.OwnerId;

            for(User u : uList) {
                if(ownerId.equals(u.Id)) {
                    if(hnBr.equals(u.Division)) {
                        sp.BSL_Branch__c = 'Hanoi';
                    }

                    if(dnBr.equals(u.Division)) {
                        sp.BSL_Branch__c = 'Danang';
                    }

                    if(hcmBr.equals(u.Division)) {
                        sp.BSL_Branch__c = 'HCM';
                    }
                }
            }
        }

        
    }

    /**
     * 
     */
    public static void updateNewSupplierNumberInKpi(List<Supplier__c> sList) {
        String q1 = 'Q1';
        String q2 = 'Q2';
        String q3 = 'Q3';
        String q4 = 'Q4';

        String quarterRcId;
        String monthRcId;
        String yearRcId;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'KPI_Value__c'];
        
        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Quarter')) {
                quarterRcId = r.Id;
            }

            if(rName.equals('Month')) {
                monthRcId = r.Id;
            }

            if(rName.equals('Year')) {
                yearRcId = r.Id;
            }
        }

        // List of Owner Id
        List<String> ownerIdList = new List<String>();
        // List of KPI Id
        List<String> kpiIdList = new List<String>();
        // List of Updated Kpi Value
        List<KPI_Value__c> updatedKpiValueList = new List<KPI_Value__c>();

        for(Supplier__c s : sList) {
            ownerIdList.add(s.OwnerId);
        }

        List<Supplier__c> checkedSupplierList = [SELECT Id, OwnerId, Supplier_Created_Date__c
                                                 FROM Supplier__c
                                                 WHERE OwnerId IN :ownerIdList];

        System.debug('Checked Supplier List: ' + checkedSupplierList.size());

        List<KPI__c> kpiList = [SELECT Id, OwnerId, KPI_Type__c, Year__c
                                FROM KPI__c
                                WHERE OwnerId IN :ownerIdList];

        for(KPI__c k : kpiList) {
            kpiIdList.add(k.Id);
        }

        List<KPI_Value__c> kpiValueList = [SELECT Id, Months__c, Quarter__c, Year__c, KPI__c, RecordTypeId, Actual_New_Supplier_Development__c
                                           FROM KPI_Value__c
                                           WHERE KPI__c IN :kpiIdList];

        for(KPI_Value__c kv : kpiValueList) {
            String ownerId;
            String quarterOfKpiValue;
            String monthOfKpiValue;
            String yearOfKpiValue;

            if(kv.Quarter__c != null) {
                quarterOfKpiValue = kv.Quarter__c;
            }
            else {
                quarterOfKpiValue = 'x';
            }
            
            if(kv.Months__c != null) {
                monthOfKpiValue = kv.Months__c;
            }
            else {
                monthOfKpiValue = 'x';
            }

            if(kv.Year__c != null) {
                yearOfKpiValue = kv.Year__c;
            }
            else {
                yearOfKpiValue = 'x';
            }

            List<Supplier__c> newSuppList = new List<Supplier__c>();

            // define ownerId
            for(KPI__c kpi : kpiList) {
                String kpiId = kpi.Id;

                if(kpiId.equals(kv.KPI__c)) {
                    ownerId = kpi.OwnerId;
                }
            }

            System.debug('KPI Owner: ' + ownerId);

            for(Supplier__c sp : checkedSupplierList) {
                Integer createdMonth;
                Integer createdYear;

                System.debug('Supplier Owner Id: ' + sp.OwnerId);

                if(sp.Supplier_Created_Date__c != null) {
                    createdMonth = sp.Supplier_Created_Date__c.month();
                    createdYear = sp.Supplier_Created_Date__c.year();
                }
                else {
                    createdMonth = 0;
                    createdYear = 0;
                }

                System.debug('Created Month: ' + createdMonth);
                System.debug('Created Month: ' + createdYear);

                if(ownerId.equals(sp.OwnerId)) {
                    if(quarterRcId.equals(kv.RecordTypeId)) {
                        if(q1.equals(quarterOfKpiValue) && (yearOfKpiValue.equals(String.valueOf(createdYear)))) {
                            if((createdMonth >= 1) && (createdMonth <= 3)) {
                                System.debug('Quarter 1 Entry');
                                newSuppList.add(sp);
                            }
                        }
                        else if(q2.equals(quarterOfKpiValue) && (yearOfKpiValue.equals(String.valueOf(createdYear)))) {
                            if((createdMonth >= 4) && (createdMonth <= 6)) {
                                System.debug('Quarter 2 Entry');
                                newSuppList.add(sp);
                            }
                        }
                        else if(q3.equals(quarterOfKpiValue) && (yearOfKpiValue.equals(String.valueOf(createdYear)))) {
                            if((createdMonth >= 7) && (createdMonth <= 9)) {
                                System.debug('Quarter 3 Entry');
                                newSuppList.add(sp);
                            }
                        }
                        else if(q4.equals(quarterOfKpiValue) && (yearOfKpiValue.equals(String.valueOf(createdYear)))) {
                            if((createdMonth >= 10) && (createdMonth <= 12)) {
                                System.debug('Quarter 4 Entry');
                                newSuppList.add(sp);
                            }
                        }
                    }
                    
                    if(monthRcId.equals(kv.RecordTypeId)) {
                        String monthOfSup = String.valueOf(createdMonth);

                        if(monthOfSup.equals(monthOfKpiValue) && (yearOfKpiValue.equals(String.valueOf(createdYear)))) {
                            newSuppList.add(sp);
                        }
                    }

                    if(yearRcId.equals(kv.RecordTypeId)) {
                        String yearOfSup = String.valueOf(createdYear);

                        System.debug('Record Type Year of KPI');

                        if(yearOfSup.equals(yearOfKpiValue)) {
                            newSuppList.add(sp);

                            System.debug('HAIL IN LOOP');
                        }
                    }
                }
            }

            System.debug('New Supp Size: ' + newSuppList.size());
            kv.Actual_New_Supplier_Development__c = newSuppList.size();
            updatedKpiValueList.add(kv);
        }

        update updatedKpiValueList;
    }
}