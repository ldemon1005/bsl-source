public without sharing class ContactTriggerHandler {
    /**
    * checkContact method
    * Contact cannot belong to Referral and Supplier at the same time
    *
    * @param: List<Contact> ctList
    */
    public static void checkContact(List<Contact> ctList) {
        // check when creating Contact
        for(Contact c : ctList) {
            if((c.Referral_Name__c != null) && (c.AccountId != null)) {
                c.addError('Contact cannot belong to Referral and Account at the same time!');
            }

            if((c.Supplier_Name__c != null) && (c.AccountId != null)) {
                c.addError('Contact cannot belong to Supplier and Account at the same time!');
            }
        }
    }

    /**
    *
    *
    */
    public static void setAccountLineForReferralAndSupplierContact(List<Contact> ctList) {
        
    }
}