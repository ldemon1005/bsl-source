@isTest
public class SampleLookupControllerTest {
    static testMethod void search_should_return_Account() {
        Id [] fixedResults = new Id[1];
        Account account = createTestAccount('Account');
        account.Business_Registration_Number__c = '1';
        fixedResults.add(account.Id);
        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();

        List<LookupSearchResult> results = SampleLookupController.search('Acc', selectedIds);
    }

    static testMethod void search_should_not_return_selected_item() {
        Id [] fixedResults = new Id[1];
        Account account1 = createTestAccount('Account1');
        fixedResults.add(account1.Id);
        Account account2 = createTestAccount2('Account2');
        fixedResults.add(account2.Id);
        Test.setFixedSearchResults(fixedResults);
        List<String> selectedIds = new List<String>();
        selectedIds.add(account2.Id);

        List<LookupSearchResult> results = SampleLookupController.search('Acc', selectedIds);
    }
    
    @IsTest(SeeAllData=true) static void test(){
        Campaign cp = new Campaign(Name='Test Campaign');
        insert cp;
        
        Campaign campaign = SampleLookupController.findCampaign(cp.Id);
        List<LookupSearchResult> searchCampaign = SampleLookupController.searchCampaign('Test Campaign');
        SampleLookupController.PagedResult result = SampleLookupController.getListData(10, 1);
        
        Opportunity opp = new Opportunity();
        opp.Name='Test' ;
        opp.StageName = 'D1';
        opp.CloseDate = System.today() + 5;
        Insert opp ; 
        
        list<String> selection = new list<String>();
        selection.add(opp.Id);
        
        Opportunity oppRes = SampleLookupController.findOp(opp.Id);
        String isLightningExperience = SampleLookupController.isLightningExperience();
        String opsIdSelected = SampleLookupController.opsIdSelected();
        SampleLookupController.SaveLookup(selection, cp.Id, 'Sent');
    }

    private static Account createTestAccount(String name) {
        Account account = new Account(Name = name);
        account.Business_Registration_Number__c = '1';
        account.BillingStreet = '11 duy tan';
        insert account;
        return account;
    }

    private static Account createTestAccount2(String name) {
        Account account = new Account(Name = name);
        account.Business_Registration_Number__c = '2';
        account.BillingStreet = '11 duy tan';
        insert account;
        return account;
    }
}