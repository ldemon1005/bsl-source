@IsTest(SeeAllData=true)
private class CoOperationContractControllerTest {
    private static final RecordType rt = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Contract' AND Name = 'Co-operation'
                         						LIMIT 1];
    @isTest() private static Contract createContract() {
        Account a = [SELECT Id
                     FROM Account
                     WHERE Name = '[BSL]-sharing--line'
                     LIMIT 1];
        
        Contract ctr = new Contract(StartDate = Date.today(), 
                                    ContractTerm = 48,
                                    AccountId = a.Id, 
                                    Status = 'Draft',
                                    RecordTypeId = rt.Id);
        insert ctr;
        return ctr;
    }
    
    @isTest() static void getContractsTest() {
        Contract ctr = createContract();
        
        Test.startTest();
        List<Contract> l_contract = CoOperationContractController.getContracts();
        Test.stopTest();
    }
    
    @isTest() static void saveContractTest() {
        Account a = new Account(Name = 'Test Account');
        a.Business_Registration_Number__c = '1';
        a.BillingStreet = '11 duy tan';
        insert a;
        
        Contract ctr = new Contract(StartDate = Date.today(), ContractTerm = 48, AccountId = a.Id, Status = 'Draft', RecordTypeId = rt.Id);
        
        Test.startTest();
        Contract cont = CoOperationContractController.saveContract(ctr, 48);
        Test.stopTest();
    }
    @isTest() static void test(){
        String abc = CoOperationContractController.getLinkedAccount();
        String bca = CoOperationContractController.getCoOpContract();
    }
}