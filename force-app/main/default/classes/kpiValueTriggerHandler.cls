public without sharing class kpiValueTriggerHandler {
    /**
    * updateActualCrossSellSmtb method
    * update Total Actual Cross-sell SMTB in KPI
    *
    * @param: List<KPI_Value__c> kvList
    */
    public static void updateActualCrossSellSmtb(List<KPI_Value__c> kvList) {
        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(KPI_Value__c k : kvList) {
            kpiIdList.add(k.KPI__c);
        }

        List<KPI__c> kpiList = [SELECT Id, Actual_Total_Disbursed_Cross_sell_SMTB__c
                                FROM KPI__c
                                WHERE Id IN :kpiIdList];

        for(KPI__c kpi : kpiList) {
            String kpiId = kpi.Id;
            Decimal ref = 0;

            for(KPI_Value__c kv : kvList) {
                if(kpiId.equals(kv.KPI__c)) {
                    if(kv.Actual_Total_Disbursed_Cross_sell_SMTB__c != null) {
                        ref += kv.Actual_Total_Disbursed_Cross_sell_SMTB__c;
                    }
                }
            }

            kpi.Actual_Total_Disbursed_Cross_sell_SMTB__c = ref;
        }

        update kpiList;
    }

    /**
    * updateActualCrossSellSmtpfc method
    * update Total Actual Cross-sell SMTPFC in KPI
    *
    * @param: List<KPI_Value__c> kvList
    */
    public static void updateActualCrossSellSmtpfc(List<KPI_Value__c> kvList) {
        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(KPI_Value__c k : kvList) {
            kpiIdList.add(k.KPI__c);
        }

        List<KPI__c> kpiList = [SELECT Id, Actual_Total_Disbursed_Cross_sell_SMTPFC__c
                                FROM KPI__c
                                WHERE Id IN :kpiIdList];

        for(KPI__c kpi : kpiList) {
            String kpiId = kpi.Id;
            Decimal ref = 0;

            for(KPI_Value__c kv : kvList) {
                if(kpiId.equals(kv.KPI__c)) {
                    if(kv.Actual_Total_Disbursed_Cross_sell_SMTPFC__c != null) {
                        ref += kv.Actual_Total_Disbursed_Cross_sell_SMTPFC__c;
                    }
                }
            }

            kpi.Actual_Total_Disbursed_Cross_sell_SMTPFC__c = ref;
        }

        update kpiList;
    }

    /**
    * updateActualScf method
    * update Total Actual SCF in KPI
    *
    * @param: List<KPI_Value__c> kvList
    */
    public static void updateActualScf(List<KPI_Value__c> kvList) {
        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(KPI_Value__c k : kvList) {
            kpiIdList.add(k.KPI__c);
        }

        List<KPI__c> kpiList = [SELECT Id, Actual_Total_Disbursed_SCF__c
                                FROM KPI__c
                                WHERE Id IN :kpiIdList];

        for(KPI__c kpi : kpiList) {
            String kpiId = kpi.Id;
            Decimal ref = 0;

            for(KPI_Value__c kv : kvList) {
                if(kpiId.equals(kv.KPI__c)) {
                    if(kv.Actual_Total_Disbursed_SCF__c != null) {
                        ref += kv.Actual_Total_Disbursed_SCF__c;
                    }
                }
            }

            kpi.Actual_Total_Disbursed_SCF__c = ref;
        }

        update kpiList;
    }

    /**
    * updateActualBslSelfDevelop method
    * update Actual Total Disbursed BSL Self Develop in KPI
    *
    * @param: List<KPI_Value__c> kvList
    */
    public static void updateActualBslSelfDevelop(List<KPI_Value__c> kvList) {
        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(KPI_Value__c k : kvList) {
            kpiIdList.add(k.KPI__c);
        }

        List<KPI__c> kpiList = [SELECT Id, Actual_Total_Disbursed_BSL_Self_Develop__c
                                FROM KPI__c
                                WHERE Id IN :kpiIdList];

        for(KPI__c kpi : kpiList) {
            String kpiId = kpi.Id;
            Decimal ref = 0;

            for(KPI_Value__c kv : kvList) {
                if(kpiId.equals(kv.KPI__c)) {
                    if(kv.Actual_Total_Disbursed_BSL_Self_Develop__c != null) {
                        ref += kv.Actual_Total_Disbursed_BSL_Self_Develop__c;
                    }
                }
            }

            kpi.Actual_Total_Disbursed_BSL_Self_Develop__c = ref;
        }

        update kpiList;
    }

    /**
    * updateActualCustomerInitiate method
    * update Actual Total Disbursed Customer Initiate in KPI
    *
    * @param: List<KPI_Value__c> kvList
    */
    public static void updateActualCustomerInitiate(List<KPI_Value__c> kvList) {
        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(KPI_Value__c k : kvList) {
            kpiIdList.add(k.KPI__c);
        }

        List<KPI__c> kpiList = [SELECT Id, Actual_Total_Disbursed_Customer_Initiate__c
                                FROM KPI__c
                                WHERE Id IN :kpiIdList];

        for(KPI__c kpi : kpiList) {
            String kpiId = kpi.Id;
            Decimal ref = 0;

            for(KPI_Value__c kv : kvList) {
                if(kpiId.equals(kv.KPI__c)) {
                    if(kv.Actual_Total_Disbursed_Customer_Initiate__c != null) {
                        ref += kv.Actual_Total_Disbursed_Customer_Initiate__c;
                    }
                }
            }

            kpi.Actual_Total_Disbursed_Customer_Initiate__c = ref;
        }

        update kpiList;
    }

    /**
    * updateActualOther method
    * update Actual Other Initiate in KPI
    *
    * @param: List<KPI_Value__c> kvList
    */
    public static void updateActualOther(List<KPI_Value__c> kvList) {
        // List of KPI Id
        List<String> kpiIdList = new List<String>();

        for(KPI_Value__c k : kvList) {
            kpiIdList.add(k.KPI__c);
        }

        List<KPI__c> kpiList = [SELECT Id, Actual_Total_Disbursed_Others__c
                                FROM KPI__c
                                WHERE Id IN :kpiIdList];

        for(KPI__c kpi : kpiList) {
            String kpiId = kpi.Id;
            Decimal ref = 0;

            for(KPI_Value__c kv : kvList) {
                if(kpiId.equals(kv.KPI__c)) {
                    if(kv.Actual_Total_Disbursed_Others__c != null) {
                        ref += kv.Actual_Total_Disbursed_Others__c;
                    }
                }
            }

            kpi.Actual_Total_Disbursed_Others__c = ref;
        }

        update kpiList;
    }
}