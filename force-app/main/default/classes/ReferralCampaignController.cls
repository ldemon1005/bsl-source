public with sharing class ReferralCampaignController {

    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
            Referral__c (Id, Name WHERE id NOT IN :selectedIds)
            LIMIT :MAX_RESULTS];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Opportunities & convert them into LookupSearchResult
        String referralIcon = 'standard:opportunity';
        Referral__c [] referrals = ((List<Referral__c>) searchResults[0]);
        for (Referral__c referral : referrals) {
            results.add(new LookupSearchResult(referral.Id, 'Referral', referralIcon, referral.Name,''));
        }
        return results;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> searchCampaign(String searchTerm) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
            Campaign (Id, Name)
            LIMIT :MAX_RESULTS];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Opportunities & convert them into LookupSearchResult
        String campIcon = 'standard:campaign';
        Campaign [] campaigns = ((List<Campaign>) searchResults[0]);
        for (Campaign camp : campaigns) {
            results.add(new LookupSearchResult(camp.Id, 'Campaign', campIcon, camp.Name, ''));
        }
        return results;
    }
    
    @AuraEnabled(cacheable=true)
	public static PagedResult getListData(Decimal pageSize,Decimal pageNumber) {
		Integer pSize = (Integer)pageSize;
		Integer offset = ((Integer)pageNumber - 1) * pSize;
		PagedResult res = new PagedResult();
		res.page = (Integer)pageNumber;
		List<Referral__c> results = [select id,Name,Referral_Branch_Name__c from Referral__c LIMIT :pSize OFFSET :offset];
        res.total = [select count() from Referral__c]; 
        res.data = results;
        return res;
	}
    @AuraEnabled
	public static Campaign findCampaign(String idCamp) {
		Campaign camp = [select id,Name from Campaign where id = :idCamp limit 1];
        return camp;
    }
    
    @AuraEnabled
	public static Referral__c findRef(String idRef) {
		Referral__c camp = [select id,Name from Referral__c where id = :idRef limit 1];
        return camp; 
	}
    
    @AuraEnabled
	public static void SaveLookup(List<String> selection, String campaignId,String optionSelected) {
        List<CampaignMember_c__c> campMembers = new List<CampaignMember_c__c>();
        for(String selectionId : selection){
            CampaignMember_c__c campMember = new CampaignMember_c__c();
            campMember.Campaign__c = campaignId;
            campMember.ReferralId__c = selectionId;
            campMember.Status__c = optionSelected;
            campMembers.add(campMember);
        }
        System.debug(campMembers);
        upsert campMembers;
	}
    public class PagedResult {
		@AuraEnabled
		public Integer page { get;set; }

		@AuraEnabled
		public Integer total { get;set; }

		@AuraEnabled
		public List<Referral__c> data { get;set; }
	}
}