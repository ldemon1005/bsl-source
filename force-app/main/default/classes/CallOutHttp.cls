global class CallOutHttp {
     @Future(callout=true)
     Public static void updateLatLng(String acc_id)
     {
        List <Account> accounts = [SELECT Id, BillingStreet, BillingCity, BillingCountry,BillingPostalCode, BillingState, BillingLatitude, BillingLongitude FROM Account WHERE Id = :acc_id];
        String geocodingKey = 'AIzaSyDvExojagB2ARgQNagPQMBK1Jvz-R0HGY4';
        List<Account> result = new List<Account>();
        for(Account acc : accounts){
            if((acc.BillingStreet != null) || (acc.BillingCity != null)) {
                    // create a string for the address to pass to Google Geocoding API
                    String geoAddress = '';
                    if(acc.BillingStreet != null){
                        geoAddress += acc.BillingStreet + ', ';
                    }
                    if(acc.BillingCity != null){
                        geoAddress += acc.BillingCity + ', ';
                    }
                    if(acc.BillingState != null){
                        geoAddress += acc.BillingState + ', ';
                    }
                    if(acc.BillingCountry != null){
                        geoAddress += acc.BillingCountry + ', ';
                    }
                        if(acc.BillingPostalCode != null){
                            geoAddress += acc.BillingPostalCode;
                        }
                  
                        // encode the string so we can pass it as part of URL
                        geoAddress = EncodingUtil.urlEncode(geoAddress, 'UTF-8');
                        // build and make the callout to the Geocoding API
                        Http http = new Http();
                        HttpRequest request = new HttpRequest();
                        request.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' + geoAddress + '&key=' + geocodingKey + '&sensor=false');
                        request.setMethod('GET');
                        request.setTimeout(60000);
                        try {
                            System.debug('alo alo 1234');
                            // make the http callout
                            HttpResponse response = http.send(request);
                            System.debug('alo alo');
                            System.debug(response);
                            // parse JSON to extract co-ordinates
                            JSONParser responseParser = JSON.createParser(response.getBody());
                           
                            // initialize co-ordinates
                            double latitude = null;
                            double longitude = null;
                            while(responseParser.nextToken() != null) {
                                if((responseParser.getCurrentToken() == JSONToken.FIELD_NAME) && (responseParser.getText() == 'location')) {
                                    responseParser.nextToken();
                                    while(responseParser.nextToken() != JSONToken.END_OBJECT) {         
                                        String locationText = responseParser.getText();
                                        responseParser.nextToken();
                                        if (locationText == 'lat'){
                                            latitude = responseParser.getDoubleValue();
                                        } else if (locationText == 'lng'){
                                            longitude = responseParser.getDoubleValue();
                                        }
                                    }
                                }
                            }
                            if(latitude != null) {
                              	acc.BillingLongitude = longitude;
                                acc.BillingLatitude  = latitude;
                                result.add(acc);
                            }
                        } catch (Exception e) {
                            System.debug(LoggingLevel.ERROR, 'Error Geocoding Address - ' + e.getMessage());
                        }
                    }
                }
         	System.debug('chào');
         	System.debug(result);
            update result;
	 }
}