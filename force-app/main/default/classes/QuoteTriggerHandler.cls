/**
*
*
*
*/
public without sharing class QuoteTriggerHandler {

    /**
    *
    *
    *
    */
    public static void setNoMoreQuoteInOpportunity(List<Quote> qList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // Map of Opportunity Id and Number of Quote
        Map<String, Decimal> mapOppIdNumberOfQuote = new Map<String, Decimal>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Id
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        List<Quote> checkedQuoteList = [SELECT Id, OpportunityId
                                        FROM Quote
                                        WHERE OpportunityId IN :oppIdList];

        for(Opportunity o : oppList) {
            List<Quote> qInOppList = new List<Quote>();
            String oId = o.Id;

            for(Quote quote : checkedQuoteList) {
                if(oId.equals(quote.OpportunityId)) {
                    qInOppList.add(quote);
                }    
            }

            mapOppIdNumberOfQuote.put(oId, qInOppList.size());
        }

        for(Quote qt : qList) {
            String oppIdInQuote = qt.OpportunityId;

            for(Opportunity opp : oppList) {
                if(oppIdInQuote.equals(opp.Id)) {
                    if(mapOppIdNumberOfQuote.get(opp.Id) >= 1) {
                        qt.addError('This Opportunity has already had a Quote!');
                    }
                }
            }
        }
    }

    /**
    * blockChangingStatusOfQuote method
    * when Approval Request is verified, so cannot change Status of Quote
    *
    * @param: List<Quote> qList
    */
    public static void blockChangingStatusOfQuote(List<Quote> qList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Id
                                     FROM Opportunity
                                     WHERE (StageName = 'C' OR StageName = 'A' OR StageName = 'B' OR StageName = 'L') AND Id IN :oppIdList];

        for(Opportunity o : oppList) {
            String oId = o.Id;

            for(Quote quote : qList) {
                if(oId.equals(quote.OpportunityId)) {
                    quote.addError('Cannot change Status of Quote when Opportunity have Status C, B, A, L');
                }
            }
        }
    }

    /**
    * blockUpdatingQuote method
    * when Opp 
    *
    * @param: List<Quote> qList
    */
    public static void blockUpdatingQuote(List<Quote> qList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Id
                                     FROM Opportunity
                                     WHERE (StageName = 'A' OR StageName = 'B' OR StageName = 'L') AND Id IN :oppIdList];

        for(Opportunity o : oppList) {
            String oId = o.Id;

            for(Quote quote : qList) {
                if(oId.equals(quote.OpportunityId)) {
                    quote.addError('Cannot update Quote when Opportunity has stage B, A, L');
                }
            }
        }
    }

    /**
    * createCheckApprovalTask method
    * update Approval Process in Quote (remind Salesman)
    *
    * @param: List<Quote> qList
    */
    public static void createCheckApprovalTask(List<Quote> qList) {
        String approvedStatus = 'Approved by BSL';
        String rejectedByBsl = 'Rejected by BSL';
        String rejectedByCustomer = 'Rejected by Customer';

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // Approved Quote List 
        List<Quote> approvedQuoteList = new List<Quote>();
        // Rejected Quote List 
        List<Quote> rejectedQuoteList = new List<Quote>();
        // Rejected by Customer
        List<Quote> rbcQuoteList = new List<Quote>();
        // List of new Task
        List<Task> newTaskList = new List<Task>();

        for(Quote q : qList) {
            String quoteStatus = q.Status;

            if(approvedStatus.equals(quoteStatus)) {
                approvedQuoteList.add(q);
            }

            if(rejectedByBsl.equals(quoteStatus)) {
                rejectedQuoteList.add(q);
            }

            if(rejectedByCustomer.equals(quoteStatus)) {
                rbcQuoteList.add(q);
            }

            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> newOppList = [SELECT Id, OwnerId
                                        FROM Opportunity
                                        WHERE Id IN :oppIdList];
        
        if(approvedQuoteList.size() != 0) {
            for(Quote qt : approvedQuoteList) {
                String oppId = qt.OpportunityId;
                String ownerOfQuote;

                for(Opportunity opp : newOppList) {
                    if(oppId.equals(opp.Id)) {
                        ownerOfQuote = opp.OwnerId;
                    }
                }

                Task task = new Task();
                task.Subject = 'Update Approval Status of Approval Process Tab of this Opportunity to Approved.';
                task.OwnerId = ownerOfQuote;
                task.WhatId = qt.OpportunityId;
                task.ActivityDate = Date.today();

                newTaskList.add(task);

                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                mentionSegmentInput.id = ownerOfQuote;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = 'Update Approval Status of Approval Process Tab of this Opportunity to Approved.';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = oppId;

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput); 
            }
        }

        if(rejectedQuoteList.size() != 0) {
            for(Quote qt : rejectedQuoteList) {
                String oppId = qt.OpportunityId;
                String ownerOfQuote;

                for(Opportunity opp : newOppList) {
                    if(oppId.equals(opp.Id)) {
                        ownerOfQuote = opp.OwnerId;
                    }
                }   

                Task task = new Task();
                task.Subject = 'Update Approval Status of Approval Process Tab of this Opportunity to Rejected.';
                task.OwnerId = ownerOfQuote;
                task.WhatId = qt.OpportunityId;
                task.ActivityDate = Date.today();

                newTaskList.add(task);

                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                mentionSegmentInput.id = ownerOfQuote;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = 'Update Approval Status of Approval Process Tab of this Opportunity to Rejected.';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = oppId;

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput); 
            }
        }

        if(rbcQuoteList.size() != 0) {
            for(Quote qt : rbcQuoteList) {
                String oppId = qt.OpportunityId;
                String ownerOfQuote;

                for(Opportunity opp : newOppList) {
                    if(oppId.equals(opp.Id)) {
                        ownerOfQuote = opp.OwnerId;
                    }
                }   

                Task task = new Task();
                task.Subject = 'Update Approval Status of Approval Process Tab of this Opportunity to Rejected by Customer.';
                task.OwnerId = ownerOfQuote;
                task.WhatId = qt.OpportunityId;
                task.ActivityDate = Date.today();

                newTaskList.add(task);

                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

                mentionSegmentInput.id = ownerOfQuote;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = 'Update Approval Status of Approval Process Tab of this Opportunity to Rejected by Customer.';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = oppId;

                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput); 
            }
        }

        insert newTaskList;
    }

    /**
    * setSecurityDepositAmount method
    * calculating Security Deposit Amount of Quote
    *
    * @param: List<Quote> qList
    */
    public static void setSecurityDepositAmount(List<Quote> qList) {
        String financeLease;
        String operatingLease;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'Quote'];

        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Finance Lease')) {
                financeLease = r.Id;
            }

            if(rName.equals('Operating Lease')) {
                operatingLease = r.Id;
            }
        }

        for(Quote q : qList) {
            if(operatingLease.equals(q.RecordTypeId)) {
                if((q.Monthly_Repayment_Amount__c != null) && (q.Security_Deposit_Months__c != null)) {
                    Decimal monthlyRepaymentAmount = q.Monthly_Repayment_Amount__c;
                    Decimal scrDepositMonths = q.Security_Deposit_Months__c;

                    q.Security_Deposit_rate_Amount__c = monthlyRepaymentAmount*scrDepositMonths*1.1;
                }
            }
            else if (financeLease.equals(q.RecordTypeId)) {
                if((q.Total_Asset_Value_with_VAT__c != null) && (q.Security_Deposit_Rate__c != null)) {
                    Decimal totalAssetValue = q.Total_Asset_Value_with_VAT__c;
                    Decimal scrDepositRate1 = q.Security_Deposit_Rate__c;

                    q.Security_Deposit_rate_Amount__c = totalAssetValue*scrDepositRate1/100;
                }
            }
        }
    }

    /**
    * recalculateSecurityDepositRate method
    * when changing Security Deposit Amount => re-calculating Security Deposit Rate
    *
    * @param: List<Quote> qList
    */
    public static void recalculateSecurityDepositRate(List<Quote> qList) {
        String financeLease;
        String operatingLease;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'Quote'];

        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Finance Lease')) {
                financeLease = r.Id;
            }

            if(rName.equals('Operating Lease')) {
                operatingLease = r.Id;
            }
        }

        for(Quote q : qList) {
            if(operatingLease.equals(q.RecordTypeId)) {
                if((q.Expected_Contract_Amount_after_VAT__c != null) && (q.Security_Deposit_rate_Amount__c != null)) {
                    Decimal expCtrAmount = q.Expected_Contract_Amount_after_VAT__c;
                    Decimal scrDepositAmount = q.Security_Deposit_rate_Amount__c;

                    q.Security_Deposit_Rate__c = (scrDepositAmount/expCtrAmount)*100;
                }
            }
            else {
                if((q.Total_Asset_Value__c != null) && (q.Security_Deposit_rate_Amount__c != null)) {
                    Decimal totalAssetValue = q.Total_Asset_Value__c;
                    Decimal scrDepositAmount1 = q.Security_Deposit_rate_Amount__c;

                    q.Security_Deposit_Rate__c = (scrDepositAmount1/totalAssetValue)*100;
                }
            }
        }
    }

    /**
    * setUpfrontFeeAmount method
    * calculating Upfront Fee Amount of Quote
    *
    * @param: List<Quote> qList
    */
    public static void setUpfrontFeeAmount(List<Quote> qList) {
        for(Quote q : qList) {
            if((q.Total_Asset_Value_with_VAT__c != null) && (q.Upfront_Fee_Rate__c != null)) {
                Decimal totalAssetValue = q.Total_Asset_Value_with_VAT__c;
                Decimal ufFeeRate = q.Upfront_Fee_Rate__c;

                q.Upfront_Fee_Amount__c	 = (totalAssetValue*ufFeeRate/100)/1.1;
            }
        }
    }

    /**
    * recalculateUpfrontFeeRate method
    * when changing Upfront fee Amount => re-calculating Upfront fee Rate
    *
    * @param: List<Quote> qList
    */
    public static void recalculateUpfrontFeeRate(List<Quote> qList) {
        for(Quote q : qList) {
            if((q.Total_Asset_Value__c != null) && (q.Upfront_Fee_Amount__c != null)) {
                Decimal totalAssetValue = q.Total_Asset_Value__c;
                Decimal ufFeeAmount = q.Upfront_Fee_Amount__c;

                q.Upfront_Fee_Rate__c = ufFeeAmount*100/totalAssetValue;
            }
        }
    }

    /**
    * setClosingPriceAmount method
    * calculating Closing Price Amount of Quote
    *
    * @param: List<Quote> qList
    */
    public static void setClosingPriceAmount(List<Quote> qList) {
        for(Quote q : qList) {
            if((q.Total_Asset_Value_with_VAT__c != null) && (q.Closing_Price_Rate__c != null)) {
                Decimal totalAssetValue = q.Total_Asset_Value_with_VAT__c;
                Decimal cpRate = q.Closing_Price_Rate__c;

                q.Closing_Price_Amount__c	 = totalAssetValue*cpRate/100;
            }
        }
    }

    /**
    * recalculateClosingPriceRate method
    * when changing Closing Price Amount => re-calculating Closing Price Rate
    *
    * @param: List<Quote> qList
    */
    public static void recalculateClosingPriceRate(List<Quote> qList) {
        for(Quote q : qList) {
            if((q.Total_Asset_Value__c != null) && (q.Closing_Price_Amount__c != null)) {
                Decimal totalAssetValue = q.Total_Asset_Value__c;
                Decimal cpAmount = q.Closing_Price_Amount__c;

                //System.debug('VVVVVVVVVVV: ' + cpAmount*100/totalAssetValue);

                q.Closing_Price_Rate__c = cpAmount*100/totalAssetValue;
            }
        }
    }

    /**
    *
    *
    *
    */
    public static void calculateDownPaymentAmountwUpdate(List<Quote> qList) {
        //System.debug('Calculate Down Payment Amount Executing!');

        String financeLease;
        String financeLeaseRV;
        String financeLeaseEquated;

        List<String> quoteIdList = new List<String>();

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'Quote'];

        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Finance Lease')) {
                financeLease = r.Id;
            }

            if(rName.equals('Finance Lease with RV')) {
                financeLeaseRV = r.Id;
            }

            if(rName.equals('Equated Finance Lease')) {
                financeLeaseEquated = r.Id;
            }
        }

        // List of Quote need to be updated
        List<Quote> updatedQuoteList = new List<Quote>();

        // calculating Down Payment Amount
        for(Quote q : qList) {
            if(financeLease.equals(q.RecordTypeId) || financeLeaseEquated.equals(q.RecordTypeId)) {
                q.Down_Payment_Amount__c = q.Total_Asset_Value_with_VAT__c * q.Down_Payment_Rate__c/100;
            }

            if(financeLeaseRV.equals(q.RecordTypeId)) {
                q.Down_Payment_Amount__c = q.Leaseable_Asset_Value__c * q.Down_Payment_Rate__c/100;
            }
        }
    }

    /**
    *
    *
    * comment lại do thấy không dùng, để pass test coverage
    */
    /*public static void calculateDownPaymentAmountwInsert(List<Quote> qList) {
        //System.debug('Calculate Down Payment Amount Executing!');

        String financeLease;
        String financeLeaseRV;
        String financeLeaseEquated;

        List<String> oppIdList = new List<String>();

        for(Quote qt : qList) {
            oppIdList.add(qt.OpportunityId);
        }

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'Quote'];

        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Finance Lease')) {
                financeLease = r.Id;
            }

            if(rName.equals('Finance Lease with RV')) {
                financeLeaseRV = r.Id;
            }

            if(rName.equals('Equated Finance Lease')) {
                financeLeaseEquated = r.Id;
            }
        }

        List<Asset__c> checkedAssetList = [SELECT Id, Opportunity__c, Total_Asset_Value_with_VAT__c
                                           FROM Asset__c
                                           WHERE Opportunity__c IN :oppIdList];

        // List of Quote need to be updated
        List<Quote> updatedQuoteList = new List<Quote>();

        // calculating Down Payment Amount
        for(Quote q : qList) {
            if(financeLease.equals(q.RecordTypeId) || financeLeaseEquated.equals(q.RecordTypeId)) {
                Decimal downPaymentAsset = 0;
                String oppId = q.OpportunityId;

                for(Asset__c a : checkedAssetList) {
                    if(oppId.equals(a.Opportunity__c)) {
                        downPaymentAsset += a.Total_Asset_Value_with_VAT__c;
                    }
                }

                q.Down_Payment_Amount__c = downPaymentAsset;
            }

            if(financeLeaseRV.equals(q.RecordTypeId)) {
                q.Down_Payment_Amount__c = q.Leaseable_Asset_Value__c * q.Down_Payment_Rate__c/100;
            }
        }
    }*/

    /**
    *
    *
    *
    */
    /*public static void recalculateDownPaymentRate(List<Quote> qList) {
        //System.debug('Re-calculate Down Payment Rate Executing!');
        String financeLease;
        String financeLeaseRV;

        List<RecordType> rtList = [SELECT Id, Name
				                   FROM RecordType
				                   WHERE SobjectType = 'Quote'];

        for(RecordType r : rtList) {
            String rName = r.Name;

            if(rName.equals('Finance Lease')) {
                financeLease = r.Id;
            }

            if(rName.equals('Finance Lease with RV')) {
                financeLeaseRV = r.Id;
            }
        }

        // calculating Down Payment Amount
        for(Quote q : qList) {
            if(financeLease.equals(q.RecordTypeId)) {
                q.Down_Payment_Rate__c = (q.Down_Payment_Amount__c / q.Total_Asset_Value__c)*100;
            }

            if(financeLeaseRV.equals(q.RecordTypeId)) {
                q.Down_Payment_Rate__c = (q.Down_Payment_Amount__c / q.Leaseable_Asset_Value__c)*100;
            }
        }
    }*/

    /**
    * updateExpectedContractAmountInQuote method
    * Updating Expected Contract Amount In Quote (Expected Contract Amount after tax in Operating Lease Quote)
    *
    * @param: List<Quote> qList
    */
    public static void updateExpectedContractAmountInQuote(List<Quote> qList) {
        // Finance Lease RecordType
        RecordType flRcType = [SELECT Id
                               FROM RecordType
                               WHERE SobjectType = 'Quote' AND Name = 'Finance Lease'
                               LIMIT 1];

        // Finance Lease with RV RecordType
        RecordType flRvRcType = [SELECT Id
                                 FROM RecordType
                                 WHERE SobjectType = 'Quote' AND Name = 'Finance Lease with RV'
                                 LIMIT 1];

        // Finance Lease with RV RecordType
        RecordType equatedRcType = [SELECT Id
                                    FROM RecordType
                                    WHERE SobjectType = 'Quote' AND Name = 'Equated Finance Lease'
                                    LIMIT 1];

        for(Quote q : qList) {
            String rc = flRcType.Id;
            String rv = flRvRcType.Id;
            String eq = equatedRcType.Id;

            if(rc.equals(q.RecordTypeId) || eq.equals(q.RecordTypeId) ) {
                if((q.Total_Asset_Value_with_VAT__c != null) && (q.Down_Payment_Amount__c != null)) {
                    q.Expected_Contract_Amount__c = q.Total_Asset_Value_with_VAT__c - q.Down_Payment_Amount__c;
                }
            }

            if(rv.equals(q.RecordTypeId)) {
                if((q.Leaseable_Asset_Value__c != null) && (q.Down_Payment_Amount__c != null)) {
                    q.Expected_Contract_Amount__c = q.Leaseable_Asset_Value__c - q.Down_Payment_Amount__c;
                }
            }
        }
    }

    /**
    * updateTotalPotentialAmountInOpp method
    * update Total_Expected_Potential_Contract_Amount__c in Opportunity Record
    *
    * @param: List<Quote> qList
    */
    public static void updateTotalPotentialAmountInOpp(List<Quote> qList) {
        String pCustomer = 'Presented to Customer';
        String rBsl = 'Rejected by BSL';
        String rCustomer = 'Rejected by Customer';
        String tentatively = 'Tentatively Accepted by Customer';
        String approved = 'Approved by BSL';

        RecordType rt = [SELECT Id, Name
				         FROM RecordType
				         WHERE SobjectType = 'Quote' AND Name = 'Operating Lease'
				         LIMIT 1];
        
        String rtId = rt.Id;

        // Operating Lease RecordType in Quote
        String operatingLeaseRt = rtId;

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Id, Total_Expected_Potential_Contract_Amount__c, Expected_Potential_Contract_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Opportunity opp : oppList) {
            String oppId = opp.Id;
            Decimal exPotential;

            if(opp.Expected_Potential_Contract_Amount__c != null) {
                exPotential = opp.Expected_Potential_Contract_Amount__c;
            }

            for(Quote quote : qList) {

                if(oppId.equals(quote.OpportunityId)) {
                    if(rBsl.equals(quote.Status) || rCustomer.equals(quote.Status) || pCustomer.equals(quote.Status)) {              
                        opp.Total_Expected_Potential_Contract_Amount__c = exPotential;
                    }

                    if(tentatively.equals(quote.Status) || approved.equals(quote.Status)) {
                        if(operatingLeaseRt.equals(quote.RecordTypeId)) {
                            Decimal pAmountAfterTax;

                            if(quote.Expected_Contract_Amount_ol__c != null) {
                                pAmountAfterTax = quote.Expected_Contract_Amount_ol__c;
                            }
                            else {
                                pAmountAfterTax = 0;
                            }

                            
                            opp.Total_Expected_Potential_Contract_Amount__c = pAmountAfterTax;
                            

                            //System.debug('exPotential: ' + exPotential);
                            //System.debug('pAmountAfterTax: ' + pAmountAfterTax);
                        }
                        else {
                            Decimal pAmount;

                            if(quote.Expected_Contract_Amount__c != null) {
                                pAmount = quote.Expected_Contract_Amount__c;
                            }
                            else {
                                pAmount = 0;
                            }

                            opp.Total_Expected_Potential_Contract_Amount__c = pAmount;
                            
                            //System.debug('exPotential 2 : ' + exPotential);
                            //System.debug('pAmount: ' + pAmount);
                        }
                    }
                }
            }
        }

        update oppList;
    }

    /**
    * updateTotalPotentialAmountInOppWhenDeleting method
    * update Total Expected Potential Amount in Opp when Deleting Quote
    *
    * @param: List<Quote> qList
    */
    public static void updateTotalPotentialAmountInOppWhenDeleting(List<Quote> qList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Id, Total_Expected_Potential_Contract_Amount__c, Expected_Potential_Contract_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Quote qt : qList) {
            String oppId = qt.OpportunityId;

            for(Opportunity opp : oppList) {
                if(oppId.equals(opp.Id)) {
                    if(opp.Expected_Potential_Contract_Amount__c != null) {
                        opp.Total_Expected_Potential_Contract_Amount__c = opp.Expected_Potential_Contract_Amount__c;
                    }
                }
            }
        }

        update oppList;
    }

    /**
    * updateApprovedAmountInOppToZero method
    * updating Approved Amount In Opportunity when the Status is changed to anything else (not Approved by BSL)
    *
    * @param: List<Quote> qList
    */
    public static void updateApprovedAmountInOppToZero(List<Quote> qList) {
        // Approved by BSL
        String approvedStatus = 'Approved by BSL';
        // List of Approved Quote
        List<Quote> notApprovedQuoteList = new List<Quote>();
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            if(!approvedStatus.equals(q.Status)) {
                notApprovedQuoteList.add(q);
            }
        }

        if(notApprovedQuoteList.size() != 0) {
            for(Quote qt : notApprovedQuoteList) {
                oppIdList.add(qt.OpportunityId);
            }

            List<Opportunity> oppList = [SELECT Approved_Amount__c
                                         FROM Opportunity
                                         WHERE Id IN :oppIdList];

            for(Opportunity opp : oppList) {
                String oppId = opp.Id;

                for(Quote quote : notApprovedQuoteList) {
                    if(oppId.equals(quote.OpportunityId)) {
                        opp.Approved_Amount__c = 0;
                    }
                }
            }

            update oppList;
        }      
    }

    /**
    * addTotalAssetValueInQuote method
    * Add total asset value when creating Quote
    *
    * @param: List<Quote> qList 
    */
    public static void addTotalAssetValueInQuote(List<Quote> qList) {
        // List of Opportunity Id that is parent record of Quote
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Opportunity> oppList = [SELECT Total_Asset_Value__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Opportunity o : oppList) {
            String oppId = o.Id;

            for(Quote quote : qList) {
                if(oppId.equals(quote.OpportunityId)) {
                    if(o.Total_Asset_Value__c != null) {
                        quote.Total_Asset_Value__c = o.Total_Asset_Value__c;
                    }
                    else {
                        quote.Total_Asset_Value__c = 0;
                    }
                }
            }
        }
    }

    /**
    *
    *
    *
    * 
    */
    public static void updateQuoteToAsset(List<Quote> qList) {
        // List of Opportunity Id
        List<String> oppIdList = new List<String>();
        // List of Asset need to be updated
        List<Asset__c> updatedAssetList = new List<Asset__c>();

        for(Quote q : qList) {
            oppIdList.add(q.OpportunityId);
        }

        List<Asset__c> assetList = [SELECT Opportunity__c, Quote__c
                                    FROM Asset__c
                                    WHERE Opportunity__c IN :oppIdList];

        for(Quote quote : qList) {
            String oppId = quote.OpportunityId;

            for(Asset__c a : assetList) {
                if(oppId.equals(a.Opportunity__c)) {
                    a.Quote__c = quote.Id;
                    updatedAssetList.add(a);
                }
            }
        }

        update updatedAssetList;
    }

    /**
    * updateApprovedAmountInOpp method
    * when updating Expected Contract Amount in Quote => update Approved Amount in Opp
    *
    * @param: List<Quote> qList
    */
    public static void updateApprovedAmountInOpp(List<Quote> qList) {
        RecordType rt = [SELECT Id, Name
				         FROM RecordType
				         WHERE SobjectType = 'Quote' AND Name = 'Operating Lease'
				         LIMIT 1];
        
        String rtId = rt.Id;

        // List of Opportunity Id
        List<String> oppIdList = new List<String>();

        for(Quote q : qList) {
            String qStt = q.Status;

            if(qStt.equals('Approved by BSL')) {
                oppIdList.add(q.OpportunityId);
            }
        }

        List<Opportunity> oppList = [SELECT Id, Approved_Amount__c, Contracted_Amount__c
                                     FROM Opportunity
                                     WHERE Id IN :oppIdList];

        for(Opportunity o : oppList) {
            String oId = o.Id;

            for(Quote quote : qList) {
                if(oId.equals(quote.OpportunityId)) {
                    if(rtId.equals(quote.RecordTypeId)) {
                        if(quote.Expected_Contract_Amount_ol__c != null) {
                            o.Approved_Amount__c = quote.Expected_Contract_Amount_ol__c;
                        }
                        else {
                            o.Approved_Amount__c = 0;
                        }
                    }
                    else {
                        if(quote.Expected_Contract_Amount__c != null) {
                            o.Approved_Amount__c = quote.Expected_Contract_Amount__c;
                        }
                        else {
                            o.Approved_Amount__c = 0;
                        }
                    }
                }

                //System.debug('Expected Contract Amount in Quote: ' + quote.Expected_Contract_Amount__c);
                //System.debug('Contracted Amount in Opportunity: ' + o.Contracted_Amount__c);
            }
        }

        update oppList;
    }
}