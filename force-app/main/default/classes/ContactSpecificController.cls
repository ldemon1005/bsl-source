public class ContactSpecificController {
 		public ApexPages.StandardController controller{get;set;}
   
    public ContactSpecificController(ApexPages.StandardController controller) {
        this.controller = controller;
    }
    
    public ContactSpecificController(ApexPages.StandardSetController stdSetController) {
	}

  
    public PageReference redirect() {     
    	List<RecordType> rts = [SELECT Id FROM RecordType WHERE SObjectType='Contact'];
		System.debug(rts);
	    return new PageReference ('/003/e?nooverride=1&RecordType=012N0000000a44JIAQ');  
    }
}