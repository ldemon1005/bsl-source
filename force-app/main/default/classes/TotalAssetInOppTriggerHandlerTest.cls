@IsTest(SeeAllData=true)
public class TotalAssetInOppTriggerHandlerTest {
    @isTest() static void test(){
        RecordType rt = [SELECT Id, Name
                         FROM RecordType
                         WHERE SobjectType = 'Quote' AND Name = 'Finance Lease'
                         LIMIT 1];
        
        Opportunity opp1= new Opportunity();
        opp1.Name='Test' ;
        opp1.StageName = 'D1';
        opp1.CloseDate = System.today() + 5;
        insert opp1;
        
        Opportunity opp2= new Opportunity();
        opp2.Name='Test' ;
        opp2.StageName = 'D1';
        opp2.CloseDate = System.today() + 5;
        insert opp2;
        
        Quote quote = new Quote();
        quote.Name = 'test';
        quote.RecordTypeId = rt.Id; //thay đổi khi deploy
        quote.Status = 'Tentatively Accepted by Customer';
        quote.Down_Payment_Rate__c = 10;
        quote.Down_Payment_Paid_To__c = 'BSL';
        quote.Lease_Term__c = 3;
        quote.Interest_Base__c = 6;
        quote.Margin__c = 6;
        quote.Fixed_Rate_In_The_First_Period__c = 6;
        quote.Number_Of_Months_For_Fixed_Rate__c = 6;
        quote.Security_Deposit_rate_Amount__c = 6;
        quote.Closing_Price_Rate__c = 6;
        quote.Grace_Period__c = 0;
        quote.IRR__c = 6;
        quote.OpportunityId = opp2.Id;
        
        Insert quote;
        
       	Supplier__c supplier = new Supplier__c();
        supplier.Name = 'supplier test';
        insert supplier;
        
        Asset__c asset = new Asset__c();
        asset.Name = 'asset test';
        asset.Opportunity__c = opp1.Id;
        asset.Supplier__c = supplier.Id;
        asset.Currency__c = 'USD';
        asset.Expected_Contract_Amount__c = 5;
        
        List<Asset__c> assetList = new list<Asset__c>();
        assetList.add(asset);
        insert assetList;
        
        TotalAssetInOppTriggerHandler.setTotalAssetInOpportunity(assetList);
    }
}