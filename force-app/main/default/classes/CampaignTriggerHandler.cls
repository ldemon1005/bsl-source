/**
* CMC Technology & Solution
* CampaignTriggerHandler class
* 
* @author: tqtai
*/
public without sharing class CampaignTriggerHandler {
    /**
    * 
    *
    *
    *
    */
    public static void createNotificationForAllUser(List<Campaign> cpList, String body, String comment) {
        

        // List of Salesman Dev Name
        List<String> salesmanDevNameList = new List<String>{'DanangBusiness1PartnerUser','HanoiBusiness1PartnerUser','HoChiMinhBusiness1PartnerUser'};
        // List of User Role Id
        List<String> srIdList = new List<String>();
        // List of Salesman User Role
        List<UserRole> srList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :salesmanDevNameList];

        for(UserRole ur : srList) {
            srIdList.add(ur.Id);
        }

        // List of Salesman User
        List<User> smList = [SELECT Id 
                             FROM User
                             WHERE UserRoleId IN :srIdList
                             LIMIT 10];

        System.debug('Checked User List Size: ' + smList.size());

        // List of User Role Dev Name
        List<String> urDevNameList = new List<String>{'BSLPartnerManager',
                                                      'DanangBusiness1PartnerManager',
                                                      'HanoiBusiness1PartnerManager',
                                                      'HoChiMinhBusiness1PartnerManager',
                                                      'Deputy_Director_of_Hanoi_Branch',
                                                      'Deputy_Director_of_Ho_Chi_Minh_Branch',
                                                      'Director_of_Danang_Branch',
                                                      'Director_of_Hanoi_Branch',
                                                      'Director_of_Ho_Chi_Minh_Branch',
                                                      'Manager_of_Danang_Business_1',
                                                      'Manager_of_Danang_Business_2',
                                                      'Manager_of_Hanoi_Business_1',
                                                      'Manager_of_Hanoi_Business_2',
                                                      'Manager_of_Ho_Chi_Minh_Business_1',
                                                      'Manager_of_Ho_Chi_Minh_Business_2'};
        // List of User Role Id
        List<String> urIdList = new List<String>();
        // List of User Role
        List<UserRole> urList = [SELECT Id
                                 FROM UserRole
                                 WHERE DeveloperName IN :urDevNameList];

        for(UserRole ur : urList) {
            urIdList.add(ur.Id);
        }

        // List of Director User
        List<User> uList = [SELECT Id 
                            FROM User
                            WHERE UserRoleId IN :urIdList
                            LIMIT 10];

        System.debug('Checked User List Size: ' + uList.size());
        
        for(Campaign c : cpList) {
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            for(User u : uList) {
                System.debug('IN LOOP 2222222222');
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput.id = u.Id;
                messageBodyInput.messageSegments.add(mentionSegmentInput);
            }

            textSegmentInput.text = body;
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = c.Id;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;

            System.debug('IN LOOP 111111111');

            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);

            System.debug('Feed Element Id: ' + feedElement.Id);


            // Add comments
            String communityId = null;
            String feedElementId = feedElement.Id;

            ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
            ConnectApi.MessageBodyInput messageBodyInput1 = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput1 = new ConnectApi.TextSegmentInput();

            messageBodyInput1.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            for(User u : smList) {
                System.debug('IN LOOP 33333333');
                ConnectApi.MentionSegmentInput mentionSegmentInput1 = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput1.id = u.Id;
                messageBodyInput1.messageSegments.add(mentionSegmentInput1);
            }

            textSegmentInput1.text = comment;
            messageBodyInput1.messageSegments.add(textSegmentInput1);

            commentInput.body = messageBodyInput1;

            ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(communityId, feedElementId, commentInput, null);
        }
    }
}