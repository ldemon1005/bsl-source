@isTest(SeeAllData=true)
private class TaskTriggerTest {
	@isTest static void TestInsertTask() {
        Opportunity opp = new Opportunity();
        opp.Name='Test' ;
        opp.StageName = 'D1';
        opp.CloseDate = System.today() + 5;
        Insert opp ; 
        
        
        Task t = new Task(Subject='Test Task', Type='Email', ActivityDate = Date.today(), WhatId = opp.Id);
        insert t;
        
        t.Status = 'Completed';
        t.Type = 'Call';
        update t;
        
        // Perform test
        Test.startTest();
        Database.DeleteResult result = Database.delete(t, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestDeleteTask() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Task t = new Task(Subject='Test Task', Type='Email', ActivityDate = Date.today());
        insert t;

        
        // Perform test
        Test.startTest();
        Database.SaveResult srList = Database.insert(t, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(srList.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestUpdateTask() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Task t = new Task(Subject='Test Task', Type='Email', ActivityDate = Date.today());
        insert t;
        
        // Perform test
        Test.startTest();
        t.Status = 'Completed';
        Database.SaveResult result = Database.update(t, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
}