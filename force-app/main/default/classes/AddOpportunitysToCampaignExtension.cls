public with sharing class AddOpportunitysToCampaignExtension {
	public ApexPages.StandardSetController  controller;

    public AddOpportunitysToCampaignExtension(ApexPages.StandardSetController  constructor) {
 		controller = constructor;
        OpIds = '';
		OpList = controller.getSelected();  
       	for(Opportunity o : OpList){
            OpIds += o.id + ',';
        }
 	}
    
     public AddOpportunitysToCampaignExtension(ApexPages.StandardController constructor) {
      	currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
     }
    
    public String currentRecordId {get;set;}
    transient public String OpIds  {get; set;}
    
    transient public List<Opportunity> OpList  {get; set;}
    

    public pageReference deleteOps(){
		delete controller.getSelected();
        return controller.cancel();
 	}
	
    public String getOpsIdSelected(){
        return OpIds;
    }
}