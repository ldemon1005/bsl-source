@IsTest(SeeAllData=true)
private class AssetTriggerHandlerTest{
	
	@isTest static void test() {
		Test.startTest();
        
        Opportunity opp= new Opportunity();
        opp.Name='Test' ;
        opp.StageName = 'D1';
        opp.CloseDate = System.today() + 5;
        insert opp;
        
       	Supplier__c supplier = new Supplier__c();
        supplier.Name = 'supplier test';
        insert supplier;
        
        Asset__c asset = new Asset__c();
        asset.Name = 'asset test';
        asset.Opportunity__c = opp.Id;
        asset.Supplier__c = supplier.Id;
        asset.Currency__c = 'USD';
        asset.Expected_Contract_Amount__c = 5;
        
        List<Asset__c> assetList = new list<Asset__c>();
        assetList.add(asset);
        insert assetList;

        Asset__c aTest = [SELECT Id, Quantity__c
                          FROM Asset__c
                          WHERE Name = 'asset test'
                          LIMIT 1];

        aTest.Quantity__c = 3;
        update aTest;
        
        AssetTriggerHandler.setExpectedPotentialContractAmountInOppwDeleting(assetList);
        
		Test.stopTest();
		
		System.assertEquals('test1','test1');
		
	}
}