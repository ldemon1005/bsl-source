/**
 * @File Name          : AddReferralsToCampaignExtensionTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/5/2019, 8:57:09 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/5/2019, 8:57:09 AM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest(SeeAllData=true)
public class AddReferralsToCampaignExtensionTest {
    private static final RecordType rt = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                         						LIMIT 1];
    @isTest() static void test1(){
        Referral__c referral1 = new Referral__c();
        referral1.RecordTypeId = rt.Id;
        referral1.Name = 'referral';
        
        Referral__c referral2 = new Referral__c();
        referral2.RecordTypeId = rt.Id;
        referral2.Name = 'referral';
        
        list<Referral__c> l_referral = new list<Referral__c>();
        l_referral.add(referral1);
        l_referral.add(referral2);
        insert l_referral;
        
        
        PageReference pageRef = Page.AddReferralToCampaignPage;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(l_referral);
        stdSetController.setSelected(l_referral);
      
        AddReferralsToCampaignExtension ext = new AddReferralsToCampaignExtension(stdSetController);
        
        String id = ext.ReferralId;
        List<Referral__c> ll = ext.ReferralList;
        ext.deleteReferrals();
        String ReferralId = ext.currentRecordId;
        String ReferralId1 = ext.getReferralIdSelected();
    }
}