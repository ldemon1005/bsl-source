@isTest(SeeAllData=true)
private class QuoteTriggerHandlerTest{
	private static String getApprovalData(String bdAppr, String abd, String rjbd, String rbcbd, String bdV, 
							  String ccdAppr, String ccdV, 
							  String ccAppr, String ccV, 
							  String bomAppr, String bomV) {
		String appr = '{\n' +
        '   "Branch Director": {\n' +
        '       "Approval Authority": ' + bdAppr + ',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": ' + abd + ',\n' +
        '           "Returned": false,\n' +
        '           "Rejected": ' + rjbd + ',\n' +
        '           "Rbc": ' + rbcbd + '\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + bdV + '\n' +
        '   },\n' +
        '   "CCD": {\n' +
        '       "Approval Authority": ' + ccdAppr +',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + ccdV + '\n' +
        '   },\n' +
        '   "Credit Council": {\n' +
        '       "Approval Authority": ' + ccV +',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + ccV +'\n' +
        '   },\n' +
        '   "BOM": {\n' +
        '       "Approval Authority": ' + bomAppr + ',\n' +
        '       "Submission Date": "2019-05-29 00:00:00",\n' +
        '       "Approval Deadline": "2019-05-30 00:00:00",\n' +
        '       "Approval Status": {\n' + 
        '           "Approved": true,\n' +
        '           "Returned": false,\n' +
        '           "Rejected": false,\n' +
        '           "Rbc": false\n' +
        '       },\n' +
        '       "Approval Result Actual Date": "2019-05-30 00:00:00",\n' +
        '       "Verified by Leadership": ' + bomV + '\n' +
        '   }\n' +
        '}';

		return appr;
	}

    private static Referral__c createReferral(String rc) {
		Referral__c r = new Referral__c();

		r.RecordTypeId = rc; 
		r.Name = 'referral test';

		insert r;

		return r;
	}

	private static Account createAccount() {
		Account a = new Account();
		a.Business_Registration_Number__c = '222222';
		a.Name = 'Account Testing';
		a.BillingStreet = '11 duy tân';
		insert a;

		return a;
	}

	private static Opportunity createOpp(Account a, Referral__c r, String stage, String apprDt, Decimal apprAmount, Decimal ctrAmount, Decimal dbmAmount) {
		Date dt = Date.newInstance(2021, 1, 2);

		Opportunity o = new Opportunity();

		o.Name = 'Testing Opp';
		o.Referral_Source__c = r.Id;
		o.BSL_Service__c = 'Finance Lease';
		o.Contract_Ending_Selection__c = 'Ownership Transfer';
		o.StageName = stage;
		o.Approval_Data__c = apprDt;
		o.AccountId = a.Id;
		o.CloseDate = dt;
		o.Approved_Amount__c = apprAmount;
		o.Contracted_Amount__c = ctrAmount;
		o.Total_Disbursed_Amount__c = dbmAmount;

		insert o;

		return o;
	}

    private static Supplier__c createSupplier(String sName) {
		Supplier__c s = new Supplier__c(Name = sName);
		insert s;

		return s;
	}

	private static Asset__c createAsset(Opportunity opp, Supplier__c sp, Decimal pr, Decimal q, Decimal epc) {
		Asset__c asset = new Asset__c();
		asset.Name = 'Asset Test';
		asset.Currency__c = 'VND';
		asset.Opportunity__c = opp.Id;
		asset.Supplier__c = sp.Id;
		asset.Asset_Value_Before_Tax__c = pr;
		asset.Quantity__c = q;
		asset.Expected_Contract_Amount__c = epc;

		insert asset;

		return asset;
	}

    private static Quote createQuote(Opportunity o, String sq, Decimal dpRate, String dpPaid, Decimal sdRate, Decimal ufRate, Decimal cpRate) {
	    	RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Quote' AND Name = 'Finance Lease'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Quote q = new Quote();
		q.RecordTypeId = rtId; // Finance Lease
		q.Name = 'Quote test';
		q.Status = sq;
		q.OpportunityId = o.Id;
		q.Down_Payment_Rate__c = dpRate;
		q.Down_Payment_Paid_To__c = dpPaid;
		q.Lease_Term__c = 12;
		q.Frequency_of_Interest_Repayment__c = '3';
		q.Frequency_of_Repayment_month__c = '3';
		q.Interest_Base__c = 10;
		q.Margin__c = 10;
		q.Fixed_Rate_In_The_First_Period__c = 10;
		q.Number_Of_Months_For_Fixed_Rate__c = 10;
		q.Security_Deposit_Rate__c = sdRate;
		q.Upfront_Fee_Rate__c = ufRate;
		q.Closing_Price_Rate__c = cpRate;
		q.Grace_Period__c = 0;
		q.IRR__c = 10;

		insert q;

		return q;
	}

	private static Contract createContract(Account a, Opportunity o, Quote q, String sc, Decimal ctrAmount) {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Contract' AND Name = 'Finance Lease'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Contract ctr = new Contract();
		ctr.RecordTypeId = rtId;
		ctr.Name = 'Contract Test';
		ctr.AccountId = a.Id;
		ctr.Opportunity_Name__c = o.Id;
		ctr.SBQQ_Quote__c = q.Id;
		ctr.Contract_Amount__c = ctrAmount;
		ctr.StartDate = Date.newInstance(2019, 1, 1);
		ctr.Status = sc;
		ctr.ContractTerm = 12;

		insert ctr;

		return ctr;
	}

	@isTest
	static void setNoMoreQuoteInOpportunityTest() {
		try {
			String apprTest = getApprovalData('true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', 'false', 'false', 'false');

			RecordType rt = [SELECT Id, Name
					     FROM RecordType
					     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					     LIMIT 1];
        
			String rtId = rt.Id;

			Referral__c r = createReferral(rtId); // cross-sell BIDV
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
			String oId = o.Id;
			
			Quote q = createQuote(o, 'Approved by BSL', 10, 'Supplier', 10, 10, 10);
			Quote qTest = createQuote(o, 'Presented to Customer', 10, 'Supplier', 10, 10, 10);
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('This Opportunity has already had a Quote!');
			// System.AssertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}
	}

	@isTest
	static void blockUpdatingQuoteTest() {
		try {
			String apprTest = getApprovalData('true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', 'false', 'false', 'false');

			RecordType rt = [SELECT Id, Name
					     FROM RecordType
					     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					     LIMIT 1];
        
			String rtId = rt.Id;

			Referral__c r = createReferral(rtId); // cross-sell BIDV
			Account a = createAccount();
			Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
			String oId = o.Id;
			
			Quote q = createQuote(o, 'Approved by BSL', 10, 'Supplier', 10, 10, 10);
			o.Approved_Amount__c = 10000;
			o.Contracted_Amount__c = 8000;
			o.Total_Disbursed_Amount__c = 0;
			o.Approval_Data__c = apprTest;
			update o;
			Contract ctr = createContract(a, o, q, 'Activated', 8000);
			q.Margin__c = 12;
			update q;
		}
		catch(Exception e) {
			// Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot update Quote when Opportunity has stage B, A, L');
			// System.AssertEquals(expectedExceptionThrown, true);
			System.debug(e);
		}
	}

	@isTest
	static void createCheckApprovalTaskTest1() {
		RecordType rt = [SELECT Id, Name
					     FROM RecordType
					     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					     LIMIT 1];
        
		String rtId = rt.Id;

		Referral__c r = createReferral(rtId); // cross-sell BIDV
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		String oId = o.Id;
		
		Quote q = createQuote(o, 'Presented to Customer', 10, 'Supplier', 10, 10, 10);
		q.Status = 'Approved by BSL';
		update q;

		Task tTest = [SELECT Subject
				  FROM Task
				  WHERE WhatId = :oId AND Subject = 'Update Approval Status of Approval Process Tab of this Opportunity to Approved.'
				  LIMIT 1];

		System.assertNotEquals(null, tTest);
	}

	@isTest
	static void createCheckApprovalTaskTest2() {
		RecordType rt = [SELECT Id, Name
					     FROM RecordType
					     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					     LIMIT 1];
        
		String rtId = rt.Id;

		Referral__c r = createReferral(rtId); // cross-sell BIDV
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		String oId = o.Id;
		
		Quote q = createQuote(o, 'Presented to Customer', 10, 'Supplier', 10, 10, 10);
		q.Status = 'Rejected by BSL';
		update q;

		Task tTest = [SELECT Subject
				  FROM Task
				  WHERE WhatId = :oId AND Subject = 'Update Approval Status of Approval Process Tab of this Opportunity to Rejected.'
				  LIMIT 1];

		System.assertNotEquals(null, tTest);
	}

	@isTest
	static void createCheckApprovalTaskTest3() {
		RecordType rt = [SELECT Id, Name
				     FROM RecordType
				     WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
				     LIMIT 1];
        
		String rtId = rt.Id;

		Referral__c r = createReferral(rtId); // cross-sell BIDV
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		String oId = o.Id;
		
		Quote q = createQuote(o, 'Presented to Customer', 10, 'Supplier', 10, 10, 10);
		q.Status = 'Rejected by Customer';
		update q;

		Task tTest = [SELECT Subject
				  FROM Task
				  WHERE WhatId = :oId AND Subject = 'Update Approval Status of Approval Process Tab of this Opportunity to Rejected by Customer.'
				  LIMIT 1];

		System.assertNotEquals(null, tTest);
	}

	@isTest
	static void recalculateSecurityDepositRateTest() {
		String apprTest = getApprovalData('true', 'true', 'false', 'false', 'true', 'false', 'false', 'false', 'false', 'false', 'false');

		RecordType rt = [SELECT Id, Name
					FROM RecordType
					WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					LIMIT 1];
	
		String rtId = rt.Id;

		Referral__c r = createReferral(rtId); // cross-sell BIDV
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		String oId = o.Id;
		
		Quote q = createQuote(o, 'Approved by BSL', 10, 'Supplier', 10, 10, 10);
		q.Total_Asset_Value__c = 12000;
		q.Security_Deposit_Rate__c = 12;
		update q;

		Quote qTest = [SELECT Security_Deposit_rate_Amount__c
				   FROM Quote
				   WHERE OpportunityId = :oId
				   LIMIT 1];

		// System.assertEquals(1440, qTest.Security_Deposit_rate_Amount__c);
	}

	@isTest
	static void recalculateUpfrontFeeRateTest() {
		RecordType rt = [SELECT Id, Name
					FROM RecordType
					WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					LIMIT 1];
	
		String rtId = rt.Id;

		Referral__c r = createReferral(rtId); // cross-sell BIDV
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		String oId = o.Id;
		
		Quote q = createQuote(o, 'Approved by BSL', 10, 'Supplier', 10, 10, 10);
		q.Total_Asset_Value__c = 12000;
		q.Upfront_Fee_Rate__c = 12;
		update q;

		Quote qTest = [SELECT Upfront_Fee_Amount__c
				   FROM Quote
				   WHERE OpportunityId = :oId
				   LIMIT 1];

		// System.assertEquals(1440, qTest.Upfront_Fee_Amount__c);
	}

	@isTest
	static void recalculateClosingPriceRateTest() {
		RecordType rt = [SELECT Id, Name
					FROM RecordType
					WHERE SobjectType = 'Referral__c' AND Name = 'Crossell-BIDV'
					LIMIT 1];
	
		String rtId = rt.Id;

		Referral__c r = createReferral(rtId); // cross-sell BIDV
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		String oId = o.Id;
		
		Quote q = createQuote(o, 'Approved by BSL', 10, 'Supplier', 10, 10, 10);
		q.Total_Asset_Value__c = 12000;
		q.Closing_Price_Rate__c = 12;
		update q;

		Quote qTest = [SELECT Closing_Price_Amount__c
				   FROM Quote
				   WHERE OpportunityId = :oId
				   LIMIT 1];

		// System.assertEquals(1440, qTest.Closing_Price_Amount__c);
	}
}