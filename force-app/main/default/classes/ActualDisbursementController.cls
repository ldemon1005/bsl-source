public with sharing class ActualDisbursementController {
    @AuraEnabled
    public static String getOpportunityFromContract(String ctrId) {        
        List<Contract> ctrList = [SELECT Id, Opportunity_Name__c
                                  FROM Contract
                                  WHERE Id = :ctrId
                                  LIMIT 1];

        if(ctrList.size() != 0) {
            return ctrList[0].Opportunity_Name__c;
        }
        else {
            return null;
        }
    }

    @AuraEnabled
    public static List<Disbursement__c> getDisbursements() {        
        // OK, they're cool, let 'em through
        return [SELECT Id, Amount__c, Contract__c, Disbursement_Date__c, Opportunity__c, Status__c 
                FROM Disbursement__c];
    }
    
    @AuraEnabled
    public static Disbursement__c saveDisbursement(Disbursement__c disbursement, String ctrId) {
        // Perform isUpdateable() checking first, then
        System.debug('AAAAAAAAAAAAAA');
        List<Contract> ctrList = [SELECT Id, Opportunity_Name__c
                                  FROM Contract
                                  WHERE Id = :ctrId
                                  LIMIT 1];

        if(ctrList.size() != 0) {
            disbursement.Opportunity__c = ctrList[0].Opportunity_Name__c;
        }
        else {
            disbursement.Opportunity__c = null;
        }

        upsert disbursement;
        return disbursement;
    }
}