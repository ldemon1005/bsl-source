/**
 * @File Name          : AddOpportunitysToCampaignExtensionTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/5/2019, 8:59:46 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/5/2019, 8:59:46 AM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest(SeeAllData=true)
public class AddOpportunitysToCampaignExtensionTest {
    public ApexPages.StandardSetController  controller;
    @isTest() static void test(){
        Test.startTest();
        
        PageReference pageRef = Page.AddOpportunityToCampaignPage;
        Test.setCurrentPage(pageRef);
        
        list<Opportunity> lstOpp = new list<Opportunity>();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name='Test' ;
        opp1.StageName = 'D1';
        opp1.CloseDate = System.today() + 5;
        
        Opportunity opp2 = new Opportunity();
        opp2.Name='Test' ;
        opp2.StageName = 'D1';
        opp2.CloseDate = System.today() + 5;
        
        lstOpp.add(opp1);
        lstOpp.add(opp2);
        
        insert lstOpp;
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstOpp);
        stdSetController.setSelected(lstOpp);
        System.debug(stdSetController.getSelected());
        AddOpportunitysToCampaignExtension ext = new AddOpportunitysToCampaignExtension(stdSetController);
        
        String getOpsIdSelected = ext.getOpsIdSelected();
        String currentRecordId = ext.currentRecordId;
        String OpIds = ext.OpIds;
        ext.deleteOps();
        //list <Opportunity> opp = ext.OpList;
        Test.stopTest();
    }
}