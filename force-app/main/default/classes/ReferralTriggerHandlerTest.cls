@isTest
private class ReferralTriggerHandlerTest{
	private static Supplier__c createSupplier(String n) {
		Supplier__c sp = new Supplier__c();

		sp.Name = n;

		insert sp;

		return sp;
	}

	private static Contact createContact(Supplier__c s) {
		RecordType rt = [SELECT Id, Name
						FROM RecordType
						WHERE SobjectType = 'Contact' AND Name = 'Supplier'
						LIMIT 1];
			
		String rtId = rt.Id;

		Contact c = new Contact();
		c.RecordTypeId = rtId; // Supplier RecordType
		c.LastName = 'Testing Contact';
		c.Supplier_Name__c = s.Id;

		insert c;

		return c;
	}

	private static Referral__c createReferral(Supplier__c s) {
		RecordType rt = [SELECT Id, Name
						FROM RecordType
						WHERE SobjectType = 'Referral__c' AND Name = 'Supply Chain Finance'
						LIMIT 1];
			
		String rtId = rt.Id;

		Referral__c r = new Referral__c();
		r.RecordTypeId = rtId;
		r.Name = 'Ref Test';
		r.Supplier__c = s.Id;

		insert r;

		return r;
	}

	@isTest
	static void updateContactInReferralTest1() {
		Supplier__c s = createSupplier('Sup Test');
		Contact c = createContact(s);
		Referral__c r = createReferral(s);
		String rId = r.Id;

		Contact cTest = [SELECT Referral_Name__c
				     FROM Contact
				     WHERE LastName = 'Testing Contact'
				     LIMIT 1];

		System.assertEquals(rId, cTest.Referral_Name__c);
	}

	@isTest
	static void updateContactInReferralTest2() {
		Supplier__c s1 = createSupplier('Sup Test 1');
		Supplier__c s = createSupplier('Sup Test');
		Contact c = createContact(s);
		Referral__c r = createReferral(s1);
		r.Supplier__c = s.Id;
		update r;
		String rId = r.Id;

		Contact cTest = [SELECT Referral_Name__c
				     FROM Contact
				     WHERE LastName = 'Testing Contact'
				     LIMIT 1];

		System.assertEquals(rId, cTest.Referral_Name__c);
	}
}