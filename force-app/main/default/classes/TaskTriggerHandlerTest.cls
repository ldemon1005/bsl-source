@isTest(SeeAllData=true)
private class TaskTriggerHandlerTest{
	private static RecordType rtQuote;
	private static RecordType rtContract;
	private static RecordType rtPlan;
	private static RecordType rtActual;
	private static RecordType rtMonth;
	private static RecordType rtQuarter;
	private static RecordType rtCrossellBIDV;
	private static RecordType rtOther;
	private static RecordType rtBSLselfdev;
	private static RecordType rtCustomerInitiate;
	private static RecordType rtSupplyChainFinance;
	private static RecordType rtCrossellSMTB;
	private static RecordType rtCrossellSMTPFC;
	
	static void init(){
		list<RecordType> l_RecordType = [SELECT Id, Name, SobjectType FROM RecordType];
		
		for(RecordType rt : l_RecordType){
			if(rt.SobjectType == 'Referral__c'){
				switch on rt.Name {
					when 'Crossell-BIDV' {
						rtCrossellBIDV = rt;
					}	
					when 'Other' {		
						rtOther = rt;
					}
					when 'BSLselfdev' {		
						rtBSLselfdev = rt;
					}
					when 'Customer-initiate' {		
						rtCustomerInitiate = rt;
					}
					when 'Supply Chain Finance' {		
						rtSupplyChainFinance = rt;
					}
					when 'Crossell-SMTB' {		
						rtCrossellSMTB = rt;
					}
					when 'Crossell-SMTPFC' {		
						rtCrossellSMTPFC = rt;
					}
					when else {		  
						System.debug('error record type');
					}
				}
			}
			
			if(rt.SobjectType == 'Disbursement__c') {
				switch on rt.Name {
					when 'Plan' {
						rtPlan = rt;
					}	
					when 'Actual' {		
						rtActual = rt;
					}
					when else {		  
						System.debug('error record type');
					}
				}
			}
			
			if(rt.SobjectType == 'KPI_Value__c') {
				switch on rt.Name {
					when 'Month' {
						rtMonth = rt;
					}	
					when 'Quarter' {		
						rtQuarter = rt;
					}
					when else {		  
						System.debug('error record type');
					}
				}
			}
			
			if(rt.SobjectType == 'Quote' && rt.Name == 'Finance Lease'){
				rtQuote = rt;
			}
			
			if(rt.SobjectType == 'Contract' && rt.Name == 'Finance Lease'){
				rtContract = rt;
			}
		}
	}

	private static KPI__c createKpiForUser(String n, String y, String tp) {
		KPI__c kpi = new KPI__c();

		kpi.Name = n;
		kpi.Year__c = y;
		kpi.KPI_Type__c = tp;

		insert kpi;

		return kpi;
	}

	private static KPI_Value__c createKpiValueForUser(String rc, String t, KPI__c kpi) {
		KPI_Value__c kv = new KPI_Value__c();

		kv.RecordTypeId = rc;
		// Month RecordType
		if(rc.equals(rtMonth.Id)) {
			kv.Months__c = t;
		}

		// Quarter RecordType
		if(rc.equals(rtQuarter.Id)) {
			kv.Quarter__c = t;
		}

		kv.KPI__c = kpi.Id;

		insert kv;

		return kv;
	}

	private static Supplier__c createSupplier(String sName) {
		Supplier__c s = new Supplier__c(Name = sName);
		insert s;

		return s;
	}

	private static Referral__c createReferral(String rc) {
			Referral__c r = new Referral__c();

			r.RecordTypeId = rc; 
			r.Name = 'referral test';

			insert r;

			return r;
		}

	private static Account createAccount() {
		Account a = new Account();
		a.Business_Registration_Number__c = '1111';
		a.Name = 'Account Testing';
		a.BillingStreet = '11 duy tan';
		insert a;

		return a;
	}

	private static Opportunity createOpp(Account a, Referral__c r, String stage, String apprDt, Decimal apprAmount, Decimal ctrAmount, Decimal dbmAmount) {
			Date dt = Date.newInstance(2021, 1, 2);

			Opportunity o = new Opportunity();

			o.Name = 'Testing Opp';
			o.Referral_Source__c = r.Id;
			o.BSL_Service__c = 'Finance Lease';
			o.Contract_Ending_Selection__c = 'Ownership Transfer';
			o.StageName = stage;
			o.Approval_Data__c = apprDt;
			o.AccountId = a.Id;
			o.CloseDate = dt;
			o.Approved_Amount__c = apprAmount;
			o.Contracted_Amount__c = ctrAmount;
			o.Total_Disbursed_Amount__c = dbmAmount;

			insert o;

			return o;
		}

	private static Event createEvent(String t, Datetime start, Datetime endt, Id wId) {
		Event e = new Event();

		e.Subject = 'Test Event';
		e.StartDateTime = start;
		e.EndDateTime = endt;
		e.WhatId = wId;
		e.Type = t;

		insert e;

		return e;
	}

	private static Task createTask(String t, Date dt, Id wId) {
		Task ts = new Task();

		ts.Subject = 'Test Task';
		ts.ActivityDate = dt;
		ts.WhatId = wId;
		ts.Type = t;

		insert ts;

		return ts;
	}

	// @isTest
	// static void updateNumberOfActivitiesTriggeredTest() {
	// 	init();

	// 	Referral__c r = createReferral(rtCrossellBIDV.Id);
	// 	Account a = createAccount();
	// 	Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
	// 	Task t = createTask('Meeting at BSL', Date.newInstance(2019,6,13), o.Id);

	// }

	@isTest
	static void updateActualCustomerVisitNoTest1() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q2', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,6,13), Datetime.newInstance(2019,6,14), o.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 5,5), o.Id);
	}

	@isTest
	static void updateActualCustomerVisitNoTest2() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q1', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,1,13), Datetime.newInstance(2019,1,14), o.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 1,5), o.Id);
		
	}

	@isTest
	static void updateActualCustomerVisitNoTest3() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q3', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,8,13), Datetime.newInstance(2019,8,14), o.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 8,5), o.Id);
		
	}

	@isTest
	static void updateActualCustomerVisitNoTest4() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q4', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		Account a = createAccount();
		Opportunity o = createOpp(a, r, 'D3', null, 10000, 8000, 6000);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,11,13), Datetime.newInstance(2019,11,14), o.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 11,5), o.Id);
		
	}

	@isTest
	static void updateActualBidvVisitNoTest1() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q2', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,6,13), Datetime.newInstance(2019,6,14), r.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 5,5), r.Id);
		
	}

	@isTest
	static void updateActualBidvVisitNoTest2() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q1', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,1,13), Datetime.newInstance(2019,1,14), r.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 1,5), r.Id);
		
	}

	@isTest
	static void updateActualBidvVisitNoTest3() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q3', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,8,13), Datetime.newInstance(2019,8,14), r.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 8,5), r.Id);
		
	}

	@isTest
	static void updateActualBidvVisitNoTest4() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q4', kpiTest);
		Referral__c r = createReferral(rtCrossellBIDV.Id);
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,11,13), Datetime.newInstance(2019,11,14), r.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 11,5), r.Id);
		
	}

	@isTest
	static void updateActualSupplierVisitNoTest1() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q1', kpiTest);
		Supplier__c sp = createSupplier('Sup Test');
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,1,13), Datetime.newInstance(2019,1,14), sp.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 1,5), sp.Id);
		
	}

	@isTest
	static void updateActualSupplierVisitNoTest2() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q2', kpiTest);
		Supplier__c sp = createSupplier('Sup Test');
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,6,13), Datetime.newInstance(2019,6,14), sp.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 6,5), sp.Id);
		
	}

	@isTest
	static void updateActualSupplierVisitNoTest3() {
		init();

		KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
        	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q3', kpiTest);
		Supplier__c sp = createSupplier('Sup Test');
		
		Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,8,13), Datetime.newInstance(2019,8,14), sp.Id);
		Task t = createTask('Meeting at BSL', Date.newInstance(2019, 8,5), sp.Id);
		
	}

	// @isTest
	// static void updateActualSupplierVisitNoTest4() {
	// 	init();

	// 	KPI__c kpiTest = createKpiForUser('Test KPI', '2019', 'Quarter');
      //   	KPI_Value__c kvTest = createKpiValueForUser(rtQuarter.Id, 'Q4', kpiTest);
	// 	Supplier__c sp = createSupplier('Sup Test');
		
	// 	Event e = createEvent('Meeting at BSL', Datetime.newInstance(2019,11,13), Datetime.newInstance(2019,11,14), sp.Id);
	// 	Task t = createTask('Meeting at BSL', Date.newInstance(2019, 11,5), sp.Id);
		
	// }
}