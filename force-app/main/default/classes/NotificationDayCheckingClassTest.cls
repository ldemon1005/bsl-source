@IsTest(SeeAllData=true)
public class NotificationDayCheckingClassTest {
    @IsTest() static void test1(){
        Test.startTest();
        Contact contact1 = new Contact();
        contact1.Birthday__c = Date.today().addDays(7);
        contact1.LastName = 'contact1';
        
        Contact contact2 = new Contact();
        contact2.Birthday__c = Date.today().addDays(15);
        contact2.LastName = 'contact2';
        
        Contact contact3 = new Contact();
        contact3.Birthday__c = Date.today().addMonths(2);
        contact3.LastName = 'contact3';
        
        list<contact> l_contact = new list<contact>();
        l_contact.add(contact1);
        l_contact.add(contact2);
        insert l_contact;
        
        
        Account acc1 = new Account();
        acc1.Name = 'acc1';
        acc1.Business_Registration_Number__c = '1111';
        acc1.Establishment_Date__c = Date.today().addDays(7);
        acc1.BillingStreet = '11 duy tan';
        
        Account acc2 = new Account();
        acc2.Name = 'acc2';
        acc2.Business_Registration_Number__c = '1121';
        acc2.Establishment_Date__c = Date.today().addDays(15);
        acc2.BillingStreet = '11 duy tan';
        
        Account acc3 = new Account();
        acc3.Name = 'acc3';
        acc3.Business_Registration_Number__c = '13111';
        acc3.Establishment_Date__c = Date.today().addMonths(2);
        acc3.BillingStreet = '11 duy tan';
        
        list<account> l_account = new list<account>();
        l_account.add(acc1);
        l_account.add(acc2);
        l_account.add(acc3);
        insert l_account;
        
        Campaign camp1 = new Campaign();
        camp1.Name = 'camp1';
        camp1.EndDate = Date.today().addDays(7);
        
        Campaign camp2 = new Campaign();
        camp2.Name = 'camp1';
        camp2.EndDate = Date.today().addDays(15);
        
        Campaign camp3 = new Campaign();
        camp3.Name = 'camp1';
        camp3.EndDate = Date.today().addMonths(2);
        
        list<Campaign> l_campaign = new list<Campaign>();
        l_campaign.add(camp1);
        l_campaign.add(camp2);
        l_campaign.add(camp3);
        insert l_campaign;
        
        Contract contract1 = new Contract();
        contract1.StartDate = Date.today();
        contract1.ContractTerm = 2;
        contract1.AccountId = l_account[0].Id;
        contract1.Name = 'contract 1';
        
        Contract contract2 = new Contract();
        contract2.StartDate = Date.today();
        contract2.ContractTerm = 2;
        contract2.AccountId = l_account[0].Id;
        contract2.Name = 'contract 2';
        
        Contract contract3 = new Contract();
        contract3.StartDate = Date.today();
        contract3.ContractTerm = 2;
        contract3.AccountId = l_account[0].Id;
        contract3.Name = 'contract 3';
        
        list<contract> l_contract = new list<contract>();
        l_contract.add(contract1);
        l_contract.add(contract2);
        l_contract.add(contract3);
        insert l_contract;
        
        
        NotificationDayCheckingClass sh1 = new NotificationDayCheckingClass();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();
        // add system asserts to check your expected behaviour
    }
}