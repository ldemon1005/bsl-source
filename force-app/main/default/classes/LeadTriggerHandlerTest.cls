@isTest
private class LeadTriggerHandlerTest{
    private static final RecordType recordType = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Referral__c' AND Name = 'BSLselfdev'
                         						LIMIT 1];
	@isTest static void test1() {
		Test.startTest();
        Referral__c referral1 = new Referral__c();
        referral1.RecordTypeId = recordType.Id;
        referral1.Name = 'referral';
        
        Referral__c referral2 = new Referral__c();
        referral2.RecordTypeId = recordType.Id;
        referral2.Name = 'referral';
        
        list<Referral__c> l_referral = new list<Referral__c>();
        l_referral.add(referral1);
        l_referral.add(referral2);
        insert l_referral;
        
        Lead lead1 = new Lead();
        lead1.Company = 'lead 1 test';
        lead1.LastName = 'lead 1 test';
        lead1.Referral_Source__c = l_referral[0].Id;
        
        Lead lead2 = new Lead();
        lead2.Company = 'lead 2 test';
        lead2.LastName = 'lead 2 test';
        lead2.Referral_Source__c = l_referral[1].Id;
        
        list<lead> l_lead = new list<lead>();
        l_lead.add(lead1);
        l_lead.add(lead2);
        insert l_lead;
        
        LeadTriggerHandler.checkRelatedReferralSourceInLead(l_lead);
        
        l_lead[0].Referral_Source__c = l_referral[1].Id;
        l_lead[1].Referral_Source__c = l_referral[0].Id;
        update l_lead;
        
		Test.stopTest();
		
		System.assertEquals('test1','test1');
		
	}
}