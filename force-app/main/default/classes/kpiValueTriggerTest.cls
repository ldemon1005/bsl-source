@isTest()
public class kpiValueTriggerTest {
    private static final RecordType kpiQuarterRecordType = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'KPI__c' AND Name = 'Quarter'
                         						LIMIT 1];
    private static final RecordType kpiYearRecordType = [SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'KPI__c' AND Name = 'Years'
                         						LIMIT 1];
    private static final RecordType kpiValueQuarterRecordType = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'KPI_Value__c' AND Name = 'Quarter'
                         						LIMIT 1];
    private static final RecordType kpiValueYearRecordType = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'KPI_Value__c' AND Name = 'Year'
                         						LIMIT 1];
    @isTest() static void test(){
        KPI__c kpi = new KPI__C();
        kpi.Name = 'Test';
        kpi.Year__c = '2019';
        kpi.KPI_Type__c = 'Quarter';
        kpi.RecordTypeId = kpiQuarterRecordType.Id;//cần thay dữ liểu khi lên product
        Insert kpi;
        
        KPI_Value__c kpi_value = new KPI_Value__C();
        kpi_value.RecordTypeId = kpiValueQuarterRecordType.Id;//cần thay dữ liểu khi lên product
        kpi_value.KPI__c = kpi.id;
        kpi_value.Quarter__c = 'Q3';
        insert kpi_value;
        
        kpi_value.Actual_Total_Disbursed_Cross_sell_SMTB__c = 5;
        kpi_value.Actual_Total_Disbursed_Cross_sell_SMTPFC__c = 5;
        kpi_value.Actual_Total_Disbursed_SCF__c = 5;
        kpi_value.Actual_Total_Disbursed_BSL_Self_Develop__c = 5;
        kpi_value.Actual_Total_Disbursed_Customer_Initiate__c = 5;
        kpi_value.Actual_Total_Disbursed_Others__c = 5;
       	update kpi_value;
    }
}