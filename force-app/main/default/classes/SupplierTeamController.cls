public with sharing class SupplierTeamController {

    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
            User (Id, Name WHERE id NOT IN :selectedIds)
            LIMIT :MAX_RESULTS];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Opportunities & convert them into LookupSearchResult
        String referralIcon = 'standard:user';
        User [] users = ((List<User>) searchResults[0]);
        for (User user : users) {
            results.add(new LookupSearchResult(user.Id, 'User', referralIcon, user.Name,''));
        }
        return results;
    }
    
    @AuraEnabled(cacheable=true)
	public static PagedResult getListData(Decimal pageSize,Decimal pageNumber) {
		Integer pSize = (Integer)pageSize;
		Integer offset = ((Integer)pageNumber - 1) * pSize;
		PagedResult res = new PagedResult();
		res.page = (Integer)pageNumber;
		List<User> results = [select id,Name,Username,UserRole.Name  from User LIMIT :pSize OFFSET :offset];
        res.total = [select count() from User]; 
        res.data = results;
        return res;
	}
    
    @AuraEnabled
	public static Supplier__c findSupplier(String idSup) {
		Supplier__c camp = [select id,Name from Supplier__c where id = :idSup limit 1];
        return camp; 
	}
    
    @AuraEnabled
	public static void SaveLookup(List<String> selection, String supplierId) {
        List<CampaignMember_c__c> campMembers = new List<CampaignMember_c__c>();
        for(String selectionId : selection){
            CampaignMember_c__c campMember = new CampaignMember_c__c();
            campMember.Salesman__c  = selectionId;
            campMember.Supplier__c  = supplierId;
            campMembers.add(campMember);
        }
        System.debug(campMembers);
        upsert campMembers;
	}
    public class PagedResult {
		@AuraEnabled
		public Integer page { get;set; }

		@AuraEnabled
		public Integer total { get;set; }

		@AuraEnabled
		public List<User> data { get;set; }
	}
}