@IsTest public with sharing class ReferralCampaignController_Test {
    public static Campaign createCampaign(){
        Campaign camp = new Campaign(Name = 'Campaign test');
        camp.EndDate = Date.today().addDays(15);
        insert camp;
        return camp;
    }
    public static Referral__c createRef(){
        RecordType rt = [SELECT Id, Name FROM RecordType where SobjectType = 'Referral__c' limit 1];
        Referral__c ref = new Referral__c(Name = 'Campaign test');
        ref.RecordTypeId = rt.Id;
        insert ref;
        return ref;
    }
   	@IsTest(SeeAllData=true) public static void findCampaign() {
        // Instantiate a new controller with all parameters in the page
        //optOutController controller = new optOutController();
		ReferralCampaignController controller = new ReferralCampaignController();
        Campaign camp = createCampaign();
        Campaign c = ReferralCampaignController.findCampaign(camp.Id);
        //System.assertEquals('1', '1');                           
    }    
    
    @IsTest(SeeAllData=true) public static void findRef() {
        // Instantiate a new controller with all parameters in the page
        //optOutController controller = new optOutController();
		ReferralCampaignController controller = new ReferralCampaignController();     
        Referral__c ref = createRef();
        Referral__c c = ReferralCampaignController.findRef(ref.Id);
        list<String> selectedIds = new list<String>();
        List<LookupSearchResult> searchRef = ReferralCampaignController.search(ref.Name, selectedIds);
        //System.assertEquals('1', '1');                           
    }                              
    
    @IsTest(SeeAllData=true) public static void searchCampaign() {
        // Instantiate a new controller with all parameters in the page
        //optOutController controller = new optOutController();
		ReferralCampaignController controller = new ReferralCampaignController();  
        Campaign camp = createCampaign();
        ReferralCampaignController.searchCampaign(camp.Name);
        //System.assertEquals('1', '1');                           
    }    
    @isTest(SeeAllData=true) static void test(){
        ReferralCampaignController.getListData(10, 1);
        Campaign camp = createCampaign();
        Referral__c ref = createRef();
        list<String> selection = new list<String>();
        selection.add(ref.Id);
        ReferralCampaignController.SaveLookup(selection, camp.Id, 'sent');
    } 
}