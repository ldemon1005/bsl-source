@isTest
private class ContactSpecificControllerTest {
    private static final RecordType rt = [	SELECT Id, Name
                         						FROM RecordType
                         						WHERE SobjectType = 'Contact' AND Name = 'Account'
                         						LIMIT 1];
    static testMethod void redirectTest() {
        Contact ct = new Contact(FirstName='Test Contact', LastName = 'abc', Birthday__c  = Date.today(), RecordTypeId = rt.Id);
        insert ct;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(ct);
        ContactSpecificController c = new ContactSpecificController(sc);
        c.redirect();
        Test.stopTest();
    }
}