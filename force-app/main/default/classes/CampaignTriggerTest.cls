@isTest(SeeAllData=true)
private class CampaignTriggerTest {
	@isTest static void TestInsertCampaign() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Campaign cp = new Campaign(Name='Test Campaign');
        insert cp;
        
        // Perform test
        Test.startTest();
        Database.DeleteResult result = Database.delete(cp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestDeleteCampaign() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Campaign cp = new Campaign(Name='Test Campaign');
        insert cp;

        
        // Perform test
        Test.startTest();
        Database.SaveResult srList = Database.insert(cp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(!srList.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
    
    @isTest static void TestUpdateCampaign() {
        // Test data setup
        // Create an account with an opportunity, and then try to delete it
        Campaign cp = new Campaign(Name='Test Campaign');
        insert cp;
        
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.update(cp, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(result.isSuccess());
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
         //                    result.getErrors()[0].getMessage());
    }
}